//
//  HomeViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 18/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface HomeViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,UIGestureRecognizerDelegate,NSURLConnectionDelegate,GADBannerViewDelegate>

{
    NSNumber *value;
    
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
    NSTimer *timer;
}
@property (weak, nonatomic) IBOutlet UILabel *Waleetpoins;

@property (weak, nonatomic) IBOutlet UIView *TVIEW;
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
- (void)adView:(GADBannerView *)bannerView
didFailToReceiveAdWithError:(GADRequestError *)error;
- (void)adViewWillPresentScreen:(GADBannerView *)bannerView;
- (void)adViewDidDismissScreen:(GADBannerView *)bannerView;
- (void)adViewWillDismissScreen:(GADBannerView *)bannerView;
- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView;

@property (weak, nonatomic) IBOutlet UIView *SettingsTVIEw;

-(void)timedOut;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIButton *entertain;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@property (strong, nonatomic) IBOutlet UIButton *Walletbackbutton;

-(IBAction)WalletbackButtonAction:(UIButton *)sender;
- (IBAction)ONDEMAN2:(id)sender;
@end
