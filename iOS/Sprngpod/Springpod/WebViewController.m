//
//  WebViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 12/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "WebViewController.h"
#import "UIView+Toast.h"
#import "netWork.h"


@interface WebViewController ()
{
    UIActivityIndicatorView *activityView;
    UIView *loadingView;
    UILabel *loadingLabel;
    CSToastStyle *style;
    netWork *netReachability;
    NetworkStatus networkStatus;
    BOOL removeLoader; //to remove the loader in chat manually.
    
    
}
@property (nonatomic, retain) UIActivityIndicatorView * activityView;
@property (nonatomic, retain) UIView *loadingView;
@property (nonatomic, retain) UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UINavigationBar *webNavi;
@property (weak, nonatomic) IBOutlet UIImageView *topBackground;

@end

@implementation WebViewController

@synthesize webView;
@synthesize activityView,loadingView,loadingLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    removeLoader = false;
    
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    
    [self.webNavi setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.webNavi.shadowImage = [UIImage new];
    self.webNavi.translucent = YES;
    self.webNavi.backgroundColor = [UIColor clearColor];
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    
    // Do any additional setup after loading the view.
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 85, self.view.frame.size.height/2 - 85, 170, 170)];
    loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    loadingView.clipsToBounds = YES;
    loadingView.layer.cornerRadius = 10.0;
    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.frame = CGRectMake(65, 40, activityView.bounds.size.width, activityView.bounds.size.height);
    [loadingView addSubview:activityView];
    
    loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 115, 130, 22)];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textColor = [UIColor whiteColor];
    loadingLabel.adjustsFontSizeToFitWidth = YES;
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.text = @"Loading...";
    [loadingView addSubview:loadingLabel];
    

    [self.view addSubview:loadingView];
    [activityView startAnimating];
    
    NSString *MyWebURL=[[NSUserDefaults standardUserDefaults] objectForKey:@"MYWEBURL"];
//    NSLog(@"This is web url %@",MyWebURL);
    if ([MyWebURL isEqualToString:[NSString stringWithFormat:@"https://www.sprngpod.com/register/index.php"]]){
                     //objectForKey:@"countryCode"]]]) {
        _webNavi.topItem.title = @"Register";
        
    }
//    if ([MyWebURL isEqualToString:@"https://test.asianetmobiletv.com/mobile/register"]) {
//        _webNavi.topItem.title = @"Register";
//        
//    }
    else if ([MyWebURL isEqualToString:@"https://www.sprngpod.com/register/forgetpassword.html"]) {
        _webNavi.topItem.title = @"Forgot Password";
        
    }
    
    else{
        _topBackground.image =[UIImage imageNamed:@"settingsTop.png"];
        _webNavi.topItem.title = @"My Account";
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:MyWebURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:100];
    [webView loadRequest:req];
    timer = [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loading{
    if(!webView.loading)
    {
        [loadingView removeFromSuperview];
        [activityView stopAnimating];
    }
    else{
        if (removeLoader == true) {
            [loadingView removeFromSuperview];
            [activityView stopAnimating];
        }
        else{
        [self.view addSubview:loadingView];
        [activityView startAnimating];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return  UIInterfaceOrientationMaskPortrait;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
//        NSLog(@"Request %@",request);
    removeLoader = false;
    if ([[request.URL absoluteString] isEqualToString:@"https://www.sprngpod.com/register/success.php"]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MYWEBURL"] isEqualToString:[NSString stringWithFormat:@"https://www.sprngpod.com/register/index.php"]]){
                                                                                               //,[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]]]) {
        [self.view.window makeToast:@"Success" duration:2.0 position:CSToastPositionCenter style:style];
        }
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MYWEBURL"] isEqualToString:@"https://www.sprngpod.com/register/forgetpassword.html"]) {
            [self.view.window makeToast:@"Password is Emailed" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
        [self dismissViewControllerAnimated:NO completion:nil];
        return NO;
    }
    
   else if ([[request.URL absoluteString] isEqualToString:@"https://www.sprngpod.com/register/failure.php"]) {
       if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MYWEBURL"] isEqualToString:[NSString stringWithFormat:@"https://www.sprngpod.com/register/index.php"]]){
                //[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]]]) {
           [self.view.window makeToast:@"Registration Failed" duration:2.0 position:CSToastPositionCenter style:style];
       }
       else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MYWEBURL"] isEqualToString:@"https://www.sprngpod.com/register/forgetpassword.html"]) {
           [self.view.window makeToast:@"Forgot Password Request Failed" duration:2.0 position:CSToastPositionCenter style:style];
       }
        [self dismissViewControllerAnimated:NO completion:nil];
        return NO;
    }
    
   else if ([[request.URL absoluteString] isEqualToString:@"https://sprngpodportal.asianetmobiletv.com/mobile/onerror?aspxerrorpath=/login.aspx"]) {
       
       [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"]isDirectory:NO]]];
//       [self.view.window makeToast:@"Failed" duration:2.0 position:CSToastPositionCenter style:style];
//       [self dismissViewControllerAnimated:NO completion:nil];
//       return NO;
   }
    
   else if ([[request.URL absoluteString] containsString:@"mylivechat.com"]){
       removeLoader = true;
   }
    
    
    
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (IBAction)backPressed:(id)sender {
    if ([self.webView canGoBack]) {
        [webView goBack];
    }
    else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (IBAction)homePressed:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.webView makeToast:[NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"NSLocalizedDescription"]] duration:2.0 position:CSToastPositionCenter style:style];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
