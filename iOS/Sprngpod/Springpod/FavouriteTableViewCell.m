//
//  FavouriteTableViewCell.m
//  Springpod
//
//  Created by Shrishail Diggi on 09/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "FavouriteTableViewCell.h"

@implementation FavouriteTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
