//
//  TokenlessApiCall.h
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 22/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TokenlessApiCall : UIView

+(void)postRequestWithoutToken:(NSString *)postUrl postKeys:(NSString *)postKeys inView:(UIView *)view;

@end
