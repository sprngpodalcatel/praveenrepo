//
//  TourViewController.m
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 11/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "TourViewController.h"
#import "TourTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"
#import "netWork.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "DeviceUID.h"
#import "TourCollectionViewCell.h"

@interface TourViewController ()<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIActionSheetDelegate>
{
    
    NSString *urlString;
    
    NSString *string1;
    
    NSIndexPath *cellIndexPath;
    NSString *videoFromArray;
    
    NSMutableArray *spaGlobalArray;
    NSMutableDictionary *globalDic;
    
    NSMutableArray *tourLibraryNameArray;
    NSMutableArray *welcomeArray;
    NSMutableArray *featuresArray;
    NSMutableArray *roomsArray;
    NSMutableArray *spaArray;
    NSMutableArray *hfArray;
    NSMutableArray *globalArray;
    
    NSString *stringFromUrl;
    NSIndexPath *swipeIndex;
    NSString *lastTVid;
    NSTimer *playButtonTimer;
    UITapGestureRecognizer *tapRecogniser;
    NSIndexPath *selectedCellIndexPath;
    
    netWork *netReachability;
    NetworkStatus networkStatus;
    CSToastStyle *style;
    
    NSMutableSet *expandedCells;
    NSMutableArray *temp,*channelUrlArray;
    NSString *rateString,*IPAddress,*lastListenRadioChannelLanguage,*lastSeenTVUrl,*countryName;
    NSString *deviceID,*basicInfoArray;
    float lattitude,longitude;
    NSMutableArray *packageName,*startDate,*expiryDate,*deviceList,*platform,*osVersion,*deletionStatusArray,*blockStatusArray,*countryAllowedStatusArray;
    NSIndexPath *blockIndexpath;
    
    NSIndexPath *selectedButtonIndex;
    
    
    float lastSlidedValue;
    
    float screenHeight;
    int sessionTime,tokenRefreshTime;
    
    BOOL seleted;
    
    BOOL packageNotFound;
    BOOL noDataFound; // To avoid crash in No data when low network.
    BOOL tokenExpired;//
}

//@property (weak, nonatomic) IBOutlet UICollectionView *tourCollectionView;

@property (weak, nonatomic) IBOutlet UINavigationItem *tourNV;


@property (nonatomic) int currentValue;
@property(strong , nonatomic) NSTimer *loginTimer,*tokenRefreshTimer;
@property(strong, nonatomic) NSTimer *epgTimer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playerViewHeight;
@property (weak, nonatomic) IBOutlet UIView *tvView;
@property (weak, nonatomic) IBOutlet UIView *pausePlayView;
@property (weak, nonatomic) IBOutlet UITableView *tourTableView;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;

@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UILabel *selectedName;
@property (weak, nonatomic) IBOutlet UITextView *DescriptionTextView;
@property (weak, nonatomic) IBOutlet UIView *selectedTourView;
@property (weak, nonatomic) IBOutlet UILabel *selectedPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *selectedDuration;
@property (weak, nonatomic) IBOutlet UIButton *selectedDiscriptionButton;
@property (weak, nonatomic) IBOutlet UIView *tourMenuView;

@end

@implementation TourViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //  NameArray = [[NSMutableArray alloc] initWithObjects:@"Welcome",@"Features",@"Rooms",@"Spa",@"Health & Fitness", nil];
    tourLibraryNameArray = [[NSMutableArray alloc]init];
    //* Network connection Reachability
    self.DescriptionTextView.textContainer.maximumNumberOfLines = 1;
    
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    //  _tourCollectionView.hidden = true;
    
    spaGlobalArray = [[NSMutableArray alloc]init];
    globalDic = [[NSMutableDictionary alloc]init];
    _selectedTourView.hidden = true;
    _DescriptionTextView.hidden = true;
    _tourTableView.hidden = false;
    _tourMenuView.hidden = false;
    
    //* Arrays for Ondemand last seenTV
    
    temp = [[NSMutableArray alloc]init];
    packageName = [[NSMutableArray alloc]init];
    startDate = [[NSMutableArray alloc]init];
    expiryDate = [[NSMutableArray alloc]init];
    
    deviceID = [DeviceUID uid];
    spaArray = [[NSMutableArray alloc]init];
    welcomeArray = [[NSMutableArray alloc]init];
    featuresArray = [[NSMutableArray alloc]init];
    roomsArray = [[NSMutableArray alloc]init];
    hfArray = [[NSMutableArray alloc]init];
    globalArray = [[NSMutableArray alloc]init];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]} forState:UIControlStateSelected];
    UIFont *objFont = [UIFont fontWithName:@"Roboto-Light" size:15.0f];
    NSDictionary *dictAttributes = [NSDictionary dictionaryWithObject:objFont forKey:NSFontAttributeName];
    [[UISegmentedControl appearance] setTitleTextAttributes:dictAttributes forState:UIControlStateNormal];
    
    //     self.selectedImageView.clipsToBounds = YES;
    //     self.selectedImageView.layer.cornerRadius  = 20.0f;
    //     self.selectedImageView.layer.masksToBounds  = YES;
    //     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(220.0/255.0) green:(80.0/255.0) blue:(96.0/255.0) alpha:0.5];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    // Do any additional setup after loading the view.
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"homeNew.png"] forState:UIControlStateNormal];
    UIView *rightView = [[UIView alloc] initWithFrame:button.frame];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightView];
    
    self.navigationController.navigationItem.rightBarButtonItem = right;
    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    self.tabBarController.tabBar.shadowImage = [UIImage new];
    self.tabBarController.tabBar.translucent = YES;
    self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    
    UIBezierPath *linePath1 = [UIBezierPath bezierPath];
    [linePath1 moveToPoint:CGPointMake(8.0, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(232 , 82.0)];
    [linePath1 moveToPoint:CGPointMake(240.0/2, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(240.0/2, 117.0)];
    
    CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.path = [linePath1 CGPath];
    shapeLayer1.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer1.lineWidth = 1.0;
    shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
    
    self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    screenHeight = self.playerViewHeight.constant;
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Tour";
    //    [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.tourNV.titleView = titleLabel;
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"FS_Joey-Light" size:16.0], NSFontAttributeName,nil]];
    [_playPauseButton setHidden:YES];
    //    [_videoControlView setHidden:YES];
    
    
    tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTvView:)];
    tapRecogniser.delegate = self;
    tapRecogniser.numberOfTapsRequired = 1;
    [_pausePlayView addGestureRecognizer:tapRecogniser];
    [_pausePlayView addSubview:self.playPauseButton];
    [_pausePlayView bringSubviewToFront:self.playPauseButton];
    
    
  //  [self homeScreenApi];
    @try{
        NSString *welcomeStr =[NSString stringWithFormat:@"%@/index1.php/?fetchWelcomeURL=input",[[SprngIPConf getSharedInstance]getIpForImage]];
    
    string1 = welcomeStr;
    
    
    welcomeStr = [welcomeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:welcomeStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     
     {
         if (!(connectionError))
         {
             
             
             NSError *error = nil;
             NSDictionary *receivedData1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             welcomeArray = [receivedData1 objectForKey:@"root"];
             globalArray = welcomeArray;
             [self playTVWithValue];
             NSLog(@"welcomeArray : %@",welcomeArray);
             
             //TourTableViewCell *cell = [UITableView dequeueReusableCellWithIdentifier:@"TourCell" forIndexPath:indexPath];
             
          //   NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire0:) userInfo:nil repeats:NO];
             
             //NSDictionary * dic = tourLibraryNameArray[indexPath.row];
             //NSString *name = [dic objectForKey:@"libraryName"];
             [self setNavigationTitle:@"Welcome"];
         }
         else{
             
         }
     }];
} @catch (NSException *exception) {
    NSLog(@"");
} @finally {
    NSLog(@"");
}
   
    

   // [self httpGetRequest];
    
    moviePlayerController = [[MPMoviePlayerViewController alloc]init];
    moviePlayerController.view.hidden = NO;
    [moviePlayerController.view setFrame:self.tvView.bounds];
    [self.tvView addSubview:moviePlayerController.view];
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    [moviePlayerController.view bringSubviewToFront:_tvView];
    [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:lastSeenTVUrl]];
    [moviePlayerController.moviePlayer play];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    
}
-(void)setNavigationTitle:(NSString *)title
{
    
//    UILabel *titleLabel = [[UILabel alloc] init];
//    titleLabel.text = title;
//    [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
//    titleLabel.textColor = [UIColor whiteColor];
//    [titleLabel sizeToFit];
//    self.tourNV.titleView = titleLabel;
    
}


-(void) becomeActive
{
    NSLog(@"ACTIVE");
    //    [moviePlayerController.moviePlayer play];
    
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    [_playPauseButton setHidden:YES];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [moviePlayerController.moviePlayer pause];
    }
    else{
        [moviePlayerController.moviePlayer play];
    }
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tourTableView) {
        if(indexPath.row == 0 && ![tableView indexPathsForSelectedRows] && !selectedCellIndexPath && !cellIndexPath){
            [cell setSelected:YES animated:YES];
        }
        else if (cellIndexPath){
            if (cellIndexPath.row == indexPath.row) {
                [cell setSelected:YES animated:YES];
            }
        }
        if (blockIndexpath == indexPath) {
            [cell setSelected:NO animated:YES];
        }
        
    }
}

-(void)timedOut{
    [moviePlayerController.moviePlayer stop];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}



-(void)viewDidAppear:(BOOL)animated{
    
    [self httpGetRequest];
    
    self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    self.playPauseButton.hidden = true;
    
    
    if (networkStatus == NotReachable) {
        [self.navigationController.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Netwotk Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //        [alert show];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
   
    
    [self setNavigationTitle:@"Tour"];
    //    _epgTimer = [NSTimer scheduledTimerWithTimeInterval:30*60 target:self selector:@selector(refreshTableData) userInfo:nil repeats:YES];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromEntertainView"];
    
    if ([CLLocationManager locationServicesEnabled]) {
        if (!self.locationManager) {
            self.locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [_locationManager startUpdatingLocation];
        }
    }
    //[self homeScreenApi];
    //[self httpGetRequest];
    //    [self playTVWithValue];
    //    [self reloadTableview];
    // [self.tourTableView reloadData];
    
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    [moviePlayerController.moviePlayer play];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}


-(void)sessionExpiry:(NSNotification *)notification{
    [moviePlayerController.moviePlayer stop];
}


-(void)viewDidDisappear:(BOOL)animated
{
    
    _tourTableView.hidden = false;
    _tourMenuView.hidden = false;
    
    if(_epgTimer)
    {
        [_epgTimer invalidate];
        _epgTimer = nil;
    }
    [moviePlayerController.moviePlayer stop];
}


#pragma mark screen Rotation

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.view layoutIfNeeded];
    if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"landscape");
        [self.navigationController setNavigationBarHidden:TRUE animated:FALSE];
        self.tabBarController.tabBar.hidden = YES;
        
        NSLog(@" %f %f ",self.view.frame.size.width, self.view.frame.size.height);
        
        self.playerViewHeight.constant = MIN(self.view.frame.size.width, self.view.frame.size.height) ;
        
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        [moviePlayerController.view setFrame: self.tvView.bounds];
        
        
        
        //        moviePlayerController.moviePlayer.view.frame = [[UIScreen mainScreen] bounds];
        //        moviePlayerController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        _pinView.frame = CGRectMake(moviePlayerController.moviePlayer.view.frame.size.width/2 - 120, 8, 240, 119);
        //         self.pinView.center = moviePlayerController.view.center;
        //        [[[[UIApplication sharedApplication] windows] lastObject] addSubview:blockAlert];
        //        [[[[UIApplication sharedApplication] windows] lastObject] addSubview:moviePlayerController.view];
        
        
        
        //        [[[UIApplication sharedApplication] keyWindow]addSubview:moviePlayerController.view];
        
        //        [[[UIApplication sharedApplication] keyWindow]addSubview:self.pinView];
    }
    else
    {
        NSLog(@"portrait");
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        //self.playerViewHeight.constant = (236/386.0) *  MIN(self.view.frame.size.width, self.view.frame.size.height);
        
        self.playerViewHeight.constant = screenHeight;
        [self.navigationController setNavigationBarHidden:false animated:FALSE];
        self.tabBarController.tabBar.hidden = false;
        
        
        
        [moviePlayerController.view setFrame: self.tvView.bounds];
        
        
        
        
        
        //[self.view willRemoveSubview:<#(nonnull UIView *)#>]
        
        // [self.view addSubview:_ODTableView];
        
        
        // [_ODTableView reloadData];
        
        //        [moviePlayerController.view removeFromSuperview];
        //        [self.pinView removeFromSuperview];
        //        [moviePlayerController.view setFrame: self.tvView.bounds];
        //        [self.pinView setFrame:self.tvView.bounds];
        //        [_tvView addSubview:moviePlayerController.view];
        //        [moviePlayerController.moviePlayer setFullscreen:NO];
        //        [_tvView addSubview:self.pinView];
        //         _pinView.frame = CGRectMake(self.tvView.frame.size.width/2 - 120, 8, 240, 119);
        //        self.pinView.center = _tvView.center;
        //        [moviePlayerController.view addSubview:self.pinView];
        
        
        //        [moviePlayerController.view bringSubviewToFront:self.pinView];
        
        
        //        [moviePlayerController.moviePlayer play];
    }
    
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark GETREQUEST

-(void)httpGetRequest
{
    
    NSString *str =[NSString stringWithFormat:@"%@/index1.php/?fetchTourLibraries=input",[[SprngIPConf getSharedInstance]getIpForImage]];
    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    string1 = str;
    //        [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"urlStr1"];
    //        [[NSUserDefaults standardUserDefaults]synchronize];
    
    @try {
        NSURL *url = [NSURL URLWithString:str];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         
         {
             if (!(connectionError))
             {
                 
                 
                 NSError *error = nil;
                 NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                 tourLibraryNameArray = [receivedData objectForKey:@"root"];
                 [_tourTableView reloadData];
                 NSLog(@"%@",tourLibraryNameArray);
                 
             }
             else{
                 
             }
         }];
    } @catch (NSException *exception) {
        NSLog(@"");
    } @finally {
        NSLog(@"");
    }
    
    
    
}

#pragma mark RestAPI Delegate

//- (void)getReceivedData:(NSMutableData *)data sender:(RestAPI *)sender
//{
//
//
//    NSString *str1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlStr1"];
//    NSString *str2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlStr2"];
//    NSString *str3 = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlStr3"];
//    NSString *str4 = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlStr4"];
//    NSString *str5 = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlStr5"];
//    NSString *str6 = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlStr6"];
//
//    if ([string1 isEqualToString:str1])
//    {
//        NSError *error = nil;
//        NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//        tourLibraryNameArray = [receivedData objectForKey:@"root"];
//        [_tourTableView reloadData];
//        NSLog(@"%@",tourLibraryNameArray);
//
//    }
//    else if ([string1 isEqualToString:str2]){
//
//        NSError *error = nil;
//        NSDictionary *receivedData1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//        welcomeArray = [receivedData1 objectForKey:@"root"];
//        globalArray = welcomeArray;
//        [self playTVWithValue];
//        NSLog(@"%@",welcomeArray);
//
//
//    }
//
//    else if ([string1 isEqualToString:str3]){
//
//        NSError *error = nil;
//        NSDictionary *receivedData2 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//        featuresArray = [receivedData2 objectForKey:@"root"];
//        globalArray = featuresArray;
//        [self playTVWithValue];
//        NSLog(@"%@",featuresArray);
//
//
//    }
//    else if ([string1 isEqualToString:str4]){
//
//        NSError *error = nil;
//        NSDictionary *receivedData3 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//        roomsArray = [receivedData3 objectForKey:@"root"];
//        globalArray = roomsArray;
//        [self playTVWithValue];
//        NSLog(@"%@",roomsArray);
//
//
//    }
//
//    else if ([string1 isEqualToString:str5]){
//
//        NSError *error = nil;
//        NSDictionary *receivedData4 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//        spaArray = [receivedData4 objectForKey:@"root"];
//
//        globalArray = spaArray;
//        [self playTVWithValue];
//        NSLog(@"%@",spaArray);
//
//
//    }
//    else if ([string1 isEqualToString:str6]){
//
//        NSError *error = nil;
//        NSDictionary *receivedData5 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//         hfArray = [receivedData5 objectForKey:@"root"];
//
////        NSDictionary *hfId = [hfArray valueForKey:@"hfId"];
////
////        [[NSUserDefaults standardUserDefaults] setObject:hfId forKey:@"hfID"];
////        [[NSUserDefaults standardUserDefaults] synchronize];
//
//        globalArray = hfArray;
//       [self playTVWithValue];
//        NSLog(@"%@",hfArray);
//
//
//    }
//
//}
//
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tourLibraryNameArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *tourLibrayDic = tourLibraryNameArray[indexPath.row];
    
    NSString *tourLibraryName = [tourLibrayDic objectForKey:@"libraryName"];
    
    NSString *tourDescriptionText = [tourLibrayDic objectForKey:@"libraryContent"];
    
    
    NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[tourLibrayDic objectForKey:@"libraryImage"]];
    NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    TourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourCell" forIndexPath:indexPath];
    
    cell.titleNameLbl.text = tourLibraryName;
    
    cell.descriptionTextView.text = tourDescriptionText;
    
    [cell.descriptionTextView setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
    [cell.descriptionTextView setTextColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0]];
    
//    cell.ImageLbl.layer.masksToBounds = YES;
//    cell.ImageLbl.layer.cornerRadius = 26.0;
//    cell.ImageLbl.layer.borderWidth = 1.0;
    cell.channelDetailsView.clipsToBounds = true;
    cell.channelDetailsView.layer.cornerRadius = 12;
    
    [cell.ImageLbl sd_setImageWithURL:[NSURL URLWithString:encripString]];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0) {
        

       
        @try {
            NSString *welcomeStr =[NSString stringWithFormat:@"%@/index1.php/?fetchWelcomeURL=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = welcomeStr;
        
            
            welcomeStr = [welcomeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:welcomeStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     
                     NSError *error = nil;
                     NSDictionary *receivedData1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     welcomeArray = [receivedData1 objectForKey:@"root"];
                     globalArray = welcomeArray;
                     [self playTVWithValue];
                     NSLog(@"welcomeArray : %@",welcomeArray);
                     
                     
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire0:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryNameArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                 }
                 else{
                     
                 }
             }];
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
    }
    else if (indexPath.row == 1){
        
       
        @try {
            
               TourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourCell" forIndexPath:indexPath];
            if (indexPath.row==1) {
                
            }else{
                [cell setSelected:NO animated:NO];
            }
            NSString *featuresStr =[NSString stringWithFormat:@"%@/index1.php/?fetchFeaturesURL=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = featuresStr;
            //            [[NSUserDefaults standardUserDefaults] setObject:featuresStr forKey:@"urlStr3"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            
            featuresStr = [featuresStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:featuresStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData2 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     featuresArray = [receivedData2 objectForKey:@"root"];
                     globalArray = featuresArray;
                     [self playTVWithValue];
                     NSLog(@"%@",featuresArray);
                     
                     
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire1:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryNameArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                     
                 }
                 else{
                     
                 }
             }];
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
    }else if (indexPath.row == 2){
        
        @try {
            TourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourCell" forIndexPath:indexPath];
            if (indexPath.row==2) {
                
            }else{
                [cell setSelected:NO animated:NO];
            }
            NSString *roomsStr =[NSString stringWithFormat:@"%@/index1.php/?fetchRoomsURL=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = roomsStr;
            //            [[NSUserDefaults standardUserDefaults] setObject:roomsStr forKey:@"urlStr4"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            
            roomsStr = [roomsStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:roomsStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     
                     NSError *error = nil;
                     NSDictionary *receivedData3 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     roomsArray = [receivedData3 objectForKey:@"root"];
                     globalArray = roomsArray;
                     [self playTVWithValue];
                     NSLog(@"%@",roomsArray);
                     
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire2:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryNameArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                 }
                 else{
                     
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
    }
    
    else if(indexPath.row == 3){
    
        @try {
            TourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourCell" forIndexPath:indexPath];
            if (indexPath.row==3) {
                
            }else{
                [cell setSelected:NO animated:NO];
            }
            NSString *roomsStr =[NSString stringWithFormat:@"%@/index1.php/?fetchSpaUrl=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = roomsStr;
            //            [[NSUserDefaults standardUserDefaults] setObject:roomsStr forKey:@"urlStr5"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            
            roomsStr = [roomsStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:roomsStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData4 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     spaArray = [receivedData4 objectForKey:@"root"];
                     
                     globalArray = spaArray;
                     [self playTVWithValue];
                     NSLog(@"%@",spaArray);
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire3:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryNameArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                     
                 }
                 else{
                     
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
        
        
        
        
        
    }
    
    else if(indexPath.row == 4){
        TourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourCell" forIndexPath:indexPath];
        if (indexPath.row==4) {
            
        }else{
            [cell setSelected:NO animated:NO];
        }
        
        @try {
            NSString *hfStr =[NSString stringWithFormat:@"%@/index1.php/?fetchHFPosters=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = hfStr;
            //            [[NSUserDefaults standardUserDefaults] setObject:hfStr forKey:@"urlStr6"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            
            hfStr = [hfStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:hfStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData5 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     hfArray = [receivedData5 objectForKey:@"root"];
                     globalArray = hfArray;
                     [self playTVWithValue];
                     NSLog(@"%@",hfArray);
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire4:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryNameArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                     
                     
                 }
                 else
                 {
                     
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
        
        
        
    }
    
    self.currentValue = (int)cellIndexPath.row;
    
    
    
}
- (void)tourEventToFire0:(NSTimer*)timer0 {
    //  [self playTVWithValue];
    //    _tourTableView.hidden = true;
    //    _tourCollectionView.hidden = false;
    
}
- (void)tourEventToFire1:(NSTimer*)timer0 {
    //  [self playTVWithValue];
    //    _tourTableView.hidden = true;
    //    _tourCollectionView.hidden = false;
    
}
- (void)tourEventToFire2:(NSTimer*)timer0 {
    // [self playTVWithValue];
    //    _tourTableView.hidden = true;
    //    _tourCollectionView.hidden = false;
    
}
- (void)tourEventToFire3:(NSTimer*)timer0 {
    
    //    _tourTableView.hidden = true;
    //    _tourCollectionView.hidden = false;
    
}

- (void)tourEventToFire4:(NSTimer*)timer0 {
    
    //    _tourTableView.hidden = true;
    //    _tourCollectionView.hidden = false;
    
}



#pragma mark HomeScreen API

-(void)homeScreenApi
{
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            
            
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"])
            {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else{
                
                IPAddress = [dict objectForKey:@"ipAddress"];
                lastSeenTVUrl = [dict objectForKey:@"lastSeenTVChannelURL"];
                //                NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance]getIpForImage]];
                //                lastSeenTVChannelLogoURL =[dict objectForKey:@"lastSeenTVChannelLogo"];
                //                NSString *imageUrl4 = [imageUrl stringByAppendingString:lastSeenTVChannelLogoURL];
                //                tvLogoUrl = [NSURL URLWithString:imageUrl4];
                //                    [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
                lastListenRadioChannelLanguage = [dict objectForKey:@"lastListenRadioChannelLanguage"];
                //                lastListenRadioLogoUrl = [dict objectForKey:@"lastListenRadioChannelLogo"];
                //                NSString *imageUrl1 = [imageUrl stringByAppendingString:lastListenRadioLogoUrl];
                //                radioLogoUrl = [NSURL URLWithString:imageUrl1];
                //                        [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
                //                lastListenRadioUrl = [dict objectForKey:@"lastListenRadioChannelURL"];
                //                NSString *imageUrl2 = [dict objectForKey:@"promoLogo"];
                //                NSString *promoLogo = [imageUrl stringByAppendingString:imageUrl2];
                //                promoLogoUrl = [NSURL URLWithString:promoLogo];
                //                promoVideoUrl = [dict objectForKey:@"promoVideoURL"];
                //                defaultScreen = [dict objectForKey:@"settings"];
                
                NSNumber *number = [dict objectForKey:@"sessionTimeOut"];
                sessionTime = [number intValue];
                NSNumber *refreshTime = [dict objectForKey:@"tokenRefreshTime"];
                tokenRefreshTime = [refreshTime intValue];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastListenRadioChannelLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [_tokenRefreshTimer invalidate];
                if (tokenRefreshTime < 1) {
                    
                }
                else{
                    _tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:tokenRefreshTime target:self selector:@selector(RefreshToken) userInfo:nil repeats:YES];
                }
            }
            
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"Check Your UserId and Password" duration:2.0 position:CSToastPositionCenter style:style];
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [alert show];
            
        }
        
    }
    
    
    
}



-(void)tapOnTvView:(id) sender{
    
    if (self.playPauseButton.hidden) {
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        [self.playPauseButton setHidden:NO];
        
        
    }
    else{
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        
    }
}

-(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
}
#pragma mark RefreshToken

-(void)RefreshToken{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/refreshCustomerToken",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&deviceId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],deviceID];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                NSLog(@"Customer Does Not Exist");
            }
            
            else{
                NSString *token = [sDic objectForKey:@"token"];
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}



-(void)goToRootView
{
    //    _loginTimer = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [moviePlayerController.moviePlayer stop];
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}
#pragma mark playTV method

-(void)playTVWithValue
{
    
    moviePlayerController = [[MPMoviePlayerViewController alloc]init];
    [moviePlayerController.view setFrame: self.tvView.bounds];
    
    [self.tvView addSubview:moviePlayerController.view];
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    if (packageNotFound == YES) {
        
    }
    
    else if (tokenExpired == YES){
        
    }
    else if (noDataFound == YES);
    
    else if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]){
        [moviePlayerController.moviePlayer stop];
        [self.view endEditing:YES];
        [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
    }
    
    else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"])
    {
        [moviePlayerController.moviePlayer stop];
        [self.view endEditing:YES];
        [self.navigationController.view makeToast:@"This channel is not subscibed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
        //        [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"This channel is not subscibed forcurrent geo location" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    else
    {
        //    moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[ channelUrlArray objectAtIndex:_currentValue]]];
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
            //            [moviePlayerController.moviePlayer play];
        }
        else
        {
            if (globalArray == welcomeArray) {
                
                NSDictionary *dict1 = [welcomeArray objectAtIndex:cellIndexPath.row];
                
                videoFromArray = [dict1 objectForKey:@"welcomeUrl"];
                stringFromUrl = videoFromArray;
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",videoFromArray);
                [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:videoFromArray]];
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [moviePlayerController.moviePlayer play];
            }
            else if (globalArray == featuresArray)
            {
                NSDictionary *dict2 = [featuresArray objectAtIndex:cellIndexPath.row];
                
                videoFromArray = [dict2 objectForKey:@"featuresUrl"];
                stringFromUrl = videoFromArray;
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",videoFromArray);
                [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:videoFromArray]];
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [moviePlayerController.moviePlayer play];
            }
            else if (globalArray == roomsArray)
            {
                NSDictionary *dict3 = [roomsArray objectAtIndex:cellIndexPath.row];
                
                videoFromArray = [dict3 objectForKey:@"roomsUrl"];
                stringFromUrl = videoFromArray;
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",videoFromArray);
                [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:videoFromArray]];
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [moviePlayerController.moviePlayer play];
            }
            else if (globalArray == spaArray)
            {
                NSDictionary *dict3 = [spaArray objectAtIndex:cellIndexPath.row];
                
                videoFromArray = [dict3 objectForKey:@"spaURL"];
                stringFromUrl = videoFromArray;
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",videoFromArray);
                [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:videoFromArray]];
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [moviePlayerController.moviePlayer play];
            }
            else if (globalArray == hfArray)
            {
                NSDictionary *dict3 = [hfArray objectAtIndex:cellIndexPath.row];
                
                videoFromArray = [dict3 objectForKey:@"HFUrl"];
                stringFromUrl = videoFromArray;
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",videoFromArray);
                [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:videoFromArray]];
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [moviePlayerController.moviePlayer play];
            }
            
        }
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)playPauseButton:(id)sender {
    
    if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
        //   [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:stringFromUrl]];
        [moviePlayerController.moviePlayer play];
        playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    }
    
    else{
        [moviePlayerController.moviePlayer pause];
        [playButtonTimer invalidate];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
    }
    
    
}
- (IBAction)backBarButton:(id)sender {
    
    
    //     if (self.selectedTourView.hidden == false)
    //    {
    //        _selectedTourView.hidden = true;
    //        //_tourCollectionView.hidden = false;
    //    }
    //    else if (_tourCollectionView.hidden == false) {
    //
    //        //_tourCollectionView.hidden = true;
    //        _tourTableView.hidden = false;
    //    }
    // else{
    [moviePlayerController.moviePlayer stop];
    [self dismissViewControllerAnimated:YES completion:nil];
    // }
    
    
    
}
- (IBAction)homeBarButton:(id)sender {
    
    [moviePlayerController.moviePlayer stop];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
//- (IBAction)selectedDiscriptionPressed:(id)sender {
//
//    CGRect newFrame = _selectedTourView.frame;
//
//    self.tourCollectionView.hidden = true;
//    if(newFrame.size.height == 135)
//    {
//        self.tourCollectionView.hidden = true;
//        [self.selectedDiscriptionButton setImage:[UIImage imageNamed:@"whiteMinus.png"] forState:UIControlStateNormal];
//        newFrame.size.height = 500;
//        _selectedTourView.frame = newFrame;
//        self.DescriptionTextView.hidden =false;
//        [self.selectedTourView addSubview:_DescriptionTextView];
//    }
//    else
//    {
//        self.tourCollectionView.hidden = false;
//        [self.selectedDiscriptionButton setImage:[UIImage imageNamed:@"whitePlus.png"] forState:UIControlStateNormal];
//        newFrame.size.height = 135;
//        _selectedTourView.frame = newFrame;
//        self.DescriptionTextView.hidden =true;
//
//
//    }
//
//
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
