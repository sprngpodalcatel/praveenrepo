//
//  DeviceTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 22/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceListLabel;

@end
