//
//  DeviceSettingsTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceSettingsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *uuidlabel;
@property (weak, nonatomic) IBOutlet UILabel *platformLabel;

@property (weak, nonatomic) IBOutlet UILabel *deviceModel;
@property (weak, nonatomic) IBOutlet UILabel *deviceID;
@property (weak, nonatomic) IBOutlet UILabel *platform;
@property (weak, nonatomic) IBOutlet UILabel *OSversion;
@property (weak, nonatomic) IBOutlet UIButton *expationButton;


@end
