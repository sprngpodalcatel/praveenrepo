//
//  LocationUpdate.h
//  Springpod
//
//  Created by Shrishail Diggi on 26/02/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
@interface LocationUpdate : NSObject <CLLocationManagerDelegate>

{
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
    
}
@property (strong, nonatomic) CLLocationManager *locationManager;

-(void)getCurrentLocation;

@end
