    //
    //  FirstViewController.m
    //  Springpod
    //
    //  Created by Shrishail Diggi on 07/11/15.
    //  Copyright © 2015 Appface. All rights reserved.
    //

    #import "FirstViewController.h"
    #import "TVTableViewCell.h"
    #import "UIImageView+WebCache.h"
    #import "FourthViewController.h"
    #import "netWork.h"
    #import <QuartzCore/QuartzCore.h>
    #import "Flurry.h"
    #import "UIView+Toast.h"
    #import "SprngIPConf.h"
    #import "GMDCircleLoader.h"
    #import "AppDelegate.h"
    #import "LoginViewController.h"
    #import "UrlRequest.h"
    #import "LocationUpdate.h"
    #import "LastSeenIndex.h"


    @interface FirstViewController ()
    {
        NSString *lastSeenTVFilterType;
        NSString *lastSeenTVFilterTypeValue;
        
        NSString *TvFilterType;
        NSString *TvFilterTypeValue;
        NSMutableArray *selectedLanguageArray;

        NSMutableDictionary* receivedData1;
        NSArray *logoArray1;
        NSArray *channelNameArray1;
        NSArray *urlArray;
        NSIndexPath *selectedCellIndexPath;
        NSIndexPath *cellIndexPath;
        NSIndexPath *blockIndexpath;
        NSIndexPath *lockedIndexpath;
        NSString *EPGString,*pinString;
        //    NSTimeInterval  time;
        NSString *lastTVid;
        netWork *netReachability;
        NetworkStatus networkStatus;
        UIAlertView *blockAlert,*blockAlert1,*pinviewAlert;
        NSMutableSet *expandedCells;
        NSString *stringFromUrl;
        CSToastStyle *style;
        NSIndexPath *swipeIndex;
        
        NSTimer *playButtonTimer;
        
        float screenHeight;
        
        BOOL teluguLanguage;
        BOOL hindiLanguage;
        BOOL tamilLanguage;
        BOOL malayalamLanguage;
        BOOL englishLanguage;
        BOOL kannadaLanguage;
        
        BOOL selectedGenreLanguage;
        
        BOOL languagePressed;
        
        BOOL pinAtfirstTime;
        BOOL pinOnSelection;
        BOOL isPlayerLoaded;
        BOOL packageNotFound;
        BOOL noDataFound; // To avoid crash in No data when low network.
        BOOL tokenExpired;// This condition has to be checked else playTvWithValue Method will looking for blockedStatusArray Values but because of token expiry there will not be any value in that array this leads to crash the app.
        NSString*languagestring;
        NSString *fetchTVLanguage;
        NSString *urlString;
        NSString * urlRequest;
        NSMutableArray *temp,*temp1,*channelNameArray,*channelLogoArray,*channelUrlArray,*epgArray,*ratingArray,*descriptionArray,*otherEndTimesArray,*channelIDArray,*otherStartTimesArray,*hdArray,*lastListenStatusArray,*TVChannelLanguageArray,*sublanguagefilterArray,*filterlistArray,*genreFilterArray;
        
        NSMutableArray *nextEventNameArray, *nextStartTimeArray,*nextEndTimeArray,*nextChannelIdArray,*temp2,*blockStatusArray,*countryAllowedStatusArray,*deletionStatusArray;
        
        NSString *fetchTVGenre;
        NSMutableArray *fetchTVChannelLanguageArray,*genreTVChannelListArray;

        TVTableViewCell *tableCell;
        __weak IBOutlet UIActivityIndicatorView *loader;
        
        UISwipeGestureRecognizer *leftRecognizer;
        UISwipeGestureRecognizer *rightRecognizer;
        UITapGestureRecognizer *tapRecognizer;
        CGPoint startPosition;
        
        LocationUpdate *locationRefresh;
        
    }

@property (nonatomic,strong)AVPlayer*player;
@property (nonatomic,strong)AVPlayerViewController *AVPlayerController;

    @property(strong, nonatomic) NSTimer *epgTimer;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *pinVerticalConstraint;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *playerViewHeight;
    @property (nonatomic) int currentValue;
    @property (nonatomic) int previousInteger;
    @property (strong, nonatomic) UIAlertView *blockAlert;
    @property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeTonextUrl;

    @property (weak, nonatomic) IBOutlet UIView *tvView;
    @property (weak, nonatomic) IBOutlet UINavigationItem *tvNav;
    @property (weak, nonatomic) IBOutlet UITableView *tvTableView;
    @property (weak, nonatomic) IBOutlet UIButton *languageButton;
    @property (weak, nonatomic) IBOutlet UIButton *genreButton;
    @property (weak, nonatomic) IBOutlet UIButton *previousButton;
    @property (weak, nonatomic) IBOutlet UIButton *nNextButton;
    @property (weak, nonatomic) IBOutlet UIButton *nPreviousButton;
    
    @property (weak, nonatomic) IBOutlet UIView *pinView;
    @property (weak, nonatomic) IBOutlet UITextField *pinTextField;
    @property (weak, nonatomic) IBOutlet UIImageView *leftArrowImage;
    @property (weak, nonatomic) IBOutlet UIImageView *rightArrowImage;
    @property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
    @property (weak, nonatomic) IBOutlet UIView *playPauseView;
//@property (weak, nonatomic) IBOutlet UIButton *bannerButton;
@property (weak, nonatomic) IBOutlet UIButton *bannerButton;
@property (weak, nonatomic) IBOutlet UIView *liveTVMenuView;
@property(nonatomic, strong) GADInterstitial *interstitial;


    @end

    @implementation FirstViewController
    @synthesize blockAlert;

    - (void)viewDidLoad {
        [super viewDidLoad];
        
       // self.pinView.hidden = YES;
        self.leftArrowImage.hidden = NO;
        self.rightArrowImage.hidden = NO;
        temp = [[NSMutableArray alloc]init];
        temp1 = [[NSMutableArray alloc]init];
        
        filterlistArray =[[NSMutableArray alloc]init];
        receivedData1 =[[NSMutableDictionary alloc]init];
        TVChannelLanguageArray=[[NSMutableArray alloc]init];
        sublanguagefilterArray=[[NSMutableArray alloc]init];
        fetchTVChannelLanguageArray = [[NSMutableArray alloc]init];
        channelIDArray = [[NSMutableArray alloc]init];
        channelNameArray = [[NSMutableArray alloc]init];
        channelLogoArray = [[NSMutableArray alloc]init];
        channelUrlArray = [[NSMutableArray alloc]init];
        expandedCells = [[NSMutableSet alloc]init];
        selectedLanguageArray = [[NSMutableArray alloc]init];
        genreTVChannelListArray =[[NSMutableArray alloc]init];
        genreFilterArray =[[NSMutableArray alloc]init];
        
        NSString *selectedFileName = @"";
        //self.pinView.layer.cornerRadius = 5;
        [[NSUserDefaults standardUserDefaults] setObject:selectedFileName forKey:@"selectedListName"];
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(becomeActive)
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionExpiry:) name:@"sessionExpiry" object:nil];
        netReachability = [netWork reachabilityForInternetConnection];
        networkStatus = [netReachability currentReachabilityStatus];
        EPGString = [[NSString alloc]init];
        epgArray = [[NSMutableArray alloc]init];
        ratingArray = [[NSMutableArray alloc]init];
        descriptionArray = [[NSMutableArray alloc]init];
        otherEndTimesArray = [[NSMutableArray alloc]init];
        otherStartTimesArray = [[NSMutableArray alloc]init];
        channelIDArray = [[NSMutableArray alloc]init];
        lastListenStatusArray = [[NSMutableArray alloc]init];
        lastTVid = @"";
    //    NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"lastTV"]);
        lastTVid = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastTV"];
        temp2 = [[NSMutableArray alloc]init];
        nextChannelIdArray = [[NSMutableArray alloc]init];
        nextEndTimeArray = [[NSMutableArray alloc]init];
        nextStartTimeArray = [[NSMutableArray alloc]init];
        nextEventNameArray = [[NSMutableArray alloc]init];
        countryAllowedStatusArray = [[NSMutableArray alloc]init];
        deletionStatusArray = [[NSMutableArray alloc] init];
        style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
        style.messageColor = [UIColor whiteColor];
        style.messageAlignment = NSTextAlignmentCenter;
        style.backgroundColor = [UIColor grayColor];
        
        loader.hidden = YES;
        
        tokenExpired = NO;
        noDataFound = NO;
        isPlayerLoaded = NO;
        [_playPauseButton setHidden:YES];
//        [self callApiToFetchEPG];
        self.tvTableView.backgroundView = [UIView new];
        self.tvTableView.backgroundView.backgroundColor = [UIColor clearColor];
        
        //    _previousInteger = 0;
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.view.backgroundColor = [UIColor clearColor];
        self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
        self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
        
        
        
        //    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"homeNew.png"] style:UIBarButtonItemStylePlain target:self action:nil];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [button setImage:[UIImage imageNamed:@"homeNew.png"] forState:UIControlStateNormal];
        UIView *rightView = [[UIView alloc] initWithFrame:button.frame];
        
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightView];
        
        self.navigationController.navigationItem.rightBarButtonItem = right;
        [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
        self.tabBarController.tabBar.shadowImage = [UIImage new];
        self.tabBarController.tabBar.translucent = YES;
        self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
        self.tabBarController.view.backgroundColor = [UIColor clearColor];
        
        UIBezierPath *linePath1 = [UIBezierPath bezierPath];
        [linePath1 moveToPoint:CGPointMake(8.0, 82.0)];
        [linePath1 addLineToPoint:CGPointMake(232 , 82.0)];
        [linePath1 moveToPoint:CGPointMake(240.0/2, 82.0)];
        [linePath1 addLineToPoint:CGPointMake(240.0/2, 117.0)];
        
        CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
        shapeLayer1.path = [linePath1 CGPath];
        shapeLayer1.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
        shapeLayer1.lineWidth = 1.0;
        shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
        
       // [self.pinView.layer addSublayer:shapeLayer1];
        self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
        screenHeight = self.playerViewHeight.constant;
        //    self.pinView.hidden = YES;
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.backgroundColor = [UIColor grayColor];
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
        [numberToolbar sizeToFit];
        _pinTextField.inputAccessoryView = numberToolbar;
        
//        self.adBannerView.adUnitID = @"ca-app-pub-1319032657289195~5219627869";
//        self.adBannerView.rootViewController = self;
//        [self.adBannerView loadRequest:[GADRequest request]];
        
        self.adBannerView.delegate = self;
        //self.adBannerView.adUnitID = @"ca-app-pub-5834410729019986/4549014756";
        self.adBannerView.adUnitID = @"ca-app-pub-9149503938080598/8140410483";
        self.adBannerView.rootViewController = self;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
        {
            _adBannerView.hidden = true;
            _bannerButton.hidden = true;
        }
        else{
            [self.adBannerView loadRequest:[GADRequest request]];
        }
        
        
        
      //  [self startNewGame];
      
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"Live TV";
    //    [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        self.tvNav.titleView = titleLabel;
        [[UINavigationBar appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor whiteColor], NSForegroundColorAttributeName,
          [UIFont fontWithName:@"FS_Joey-Light" size:16.0], NSFontAttributeName,nil]];
        
        [self willRotateToInterfaceOrientation:(int)[[UIDevice currentDevice] orientation] duration:0];
        
        
        _AVPlayerController = [[AVPlayerViewController alloc]init];
        
        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromUrl]];
        
        
        _AVPlayerController.view.frame = self.tvView.bounds;
        [self addChildViewController:_AVPlayerController];
        [self.tvView addSubview:_AVPlayerController.view];
        _AVPlayerController.showsPlaybackControls = false;
        //        _AVPlayerController.view.userInteractionEnabled = false;
        _player.closedCaptionDisplayEnabled = NO;
        _AVPlayerController.player = _player;
        
        [_player play];


        
    }
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
{
    _adBannerView.hidden = true;
    _bannerButton.hidden = true;
}
else{
    _adBannerView.hidden = false;
    _bannerButton.hidden = false;
    [self.adBannerView loadRequest:[GADRequest request]];
}
}

- (IBAction)bannerClosePressed:(id)sender {
    
    
    _adBannerView.hidden = true;
    _bannerButton.hidden = true;
}

- (void)adView:(GADBannerView* )adView didFailToReceiveAdWithError:(GADRequestError* )error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}
-(void)adViewWillPresentScreen:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _bannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
         _bannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
    
}

-(void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _bannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        _bannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
    
}


//- (void)startNewGame {
//    [self createAndLoadInterstitial];
//
//    // Set up a new game.
//}

-(void)adViewWillDismissScreen:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _bannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        _bannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
   
}

//- (void)createAndLoadInterstitial {
//    self.interstitial =
//    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6757355035734715/6481967788"];
//    //self.interstitial.delegate  = self;
//    GADRequest *request = [GADRequest request];
//    // Request test ads on devices you specify. Your test device ID is printed to the console when
//    // an ad request is made.
// //   request.testDevices = @[ kGADSimulatorID, @"2077ef9a63d2b398840261c8221a0c9b" ];
//    [self.interstitial loadRequest:request];
//}
//- (void)endGame {
//    [[[UIAlertView alloc]
//      initWithTitle:@"Game Over"
//      message:@"Your time ran out!"
//      delegate:self
//      cancelButtonTitle:@"Ok"
//      otherButtonTitles:nil] show];
//}
//
//- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if (self.interstitial.isReady) {
//        [self.interstitial presentFromRootViewController:self];
//    } else {
//        NSLog(@"Ad wasn't ready");
//    }
//    // Give user the option to start the next game.
//}
    -(BOOL)textFieldShouldReturn:(UITextField *)textField
    {
        [textField resignFirstResponder];
        return  YES;
    }

    -(void)cancelNumberPad{
        [_pinTextField resignFirstResponder];
        _pinTextField.text = @"";
    }

    -(void)doneWithNumberPad{
        //    NSString *numberFromTheKeyboard = numberTextField.text;
        [_pinTextField resignFirstResponder];
    }

    - (IBAction)homePressed:(id)sender {
        
        
        
        if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying)
        {
            [_player pause];
            
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }

#pragma mark UsageDetails


-(void)addingChannelUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]];
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSString *serviceIDN=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIDN forKey:@"serviceIdN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                
                
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
    });
    
}

-(void)updateChannelUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}



    -(void)setNavigationTitle:(NSString *)title
    {
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = title;
        [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        
        //
        //    if ([[self.childViewControllers valueForKey:@"class"] containsObject:[FourthViewController class]]) {
        //         titleLabel.text = @"MORE";
        //    }
        self.tvNav.titleView = titleLabel;
    }

 -(void) becomeActive
   {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _bannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        _bannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
    }

    NSLog(@"ACTIVE");
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    [_playPauseButton setHidden:YES];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [_player pause];
        
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inLiveTVView"] == YES)
    {
        [_player play];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inFavouriteView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inOnDemandView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inRadioView"] == YES)
    {
        [_player pause];
    }
    else{
        
        [_player pause];
        
    }
    
}

    -(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
    {
        @try {
            
            currentLocation = [locations objectAtIndex:0];
            [_locationManager stopUpdatingLocation];
            CLGeocoder *geocoder = [[CLGeocoder alloc]init];
            [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
             {
                 if (!(error))
                 {
                     if (placemarks) {
                         if (placemarks.count >= 1) {
                             
                             CLPlacemark *placemark = [placemarks objectAtIndex:0];
                             
                             if (placemark) {
                                 
                                 NSLog(@"\nCurrent Location Detected\n");
                                 if (placemark.ISOcountryCode && placemark.country ) {
                                     if (![placemark.ISOcountryCode isEqualToString:@""]  ) {
                                         NSString *Country = [[NSString alloc]initWithString:placemark.ISOcountryCode];
                                         [[NSUserDefaults standardUserDefaults] setObject:Country forKey:@"countryCode"];
                                     }
                                     if (![placemark.country isEqualToString:@""]) {
                                         NSString *countryName1 = [[NSString alloc]initWithString:placemark.country];
                                         [[NSUserDefaults standardUserDefaults] setObject:countryName1 forKey:@"countryName"];
                                     }
                                     
                                 }
                                 
                                 if (_locationManager.location.coordinate.latitude && _locationManager.location.coordinate.longitude) {
                                     float lattitudeFromGoe = _locationManager.location.coordinate.latitude;
                                     float longitudeFromGeo = _locationManager.location.coordinate.longitude;
                                     
                                     [self callTimeZoneAPI:lattitudeFromGoe longitude:longitudeFromGeo];
                                 }
                                 
                                 
                                 
                             }
                             
                             
                         }
                         
                     }
                     
                 }
                 else
                 {
                     NSLog(@"Geocode failed with error %@", error);
                     NSLog(@"\nCurrent Location Not Detected\n");
                     //return;
                     
                 }
                 
             }];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        
    }
    -(void)callTimeZoneAPI :(float)lattitude longitude:(float)longitude
    {
        @try {
            NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/timezone/json?location=%f,%f&timestamp=00000&sensor=true",lattitude,longitude]]];
            
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                          returningResponse:&response
                                                                      error:&error];
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                NSInteger status =  [httpResponse statusCode];
                
                NSLog(@"status = %ld",(long)status);
                
                if(dictionary && status == 200) {
                    [[NSUserDefaults standardUserDefaults]setObject:@"Default" forKey:@"timeZone"];
                    if ([[dictionary allKeys] containsObject:@"timeZoneID"]) {
                        if ([dictionary objectForKey:@"timeZoneId"]) {
                            NSString *timeZoneID = [dictionary objectForKey:@"timeZoneId"];
                            [[NSUserDefaults standardUserDefaults]setObject:timeZoneID forKey:@"timeZone"];
                        }
                        
                    }
                    
                }
                else{
                    NSLog (@"Oh Sorry");
                    [self.navigationController.view makeToast:@"Check Your Username/Password" duration:2.0 position:CSToastPositionCenter style:style];
                    
                }
                
            }
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        
    }
    -(void)viewDidAppear:(BOOL)animated
    {
        
       
        //    [self.activityInd startAnimating];
        //    [self.view bringSubviewToFront:self.activityInd];
        
        self.leftArrowImage.hidden = NO;
        self.rightArrowImage.hidden = NO;
        self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
       // self.pinView.hidden = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWasShown:)
                                                     name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillBeHidden:)
                                                     name:UIKeyboardWillHideNotification object:nil];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inHomeView"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inLiveTVView"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inRadioView"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inFavouriteView"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inOnDemandView"];
        
        
        [Flurry logEvent:@"TV_Screen" withParameters:nil];
        
        [self setNavigationTitle:@"Live TV"];
        if (_epgTimer) {
            [_epgTimer invalidate];
        }
        _epgTimer = [NSTimer scheduledTimerWithTimeInterval:30*60 target:self selector:@selector(refreshTableData) userInfo:nil repeats:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromEntertainView"];
        
        locationRefresh = [[LocationUpdate alloc] init];
        [locationRefresh getCurrentLocation];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
        {
            _adBannerView.hidden = true;
            _bannerButton.hidden = true;
        }
        else{
            _adBannerView.hidden = false;
            _bannerButton.hidden = false;
            [self.adBannerView loadRequest:[GADRequest request]];
        }
        
        isPlayerLoaded = NO;
        [self reloadTableview];
        //    [self.tvTableView reloadData];
        
    }



    -(void)reloadTableview
    {
        //[self refreshTableData];
        [self callApiToFetchspecificTvChannelList];

        
    }



-(void)callApiToFetchspecificTvChannelList{
    
    //  @try {
    
    loader.hidden = NO;
    [loader startAnimating];
    
    //lastSeenTVFilterType = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterType"];
    lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterTypeValue"];
    lastSeenTVFilterType =[[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterType"];
    NSString *null = @"";
    NSString *All = @"All";
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES)
    {
        TvFilterType = @"category";
        TvFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"];
        
        [self.languageButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        
        
        [self.genreButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        
        
    }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES){
        TvFilterType = @"genre";
        TvFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelGenreFilterTypeValue"];
        [self.genreButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];    }
    else
    {
        if ([lastSeenTVFilterTypeValue isEqual:[NSNull null]])
        {
            TvFilterType = lastSeenTVFilterType;
            TvFilterTypeValue = All;
            
        }
        else if ([lastSeenTVFilterTypeValue isEqualToString:null])
        {
            TvFilterType = lastSeenTVFilterType;
            TvFilterTypeValue = null;
        } else if ([lastSeenTVFilterTypeValue isEqualToString:All])
        {
            TvFilterType = lastSeenTVFilterType;
            TvFilterTypeValue = All;
        }
        
        else
        {
            if ([lastSeenTVFilterType isEqualToString:@"category"]) {
                [self.languageButton setTitleColor:[UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                
                [self.genreButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                
                
            }else if([lastSeenTVFilterType isEqualToString:@"genre"]){
                [self.genreButton setTitleColor:[UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                [self.languageButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            }
            
            
            TvFilterType = lastSeenTVFilterType;
            TvFilterTypeValue = lastSeenTVFilterTypeValue;
            
        }
    }
    if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying)
    {
        [_player pause];
        
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else{
        
    }
    urlRequest = [NSString stringWithFormat:@"?specificTvChannelList=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],TvFilterType,TvFilterTypeValue];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchEpgData:) name:urlRequest object:nil];
    
    [UrlRequest getRequest:urlRequest view:self.view];
}

    - (void)viewDidDisappear:(BOOL)animated {
        //BEFORE DOING SO CHECK THAT TIMER MUST NOT BE ALREADY INVALIDATED
        //Always nil your timer after invalidating so that
        //it does not cause crash due to duplicate invalidate
        if(_epgTimer)
        {
            [_epgTimer invalidate];
            _epgTimer = nil;
        }
        [_player pause];
        
           }
    -(void)sessionExpiry:(NSNotification *)notification{
        [_player pause];
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        [self updateChannelUsageDetails];
        _epgTimer = nil;
    }

    -(void)goToHome{
        [_player pause];
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        [self updateChannelUsageDetails];
        if(_epgTimer)
        {
            [_epgTimer invalidate];
            _epgTimer = nil;
        }
        AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
        [appDel.window setRootViewController:rootView];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
        NSLog(@"hello I am Back");
    }

    -(void)playTVWithValue
{
    isPlayerLoaded = YES;
    //        _AVPlayerController = [[AVPlayerViewController alloc]init];
    //        _AVPlayerController.view.frame = self.tvView.bounds;
    //
    //        [self addChildViewController:_AVPlayerController];
    //        [self.tvView addSubview:_AVPlayerController.view];
    //
    //        _AVPlayerController.showsPlaybackControls = false;
    leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFromLeft: )];
    leftRecognizer.delegate = self;
    [leftRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
    [_playPauseView addGestureRecognizer:leftRecognizer];
    
    
    
    rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFromRight:)];
    rightRecognizer.delegate = self;
    [rightRecognizer setDirection: UISwipeGestureRecognizerDirectionRight];
    [_playPauseView addGestureRecognizer:rightRecognizer];
    
    //[self.tvView addSubview:_AVPlayerController.view];
    
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTvView:)];
    tapRecognizer.delegate = self;
    tapRecognizer.numberOfTapsRequired = 1;
    [_playPauseView addGestureRecognizer:tapRecognizer];
    [_playPauseView addSubview:self.playPauseButton];
    [_playPauseView bringSubviewToFront:self.playPauseButton];
    [self.playPauseButton setHidden:YES];
    
    [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    if (packageNotFound == YES) {
        
    }
    
    else if (tokenExpired == YES){
        
    }
    else if (noDataFound == YES){
        
    }
    
    else{
        if ([blockStatusArray[cellIndexPath.row] isEqualToString:@"true"]) {
            [_player pause];
            //****** Getting the Update Channel Usage Details Of chaneels *****//
            [self updateChannelUsageDetails];
            [self.view endEditing:YES];
            
            pinAtfirstTime = YES;
            pinOnSelection = NO;
            // self.pinView.hidden = NO;
            pinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                      message:@""
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Ok",nil];
            pinviewAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
            
            UITextField *pinTextField = [pinviewAlert textFieldAtIndex:0];
            pinTextField.delegate = self;
            
            //            blockAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
            //textField.delegate = self;
            //            textField.keyboardType = UIKeyboardTypeNumberPad;
            
            
            [pinTextField becomeFirstResponder];
            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
            pinTextField.placeholder = @"Enter PIN";
            
            [pinviewAlert show];
            
            
            
        }
        
        else if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]){
            [_player pause];
            //****** Getting the Update Channel Usage Details Of chaneels *****//
            [self updateChannelUsageDetails];
            [self.view endEditing:YES];
            [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
        else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"])
        {
            [_player pause];
            //****** Getting the Update Channel Usage Details Of chaneels *****//
            [self updateChannelUsageDetails];
            [self.view endEditing:YES];
            [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
        }
        else
        {
            
            if (_AVPlayerController.player.status == AVPlayerTimeControlStatusPlaying) {
            }
            else
            {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES || [[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES){
                    for (int i = 0; i < [temp count]; i++)
                    {
                        NSDictionary *dict = [[NSDictionary alloc]init];
                        dict = temp[i];
                        
                        NSString *lastSeenStatus =[[temp objectAtIndex:i]objectForKey:@"lastSeenStatus"];
                        if ([lastSeenStatus isEqualToString:@"true"])
                        {
                            
                            NSString *stringFromArray = [channelUrlArray objectAtIndex:i];
                            stringFromUrl = stringFromArray;
                            self.currentValue = (int)cellIndexPath.row;
                            //                    NSLog(@"Url : %@",stringFromArray);
                            
                            NSString *lastSeen = @"";
                            lastSeen = [channelIDArray objectAtIndex:i];
                            [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastTV"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                            [self addChildViewController:_AVPlayerController];
                            [self.tvView addSubview:_AVPlayerController.view];
                            
                            _AVPlayerController.view.frame = self.tvView.bounds;
                            
                            _AVPlayerController.showsPlaybackControls = false;
                            _AVPlayerController.view.userInteractionEnabled = false;
                            _player.closedCaptionDisplayEnabled = NO;
                            _AVPlayerController.player = _player;
                            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                            [_playPauseButton setHidden:YES];
                            
                            [_player play];
                            //****** Getting the Add Channel Usage Details Of chaneels *****//
                            [self addingChannelUsageDetails];
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                
                                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/lastSeenTvChannelDetails1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                                NSString *Filtertype;
                                NSString *post;
                                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES){
                                    
                                    Filtertype = @"category";
                                    post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"]];
                                }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
                                {
                                    Filtertype = @"genre";
                                    post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelGenreFilterTypeValue"]];
                                }
                                NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                                
                                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                                [request setHTTPMethod:@"POST"];
                                [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                                [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                                [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                                [request setHTTPBody:postData];
                                
                                NSURLResponse *response;
                                NSError *error = nil;
                                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                                
                                if (!error) {
                                    NSError *myError = nil;
                                    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                                    //                NSLog(@"%@",dictionary);
                                    temp = [dictionary objectForKey:@"root"];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if (temp) {
                                            NSDictionary *sDic = temp[0];
                                            
                                            if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                                                [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                                                
                                            }
                                            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                                                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                                [self goToHome];
                                            }
                                            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                                NSLog(@"Customer Does Not Exist");
                                            }
                                            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Scene TV Channel Details Stored Successfully" ]) {
                                                
                                                
                                                //                            NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"]);
                                            }
                                            
                                            else{
                                                NSLog(@"temp is getting Null");
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            NSLog(@"%@",error);
                                        }
                                    });
                                    
                                    
                                }
                                
                                
                                //            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                                //            NSDictionary *json_dict = (NSDictionary *)json_string;
                                //            NSLog(@"json_dict\n%@",json_dict);
                                //            NSLog(@"json_string\n%@",json_string);
                                
                                //            NSLog(@"%@",response);
                                //            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                                //            NSLog(@" error is %@",error);
                                
                                
                            });
                            
                            break;
                            
                        }
                        else{
                            
                        }
                        
                    }
                }else{
                    NSString *stringFromArray = [channelUrlArray objectAtIndex:cellIndexPath.row];
                    stringFromUrl = stringFromArray;
                    self.currentValue = (int)cellIndexPath.row;
                    //                    NSLog(@"Url : %@",stringFromArray);
                    
                    NSString *lastSeen = @"";
                    lastSeen = [channelIDArray objectAtIndex:cellIndexPath.row];
                    [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveDefaultLastTV"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                    [self addChildViewController:_AVPlayerController];
                    [self.tvView addSubview:_AVPlayerController.view];
                    
                    _AVPlayerController.view.frame = self.tvView.bounds;
                    
                    _AVPlayerController.showsPlaybackControls = false;
                    _AVPlayerController.view.userInteractionEnabled = false;
                    _player.closedCaptionDisplayEnabled = NO;
                    _AVPlayerController.player = _player;
                    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                    [_playPauseButton setHidden:YES];
                    
                    [_player play];
                    //****** Getting the Add Channel Usage Details Of chaneels *****//
                    [self addingChannelUsageDetails];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        NSString *Filtertype;
                        
                        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES)
                        {
                            Filtertype = @"category";
                        }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
                        {
                            Filtertype = @"genre";
                        }
                        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/lastSeenTvChannelDetails1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                        
                        NSString *post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveDefaultLastTV"],Filtertype, [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterTypeValue"]];
                        
                        NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                        
                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                        [request setHTTPMethod:@"POST"];
                        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                        [request setHTTPBody:postData];
                        
                        NSURLResponse *response;
                        NSError *error = nil;
                        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                        //            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                        //            NSDictionary *json_dict = (NSDictionary *)json_string;
                        //            NSLog(@"json_dict\n%@",json_dict);
                        //            NSLog(@"json_string\n%@",json_string);
                        
                        //            NSLog(@"%@",response);
                        //            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                        //            NSLog(@" error is %@",error);
                        
                        if (!error) {
                            NSError *myError = nil;
                            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                            //                NSLog(@"%@",dictionary);
                            temp = [dictionary objectForKey:@"root"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (temp) {
                                    NSDictionary *sDic = temp[0];
                                    
                                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                                        
                                    }
                                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                        [self goToHome];
                                    }
                                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                        NSLog(@"Customer Does Not Exist");
                                    }
                                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Scene TV Channel Details Stored Successfully" ]) {
                                        //                            NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"]);
                                    }
                                    
                                    else{
                                        NSLog(@"temp is getting Null");
                                    }
                                    
                                    
                                }
                                else
                                {
                                    NSLog(@"%@",error);
                                }
                            });
                            
                            
                        }
                    });
                    
                    //****** Getting the Add Channel Usage Details Of chaneels *****//
                    [self addingChannelUsageDetails];
                    
                    lastTVid = nil;
                    
                    
                }
            }
        }
    }
    
}


    - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        
        return 1;
    }

    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        
        return [channelLogoArray count];
    }

-(void)tapOnTvView:(id) sender{
    
    //    _AVPlayerController.showsPlaybackControls = false;
    
    if (self.playPauseButton.hidden) {
        //if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
        if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){
            
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
            
        }
        
        else{
            
            
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        [self.playPauseButton setHidden:NO];
        
        
    }
    else{
        //if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
        if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){
            
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        
    }
}

-(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
}

- (IBAction)playAndPausePressed:(id)sender {
    
    if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
        // _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromUrl]];
        [_player play];
        
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        [self addingChannelUsageDetails];
        
        playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        
    }
    else{
        [_player pause];
        [playButtonTimer invalidate];
        
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        [self updateChannelUsageDetails];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    TVTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TVCell" forIndexPath:indexPath];
    cell.tag = indexPath.row;
    cell.plusButton.tag = indexPath.row;
    
    cell.backgroundColor = cell.contentView.backgroundColor;
    NSURL *url = [NSURL URLWithString:channelLogoArray[indexPath.row]];
    //    cell.tvLogoImageView.layer.cornerRadius = 10;
   // cell.tvLogoBG.layer.cornerRadius = 10;
//    cell.tvLogoBG.layer.masksToBounds = YES;
//    cell.tvLogoBG.layer.cornerRadius = 26.0;
//    cell.tvLogoBG.layer.borderWidth = 1.0;
    
    cell.channelDetailsView.clipsToBounds = true;
    cell.channelDetailsView.layer.cornerRadius = 12;
    
    [cell.tvLogoImageView sd_setImageWithURL:url];
    //    [cell.tvLogoImageView sd_setImageWithURL:url placeholderImage:nil options:SDWebImageRefreshCached];
    //    CALayer *borderLayer = [CALayer layer];
    //    CGRect borderFrame = CGRectMake(0, 0, (cell.tvLogoBG.frame.size.width), (cell.tvLogoBG.frame.size.height));
    //    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    //    [borderLayer setFrame:borderFrame];
    //    [borderLayer setCornerRadius:10];
    //    [borderLayer setBorderWidth:0.1];
    //    [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    //    [cell.tvLogoBG.layer addSublayer:borderLayer];
    
    //    cell.tvLogoImageView.image = [UIImage imageWithData:[channelLogoArray objectAtIndex:indexPath.row]];
    if (descriptionArray[indexPath.row]) {
        cell.descriptionTextView.text = descriptionArray[indexPath.row];
    }
    
    //    cell.descriptionTextView.font = [UIFont fontWithName:@"System 15.0" size:20];
    [cell.descriptionTextView setFont:[UIFont fontWithName:@"Raleway-Light" size:11]];
    [cell.descriptionTextView setTextColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0]];
    cell.descriptionTextView.hidden = YES;
    if (selectedCellIndexPath) {
        cell.descriptionTextView.hidden = NO;
    }
    
    cell.channelNameLabel.text = epgArray[indexPath.row];
    if ([blockStatusArray[indexPath.row] isEqualToString:@"true"]) {
        cell.lockImage.image = [UIImage imageNamed:@"lock.png"];
    }
    else
    {
        cell.lockImage.image = [UIImage imageNamed:@"non"];
    }
    int rn = (int)[otherStartTimesArray indexOfObject:@"default"];
    NSLog(@"%d",rn);
    NSString *dateString = @"";
    if ([self timeReturn:otherStartTimesArray[indexPath.row ]  format:@"yyyy-MM-dd HH:mm:ssZ"] && [self timeReturn:otherEndTimesArray[indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ"]) {
        dateString = [NSString stringWithFormat:@"%@ to %@",[self timeReturn:otherStartTimesArray[indexPath.row ]  format:@"yyyy-MM-dd HH:mm:ssZ"],[self timeReturn:otherEndTimesArray[indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ"]];
    }
    if ([dateString isEqualToString:@""]) {
        dateString = @"NA to NA";
    }
    cell.egpLabel.text = dateString;
    cell.hdLabel.text = [NSString stringWithFormat:@"%@ |",hdArray[indexPath.row]];
    
    cell.endTime.text = otherEndTimesArray[indexPath.row];
    [cell.plusButton addTarget:self action:@selector(discriptionPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    cell.ratingLabel.text = @"Rating %d",ratingArray[indexPath.row];
    //    if ([ratingArray[indexPath.row]  isEqual: @""]) {
    //        cell.ratingLabel.text = @"";
    //    }
    //    else{
    //    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Rating : %@",ratingArray[indexPath.row]]];
    //    NSString *ratingString = [NSString stringWithFormat:@""]
    //    cell.ratingLabel.text = [(NSString *)ratingArray[indexPath.row] length]>0?[NSString stringWithFormat:@"|PG:%@",ratingArray[indexPath.row]]:@"";
    cell.ratingLabel.text = @"| PG";
    cell.descriptionTextView.textContainer.maximumNumberOfLines = 1;
    //    cell.descriptionTextView.frame = CGRectMake(94, 30, 222, 20);
    //    [cell.ratingLabel setAttributedText:string];
    //    }
    NSLog(@"height %f",cell.frame.size.height);
    
    
    // Configure the cell...
    if (cell.frame.size.height == 140) {
        [cell.oldPlusButton setImage:[UIImage imageNamed:@"LTlock.png"] forState:UIControlStateNormal];
        cell.descriptionTextView.textContainer.maximumNumberOfLines = 5;
        //        cell.descriptionTextView.frame = CGRectMake(94, 30, 222, 118);
        //        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"desc"];
    }
    else{
        [cell.oldPlusButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
        //        cell.descriptionTextView.frame = CGRectMake(94, 30, 222, 20);
    }
    return cell;
    }


    -(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
    {
        [tableCell.plusButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }


    -(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
    {
        [self.view layoutIfNeeded];
        if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||
            toInterfaceOrientation == UIDeviceOrientationLandscapeRight) {
            NSLog(@"landscape");
            [self.navigationController setNavigationBarHidden:TRUE animated:FALSE];
            self.tabBarController.tabBar.hidden = YES;
            
            [self.adBannerView removeFromSuperview];
            [self.bannerButton removeFromSuperview];
            
            [self.view layoutIfNeeded];
            self.playerViewHeight.constant = MIN(self.view.frame.size.width, self.view.frame.size.height) ;
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
           // [moviePlayerController.view setFrame: self.tvView.bounds];
            _AVPlayerController.view.frame = self.tvView.bounds;
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"landscape"];
            
        }
        else
        {
            NSLog(@"portrait");
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            [self.view layoutIfNeeded];
                    self.playerViewHeight.constant = (236/386.0) *  MIN(self.view.frame.size.width, self.view.frame.size.height);
    //        self.playerViewHeight.constant = screenHeight;
            [self.navigationController setNavigationBarHidden:false animated:FALSE];
            [self setNavigationTitle:@"Live TV"];
            self.tabBarController.tabBar.hidden = false;
            
            [self.view addSubview:_adBannerView];
            [self.view addSubview:_bannerButton];
    
            _AVPlayerController.view.frame = self.tvView.bounds;
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"landscape"];
        
        }
        
        [UIView animateWithDuration:duration animations:^{
            [self.view layoutIfNeeded];
        }];
    }

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES || [[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
    {
        //        for (int i = 0; i < [temp count]; i++)
        //        {
        if (!cellIndexPath) {
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = [selectedLanguageArray objectAtIndex:indexPath.row];
            
            NSString *lastSeenStatus =[dict objectForKey:@"lastSeenStatus"];
            //
            if ([lastSeenStatus isEqualToString:@"true"])
            {
                [cell setSelected:YES animated:YES];
                //
            }
        }
        
        else if(cellIndexPath)
        {
            
            if(cellIndexPath.row == indexPath.row) {
                [cell setSelected:YES animated:YES];
            }
        }
        else
        {
        }
        
        //}
    }else
    {
        if(indexPath.row == 0 && ![tableView indexPathsForSelectedRows] && !selectedCellIndexPath && !cellIndexPath && ![channelIDArray containsObject:lastTVid]){
            
            [cell setSelected:YES animated:YES];
            
        }
        else if(cellIndexPath)
        {
            
            if(cellIndexPath.row == indexPath.row) {
                [cell setSelected:YES animated:YES];
            }
        }
        
    }
    
    if (blockIndexpath == indexPath)
    {
        [cell setSelected:NO animated:YES];
    }
    //
    //
    //    if ([channelIDArray indexOfObject:lastTVid] == indexPath.row) {
    //         [cell setSelected:YES animated:YES];
    //
    //    }
    
    
}
    - (IBAction)discriptionPressed:(id)sender {
        
        UIButton *button = (UIButton *)sender;
        CGRect buttonFrame = [button convertRect:button.bounds toView:_tvTableView];
        NSIndexPath *indexPath = [_tvTableView indexPathForRowAtPoint:buttonFrame.origin];
        
        selectedCellIndexPath = indexPath;
        //    if (nowSelectedIndexPath.row == selectedCellIndexPath.row) {
        
        if ([expandedCells containsObject:indexPath]) {
            [expandedCells removeObject:indexPath];
        }
        else
        {
            [expandedCells addObject:indexPath];
        }
        //        tableCell.descriptionTextView.hidden = NO;
        [_tvTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    }

    -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
    swipeIndex = indexPath;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    if ([blockStatusArray[indexPath.row] isEqualToString:@"true"]) {
        blockIndexpath = indexPath;
        [[_tvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
        [self.view endEditing:YES];
        
        //self.pinView.hidden = NO;
        
        pinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                  message:@""
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Ok",nil];
        pinviewAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        
        UITextField *pinTextField = [pinviewAlert textFieldAtIndex:0];
        pinTextField.delegate = self;
        
        //            blockAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        //textField.delegate = self;
        //            textField.keyboardType = UIKeyboardTypeNumberPad;
        
        
        [pinTextField becomeFirstResponder];
        pinTextField.keyboardType = UIKeyboardTypeNumberPad;
        pinTextField.placeholder = @"Enter PIN";
        
        [pinviewAlert show];
        
        
        pinOnSelection = YES;
        pinAtfirstTime = NO;
        
    }
    
    else if ([deletionStatusArray[indexPath.row] isEqualToString:@"true"]){
        [[_tvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
        [self.view endEditing:YES];
        
        [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
        
    }
    
    else if ([countryAllowedStatusArray[indexPath.row] isEqualToString:@"false"])
    {
        [[_tvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
        [self.view endEditing:YES];
        
        [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
        
    }
    
    else{
        
        [_player pause];
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
        
        
        NSLog(@"selectedRow = %ld",(long)indexPath.row);
        //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        //    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:NO animated:YES];
        selectedCellIndexPath = nil;
        cellIndexPath  = indexPath;
        NSMutableArray *avisiBleIndexpaths = [[NSMutableArray alloc]init];
        for (NSIndexPath *indexpath1 in tableView.indexPathsForVisibleRows) {
            if(indexpath1.row != indexPath.row)
                [avisiBleIndexpaths addObject:indexpath1];
        }
        
        [_tvTableView reloadRowsAtIndexPaths:avisiBleIndexpaths withRowAnimation:UITableViewRowAnimationNone];
        
        
        //    if(!indexPath.row == 0) {
        for (TVTableViewCell *cell in [tableView visibleCells]) {
            [cell setSelected:NO animated:YES];
        }
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:YES animated:YES];
        //    }
        NSString *lastSeen = @"";
        lastSeen = [channelIDArray objectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastTV"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        //            NSLog(@"lastSaveTv =%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastTV"]);
        NSString *stringFromArray = [channelUrlArray objectAtIndex:indexPath.row];
        stringFromUrl = [channelUrlArray objectAtIndex:indexPath.row];
        self.currentValue = (int)indexPath.row;
        lastTVid = nil;
        //            NSLog(@"Url : %@",stringFromArray);
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        [_playPauseButton setHidden:YES];
        
        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
        
        [self addChildViewController:_AVPlayerController];
        [self.tvView addSubview:_AVPlayerController.view];
        _AVPlayerController.view.frame = self.tvView.bounds;
        _AVPlayerController.showsPlaybackControls = false;
        _AVPlayerController.view.userInteractionEnabled = false;
        _player.closedCaptionDisplayEnabled = NO;
        _AVPlayerController.player = _player;
        
        
        [_player play];
        
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        [self addingChannelUsageDetails];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/lastSeenTvChannelDetails1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
            
            NSString *Filtertype;
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES)
            {
                Filtertype = @"category";
                lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"];
            }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
            {
                Filtertype = @"genre";
                lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelGenreFilterTypeValue"];
            }
            else{
                Filtertype = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterType"];
                lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterTypeValue"];
            }
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,lastSeenTVFilterTypeValue];
            
            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setHTTPBody:postData];
            
            NSURLResponse *response;
            NSError *error = nil;
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            //            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            //            NSDictionary *json_dict = (NSDictionary *)json_string;
            //            NSLog(@"json_dict\n%@",json_dict);
            //            NSLog(@"json_string\n%@",json_string);
            
            //            NSLog(@"%@",response);
            //            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
            //            NSLog(@" error is %@",error);
            
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                //                NSLog(@"%@",dictionary);
                temp = [dictionary objectForKey:@"root"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (temp) {
                        NSDictionary *sDic = temp[0];
                        
                        if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                            [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                            
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToHome];
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                            NSLog(@"Customer Does Not Exist");
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Scene TV Channel Details Stored Successfully" ]) {
                            //                            NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"]);
                        }
                        
                        else{
                            NSLog(@"temp is getting Null");
                        }
                        
                        
                    }
                    else
                    {
                        NSLog(@"%@",error);
                    }
                });
                
                
            }
        });
    }
}

    - (void)keyboardWasShown:(NSNotification*)aNotification
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        float diff = self.view.frame.size.height/2.0 - kbSize.height - self.pinView.frame.size.height/2.0;
        
        if (diff < 0) {
            //        self.pinVerticalConstraint.constant = diff;
        }
        
    }

    // Called when the UIKeyboardWillHideNotification is sent
    - (void)keyboardWillBeHidden:(NSNotification*)aNotification
    {
        //    self.pinVerticalConstraint.constant = 0;
        
    }

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView == pinviewAlert) {
        if (buttonIndex == 0) {
            
            if (pinOnSelection == YES) {
                @try {
                    [self.view endEditing:YES];
                    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                    pinString = [pinviewAlert textFieldAtIndex:0].text;
                    // pinString = @"";
                    // pinString = self.pinTextField.text;
                    self.pinTextField.text = @"";
                    
                    NSString *url = @"verifyPIN1";
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchVerifyPin:) name:url object:nil];
                    [UrlRequest postRequest:url arguments:post view:self.view];
                    
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
            }
            
            if (pinAtfirstTime == YES) {
                
                @try {
                    [self.view endEditing:YES];
                    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                    
                    pinString = [pinviewAlert textFieldAtIndex:0].text;
                    // pinString = @"";
                    // pinString = self.pinTextField.text;
                    self.pinTextField.text = @"";
                    //            long row = [self.tvTableView indexPathForSelectedRow].row;
                    
                    
                    NSString *url = @"verifyPIN1";
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                    //        NSLog(@"post %@",post);
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchVerifyPin:) name:url object:nil];
                    
                    [UrlRequest postRequest:url arguments:post view:self.view];
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
            }
            
            
            
        }else{
            
            if (pinOnSelection == YES) {
                @try {
                    [self.view endEditing:YES];
                    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                    
                    pinString = [pinviewAlert textFieldAtIndex:0].text;
                    // pinString = @"";
                    // pinString = self.pinTextField.text;
                    self.pinTextField.text = @"";
                    NSString *url = @"verifyPIN1";
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchVerifyPin:) name:url object:nil];
                    [UrlRequest postRequest:url arguments:post view:self.view];
                    
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
            }
            
            if (pinAtfirstTime == YES) {
                
                @try {
                    [self.view endEditing:YES];
                    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                    
                    pinString = [pinviewAlert textFieldAtIndex:0].text;
                    // pinString = @"";
                    // pinString = self.pinTextField.text;
                    self.pinTextField.text = @"";
                    //            long row = [self.tvTableView indexPathForSelectedRow].row;
                    
                    
                    NSString *url = @"verifyPIN1";
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                    //        NSLog(@"post %@",post);
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchVerifyPin:) name:url object:nil];
                    
                    [UrlRequest postRequest:url arguments:post view:self.view];
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
            }
            
        }
    }
    
    
    
    if (alertView == blockAlert) {
        if (buttonIndex == 1) {
            blockAlert = nil;
            [self.view endEditing:YES];
            //  UITextField *pin = (UITextField *)[alertView textFieldAtIndex:0];
            pinString = [pinviewAlert textFieldAtIndex:0].text;
            // pinString = @"";
            // pinString = self.pinTextField.text;
            self.pinTextField.text = @"";
            pinString = @"";
            //pinString = pin.text;
            long row = [self.tvTableView indexPathForSelectedRow].row;
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/verifyPIN1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
            //            NSLog(@"post %@",post);
            
            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:postData];
            
            NSURLResponse *response;
            NSError *error = nil;
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSLog(@" error is %@",error);
            
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                //                NSLog(@"%@",dictionary);
                temp = [dictionary objectForKey:@"root"];
                if (temp) {
                    NSDictionary *sDic = temp[0];
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                        
                        [self.navigationController.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        //                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN" message:@"Enter Correct PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        selectedCellIndexPath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        //                         [[_tvTableView cellForRowAtIndexPath:blockIndexpath] setSelected:NO animated:YES];
                        blockIndexpath = nil;
                        [self.view endEditing:YES];
                        
                        //                        [alert show];
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                        [_player pause];
                        
                        //****** Getting the Update Channel Usage Details Of chaneels *****//
                        [self updateChannelUsageDetails];
                        
                        selectedCellIndexPath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:NO animated:YES];
                        cellIndexPath  = blockIndexpath;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        blockIndexpath = nil;
                        
                        NSString *stringFromArray = [channelUrlArray objectAtIndex:cellIndexPath.row];
                        self.currentValue = (int)cellIndexPath.row;
                        NSMutableArray *avisiBleIndexpaths = [[NSMutableArray alloc]init];
                        for (NSIndexPath *indexpath1 in _tvTableView.indexPathsForVisibleRows) {
                            if(indexpath1.row != cellIndexPath.row)
                                [avisiBleIndexpaths addObject:indexpath1];
                        }
                        
                        [_tvTableView reloadRowsAtIndexPaths:avisiBleIndexpaths withRowAnimation:UITableViewRowAnimationNone];
                        //                        for (TVTableViewCell *cell in [_tvTableView visibleCells]) {
                        //                            [cell setSelected:NO animated:YES];
                        //                        }
                        //                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        //                        moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[ channelUrlArray objectAtIndex:row]]];
                        //                        [moviePlayerController.view setFrame: self.tvView.bounds];
                        //                        [self.tvView addSubview:moviePlayerController.view];
                        //                        [self.tvView bringSubviewToFront:moviePlayerController.view];
                        //                        moviePlayerController.moviePlayer.fullscreen = NO;
                        //                        [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
                        //                        [moviePlayerController.moviePlayer play];
                        NSString *lastSeen = @"";
                        lastSeen = [channelIDArray objectAtIndex:cellIndexPath.row];
                        [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastTV"];
                        //                            NSLog(@"lastSaveTv =%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastTV"]);
                        
                        
                        NSLog(@"Url : %@",stringFromArray);
                        
                        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                        
                        //                            [self addChildViewController:_AVPlayerController];
                        //                            [self.tvView addSubview:_AVPlayerController.view];
                        //
                        //                            _AVPlayerController.view.frame = self.tvView.bounds;
                        //
                        //                            _AVPlayerController.player = _player;
                        
                        [_player play];
                        
                        
                        //****** Getting the Add Channel Usage Details Of chaneels *****//
                        [self addingChannelUsageDetails];
                        
                        TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                        cell.lockImage.image = [UIImage imageNamed:@"lock.png"];
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            
                            NSString* Filtertype = @"category";
                            
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/lastSeenTvChannelDetails1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                            
                            NSString *post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"]];
                            //                            NSLog(@"post %@",post);
                            
                            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                            
                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                            [request setHTTPMethod:@"POST"];
                            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                            [request setHTTPBody:postData];
                            
                            NSURLResponse *response;
                            NSError *error = nil;
                            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                            //                            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                            //                            NSDictionary *json_dict = (NSDictionary *)json_string;
                            //                            NSLog(@"json_dict\n%@",json_dict);
                            //                            NSLog(@"json_string\n%@",json_string);
                            
                            //                            NSLog(@"%@",response);
                            //                            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                            //                            NSLog(@" error is %@",error);
                            
                            if (!error) {
                                NSError *myError = nil;
                                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                                //                                NSLog(@"%@",dictionary);
                                temp = [dictionary objectForKey:@"root"];
                                if (temp) {
                                    NSDictionary *sDic = temp[0];
                                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                        NSLog(@"Customer Does Not Exist");
                                    }
                                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Scene TV Channel Details Stored Successfully" ]) {
                                        //                                            NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"]);
                                    }
                                    
                                    else{
                                        NSLog(@"temp is getting Null");
                                    }
                                    
                                    
                                }
                                else
                                {
                                    NSLog(@"%@",error);
                                }
                                
                            }
                        });
                        
                    }
                    
                    else{
                        NSLog(@"temp is getting Null");
                    }
                    
                    
                }
                else
                {
                    NSLog(@"%@",error);
                }
                
            }
            
        }
    }
    if (alertView == blockAlert1) {
        if (buttonIndex == 1) {
            blockAlert1 = nil;
            [self.view endEditing:YES];
            //                UITextField *pin = (UITextField *)[alertView textFieldAtIndex:0];
            //                pinString = @"";
            //                pinString = pin.text;
            pinString = [pinviewAlert textFieldAtIndex:0].text;
            // pinString = @"";
            // pinString = self.pinTextField.text;
            self.pinTextField.text = @"";
            long row = [self.tvTableView indexPathForSelectedRow].row;
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/verifyPIN1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
            //                NSLog(@"post %@",post);
            
            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:postData];
            
            NSURLResponse *response;
            NSError *error = nil;
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            //            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            //            NSDictionary *json_dict = (NSDictionary *)json_string;
            //            NSLog(@"json_dict\n%@",json_dict);
            //            NSLog(@"json_string\n%@",json_string);
            
            //                NSLog(@"%@",response);
            //                NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
            //                NSLog(@" error is %@",error);
            
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                //                    NSLog(@"%@",dictionary);
                temp = [dictionary objectForKey:@"root"];
                if (temp) {
                    NSDictionary *sDic = temp[0];
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                        
                        [self.navigationController.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        //                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN" message:@"Enter Correct PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        selectedCellIndexPath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        //                         [[_tvTableView cellForRowAtIndexPath:blockIndexpath] setSelected:NO animated:YES];
                        blockIndexpath = nil;
                        [self.view endEditing:YES];
                        
                        //                        [alert show];
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                        [_player pause];
                        //****** Getting the Update Channel Usage Details Of chaneels *****//
                        [self updateChannelUsageDetails];
                        
                        selectedCellIndexPath = nil;
                        blockIndexpath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:NO animated:YES];
                        NSString *stringFromArray = [channelUrlArray objectAtIndex:cellIndexPath.row];
                        self.currentValue = (int)cellIndexPath.row;
                        NSMutableArray *avisiBleIndexpaths = [[NSMutableArray alloc]init];
                        for (NSIndexPath *indexpath1 in _tvTableView.indexPathsForVisibleRows) {
                            if(indexpath1.row != cellIndexPath.row)
                                [avisiBleIndexpaths addObject:indexpath1];
                        }
                        
                        [_tvTableView reloadRowsAtIndexPaths:avisiBleIndexpaths withRowAnimation:UITableViewRowAnimationNone];
                        for (TVTableViewCell *cell in [_tvTableView visibleCells]) {
                            [cell setSelected:NO animated:YES];
                        }
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        
                        NSString *lastSeen = @"";
                        lastSeen = [channelIDArray objectAtIndex:cellIndexPath.row];
                        [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastTV"];
                        //                            NSLog(@"lastSaveTv =%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastTV"]);
                        
                        
                        //                            NSLog(@"Url : %@",stringFromArray);
                        //
                        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                        [_player play];
                        //****** Getting the Add Channel Usage Details Of chaneels *****//
                        [self addingChannelUsageDetails];
                        
                        TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                        cell.lockImage.image = [UIImage imageNamed:@"lock.png"];
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            
                            
                            
                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/lastSeenTvChannelDetails1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                            
                            NSString *Filtertype = @"category";
                            
                            NSString *post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"]];                            //                                NSLog(@"post %@",post);
                            
                            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                            
                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                            [request setHTTPMethod:@"POST"];
                            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                            [request setHTTPBody:postData];
                            
                            NSURLResponse *response;
                            NSError *error = nil;
                            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                            //                            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                            //                            NSDictionary *json_dict = (NSDictionary *)json_string;
                            //                            NSLog(@"json_dict\n%@",json_dict);
                            //                            NSLog(@"json_string\n%@",json_string);
                            
                            NSLog(@"%@",response);
                            //                                NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                            NSLog(@" error is %@",error);
                            
                            if (!error) {
                                NSError *myError = nil;
                                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                                //                                    NSLog(@"%@",dictionary);
                                temp = [dictionary objectForKey:@"root"];
                                if (temp) {
                                    NSDictionary *sDic = temp[0];
                                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                        NSLog(@"Customer Does Not Exist");
                                    }
                                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Scene TV Channel Details Stored Successfully" ]) {
                                        NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"]);
                                    }
                                    
                                    else{
                                        NSLog(@"temp is getting Null");
                                    }
                                    
                                    
                                }
                                else
                                {
                                    NSLog(@"%@",error);
                                }
                                
                            }
                        });
                    }
                    
                    else{
                        NSLog(@"temp is getting Null");
                    }
                    
                    
                }
                else
                {
                    NSLog(@"%@",error);
                }
                
            }
            
        }
    }
    
    
}



    - (IBAction)backPressed:(id)sender {
        
        if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying)
        {
            [_player pause];
            
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }


    - (IBAction)nNextPressed:(id)sender {
        
        if (packageNotFound == YES) {
            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
        }
        else{
            @try {
                loader.hidden = NO;
                [loader startAnimating];
                
//                self.rightArrowImage.hidden = NO;
//                self.leftArrowImage.hidden = YES;
                
                self.rightArrowImage.hidden = NO;
                self.leftArrowImage.hidden = NO;
                
                self.rightArrowImage.hidden = NO;
                self.leftArrowImage.hidden = NO;
                
                
                
                _rightArrowImage.image = [UIImage imageNamed:@"rightarrow-highlighted.png"];
                _leftArrowImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
                
                
                
                [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                [self.genreButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                
            NSString *url = @"newFetchAllOtherEpgData4";
            
            NSString *joinedComponents = [otherEndTimesArray componentsJoinedByString:@","];
            NSString *joinedComponentsforChannel= [channelIDArray componentsJoinedByString:@","];
            
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&key=next&otherTime=%@&channelIds=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],joinedComponents,joinedComponentsforChannel,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchAllOtherEPGdata:) name:url object:nil];
                
                [UrlRequest postRequest:url arguments:post view:self.view];
                
                TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:cellIndexPath];
                [cell setSelected:YES];
                
               }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
        }
    }
    - (IBAction)nPreviousPressed:(id)sender {
        
            if (packageNotFound == YES) {
            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
        }
        else{
            
            @try {
                loader.hidden = NO;
                [loader startAnimating];
                
                self.rightArrowImage.hidden = NO;
                self.leftArrowImage.hidden = NO;
                
                self.rightArrowImage.hidden = NO;
                self.leftArrowImage.hidden = NO;
                
                _rightArrowImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
                _leftArrowImage.image = [UIImage imageNamed:@"leftarrow-highlighted.png"];
                
                //                self.rightArrowImage.hidden = YES;
                //                self.leftArrowImage.hidden = NO;
                [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                [self.genreButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                
            NSString *url = @"newFetchAllOtherEpgData4";
            
            
            NSString *joinedComponents = [otherStartTimesArray componentsJoinedByString:@","];
            NSString *joinedComponentsforChannel= [channelIDArray componentsJoinedByString:@","];
            
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&key=previous&otherTime=%@&channelIds=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],joinedComponents,joinedComponentsforChannel,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchAllOtherEPGdata:) name:url object:nil];
                
                [UrlRequest postRequest:url arguments:post view:self.view];
            
                    TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:cellIndexPath];
                [cell setSelected:YES];
                
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
        }
        
    }

-(void)fetchAllOtherEPGdata:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            otherEndTimesArray = [[NSMutableArray alloc]init];
            otherStartTimesArray = [[NSMutableArray alloc]init];
            descriptionArray    = [[NSMutableArray alloc]init];
            ratingArray = [[NSMutableArray alloc]init];
            epgArray = [[NSMutableArray alloc]init];
            if (temp) {
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *insideDictionary = temp[i];
                    if ([[insideDictionary objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    else if ([[insideDictionary objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                        
                    }
                    
                    else
                    {
                        [epgArray addObject:[insideDictionary objectForKey:@"eventName"]];
                        if ([[insideDictionary objectForKey:@"start"] isEqualToString:@""]) {
                            NSString *emptyString = @"default";
                            [otherStartTimesArray addObject:emptyString];
                        }else{
                            [otherStartTimesArray addObject:[insideDictionary objectForKey:@"start"]];
                        }
                        
                        if ([[insideDictionary objectForKey:@"eventDescription"]isEqualToString:@""]) {
                            NSString *emptystring = @"No Description Available";
                            [descriptionArray addObject:emptystring];
                        }
                        
                        else{
                            [descriptionArray addObject:[insideDictionary objectForKey:@"eventDescription"]];
                        }
                        if ([[insideDictionary objectForKey:@"end"]isEqualToString:@""]) {
                            NSString *emptyString = @"default";
                            [otherEndTimesArray addObject:emptyString];
                        }
                        else{
                            [otherEndTimesArray addObject:[insideDictionary objectForKey:@"end"]];
                        }
                        [ratingArray addObject:[insideDictionary objectForKey:@"rating"]];
                        
                        
                        [self.tvTableView reloadData];
                    }
                    
                }
            }
            
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [loader stopAnimating];
    [loader hidesWhenStopped];
}


    -(NSString *)timeReturn:(NSString *)date_rec format:(NSString *)format{

        if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){
            
             //[_player play];
            
        }

        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:format];
        NSDate *dateFromString = [dateFormatter dateFromString:date_rec];
        
        //    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
        //    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateFormat:@"HH:mm"];
        NSString *formattedTimeString = [dateFormatter stringFromDate:dateFromString];
        NSLog(@"formattedDateString: %@", formattedTimeString);
        return  formattedTimeString;
    }
- (IBAction)languagePressed:(id)sender {
    languagePressed = true;
    [self.languageButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.genreButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"yyyy/MM/dd"];
    loader.hidden = NO;
    [loader startAnimating];
    
    
    //NSString *date_String=[dateformate stringFromDate:[NSDate date]];
    //        self.rightArrowImage.hidden = YES;
    //        self.leftArrowImage.hidden = YES;
    
    self.rightArrowImage.hidden = NO;
    self.leftArrowImage.hidden = NO;
    
    
    _rightArrowImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
    _leftArrowImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
    
    [self fetchTVChannelLanguageFilterData];
    
    
    [sublanguagefilterArray valueForKey:@"LanguageList"];
    
    for (NSDictionary *dict1 in sublanguagefilterArray)
    {
        [fetchTVChannelLanguageArray addObject:[dict1 objectForKey:@"language"]];
    }
    
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (int i = 0; i < [fetchTVChannelLanguageArray count]; i++)
    {
        [actionSheet  addButtonWithTitle:[fetchTVChannelLanguageArray objectAtIndex:i]];
        NSLog(@"%@",[fetchTVChannelLanguageArray objectAtIndex:i]);
    }
    [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    actionSheet.tag = 1;
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 1)
    {
        
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            selectedGenreLanguage = true;
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"languagePressed"];
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"genreSelected"];
            //            languagePressed = YES;
            //            genreSelected = NO;
            fetchTVLanguage = [fetchTVChannelLanguageArray objectAtIndex:buttonIndex];
            
            [[NSUserDefaults standardUserDefaults]setObject:fetchTVLanguage forKey:@"selectedTVChannelLanguageFilterTypeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self callApiToFetchspecificTvChannelList];
            
        }else if (buttonIndex == actionSheet.cancelButtonIndex)
        {
            [loader stopAnimating];
            [loader hidesWhenStopped];
        }
    }else if ( actionSheet.tag ==2)
    {
        
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"genreSelected"];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"languagePressed"];
        //        genreSelected = YES;
        //        languagePressed = NO;
        
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            
            fetchTVGenre = [genreTVChannelListArray objectAtIndex:buttonIndex];
            
            [[NSUserDefaults standardUserDefaults]setObject:fetchTVGenre forKey:@"selectedTVChannelGenreFilterTypeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [self callApiToFetchspecificTvChannelList];
            
        }else if (buttonIndex == actionSheet.cancelButtonIndex)
        {
            [loader stopAnimating];
            [loader hidesWhenStopped];
        }
        
    }
    [fetchTVChannelLanguageArray removeAllObjects];
    [genreTVChannelListArray removeAllObjects];
}

-(void)fetchAllFirstNext:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            temp =[[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            epgArray = [[NSMutableArray alloc]init];
            ratingArray = [[NSMutableArray alloc]init];
            otherEndTimesArray = [[NSMutableArray alloc]init];
            otherStartTimesArray = [[NSMutableArray alloc]init];
            descriptionArray = [[NSMutableArray alloc]init];
            
            if(temp) {
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *dict = [[NSDictionary alloc]init];
                    dict = temp[i];
                    if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                        
                    }
                    
                    else if ([[dict objectForKey:@"status"] isEqualToString:@"Package Not Found"]) {
                        packageNotFound = YES;
                        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                    }
                    
                    else{
                        
                        if ([[dict objectForKey:@"deletionStatus"] isEqualToString:@"true"]) {
                            
                        }
                        else{
                            if ([[dict objectForKey:@"eventDescription"]isEqualToString:@""]){
                                NSString *emptystring = @"No Description Available";
                                [descriptionArray addObject:emptystring];
                            }
                            
                            else{
                                [descriptionArray addObject:[dict objectForKey:@"eventDescription"]];
                            }
                            if ([[dict objectForKey:@"end"]isEqualToString:@""]){
                                NSString *emptystring = @"default";
                                [otherEndTimesArray addObject:emptystring];
                            }
                            
                            else{
                                [otherEndTimesArray addObject:[dict objectForKey:@"end"]];
                            }
                            [epgArray addObject:[dict objectForKey:@"eventName"]];
                            if ([[dict objectForKey:@"start"]isEqualToString:@""]){
                                NSString *emptystring = @"default";
                                [otherStartTimesArray addObject:emptystring];
                            }
                            
                            else{
                                [otherStartTimesArray addObject:[dict objectForKey:@"start"]];
                            }
                            if ([[dict objectForKey:@"rating"]isEqualToString:@""]){
                                NSString *emptystring = @"";
                                [ratingArray addObject:emptystring];
                            }
                            
                            else{
                                [ratingArray addObject:[dict objectForKey:@"rating"]];
                            }
                            [self.tvTableView reloadData];
                            noDataFound = NO;
                        }
                    }
                }
            }
            
        }
        
        else{
            noDataFound = YES;
        }
        
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [loader stopAnimating];
    [loader hidesWhenStopped];
}
- (IBAction)genrePressed:(id)sender {
    
    NSMutableArray *genreSpaceReplaceArray =[[NSMutableArray alloc]init];
    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to GTPL to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        
        //                [self refreshTableData];
        //                TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:cellIndexPath];
        //                [cell setSelected:YES];
        
        self.rightArrowImage.hidden = NO;
        self.leftArrowImage.hidden = NO;
        
        _rightArrowImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
        _leftArrowImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
        
        
        
        //            self.rightArrowImage.hidden = YES;
        //            self.leftArrowImage.hidden = YES;
        //
        
        [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.genreButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        
        
        loader.hidden = NO;
        [loader startAnimating];
        
        [self fetchTVChannelLanguageFilterData];
        
        for (NSDictionary *dict1 in genreFilterArray)
        {
            [genreTVChannelListArray addObject:[dict1 objectForKey:@"genre"]];
        }
        
        UIButton* button = (UIButton*)sender;
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                        cancelButtonTitle:nil
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:nil];
        
        for (int i = 0; i < [genreTVChannelListArray count]; i++)
        {
            NSString* genreString1 = [genreTVChannelListArray objectAtIndex:i];
            NSLog(@"String1: %@",genreString1);
            NSString* genreString2 = [genreString1 stringByReplacingOccurrencesOfString:@"_" withString:@" "];
            NSLog(@"String2: %@",genreString2);
            [genreSpaceReplaceArray insertObject:genreString2 atIndex:i];
            [actionSheet  addButtonWithTitle:[genreSpaceReplaceArray objectAtIndex:i]];
            NSLog(@"%@",[genreSpaceReplaceArray objectAtIndex:i]);
        }        [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];
        [actionSheet showFromRect:button.frame inView:self.view animated:YES];
        actionSheet.tag = 2;
        
    }
}

-(void)fetchVerifyPin:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    long row = [self.tvTableView indexPathForSelectedRow].row;
    
    @try {
        
        if (pinOnSelection == YES) {
            
            if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
                
                temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
                if (temp) {
                    
                    NSDictionary *sDic = temp[0];
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                        
                    }
                    
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                        
                        [self.navigationController.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        
                        selectedCellIndexPath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        blockIndexpath = nil;
                        [self.view endEditing:YES];
                        
                    }
                    
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                        
                        [_player pause];
                        //****** Getting the Update Channel Usage Details Of chaneels *****//
                        [self updateChannelUsageDetails];
                        
                        selectedCellIndexPath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:NO animated:YES];
                        cellIndexPath  = blockIndexpath;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        blockIndexpath = nil;
                        
                        NSString *stringFromArray = [channelUrlArray objectAtIndex:cellIndexPath.row];
                        self.currentValue = (int)cellIndexPath.row;
                        NSMutableArray *avisiBleIndexpaths = [[NSMutableArray alloc]init];
                        for (NSIndexPath *indexpath1 in _tvTableView.indexPathsForVisibleRows) {
                            if(indexpath1.row != cellIndexPath.row)
                                [avisiBleIndexpaths addObject:indexpath1];
                        }
                        
                        [_tvTableView reloadRowsAtIndexPaths:avisiBleIndexpaths withRowAnimation:UITableViewRowAnimationNone];
                        NSString *lastSeen = @"";
                        lastSeen = [channelIDArray objectAtIndex:cellIndexPath.row];
                        [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastTV"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        NSLog(@"lastSaveTv =%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastTV"]);
                        
                        
                        NSLog(@"Url : %@",stringFromArray);
                        stringFromUrl = stringFromArray;
                        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                        [_playPauseButton setHidden:YES];
                        
                        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                        _AVPlayerController.view.frame = self.tvView.bounds;
                        [self addChildViewController:_AVPlayerController];
                        [self.tvView addSubview:_AVPlayerController.view];
                        _AVPlayerController.showsPlaybackControls = false;
                        _player.closedCaptionDisplayEnabled = NO;
                        _AVPlayerController.view.userInteractionEnabled = false;
                        _AVPlayerController.player = _player;
                        [_player play];
                        
                        
                        //****** Getting the Add Channel Usage Details Of chaneels *****//
                        [self addingChannelUsageDetails];
                        
                        TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                        cell.lockImage.image = [UIImage imageNamed:@"lock.png"];
                        
                        
                        NSString *url = @"lastSeenTvChannelDetails1";
                        
                        
                        NSString *Filtertype;
                        NSString *post;
                        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES){
                            
                            Filtertype = @"category";
                            post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"]];
                        }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
                        {
                            Filtertype = @"genre";
                            post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelGenreFilterTypeValue"]];
                        }
                        else
                        {
                            lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterTypeValue"];
                            Filtertype = @"category";
                            post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,lastSeenTVFilterTypeValue];
                            
                        }
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchLastseenTvChannel:) name:url object:nil];
                        
                        [UrlRequest postRequest:url arguments:post view:self.view];
                        
                    }
                }
                
                
                
            }
        }
    
        if (pinAtfirstTime == YES) {
            if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
                
                temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
                if (temp) {
                    NSDictionary *sDic = temp[0];
                    
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                        
                    }
                    
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                        
                        
                        [self.navigationController.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        selectedCellIndexPath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        blockIndexpath = nil;
                        [self.view endEditing:YES];
                        
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                        [_player pause];
                        //****** Getting the Update Channel Usage Details Of chaneels *****//
                        [self updateChannelUsageDetails];
                        selectedCellIndexPath = nil;
                        blockIndexpath = nil;
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:NO animated:YES];
                        NSString *stringFromArray = [channelUrlArray objectAtIndex:cellIndexPath.row];
                        self.currentValue = (int)cellIndexPath.row;
                        NSMutableArray *avisiBleIndexpaths = [[NSMutableArray alloc]init];
                        for (NSIndexPath *indexpath1 in _tvTableView.indexPathsForVisibleRows) {
                            if(indexpath1.row != cellIndexPath.row)
                                [avisiBleIndexpaths addObject:indexpath1];
                        }
                        
                        [_tvTableView reloadRowsAtIndexPaths:avisiBleIndexpaths withRowAnimation:UITableViewRowAnimationNone];
                        for (TVTableViewCell *cell in [_tvTableView visibleCells]) {
                            [cell setSelected:NO animated:YES];
                        }
                        [[_tvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                        
                        NSString *lastSeen = @"";
                        lastSeen = [channelIDArray objectAtIndex:cellIndexPath.row];
                        [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastTV"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        NSLog(@"lastSaveTv =%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastTV"]);
                        
                        
                        //                     NSLog(@"Url : %@",stringFromArray);
                        stringFromUrl = stringFromArray;
                        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                        [_playPauseButton setHidden:YES];
                        
                        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                        _AVPlayerController.view.frame = self.tvView.bounds;
                        [self addChildViewController:_AVPlayerController];
                        [self.tvView addSubview:_AVPlayerController.view];
                        
                        
                        _AVPlayerController.showsPlaybackControls = false;
                        _player.closedCaptionDisplayEnabled = NO;
                        _AVPlayerController.player = _player;
                        
                        
                        
                        [_player play];
                        
                        //****** Getting the Add Channel Usage Details Of chaneels *****//
                        [self addingChannelUsageDetails];
                        
                        TVTableViewCell *cell = (TVTableViewCell *)[self.tvTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                        cell.lockImage.image = [UIImage imageNamed:@"lock.png"];
                        
                        
                        NSString *url = @"lastSeenTvChannelDetails1";
                        
                        
                        NSString *Filtertype;
                        NSString *post;
                        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES){
                            
                            Filtertype = @"category";
                            post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelLanguageFilterTypeValue"]];
                        }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
                        {
                            Filtertype = @"genre";
                            post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedTVChannelGenreFilterTypeValue"]];
                        }
                        else
                        {
                            lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterTypeValue"];
                            Filtertype = @"category";
                            post = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],Filtertype,lastSeenTVFilterTypeValue];
                            
                        }
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchLastseenTvChannel:) name:url object:nil];
                        
                        [UrlRequest postRequest:url arguments:post view:self.view];
                        
                        
                        
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        
    }@catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [loader stopAnimating];
    [loader hidesWhenStopped];
}

-(void)fetchLastseenTvChannel:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    @try {
    
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            
            if (temp) {
                NSDictionary *sDic = temp[0];
                
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                    [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                    
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                    ;
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    NSLog(@"Customer Does Not Exist");
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Scene TV Channel Details Stored Successfully" ]) {
                    NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"]);
                }
            
        }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
}



    -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        CGFloat kExpandedCellHeight = 140;
        CGFloat kNormalCellHeight = 69;
        
        
        if(selectedCellIndexPath != nil
           && [selectedCellIndexPath compare:indexPath] == NSOrderedSame)
        {
            
            [tableCell.plusButton setImage:[UIImage imageNamed:@"new"] forState:UIControlStateNormal];
            if ([expandedCells containsObject:indexPath]) {
                return kExpandedCellHeight;
            }
            else
            {
                return kNormalCellHeight;
            }
            
        }
        else
        {
            
            return kNormalCellHeight;
        }
    }



    - (void) handleSwipeFromLeft: (id) sender
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"landscape"] == YES)
        {        // code for Landscspe orientation
            int selectedRow = (int)cellIndexPath.row;
            if (selectedRow == channelLogoArray.count-1) {
                
            }
            else{
                
                [self tableView:_tvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row+1 inSection:0]];
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lastSwipeRight"];
            }
        }
        else
        {
        
        }
        
    }

    - (void) handleSwipeFromRight: (id) sender
    {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"landscape"] == YES)
        {
            // code for Landscape orientation
            
            int selectedRow = (int)cellIndexPath.row;
            if (selectedRow == 0) {
                
            }
            else{
               
                [self tableView:_tvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row - 1 inSection:0]];
               
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"lastSwipeRight"];
            }
        }
        else
        {
        
        }
        
    }



    - (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
    }


    -(void) refreshTableData
    {
        if (packageNotFound == YES) {
            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
        }
        else{
            @try {
                
                loader.hidden = NO;
                [loader startAnimating];
                
                NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
                
                [dateformate setDateFormat:@"yyyy/MM/dd"];
                //        [self.navigationController.view makeToast:@"Fetching EPG data" duration:1.0 position:CSToastPositionCenter style:style];
                NSString *date_String=[dateformate stringFromDate:[NSDate date]];
                
                    urlString = [NSString stringWithFormat:@"?newFetchAllCurrentEpgData4=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchEpgData:) name:urlString object:nil];
                    
                    [UrlRequest getRequest:urlString view:self.view];
 
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
            
        }
        
        
    }




//    -(void) callApiToFetchEPG
//    {
//        @try {
//    //        [GMDCircleLoader setOnView:self.view withTitle:@"Loading..." animated:YES];
//        //    [self.navigationController.view makeToast:@"Fetching EPG data" duration:1.0 position:CSToastPositionCenter style:style];
//        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
//        
//        [dateformate setDateFormat:@"yyyy/MM/dd"];
//        //        http://52.22.22.229/index.php/?newFetchAllCurrentEpgData4=input1&date=input2&countryCode=input3&MCC=input4&MNC=input5&ipAddress=input6&timeZone=input7
//        NSString *date_String=[dateformate stringFromDate:[NSDate date]];
//            
//            
//        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?newFetchAllCurrentEpgData4=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]]]];
//            
//            NSString *urlString = [NSString stringWithFormat:@"?newFetchAllCurrentEpgData4=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
//            
//            [[NSNotificationCenter defaultCenter] addObserver:urlString selector:@selector(fetchEpgData:) name:urlString object:nil];
//            
//            [UrlRequest getRequest:urlString view:self.view];
//            
//            
//            NSLog(@" URL%@",urlRequest);
//        [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
//        NSURLResponse * response = nil;
//        NSError * error = nil;
//        
//        NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
//                                                      returningResponse:&response
//                                                                  error:&error];
//        
//    //    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
//    //    NSDictionary *json_dict = (NSDictionary *)json_string;
//    //    NSLog(@"json_dict\n%@",json_dict);
//    //    NSLog(@"json_string\n%@",json_string);
//        
//        NSLog(@"TV %@",response);
//    //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
//        NSLog(@" error is %@",error);
//        if (!error) {
//            NSError *myError = nil;
//            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
//            
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//            NSInteger status =  [httpResponse statusCode];
//            temp =[dictionary objectForKey:@"root"];
//            NSLog(@"status = %ld",(long)status);
//            epgArray = [[NSMutableArray alloc]init];
//            ratingArray = [[NSMutableArray alloc]init];
//            otherStartTimesArray = [[NSMutableArray alloc]init];
//            otherEndTimesArray = [[NSMutableArray alloc]init];
//            channelIDArray = [[NSMutableArray alloc]init];
//            blockStatusArray = [[NSMutableArray alloc]init];
//            channelNameArray = [[NSMutableArray alloc]init];
//            channelLogoArray = [[NSMutableArray alloc]init];
//            channelUrlArray = [[NSMutableArray alloc]init];
//            hdArray = [[NSMutableArray alloc]init];
//            deletionStatusArray = [[NSMutableArray alloc]init];
//            countryAllowedStatusArray = [[NSMutableArray alloc]init];
//            lastListenStatusArray = [[NSMutableArray alloc]init];
//            
//            
//            if(temp && status == 200) {
//                NSLog(@"temp = %@",temp);
//                
//                
//                
//                
//                for (int i = 0; i < temp.count; i++) {
//                    NSDictionary *dict = [[NSDictionary alloc]init];
//                    dict = temp[i];
//                    
//                    if ([[dict objectForKey:@"status"] isEqualToString:@"Package Not Found"]) {
//                        packageNotFound = YES;
//                        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
//    //                    [GMDCircleLoader hideFromView:self.view animated:YES];
//                    }
//                    
//                   else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
//                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
//                        
//                    }
//                    else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
//                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
//                        tokenExpired = YES;
//                        [self goToHome];
//                    }
//                    
//                    else if ([[dict objectForKey:@"status"] isEqualToString:@"No Data Found"]){
//                        [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
//    //                    [GMDCircleLoader hideFromView:self.view animated:YES];
//                    }
//                    
//                    else{
//                        packageNotFound = NO;
//                        if ([[dict objectForKey:@"deletionStatus"] isEqualToString:@"true"]) {
//                            
//                        }
//                        
//                        else{
//                            
//                            if ([[dict objectForKey:@"eventDescription"]isEqualToString:@""]) {
//                                NSString *emptystring = @"No Description Available";
//                                [descriptionArray addObject:emptystring];
//                            }
//                            
//                            
//                            else{
//                                [descriptionArray addObject:[dict objectForKey:@"eventDescription"]];
//                            }
//                            [channelNameArray addObject:[dict objectForKey:@"channelName"]];
//                            
//                            
//                            NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance] getIpForImage]];
//                            
//                            imageUrl = [imageUrl stringByAppendingPathComponent:[dict objectForKey:@"channelLogo"]];
//    //                        NSLog(@"imageurl = %@", imageUrl);
//                            //    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
//                            
//                            [channelLogoArray   addObject:imageUrl];
//                            [channelUrlArray addObject:[dict objectForKey:@"streamUrl"]];
//                            [lastListenStatusArray addObject:[dict objectForKey:@"lastSeenStatus"]];
//                            
//                            [epgArray addObject:[dict objectForKey:@"eventName"]];
//                            if ([[dict objectForKey:@"rating"] isEqualToString:@""]) {
//                                NSString *emptyString = @"";
//                                [ratingArray addObject:emptyString];
//                                
//                            }
//                            else{
//                                [ratingArray addObject:[dict objectForKey:@"rating"]];
//                            }
//                            NSMutableArray *trimmedStrings = [NSMutableArray array];
//                            for (NSString *string in otherEndTimesArray) {
//                                NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//                                [trimmedStrings addObject:trimmedString];
//                            }
//                            
//                            NSMutableArray *trimmedStrings1 = [NSMutableArray array];
//                            for (NSString *string in channelIDArray) {
//                                NSString *trimmedString1 = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//                                [trimmedStrings1 addObject:trimmedString1];
//                            }
//                            otherEndTimesArray = trimmedStrings;
//                            channelIDArray = trimmedStrings1;
//                            [channelIDArray addObject:[dict objectForKey:@"channelId"]];
//                            [hdArray addObject:[dict objectForKey:@"streamType"]];
//                            [deletionStatusArray addObject:[dict objectForKey:@"deletionStatus"]];
//                            [countryAllowedStatusArray addObject:[dict objectForKey:@"countryAllowed"]];
//                            [blockStatusArray addObject:[dict objectForKey:@"blockedStatus"]];
//                            if ([[dict objectForKey:@"end"] isEqualToString:@""]) {
//                                NSString *emptyString = @"default";
//                                [otherEndTimesArray addObject:emptyString];
//                            }
//                            else{
//                                [otherEndTimesArray addObject:[dict objectForKey:@"end"] ];
//                            }
//                            
//                            if ([[dict objectForKey:@"start"]isEqualToString:@""]) {
//                                NSString *emptyString = @"default";
//                                [otherStartTimesArray addObject:emptyString];
//                            }
//                            else{
//                                [otherStartTimesArray addObject:[dict objectForKey:@"start"]];
//                            }
//                            noDataFound =   NO;
//                        }
//                    }
//                }
//            }
//            else{
//                noDataFound =   YES;
//            }
//            
//        }
//        else{
//            NSLog (@"Oh Sorry");
//            noDataFound = YES;
//            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
//            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            //            [alert show];
//        }
//        
//        self.currentValue = [self getIndexSelectedChannel:lastListenStatusArray];
//        cellIndexPath = [NSIndexPath indexPathForRow:self.currentValue inSection:0];
//        }
//        @catch (NSException *exception) {
//            
//        }
//        @finally {
//            
//        }
//        
//    }
-(void)fetchTVChannelLanguageFilterData{
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy/MM/dd"];
    NSString *date_String=[dateformate stringFromDate:[NSDate date]];
    
    @try {
        
        NSString * urlReq = [NSString stringWithFormat:@"%@/?fetchCustomerTvChannelLanguages=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[SprngIPConf getSharedInstance]getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
        
        NSURL *url = [NSURL URLWithString:urlReq];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (!(error))
        {
            receivedData1 = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
            sublanguagefilterArray =[[receivedData1 valueForKey:@"root"]valueForKey:@"LanguageList"];
            genreFilterArray =[[receivedData1 valueForKey:@"root"]valueForKey:@"GenreList"];
            NSLog(@"%@", sublanguagefilterArray);
            NSLog(@"%@",receivedData1);
            NSLog(@"%@",genreFilterArray);
        }
        else{
            NSLog(@"%@ Null Error",error);
        }
        
    } @catch (NSException *exception) {
        NSLog(@"%@ exception Error",exception);
    } @finally {
        NSLog(@"");
    }
}

-(void)fetchEpgData:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null])
        {
            temp =[[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languagePressed"] == YES)
            {
                [selectedLanguageArray addObjectsFromArray:temp];
                
            }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"genreSelected"] == YES)
            {
                [selectedLanguageArray addObjectsFromArray:temp];
            }
            
            
            epgArray= [[NSMutableArray alloc]init];
            ratingArray = [[NSMutableArray alloc]init];
            otherStartTimesArray = [[NSMutableArray alloc]init];
            otherEndTimesArray = [[NSMutableArray alloc]init];
            channelIDArray = [[NSMutableArray alloc]init];
            blockStatusArray = [[NSMutableArray alloc]init];
            channelNameArray = [[NSMutableArray alloc]init];
            channelLogoArray = [[NSMutableArray alloc]init];
            channelUrlArray = [[NSMutableArray alloc]init];
            TVChannelLanguageArray=[[NSMutableArray alloc]init];
            hdArray = [[NSMutableArray alloc]init];
            deletionStatusArray = [[NSMutableArray alloc]init];
            countryAllowedStatusArray = [[NSMutableArray alloc]init];
            lastListenStatusArray = [[NSMutableArray alloc]init];
            
            
            if(temp) {
                
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *dict = [[NSDictionary alloc]init];
                    dict = temp[i];
                    
                    //NSString *channelLanguage =[[temp objectAtIndex:i]objectForKey:@"channelLanguage"];
                    
                    if ([[dict objectForKey:@"status"] isEqualToString:@"Package Not Found"]) {
                        packageNotFound = YES;
                        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                    }
                    
                    else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                        [self.navigationController.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        tokenExpired = YES;
                        [self goToHome];
                    }
                    
                    else if ([[dict objectForKey:@"status"] isEqualToString:@"No Data Found"]){
                        [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
                    }
                    
                    else{
                        
                        packageNotFound = NO;
                        if ([[dict objectForKey:@"deletionStatus"] isEqualToString:@"true"]) {
                            
                        }
                        
                        else{
                            
                            if ([[dict objectForKey:@"eventDescription"]isEqualToString:@""]) {
                                NSString *emptystring = @"No Description Available";
                                [descriptionArray addObject:emptystring];
                            }
                            
                            else{
                                [descriptionArray addObject:[dict objectForKey:@"eventDescription"]];
                            }
                            // lastSeenTVFilterTypeValue
                            lastSeenTVFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenTVFilterTypeValue"];
                            
                            
                            [channelNameArray addObject:[dict objectForKey:@"channelName"]];
                            NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance] getIpForImage]];
                            
                            imageUrl = [imageUrl stringByAppendingPathComponent:[dict objectForKey:@"channelLogo"]];
                            //    NSLog(@"imageurl = %@", imageUrl);
                            //    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
                            
                            [channelLogoArray   addObject:imageUrl];
                            [channelUrlArray addObject:[dict objectForKey:@"streamUrl"]];
                            [lastListenStatusArray addObject:[dict objectForKey:@"lastSeenStatus"]];
                            // [TVChannelLanguageArray addObject:[dict objectForKey:@"channelLanguage"]];
                            [epgArray addObject:[dict objectForKey:@"eventName"]];
                            
                            if ([[dict objectForKey:@"rating"] isEqualToString:@""])
                            {
                                NSString *emptyString = @"";
                                [ratingArray addObject:emptyString];
                            }
                            else{
                                [ratingArray addObject:[dict objectForKey:@"rating"]];
                            }
                            NSMutableArray *trimmedStrings = [NSMutableArray array];
                            for (NSString *string in otherEndTimesArray) {
                                NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                [trimmedStrings addObject:trimmedString];
                            }
                            
                            NSMutableArray *trimmedStrings1 = [NSMutableArray array];
                            for (NSString *string in channelIDArray) {
                                NSString *trimmedString1 = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                [trimmedStrings1 addObject:trimmedString1];
                            }
                            otherEndTimesArray = trimmedStrings;
                            channelIDArray = trimmedStrings1;
                            
                            [channelIDArray addObject:[dict objectForKey:@"channelId"]];
                            [hdArray addObject:[dict objectForKey:@"streamType"]];
                            [deletionStatusArray addObject:[dict objectForKey:@"deletionStatus"]];
                            [countryAllowedStatusArray addObject:[dict objectForKey:@"countryAllowed"]];
                            [blockStatusArray addObject:[dict objectForKey:@"blockedStatus"]];
                            if ([[dict objectForKey:@"end"] isEqualToString:@""]) {
                                NSString *emptyString = @"default";
                                [otherEndTimesArray addObject:emptyString];
                            }
                            else{
                                [otherEndTimesArray addObject:[dict objectForKey:@"end"] ];
                            }
                            
                            if ([[dict objectForKey:@"start"]isEqualToString:@""]) {
                                NSString *emptyString = @"default";
                                [otherStartTimesArray addObject:emptyString];
                            }
                            else{
                                [otherStartTimesArray addObject:[dict objectForKey:@"start"]];
                            }
                            
                            noDataFound =   NO;
                            //}
                        }
                    }
                }
                [self.tvTableView reloadData];
                
                // if (isPlayerLoaded == NO) {
                self.currentValue = [LastSeenIndex getIndexSelectedChannel:lastListenStatusArray];
                cellIndexPath = [NSIndexPath indexPathForRow:self.currentValue inSection:0];
                [self playTVWithValue];
                //   }
                
                noDataFound = NO;
            }
        }
        
        else{
            noDataFound = YES;
        }
        
    }
    @catch (NSException *exception) {
        
    } @finally {
        
        [loader stopAnimating];
        [loader hidesWhenStopped];
    }
    
}
    @end
