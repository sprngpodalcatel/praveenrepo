//
//  TokenlessApiCall.m
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 22/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "TokenlessApiCall.h"
#import "SprngIPConf.h"
#import "UIView+Toast.h"

@implementation TokenlessApiCall


+(void)postRequestWithoutToken:(NSString *)postUrl postKeys:(NSString *)postKeys inView:(UIView *)view
{
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"post Request with url : %@ \n arguments : %@", postUrl, postKeys);
        @try {
            CSToastStyle *style;
            style = [[CSToastStyle alloc] initWithDefaultStyle];
            style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
            style.messageColor = [UIColor whiteColor];
            style.messageAlignment = NSTextAlignmentCenter;
            style.backgroundColor = [UIColor grayColor];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],postUrl]];
//            NSLog(@"postUrl %@",url);
            NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
            
            request.HTTPMethod = @"POST";
            NSData *data = [postKeys dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                NSLog(@"error is %@", error);
                NSDictionary *dictionary;
                NSHTTPURLResponse *httpResponse;
                NSLog(@"Running on %@ thread", [NSThread currentThread]);
                if (!error) {
                    dictionary = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingAllowFragments error:&error];
//                    NSLog(@"string is %@",[NSString stringWithUTF8String:[data bytes]]);
                    
//                    NSLog(@"dictionary is %@", dictionary);
                    httpResponse = (NSHTTPURLResponse *)response;
                    
                    
                    if(dictionary){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[dictionary, httpResponse] forKeys:@[@"dictionary", @"response"]];
                            
                            NSArray *array = [dictionary objectForKey:@"root"];
                            NSDictionary *dict = array[0];
                            NSString *token = [dict objectForKey:@"token"];
                            [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                            [myCenter postNotificationName:[NSString stringWithFormat:@"%@",postUrl] object:self userInfo:myDic];
                        });
                    }
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"Error: %@",error.userInfo);
                        
                        [view makeToast:[NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"NSLocalizedDescription"]] duration:2.0 position:CSToastPositionCenter style:style];
                        
                        NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[error, [NSNull null]] forKeys:@[@"error", @"dictionary"]];
                        NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                        [myCenter postNotificationName:[NSString stringWithFormat:@"%@",postUrl] object:self userInfo:myDic];
                    });
                }
                
            }];
            
            // 5
            [uploadTask resume];
            
        }
        @catch(NSException *exception){
            NSLog(@"%@", exception.reason);
        }
    });
}


@end
