//
//  TourOnDemandTableViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 13/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourOnDemandTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tourLogImageView;
@property (weak, nonatomic) IBOutlet UILabel *TourLibraryName;

@property (weak, nonatomic) IBOutlet UITextView *tourDescriptionText;
@property (weak, nonatomic) IBOutlet UIView *channelDetailsView;


@end
