//
//  TourCollectionViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 17/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *tourCollectionImageView;



@end
