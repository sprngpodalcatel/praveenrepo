//
//  FourthViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "FourthViewController.h"
#import "MoreTableViewCell.h"
#import "channelTableViewCell.h"
#import "DeleteTableViewCell.h"
#import "RenameTableViewCell.h"
#import "EditFavouriteTableViewCell.h"
#import "favEditTableViewCell.h"
#import "BlockTableViewCell.h"
#import "NewTableViewCell.h"
#import "ParentalTableViewCell.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "newMainTableViewCell.h"
#import "netWork.h"
#import "Flurry.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "NSString+AES.h"

#import "LocationUpdate.h"



@interface FourthViewController ()
{
    NSArray *oprionsArray;
    UITextField *addFavouriteTextField;
    NSString *textFieldString,*deleteFolderString,*renameFolderString,*renameFieldString,*editFolderString,*oldPINString,*newPINString,*blockPINString,*parentalPIN,*selectedParentalIDString;
    NSMutableArray *selectedObjectsArray;
    netWork *netReachability;
    NetworkStatus networkStatus;
    
    BOOL addFav;
    BOOL editFav;
    BOOL blockChan;
    BOOL parentalCont;
    
    CSToastStyle *style;
    LocationUpdate *locationRefresh;
    
    NSString *oPINString;
    NSString *nPINString;
    NSString *rPINString;
    
    NSIndexPath *selectedIndexPath,*deleteIndexPath,*editIndexPath;
    NSMutableArray *channelsArray,*channelNameArray,*channelIdArray,*cellSelected,*temp,*deleteArray,*temp1,*temp2;
    NSMutableArray *renameArray,*editArray,*editIDarray,*editstatusArray,*favEditArray,*selectedEditArray,*blockedStatusArray,*blockChannelArray,*blockIDArray,*selectedBlockArray,*parentalArray,*parentalIDArray,*ageStatusArray;
    BOOL UIAlertController;
    
    UIAlertView *favouriteAlert,*deleteAlert,*renameAlert,*changePINAlertView,*blockAlert,*parentalAlert;
    UIAlertController*alertcontrol;
}
@property (weak, nonatomic) IBOutlet UINavigationItem *moreNav;
@property (weak, nonatomic) IBOutlet UITableView *subTableView;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (weak, nonatomic) IBOutlet UIView *selectionView;
@property (weak, nonatomic) IBOutlet UITableView *deleteTableView;
@property (weak, nonatomic) IBOutlet UITableView *renameTableView;
@property (weak, nonatomic) IBOutlet UITableView *editTableView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UITableView *favEditTableView;
@property (weak, nonatomic) IBOutlet UITableView *blockTableView;
@property (weak, nonatomic) IBOutlet UITableView *parentalTableView;
@property (weak, nonatomic) IBOutlet UIButton *OK1;
@property (weak, nonatomic) IBOutlet UIButton *OK2;
@property (weak, nonatomic) IBOutlet UIView *pinChangeView;
@property (weak, nonatomic) IBOutlet UITextField *oldPINTextField;
@property (weak, nonatomic) IBOutlet UITextField *retype;
@property (weak, nonatomic) IBOutlet UITextField *N;
@property (weak, nonatomic) IBOutlet UIButton *OK;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UIView *headingView;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancel1;
@property (weak, nonatomic) IBOutlet UIButton *Cancel2;
@property (weak, nonatomic) IBOutlet UITableView *inTableView;
@property (weak, nonatomic) IBOutlet UIView *renameView;
@property (weak, nonatomic) IBOutlet UITableView *mainNew;
@property (weak, nonatomic) IBOutlet UILabel *mainTitle;
@property (weak, nonatomic) IBOutlet UIView *selectionNew;
@property (weak, nonatomic) IBOutlet UIButton *cancel11;
@property (weak, nonatomic) IBOutlet UIButton *OK11;
@property (weak, nonatomic) IBOutlet UIButton *renameViewCancel;
@property (weak, nonatomic) IBOutlet UIView *ranamePopUP;
@property (weak, nonatomic) IBOutlet UIView *addFavPopUp;
@property (weak, nonatomic) IBOutlet UIView *PINpopUp;
@property (weak, nonatomic) IBOutlet UIView *deletePopUp;
@property (weak, nonatomic) IBOutlet UITextField *renameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addFavTextField;
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UILabel *seperationLabel;
@property (weak, nonatomic) IBOutlet UIView *transparantView;




@end



@implementation FourthViewController
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    if ([self isKindOfClass:[UINavigationController class]]) {
        UIViewController *rootController = [((UINavigationController *)self).viewControllers objectAtIndex:0];
        return [rootController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
    }
    return [self shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _transparantView.hidden = YES;
//    _transparantView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
     [self ActionScheetDisplayMethod];
    
//    oprionsArray = [NSArray arrayWithObjects:@"Parental Control",@"Change Pin",@"Add Favourites",@"Edit Favourites",@"Rename Favourites",@"Delete Favourites",@"Block Channel",nil];
    
//    UIActionSheet *firstSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Parental Control",@"Change Pin",@"Add Favourites",@"Edit Favourites",@"Rename Favourites",@"Delete Favourites",@"Block Channel",nil];
//    firstSheet.tag = 1;
//    [firstSheet showInView:self.view];
    
    [addFavouriteTextField setDelegate:self];
    
    self.headingView.layer.cornerRadius = 5;
    self.selectionView.layer.cornerRadius = 5;
    self.pinChangeView.layer.cornerRadius = 13;
    self.mainTitle.layer.borderWidth = 1;
    self.mainTitle.layer.borderColor = [UIColor grayColor].CGColor;
    self.selectionNew.layer.borderWidth = 1;
    self.selectionNew.layer.borderColor = [UIColor grayColor].CGColor;
    self.renameViewCancel.layer.borderWidth = 1;
    self.renameViewCancel.layer.borderColor = [UIColor grayColor].CGColor;
    self.subTableView.layer.borderWidth = 1;
    self.subTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.editTableView.layer.borderWidth = 1;
    self.editTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.favEditTableView.layer.borderWidth = 1;
    self.favEditTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.renameTableView.layer.borderWidth = 1;
    self.renameTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.mainNew.layer.borderWidth = 1;
    self.mainNew.layer.borderColor = [UIColor grayColor].CGColor;
    self.deleteTableView.layer.borderWidth = 1;
    self.deleteTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.blockTableView.layer.borderWidth = 1;
    self.blockTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.parentalTableView.layer.borderWidth = 1;
    self.parentalTableView.layer.borderColor = [UIColor grayColor].CGColor;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.backgroundColor = [UIColor grayColor];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _pinTextField.inputAccessoryView = numberToolbar;
    _oldPINTextField.inputAccessoryView = numberToolbar;
    _retype.inputAccessoryView = numberToolbar;
    _N.inputAccessoryView = numberToolbar;
    _renameTextField.inputAccessoryView = numberToolbar;
    _addFavTextField.inputAccessoryView = numberToolbar;
    
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(self.seperationLabel.frame.size.width/2, 5.0)];
    [linePath addLineToPoint:CGPointMake(self.seperationLabel.bounds.size.width/2 , 37.0)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [linePath CGPath];
    shapeLayer.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer.lineWidth = 1.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    
    [self.seperationLabel.layer addSublayer:shapeLayer];
    
    UIBezierPath *linePath1 = [UIBezierPath bezierPath];
    [linePath1 moveToPoint:CGPointMake(8.0, 146.0)];
    [linePath1 addLineToPoint:CGPointMake(232 , 146.0)];
    [linePath1 moveToPoint:CGPointMake(240.0/2, 146.0)];
    [linePath1 addLineToPoint:CGPointMake(240.0/2, 178.0)];
    
    CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.path = [linePath1 CGPath];
    shapeLayer1.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer1.lineWidth = 1.0;
    shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
    
    [self.pinChangeView.layer addSublayer:shapeLayer1];
    
    
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    
    selectedObjectsArray = [[NSMutableArray alloc]init];
    selectedEditArray = [[NSMutableArray alloc]init];
    selectedBlockArray = [[NSMutableArray alloc]init];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    self.tabBarController.tabBar.shadowImage = [UIImage new];
    self.tabBarController.tabBar.translucent = YES;
    self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    
    
    [Flurry logEvent:@"More_Screen" withParameters:nil];

    
//    UILabel *titleLabel = [[UILabel alloc] init];
//    titleLabel.text = @"MORE";
//    titleLabel.textColor = [UIColor whiteColor];
//    [titleLabel sizeToFit];
//    self.moreNav.titleView = titleLabel;
    [self doSetUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if (networkStatus == NotReachable) {
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
    locationRefresh = [[LocationUpdate alloc] init];
    [locationRefresh getCurrentLocation];

}

-(void)goToHome{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [self.view endEditing:YES];
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}

-(void)doSetUp
{
    [self.subTableView reloadData];
    self.selectionView.hidden = YES;
    self.selectionNew.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.cancel1.hidden = NO;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.subTableView.hidden = YES;
    self.deleteTableView.hidden = YES;
    self.renameTableView.hidden = YES;
    //self.renameView.hidden = NO;
    self.mainTitle.text = @"More";
    self.addFavPopUp.hidden = YES;
   
    self.ranamePopUP.hidden = YES;
    _transparantView.hidden = YES;
    self.PINpopUp.hidden = YES;
    self.deletePopUp.hidden = YES;
   
    self.editTableView.hidden = YES;
    self.favEditTableView.hidden = YES;
    self.blockTableView.hidden = YES;
    self.parentalTableView.hidden = YES;
    self.pinChangeView.hidden = YES;
    self.headingView.hidden = YES;
    self.headingLabel.text = @"More";
    self.mainTableView.hidden = YES;
}
//***** More standardpopup change code*****//
-(void)ActionScheetDisplayMethod
{
    UIActionSheet *firstSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Parental Control",@"Change Pin",@"Block Channel",nil];
    [firstSheet showInView:self.view];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

-(void)cancelNumberPad{
    [_pinTextField resignFirstResponder];
    [_oldPINTextField resignFirstResponder];
    [_retype resignFirstResponder];
    [_N resignFirstResponder];
    [_addFavTextField resignFirstResponder];
    [_renameTextField resignFirstResponder];
    _pinTextField.text = @"";
}

-(void)doneWithNumberPad{
//    NSString *numberFromTheKeyboard = numberTextField.text;
    [_pinTextField resignFirstResponder];
    [_oldPINTextField resignFirstResponder];
    [_retype resignFirstResponder];
    [_addFavTextField resignFirstResponder];
    [_renameTextField resignFirstResponder];
    [_N resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];

}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
//    if (self.view.frame.size.height == 480) {
//        self.pinChangeView.frame = CGRectMake(68, 219, 240, 178);
//    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.subTableView) {
        return [channelsArray count];
    }
    if (tableView == self.parentalTableView) {
        return [parentalArray count];
    }
    
    if (tableView == self.deleteTableView) {
        return [deleteArray count];
    }
    if (tableView == self.renameTableView) {
        return [renameArray count];
    }
    if (tableView == self.editTableView) {
        return [editArray count];
    }
    if (tableView == self.favEditTableView) {
        return [favEditArray count];
    }
    if (tableView == self.blockTableView) {
        return [blockChannelArray count];
    }
    else
        return oprionsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (tableView == self.mainTableView) {
//        MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MoreCell" forIndexPath:indexPath];
//        cell.moreLabel.text = [oprionsArray objectAtIndex:indexPath.row];
//        return cell;
    }
    
    if (tableView == self.mainNew) {
        newMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newMain1" forIndexPath:indexPath];
        cell.myLable.text = [oprionsArray objectAtIndex:indexPath.row];
        _renameView.backgroundColor = [UIColor clearColor];

        return cell;
    }
       
    if (tableView == self.inTableView) {
        NewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newMain" forIndexPath:indexPath];
        cell.myLabel.text = [oprionsArray objectAtIndex:indexPath.row];
        _renameView.backgroundColor = [UIColor clearColor];

        return cell;
    }
    
    if (tableView == self.deleteTableView) {
        DeleteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deleteCell" forIndexPath:indexPath];
        cell.deleteCell.text = [deleteArray objectAtIndex:indexPath.row];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        _renameView.backgroundColor = [UIColor clearColor];

        
        return cell;
    }
    
    if (tableView == self.renameTableView) {
        RenameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"renameCell" forIndexPath:indexPath];
        cell.renameLabel.text = [renameArray objectAtIndex:indexPath.row];
        
        cell.renameLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        
        _renameView.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    
    
    if (tableView == self.parentalTableView) {
        ParentalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"parentalCell" forIndexPath:indexPath];
        cell.parentallabel.text = [parentalArray objectAtIndex:indexPath.row];
        cell.parentallabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
       // cell.backgroundColor = [UIColor clearColor];
        
        _renameView.backgroundColor = [UIColor clearColor];

        if ([ageStatusArray [indexPath.row] isEqualToString:@"true"]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        return cell;
    }
    
    
    if (tableView == self.blockTableView) {
        BlockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"blockCell" forIndexPath:indexPath];
        cell.blockLabel.text = [blockChannelArray objectAtIndex:indexPath.row];
        
        cell.blockLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        
        _renameView.backgroundColor = [UIColor clearColor];
        
        
//        if ([blockedStatusArray [indexPath.row] isEqualToString:@"true"]) {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            [selectedBlockArray addObject:blockIDArray[indexPath.row]];
//            
//        }
//        else{
            if([selectedBlockArray containsObject:blockIDArray[indexPath.row]]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            
//        }
        return cell;
    }
    
    if (tableView == self.favEditTableView) {
        favEditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favEdit" forIndexPath:indexPath];
        
        cell.favEditLabel.text = [favEditArray objectAtIndex:indexPath.row];
        
        cell.favEditLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        
        _renameView.backgroundColor = [UIColor clearColor];

    
            if([selectedEditArray containsObject:editIDarray[indexPath.row]]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            
//        }
        
        
        return cell;
        
        
    }
    
    if (tableView == self.editTableView) {
        EditFavouriteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editFav" forIndexPath:indexPath];
        
        cell.editLabel.text = [editArray objectAtIndex:indexPath.row];
        
    cell.editLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        _renameView.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    if (tableView == self.subTableView) {
        channelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"channelCell" forIndexPath:indexPath];
        cell.channelDisplayLabel.text = [channelNameArray objectAtIndex:indexPath.row];
        
        cell.channelDisplayLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        _renameView.backgroundColor = [UIColor clearColor];

        
        if ([selectedObjectsArray containsObject:channelIdArray[indexPath.row]])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.mainNew)
    {
        
   

    }
    
    
    if (tableView == self.subTableView) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        [selectedObjectsArray addObject:channelIdArray[indexPath.row]];
        selectedIndexPath = indexPath;
        selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    if (tableView == self.parentalTableView) {
        
    selectedParentalIDString = [[NSString alloc]init];
    selectedParentalIDString = [parentalIDArray objectAtIndex:indexPath.row];
       // self.parentalTableView.hidden = YES;
        
        self.renameView.hidden = YES;
        self.mainTitle.text = @"More";
        self.mainNew.hidden = YES;
        self.headingView.hidden = YES;
        self.selectionView.hidden = YES;
        self.Cancel2.hidden = YES;
        self.okButton.hidden = YES;
        self.OK1.hidden = YES;
        self.OK2.hidden = YES;
    
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/parentalControlSubmit1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
        
        NSString *post = [NSString stringWithFormat:@"customerId=%@&ageStatusId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],selectedParentalIDString];
//        NSLog(@"post %@",post);
        
        NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setHTTPBody:postData];
        
        NSURLResponse *response;
        NSError *error = nil;
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
//        NSLog(@"json_dict\n%@",json_dict);
//        NSLog(@"json_string\n%@",json_string);
        
//        NSLog(@"%@",response);
//        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
//            NSLog(@"%@",dictionary);
            temp = [dictionary objectForKey:@"root"];
            if (temp) {
                NSDictionary *sDic = temp[0];
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    
                    [self.navigationController.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Parental" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
                }
                
               else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                   [self goToHome];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Age Status Submitted Successfully" ]) {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:newPINString forKey:@"pin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self.navigationController.view makeToast:@"Age Status Submitted Successfully" duration:2.0 position:CSToastPositionCenter style:style];

                }
                else{
                    NSLog(@"temp is getting Null");
                }
                
                
            }
            else
            {
                NSLog(@"%@",error);
            }
            
        }
     //***** More standardpopup change code*****//
        NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 04.0 target: self
                                                          selector: @selector(callAfterFourSecond) userInfo: nil repeats: nil];
    }
    
    if (tableView == self.deleteTableView) {
        deleteFolderString = [[NSString alloc]init];
        deleteFolderString = [deleteArray objectAtIndex:indexPath.row];
        NSString *selectedFileName = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedListName"];
        if ([selectedFileName isEqualToString:deleteFolderString]) {
           
            
            [self.navigationController.view makeToast:@"Cannot Delete Playing List" duration:2.0 position:CSToastPositionCenter style:style];
            
//            [[[UIAlertView alloc]initWithTitle:@"Delete" message:@"This List is playing You cant delete this" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        }
        else{
            [self.view endEditing:YES];
            _transparantView.hidden = NO;
            //self.deletePopUp.hidden = NO;
           
            
        deleteAlert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Delete This Favourite ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [deleteAlert show];
            
        }
        
    }
    
    if (tableView == self.editTableView) {
        
        
        editFolderString = [[NSString alloc]init];
        editFolderString = [editArray objectAtIndex:indexPath.row];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        if (networkStatus == NotReachable) {
            
            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
        }
        else{
            NSLog(@"Network %ld",(long)networkStatus);
        }
        
        [dateformate setDateFormat:@"yyyy/MM/dd"];
        
        NSString *date_String=[dateformate stringFromDate:[NSDate date]];
//        editFolderString = [editFolderString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString *urlParameter = [NSString stringWithFormat:@"%@&listName=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[editFolderString encode],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]];
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchFavouriteTvChannelListDetails1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],urlParameter]]];
        
        [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                      returningResponse:&response
                                                                  error:&error];
        
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
//        NSLog(@"json_dict\n%@",json_dict);
//        NSLog(@"json_string\n%@",json_string);
        
//        NSLog(@"%@",response);
        //  NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger status =  [httpResponse statusCode];
            temp = [[NSMutableArray alloc]init];
            temp =[dictionary objectForKey:@"root"];
       //     NSLog(@"status = %ld",(long)status);
            favEditArray = [[NSMutableArray alloc]init];
            editstatusArray = [[NSMutableArray alloc]init];
            editIDarray = [[NSMutableArray alloc]init];
            
            if(temp && status == 200) {
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *insideDict = temp[i];
                    if ([[insideDict objectForKey:@"status"]isEqualToString:@"Package Not Found"]) {
                        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                    }
                    
                    else if ([[insideDict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                    }
                    else{
                    [favEditArray addObject:[insideDict objectForKey:@"channelName"]];
                    [editIDarray addObject:[insideDict objectForKey:@"id"]];
                    [editstatusArray addObject:[insideDict objectForKey:@"favouriteListStatus"]];
                        if ([editstatusArray[i] isEqualToString:@"Added"]) {
                            [selectedEditArray addObject:editIDarray[i]];
                            
                        }
                        
                    }
                    
                }
                [self.favEditTableView reloadData];
               
                self.editTableView.hidden = YES;
                self.selectionNew.hidden = NO;
                self.favEditTableView.hidden = NO;
                self.cancel1.hidden = YES;
                self.Cancel2.hidden = NO;
                self.OK1.hidden = NO;
                NSLog(@"%@",editstatusArray);
            }
            else
            {
                NSLog(@"API status Error");
            }
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
            
        }
    }
    
    
    if (tableView == self.favEditTableView) {
        
        UITableViewCell *selectCell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *selectedID = [[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"];
        NSString *selectedFileName = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedListName"];
        int selectedIndex = (int)[editIDarray indexOfObject:selectedID];
        if ([editFolderString isEqualToString:selectedFileName]) {
            if (indexPath.row == selectedIndex) {
                
                
                [self.navigationController.view makeToast:@"Cannot Delete Playing channel" duration:2.0 position:CSToastPositionCenter style:style];
//                [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"You Cant Delete The playing Channel" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil]show];
                
            }
            else
            {
                [selectedEditArray addObject:editIDarray[indexPath.row]];
                selectedEditArray = [NSMutableArray arrayWithArray:[[NSSet setWithArray:selectedEditArray]allObjects]];
                editIndexPath = indexPath;
                selectCell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            
        }
        else
        {
        [selectedEditArray addObject:editIDarray[indexPath.row]];
        selectedEditArray = [NSMutableArray arrayWithArray:[[NSSet setWithArray:selectedEditArray]allObjects]];
        editIndexPath = indexPath;
        selectCell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
        
    }
    
    if (tableView == self.blockTableView) {
        UITableViewCell *selectCell = [tableView cellForRowAtIndexPath:indexPath];
        [selectedBlockArray addObject:blockIDArray[indexPath.row]];
        selectedBlockArray = [NSMutableArray arrayWithArray:[[NSSet setWithArray:selectedBlockArray]allObjects]];
        selectCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    if (tableView == self.renameTableView) {
        renameFolderString = [[NSString alloc]init];
        renameFolderString = [renameArray objectAtIndex:indexPath.row];
        [self.view endEditing:YES];
       // _transparantView.hidden = NO;
        
     // self.ranamePopUP.hidden = NO;
        
        renameAlert = [[UIAlertView alloc]initWithTitle:@"Rename Selected Favourites" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        renameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *renametextField = [renameAlert textFieldAtIndex:0];
        renametextField.tag = 2;
        renametextField.delegate = self;
        
        renametextField.placeholder = @"Enter Your Favourite Name";
        renametextField.keyboardType = UIKeyboardTypeAlphabet;

        [renameAlert show];
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.subTableView) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        selectedCell.accessoryType = UITableViewCellAccessoryNone;
        
        if ([selectedObjectsArray containsObject:channelIdArray[indexPath.row]]) {
            [selectedObjectsArray removeObject:channelIdArray[indexPath.row]];
        }
        
    }
    
    if (tableView == self.favEditTableView) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
       
        NSString *selectedID = [[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"];
        NSString *selectedFileName = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedListName"];
        long selectedIndex = [editIDarray indexOfObject:selectedID];
        if ([editFolderString isEqualToString:selectedFileName]) {
            if (indexPath.row == selectedIndex) {
                
                
                [self.navigationController.view makeToast:@"Cannot Delete Playing channel" duration:2.0 position:CSToastPositionCenter style:style];
//                [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"You Cant Delete The playing Channel" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil]show];
            }
            else
            {
                if ([selectedEditArray containsObject:editIDarray[indexPath.row]]) {
                    [selectedEditArray removeObject:editIDarray[indexPath.row]];
                     selectedCell.accessoryType = UITableViewCellAccessoryNone;
                }
                
            }
        }
        else{
        
        if ([selectedEditArray containsObject:editIDarray[indexPath.row]]) {
            [selectedEditArray removeObject:editIDarray[indexPath.row]];
             selectedCell.accessoryType = UITableViewCellAccessoryNone;
        }
        }
        
        
    }
    
    if (tableView == self.blockTableView) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        
        if ([selectedBlockArray containsObject:blockIDArray[indexPath.row]]) {
            [selectedBlockArray removeObject:blockIDArray[indexPath.row]];
            selectedCell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
    
}
//***** More standardpopup change code*****//

-(void) callAfterFourSecond
{

    [self ActionScheetDisplayMethod];
}


//***** More standardpopup change code*****//
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSString *indexstring = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if([indexstring isEqual: @"Parental Control"])
    {
        //action = nil;
        [self.view endEditing:YES];
        _transparantView.hidden = NO;
        parentalCont = YES;
        
        parentalAlert = [[UIAlertView alloc]initWithTitle:@"Parental Control" message:@"select Parental Control" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        parentalAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        UITextField *pinTextField = [parentalAlert textFieldAtIndex:0];
        pinTextField.delegate = self;
        pinTextField.keyboardType = UIKeyboardTypeNumberPad;
        [parentalAlert show];
       
        
    }
    else if([indexstring isEqual: @"Change Pin"])
    {
       
        
        [self.view endEditing:YES];
        _transparantView.hidden = NO;
        
        self.oldPINTextField.text = @"";
        self.N.text = @"";
        self.retype.text = @"";
        
        self.pinChangeView.hidden = NO;
    }
    /*
    else if([indexstring isEqual: @"Add Favourites"])
    {
//        [self.view endEditing:YES];
//        _transparantView.hidden = NO;
//        [self.subTableView reloadData];
//        self.addFavTextField.text = @"";
//        self.renameView.hidden = YES;
//
//        favouriteAlert = [[UIAlertView alloc]initWithTitle:@" Favourite" message:@"Name Your Favourite List" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//        favouriteAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//        UITextField *addFavtextField = [favouriteAlert textFieldAtIndex:0];
//        addFavtextField.tag = 2;
//        addFavtextField.delegate = self;
//        //  addFavtextField.text = @"Favourite";
//        addFavtextField.placeholder = @"Enter Your Favourite Name";
//        addFavtextField.keyboardType = UIKeyboardTypeAlphabet;
//        [favouriteAlert show];
        
    }
    else if([indexstring isEqual: @"Edit Favourites"])
    {
    
        [self.view endEditing:YES];
        self.renameView.hidden = YES;
        if (networkStatus == NotReachable) {
            
            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            //                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //                [alert show];
        }
        else{
            NSLog(@"Network %ld",(long)networkStatus);
        }
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchFavouriteTvChannelListNames1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
        [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                      returningResponse:&response
                                                                  error:&error];
        
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
        NSLog(@"json_dict\n%@",json_dict);
        NSLog(@"json_string\n%@",json_string);
        
        NSLog(@"%@",response);
        //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger status =  [httpResponse statusCode];
            temp1 =[dictionary objectForKey:@"root"];
            NSLog(@"status = %ld",(long)status);
            editArray = [[NSMutableArray alloc]init];
            
            
            if(temp1 && status == 200) {
                //                    NSLog(@"temp = %@",temp1);
                NSDictionary *dict = [[NSDictionary alloc]init];
                dict = temp1[0];
                
                if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                }
                else{
                    temp2 = [dict objectForKey:@"listNames"];
                    
                    //                    NSLog(@"temp1 = %@", temp2);
                    //                    NSLog(@"temp1 Coumt = %lu ",(unsigned long)temp2.count);
                    for (int i = 0; i < temp2.count; i++) {
                        
                        [editArray addObject:temp2[i]];
                        
                    }
                    editFav = YES;
                    addFav = NO;
                    blockChan = NO;
                    [self.editTableView reloadData];
                    self.editTableView.hidden = NO;
                    self.favEditTableView.hidden = YES;
                    self.renameTableView.hidden = YES;
                    self.mainNew.hidden = YES;
                    self.deleteTableView.hidden = YES;
                    self.blockTableView.hidden = YES;
                    self.selectionView.hidden = YES;
                    self.selectionNew.hidden = YES;
                    self.renameView.hidden = NO;
                    self.mainTitle.text = @"Edit Your favourite";
                    self.headingView.hidden = YES;
                    self.cancel1.hidden = NO;
                    self.headingLabel.text = @"Edit Your favourite";
                    self.okButton.hidden = YES;
                    self.Cancel2.hidden = YES;
                    self.OK1.hidden = YES;
                    self.OK2.hidden = YES;
                }
            }
            else{
                //                    NSLog (@"Oh Sorry");
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //                    [alert show];
                
            }
        }
        
        
        
    }
    else if([indexstring isEqual: @"Rename Favourites"])
    {
        [self.view endEditing:YES];
        
        if (networkStatus == NotReachable) {
            
            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            //                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //                [alert show];
        }
        else{
            //                NSLog(@"Network %ld",(long)networkStatus);
        }
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchFavouriteTvChannelListNames1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
        [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                      returningResponse:&response
                                                                  error:&error];
        
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
        //            NSLog(@"json_dict\n%@",json_dict);
        //            NSLog(@"json_string\n%@",json_string);
        
        //            NSLog(@"%@",response);
        //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        //            NSLog(@" error is %@",error);
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger status =  [httpResponse statusCode];
            temp1 =[dictionary objectForKey:@"root"];
            //                NSLog(@"status = %ld",(long)status);
            renameArray = [[NSMutableArray alloc]init];
            
            
            if(temp1 && status == 200) {
                //                    NSLog(@"temp = %@",temp1);
                NSDictionary *dict = [[NSDictionary alloc]init];
                dict = temp1[0];
                if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                }
                
                else{
                    temp2 = [dict objectForKey:@"listNames"];
                    
                    //                    NSLog(@"temp1 = %@", temp2);
                    //                    NSLog(@"temp1 Coumt = %lu ",(unsigned long)temp2.count);
                    for (int i = 0; i < temp2.count; i++) {
                    [renameArray addObject:temp2[i]];
                }
                    self.mainNew.hidden = YES;
                    self.mainTitle.hidden = YES;
                    self.mainTitle.text = @"Rename Favourite List";
                    [self.renameTableView reloadData];
                    self.renameTableView.hidden = NO;
                    self.renameView.hidden = NO;
                    self.selectionView.hidden = YES;
                    self.headingLabel.text = @"Edit Your Favourite";
                    self.headingView.hidden = YES;
                    self.cancel1.hidden = NO;
                    self.okButton.hidden = YES;
                    self.Cancel2.hidden = YES;
                    self.OK1.hidden = YES;
                    self.OK2.hidden = YES;
                }
            }
            else{
                NSLog (@"Oh Sorry");
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //                    [alert show];
                
            }
        }
        
    }
    else if([indexstring isEqual: @"Delete Favourites"])
    {
                [self.view endEditing:YES];
    
        if (networkStatus == NotReachable) {
            
            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            //                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //                [alert show];
        }
        else{
            NSLog(@"Network %ld",(long)networkStatus);
        }
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchFavouriteTvChannelListNames1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
        [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                      returningResponse:&response
                                                                  error:&error];
        
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
        NSLog(@"json_dict\n%@",json_dict);
        NSLog(@"json_string\n%@",json_string);
        
        NSLog(@"%@",response);
        //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger status =  [httpResponse statusCode];
            temp1 =[dictionary objectForKey:@"root"];
            NSLog(@"status = %ld",(long)status);
            deleteArray = [[NSMutableArray alloc]init];
            
            
            if(temp1 && status == 200) {
                //                    NSLog(@"temp = %@",temp1);
                NSDictionary *dict = [[NSDictionary alloc]init];
                dict = temp1[0];
                
                if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                }
                else{
                    temp2 = [dict objectForKey:@"listNames"];
                    
                    //                    NSLog(@"temp1 = %@", temp2);
                    //                    NSLog(@"temp1 Coumt = %lu ",(unsigned long)temp2.count);
                    for (int i = 0; i < temp2.count; i++) {
                        
                        [deleteArray addObject:temp2[i]];
                        
                    }
                    [self.deleteTableView reloadData];
                    self.deleteTableView.hidden = NO;
                    self.renameTableView.hidden = YES;
                    self.mainNew.hidden = YES;
                    self.renameView.hidden = NO;
                    renameAlert.hidden = NO;
                     self.mainTitle.hidden = YES;
                    self.mainTitle.text = @"Delete Your Favourite";
                    self.selectionView.hidden = YES;
                    self.headingView.hidden = YES;
                    self.headingLabel.text = @"Delete Your Favourite";
                    self.okButton.hidden = YES;
                    self.Cancel2.hidden = YES;
                    self.cancel1.hidden = NO;
                    self.OK1.hidden = YES;
                    self.OK2.hidden = YES;
                }
                
            }
            else{
                NSLog (@"Oh Sorry");
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //                    [alert show];
                
            }
        }
        
        
    }
     */
    
    else if([indexstring isEqual: @"Block Channel"])
    {
        _transparantView.hidden = YES;
        [self.view endEditing:YES];
        parentalCont = NO;
        blockChan = YES;
        
        blockAlert = [[UIAlertView alloc]initWithTitle:@"Enter PIN" message:@"Block Channel" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
        blockAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        UITextField *blockField = [blockAlert textFieldAtIndex:0];
        blockField.delegate = self;
        blockField.keyboardType = UIKeyboardTypeNumberPad;
        [blockAlert show];
        
        
    }else if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        self.subTableView.hidden = YES;
        self.deleteTableView.hidden = YES;
        self.selectionView.hidden = YES;
        self.cancel1.hidden = YES;
        self.Cancel2.hidden = YES;
        self.okButton.hidden = YES;
        self.OK1.hidden = YES;
        self.OK2.hidden = YES;
        self.renameTableView.hidden = YES;
        self.renameView.hidden = YES;
        self.editTableView.hidden = YES;
        self.favEditTableView.hidden = YES;
        self.blockTableView.hidden = YES;
        self.parentalTableView.hidden  =YES;
        self.headingLabel.text = @"more";
        self.headingView.hidden = NO;
        
        
        
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
            
            FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            [firstVC setNavigationTitle:@"Live TV"];
            [firstVC reloadTableview];
            
        }
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
            
            SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            [firstVC setNavigationTitle:@"Radio"];
            
        }
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
            
            ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            //        [firstVC setNavigationTitle:@"FAVOURITES"];
            [firstVC cancelButtonApi];
            
        }
        
        
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
        
    }
    
    
}

 //***** More standardpopup change code*****//
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView == parentalAlert) {
        if (buttonIndex == 1) {
            
            UITextField *pinTextField = [parentalAlert textFieldAtIndex:0];
            blockPINString = @"";
            blockPINString = pinTextField.text;
            [self.view endEditing:YES];
            if (networkStatus == NotReachable) {
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                //            [alert show];
            }
            else{
                NSLog(@"Network %ld",(long)networkStatus);
            }
            
            if (parentalCont == NO) {
                
                @try {
                    
                    //            if ([blockPINString isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"pin"]]) {
                    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?blockedTvChannelList3=%@&PIN=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress2=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],blockPINString,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]]]];
                    
                    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    NSURLResponse * response = nil;
                    NSError * error = nil;
                    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                                  returningResponse:&response
                                                                              error:&error];
                    
                    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict = (NSDictionary *)json_string;
                    //        NSLog(@"json_dict\n%@",json_dict);
                    //        NSLog(@"json_string\n%@",json_string);
                    
                    NSLog(@"%@",response);
                    //  NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    NSLog(@" error is %@",error);
                    if (!error) {
                        NSError *myError = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                        
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                        NSInteger status =  [httpResponse statusCode];
                        temp = [[NSMutableArray alloc]init];
                        temp =[dictionary objectForKey:@"root"];
                        NSLog(@"status = %ld",(long)status);
                        blockIDArray = [[NSMutableArray alloc]init];
                        blockChannelArray = [[NSMutableArray alloc]init];
                        blockedStatusArray = [[NSMutableArray alloc]init];
                        
                        if (temp && status == 200 && [[temp[0] objectForKey:@"status"] isEqualToString:@"Customer Does Not Exists"]) {
                            
                            [self.navigationController.view makeToast:@"Entered Wrong PIN" duration:2.0 position:CSToastPositionCenter style:style];
                            //                [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Entered Wrong PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                            
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]  isEqualToString:@"Token Expired"]) {
                            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToHome];
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]isEqualToString:@"Package Not Found"]){
                            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                        }
                        
                        else if(temp && status == 200) {
                            NSLog(@"channelsArray = %@",temp);
                            for (int i = 0; i < temp.count; i++) {
                                NSDictionary *dict = [[NSDictionary alloc]init];
                                dict = temp[i];
                                
                                [blockChannelArray addObject:[dict objectForKey:@"channelName"]];
                                [blockIDArray addObject:[dict objectForKey:@"channelId"]];
                                [blockedStatusArray addObject:[dict objectForKey:@"blockedStatus"]];
                                
                                if ([blockedStatusArray[i] isEqualToString:@"true"])
                                {
                                    [selectedBlockArray addObject:blockIDArray[i]];
                                }
                                
                            }
                            [self.blockTableView reloadData];
                            self.blockTableView.hidden = NO;
                            addFav = NO;
                            editFav = NO;
                            blockChan = YES;
                            
                            self.selectionNew.hidden = NO;
                            self.deleteTableView.hidden = YES;
                            self.renameTableView.hidden = YES;
                            self.mainNew.hidden = YES;
                            self.renameView.hidden = NO;
                            self.mainTitle.hidden = YES;
                            self.mainTitle.text =  @"Block Channels";
                            self.selectionView.hidden = YES;
                            self.headingLabel.text = @"Block Channels";
                            self.OK2.hidden = NO;
                            self.OK1.hidden = YES;
                            self.cancel1.hidden = YES;
                            self.Cancel2.hidden = NO;
                            self.okButton.hidden = YES;
                            // self.PINpopUp.hidden = YES;
                            _transparantView.hidden = YES;
                        }
                        
                        else
                        {
                            NSLog(@"API status Error");
                        }
                        
                    }
                    else{
                        NSLog (@"Oh Sorry");
                        [self.view endEditing:YES];
                        
                        [self.navigationController.view makeToast:@"Check Your PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //            [alert show];
                        [self.view endEditing:YES];
                        
                    }
                    
                    
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
            }
            
            if (parentalCont == YES) {
                @try {
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/confirmPINParentalControl1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],blockPINString];
                    //        NSLog(@"post %@",post);
                    
                    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setHTTPBody:postData];
                    
                    NSURLResponse *response;
                    NSError *error = nil;
                    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict = (NSDictionary *)json_string;
                    //        NSLog(@"json_dict\n%@",json_dict);
                    //        NSLog(@"json_string\n%@",json_string);
                    parentalArray = [[NSMutableArray alloc]init];
                    parentalIDArray = [[NSMutableArray alloc]init];
                    ageStatusArray  = [[NSMutableArray alloc]init];
                    //        NSLog(@"%@",response);
                    //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    //        NSLog(@" error is %@",error);
                    
                    if (!error) {
                        NSError *myError = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                        //            NSLog(@"%@",dictionary);
                        temp = [dictionary objectForKey:@"root"];
                        if (temp && [[temp[0] objectForKey:@"status"] isEqualToString:@"Customer Does Not Exists"]) {
                            [self.view endEditing:YES];
                            [self.navigationController.view makeToast:@"Check Your PIN" duration:2.0 position:CSToastPositionCenter style:style];
                            //                [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Entered Wrong PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                            
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]isEqualToString:@"Package Not Found"]){
                            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToHome];
                        }
                        else  if (temp) {
                            for (int i = 0; i< temp.count; i++) {
                                NSDictionary *insideDictionary = temp[i];
                                [parentalArray addObject:[insideDictionary objectForKey:@"description"]];
                                [parentalIDArray addObject:[insideDictionary objectForKey:@"id"]];
                                [ageStatusArray addObject:[insideDictionary objectForKey:@"ageStatusExists"]];
                                
                            }
                            [self.parentalTableView reloadData];
                            self.parentalTableView.hidden = NO;
                            parentalAlert.hidden = YES;
                            _transparantView.hidden = YES;
                            self.renameView.hidden = NO;
                            renameAlert.hidden = NO;
                            self.mainNew.hidden = YES;
                            self.renameTableView.hidden = YES;
                              self.mainTitle.hidden = YES;
                            self.mainTitle.text = @"Select parental Control";
                            self.selectionView.hidden = YES;
                            self.headingView.hidden = YES;
                            self.cancel1.hidden = NO;
                            self.headingLabel.text = @"Select parental Control";
                            self.OK1.hidden = YES;
                            self.Cancel2.hidden = YES;
                            self.OK2.hidden = YES;
                            self.okButton.hidden = YES;
                        }
                        else{
                            NSLog(@"temp is getting Null");
                        }
                        
                        
                    }
                    else
                    {
                        NSLog(@"%@",error);
                    }
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
            }
           
            
        }else{
            parentalAlert.hidden = YES;
            _transparantView.hidden = YES;
            [self.view endEditing:YES];
            
            self.subTableView.hidden = YES;
            self.deleteTableView.hidden = YES;
            self.selectionView.hidden = YES;
            self.cancel1.hidden = YES;
            self.Cancel2.hidden = YES;
            self.okButton.hidden = YES;
            self.OK1.hidden = YES;
            self.OK2.hidden = YES;
            self.renameTableView.hidden = YES;
            self.renameView.hidden = YES;
            self.editTableView.hidden = YES;
            self.favEditTableView.hidden = YES;
            self.blockTableView.hidden = YES;
            self.parentalTableView.hidden  =YES;
            self.headingLabel.text = @"more";
            self.headingView.hidden = NO;
            
            
            
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                
                FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Live TV"];
                [firstVC reloadTableview];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                
                SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Radio"];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                
                ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                //        [firstVC setNavigationTitle:@"FAVOURITES"];
                [firstVC cancelButtonApi];
                
            }
            
            
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
            
        }
        

            
        
}
    else if (alertView == favouriteAlert) {
        if (buttonIndex == 1) {
            
        
            
            
            UITextField *addFavtextField = [favouriteAlert textFieldAtIndex:0];
            
            textFieldString = @"";
            textFieldString = addFavtextField.text;
                        
            if (networkStatus == NotReachable) {
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                //            [alert show];
            }
            else{
                NSLog(@"Network %ld",(long)networkStatus);
            }
            //        textFieldString = [textFieldString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            if (textFieldString.length > 30) {
                [self.navigationController.view makeToast:@"More than 30 Characters" duration:1.0 position:CSToastPositionCenter style:style];
            }
            else if (textFieldString.length <= 30){
                @try {
                    
                    NSString *urlParameter = [NSString stringWithFormat:@"%@&listName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[textFieldString encode]];
                    
                    NSMutableURLRequest* urlRequest1 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?favouriteTvChannelListExistStatus=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],urlParameter]]];
                    
                    [urlRequest1 setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    NSURLResponse * response1 = nil;
                    NSError * error1 = nil;
                    NSData * receivedData1 = [NSURLConnection sendSynchronousRequest:urlRequest1
                                                                   returningResponse:&response1
                                                                               error:&error1];
                    
                    NSString *json_string1 = [[NSString alloc] initWithData:receivedData1 encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict1 = (NSDictionary *)json_string1;
                    //        NSLog(@"json_dict\n%@",json_dict1);
                    //        NSLog(@"json_string\n%@",json_string1);
                    
                    NSLog(@"%@",response1);
                    //  NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    NSLog(@" error is %@",error1);
                    if (!error1) {
                        NSError *myError1 = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableLeaves error:&myError1];
                        
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response1;
                        NSInteger status =  [httpResponse statusCode];
                        NSArray *testArray = [[NSArray alloc] init];
                        testArray = [dictionary objectForKey:@"root"];
                        NSDictionary *dict = [[NSDictionary alloc] init];
                        dict = testArray[0];
                        if(dict && status == 200) {
                            
                            if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                [self goToHome];
                            }
                            else if ([[dict objectForKey:@"status" ]isEqualToString:@"Favourite Tv Channel List Does Not Exists"]) {
                                addFav = YES;
                                editFav = NO;
                                blockChan = NO;
                                self.subTableView.hidden = NO;
                                self.editTableView.hidden = YES;
                                self.favEditTableView.hidden = YES;
                                self.renameTableView.hidden = YES;
                                self.mainNew.hidden = YES;
                                self.deleteTableView.hidden = YES;
                                self.blockTableView.hidden = YES;
                                self.selectionView.hidden = YES;
                                self.renameView.hidden = NO;
                                self.mainTitle.hidden = YES;
                                self.mainTitle.text = @"Select Your Favourite Channels";
                                self.mainTitle.adjustsFontSizeToFitWidth = YES;
                                self.selectionNew.hidden = NO;
                                self.okButton.hidden = NO;
                                self.cancel1.hidden = YES;
                                self.Cancel2.hidden = NO;
                                self.headingView.hidden = YES;
                                self.headingLabel.text = @"Select Your Favourite Channels";
                                self.mainTitle.adjustsFontSizeToFitWidth = YES;
                                self.OK1.hidden = YES;
                                self.OK2.hidden = YES;
                                self.addFavPopUp.hidden = YES;
                                _transparantView.hidden = YES;
                                [self.view endEditing:YES];
                                NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchAllTvChannels3=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]]]];
                                
                                [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                                NSURLResponse * response = nil;
                                NSError * error = nil;
                                NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                                              returningResponse:&response
                                                                                          error:&error];
                                
                                NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                                NSDictionary *json_dict = (NSDictionary *)json_string;
                                //                    NSLog(@"json_dict\n%@",json_dict);
                                //                    NSLog(@"json_string\n%@",json_string);
                                
                                NSLog(@"%@",response);
                                //  NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                                NSLog(@" error is %@",error);
                                if (!error) {
                                    NSError *myError = nil;
                                    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                                    
                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                    NSInteger status =  [httpResponse statusCode];
                                    channelsArray = [[NSMutableArray alloc]init];
                                    channelsArray =[dictionary objectForKey:@"root"];
                                    NSLog(@"status = %ld",(long)status);
                                    channelIdArray = [[NSMutableArray alloc]init];
                                    channelNameArray = [[NSMutableArray alloc]init];
                                    
                                    if(channelsArray && status == 200) {
                                        NSLog(@"channelsArray = %@",channelsArray);
                                        for (int i = 0; i < channelsArray.count; i++) {
                                            NSDictionary *dict = [[NSDictionary alloc]init];
                                            dict = channelsArray[i];
                                            if ([[dict objectForKey:@"status"]isEqualToString:@"Package Not Found"]) {
                                                [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                                            }
                                            
                                            else if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                                                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                                [self goToHome];
                                            }
                                            
                                            else{
                                                [channelNameArray addObject:[dict objectForKey:@"channelName"]];
                                                [channelIdArray addObject:[dict objectForKey:@"id"]];
                                                [self.subTableView reloadData];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        NSLog(@"API status Error");
                                    }
                                    
                                }
                                else{
                                    NSLog (@"Error : %@",error);
                                    
                                    
                                }
                                
                                
                            }
                            
                            else if ([[dict objectForKey:@"status" ]isEqualToString:@"Limit Exceeded"])
                            {
                                
                                
                                [self.navigationController.view makeToast:@"Favourite Tv Channel List Limit Exceeded" duration:2.0 position:CSToastPositionCenter style:style];
                                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Favourite Tv Channel List Limit Exceeded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                //                    [alert show];
                                
                            }
                            
                            else if ([[dict objectForKey:@"status"] isEqualToString:@"Package Not Found"]){
                                [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                                
                            }
                            
                            else if ([[dict objectForKey:@"status" ]isEqualToString:@"Favourite Tv Channel List Exists"])
                            {
                                
                                [self.navigationController.view makeToast:@"Favourite Tv Channel List Already Exists" duration:2.0 position:CSToastPositionCenter style:style];
                                
                                
                                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Favourite Tv Channel List Already Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                //                    [alert show];
                                
                            }
                        }
                        
                        else
                        {
                            NSLog(@"API status Error");
                        }
                        
                    }
                    else
                    {
                        NSLog (@"Error : %@",error1);
                    }
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
                
                
            }
            
        }else{
            
            [self.view endEditing:YES];
            self.addFavPopUp.hidden = YES;
            favouriteAlert.hidden = YES;
            _transparantView.hidden = YES;
            
            
            self.subTableView.hidden = YES;
            self.deleteTableView.hidden = YES;
            self.selectionView.hidden = YES;
            self.cancel1.hidden = YES;
            self.Cancel2.hidden = YES;
            self.okButton.hidden = YES;
            self.OK1.hidden = YES;
            self.OK2.hidden = YES;
            self.renameTableView.hidden = YES;
            self.renameView.hidden = YES;
            self.editTableView.hidden = YES;
            self.favEditTableView.hidden = YES;
            self.blockTableView.hidden = YES;
            self.parentalTableView.hidden  =YES;
            self.headingLabel.text = @"more";
            self.headingView.hidden = NO;
            
            
            
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                
                FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Live TV"];
                [firstVC reloadTableview];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                
                SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Radio"];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                
                ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                //        [firstVC setNavigationTitle:@"FAVOURITES"];
                [firstVC cancelButtonApi];
                
            }
            
            
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
            
        }
        
    }else if (alertView == renameAlert) {
        if (buttonIndex == 1) {
            
            UITextField *renametextField = [renameAlert textFieldAtIndex:0];
            renameFieldString = @"";
            renameFieldString = renametextField.text;
            if (networkStatus == NotReachable) {
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                //            [alert show];
            }
            else{
                NSLog(@"Network %ld",(long)networkStatus);
            }
            
            //    renameFieldString = [textFieldString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            if (renameFieldString.length > 30) {
                [self.navigationController.view makeToast:@"More than 30 Characters" duration:1.0 position:CSToastPositionCenter style:style];
            }
            
            else{
                @try {
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/renameFavouriteList1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                    //        renameFolderString = [renameFolderString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    //        renameFieldString = [renameFieldString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&oldListName=%@&newListName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[renameFolderString encode],[renameFieldString encode]];
                    NSLog(@"post %@",post);
                    
                    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setHTTPBody:postData];
                    
                    NSURLResponse *response;
                    NSError *error = nil;
                    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict = (NSDictionary *)json_string;
                    NSLog(@"json_dict\n%@",json_dict);
                    NSLog(@"json_string\n%@",json_string);
                    
                    NSLog(@"%@",response);
                    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    NSLog(@" error is %@",error);
                    
                    if (!error) {
                        NSError *myError = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                        NSLog(@"%@",dictionary);
                        temp = [dictionary objectForKey:@"root"];
                        if (temp) {
                            NSDictionary *sDic = temp[0];
                            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                
                                
                                [self.navigationController.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Rename" message:@"Customer Does Not Exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                //                    [alert show];
                            }
                            
                            else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                [self goToHome];
                            }
                            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite Tv Channel List Does Not Exists" ]) {
                                
                                
                                [self.navigationController.view makeToast:@"Favourite Tv Channel List Already Exists" duration:2.0 position:CSToastPositionCenter style:style];
                                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Rename" message:@"Favourite Tv Channel List Does Not Exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                //                    [alert show];
                            }
                            
                            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite Tv Channel List Already Exists" ]) {
                                
                                
                                [self.navigationController.view makeToast:@"Favourite Tv Channel List Already Exists" duration:2.0 position:CSToastPositionCenter style:style];
                                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Rename" message:@"Favourite Tv Channel List Already Exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                //                    [alert show];
                            }
                            
                            
                            
                            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite List Renamed" ]) {
                                
                                [self.navigationController.view makeToast:@"Renamed Successfully" duration:2.0 position:CSToastPositionCenter style:style];
                                
                                //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Rename" message:@"Renamed Successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                //                    [alert show];
                                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"deleted"];
                                [self.renameTableView reloadData];
                                self.renameTableView.hidden = YES;
                                //self.renameView.hidden = YES;
                                renameAlert.hidden = YES;
                                self.headingView.hidden = YES;
                                self.selectionView.hidden = YES;
                                self.Cancel2.hidden = YES;
                                self.okButton.hidden = YES;
                                self.OK1.hidden = YES;
                                self.OK2.hidden = YES;
                            }
                            else{
                                NSLog(@"temp is getting Null");
                            }
                            
                            
                        }
                        else
                        {
                            NSLog(@"%@",error);
                        }
                        
                    }
                    [self.view endEditing:YES];
                    
                    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                        
                        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                        [firstVC setNavigationTitle:@"Live TV"];
                        [firstVC reloadTableview];
                        
                    }
                    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                        
                        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                        [firstVC setNavigationTitle:@"Radio"];
                        
                    }
                    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                        
                        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                        //        [firstVC setNavigationTitle:@"FAVOURITES"];
                        [firstVC cancelButtonApi];
                        
                    }
                    
                    
                    [self willMoveToParentViewController:nil];
                    [self.view removeFromSuperview];
                    [self removeFromParentViewController];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 04.0 target: self
                                                                  selector: @selector(callAfterFourSecond) userInfo: nil repeats: nil];
            }
            
        }else{
            [self.view endEditing:YES];
            self.ranamePopUP.hidden = YES;
            _transparantView.hidden = YES;
            
            self.subTableView.hidden = YES;
            self.deleteTableView.hidden = YES;
            self.selectionView.hidden = YES;
            self.cancel1.hidden = YES;
            self.Cancel2.hidden = YES;
            self.okButton.hidden = YES;
            self.OK1.hidden = YES;
            self.OK2.hidden = YES;
            self.renameTableView.hidden = YES;
            self.renameView.hidden = YES;
            self.editTableView.hidden = YES;
            self.favEditTableView.hidden = YES;
            self.blockTableView.hidden = YES;
            self.parentalTableView.hidden  =YES;
            self.headingLabel.text = @"more";
            self.headingView.hidden = NO;
            
            
            
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                
                FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Live TV"];
                [firstVC reloadTableview];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                
                SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Radio"];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                
                ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                //        [firstVC setNavigationTitle:@"FAVOURITES"];
                [firstVC cancelButtonApi];
                
            }
            
            
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
            
        }
        

    }
    
    else if (alertView == blockAlert) {
        if (buttonIndex == 1) {

            UITextField *blockField = [blockAlert textFieldAtIndex:0];
            blockPINString = @"";
            blockPINString = blockField.text;
            [self.view endEditing:YES];
            if (networkStatus == NotReachable) {
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                //            [alert show];
            }
            else{
                NSLog(@"Network %ld",(long)networkStatus);
            }
            
            if (parentalCont == NO) {
                
                @try {
                    
                    //            if ([blockPINString isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"pin"]]) {
                    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?blockedTvChannelList3=%@&PIN=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress2=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],blockPINString,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]]]];
                    
                    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    NSURLResponse * response = nil;
                    NSError * error = nil;
                    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                                  returningResponse:&response
                                                                              error:&error];
                    
                    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict = (NSDictionary *)json_string;
                    //        NSLog(@"json_dict\n%@",json_dict);
                    //        NSLog(@"json_string\n%@",json_string);
                    
                    NSLog(@"%@",response);
                    //  NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    NSLog(@" error is %@",error);
                    if (!error) {
                        NSError *myError = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                        
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                        NSInteger status =  [httpResponse statusCode];
                        temp = [[NSMutableArray alloc]init];
                        temp =[dictionary objectForKey:@"root"];
                        NSLog(@"status = %ld",(long)status);
                        blockIDArray = [[NSMutableArray alloc]init];
                        blockChannelArray = [[NSMutableArray alloc]init];
                        blockedStatusArray = [[NSMutableArray alloc]init];
                        
                        if (temp && status == 200 && [[temp[0] objectForKey:@"status"] isEqualToString:@"Customer Does Not Exists"]) {
                            
                            [self.navigationController.view makeToast:@"Entered Wrong PIN" duration:2.0 position:CSToastPositionCenter style:style];
                            //                [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Entered Wrong PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                            
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]  isEqualToString:@"Token Expired"]) {
                            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToHome];
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]isEqualToString:@"Package Not Found"]){
                            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                        }
                        
                        else if(temp && status == 200) {
                            NSLog(@"channelsArray = %@",temp);
                            for (int i = 0; i < temp.count; i++) {
                                NSDictionary *dict = [[NSDictionary alloc]init];
                                dict = temp[i];
                                
                                [blockChannelArray addObject:[dict objectForKey:@"channelName"]];
                                [blockIDArray addObject:[dict objectForKey:@"channelId"]];
                                [blockedStatusArray addObject:[dict objectForKey:@"blockedStatus"]];
                                
                                if ([blockedStatusArray[i] isEqualToString:@"true"])
                                {
                                    [selectedBlockArray addObject:blockIDArray[i]];
                                }
                                
                            }
                            [self.blockTableView reloadData];
                            self.blockTableView.hidden = NO;
                            addFav = NO;
                            editFav = NO;
                            blockChan = YES;
                            
                            self.selectionNew.hidden = NO;
                            self.deleteTableView.hidden = YES;
                            self.renameTableView.hidden = YES;
                            self.mainNew.hidden = YES;
                            self.renameView.hidden = NO;
                            self.mainTitle.hidden = YES;
                            self.mainTitle.text =  @"Block Channels";
                            self.selectionView.hidden = YES;
                            self.headingLabel.text = @"Block Channels";
                            self.OK2.hidden = NO;
                            self.OK1.hidden = YES;
                            self.cancel1.hidden = YES;
                            self.Cancel2.hidden = NO;
                            self.okButton.hidden = YES;
                            self.PINpopUp.hidden = YES;
                            _transparantView.hidden = YES;
                        }
                        
                        else
                        {
                            NSLog(@"API status Error");
                        }
                        
                    }
                    else{
                        NSLog (@"Oh Sorry");
                        [self.view endEditing:YES];
                        
                        [self.navigationController.view makeToast:@"Check Your PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //            [alert show];
                        [self.view endEditing:YES];
                        
                    }
                    
                    
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
            }
            
            if (parentalCont == YES) {
                @try {
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/confirmPINParentalControl1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],blockPINString];
                    //        NSLog(@"post %@",post);
                    
                    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setHTTPBody:postData];
                    
                    NSURLResponse *response;
                    NSError *error = nil;
                    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict = (NSDictionary *)json_string;
                    //        NSLog(@"json_dict\n%@",json_dict);
                    //        NSLog(@"json_string\n%@",json_string);
                    parentalArray = [[NSMutableArray alloc]init];
                    parentalIDArray = [[NSMutableArray alloc]init];
                    ageStatusArray  = [[NSMutableArray alloc]init];
                    //        NSLog(@"%@",response);
                    //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    //        NSLog(@" error is %@",error);
                    
                    if (!error) {
                        NSError *myError = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                        //            NSLog(@"%@",dictionary);
                        temp = [dictionary objectForKey:@"root"];
                        if (temp && [[temp[0] objectForKey:@"status"] isEqualToString:@"Customer Does Not Exists"]) {
                            [self.view endEditing:YES];
                            [self.navigationController.view makeToast:@"Check Your PIN" duration:2.0 position:CSToastPositionCenter style:style];
                            //                [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Entered Wrong PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                            
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]isEqualToString:@"Package Not Found"]){
                            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                        }
                        
                        else if (temp && [[temp[0] objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToHome];
                        }
                        else  if (temp) {
                            for (int i = 0; i< temp.count; i++) {
                                NSDictionary *insideDictionary = temp[i];
                                [parentalArray addObject:[insideDictionary objectForKey:@"description"]];
                                [parentalIDArray addObject:[insideDictionary objectForKey:@"id"]];
                                [ageStatusArray addObject:[insideDictionary objectForKey:@"ageStatusExists"]];
                                
                            }
                            [self.parentalTableView reloadData];
                            self.parentalTableView.hidden = NO;
                            self.PINpopUp.hidden = YES;
                            _transparantView.hidden = YES;
                            self.renameView.hidden = YES;
                            self.mainNew.hidden = YES;
                            self.renameTableView.hidden = YES;
                            self.mainTitle.text = @"Select parental Control";
                            self.selectionView.hidden = YES;
                            self.headingView.hidden = YES;
                            self.cancel1.hidden = NO;
                            self.headingLabel.text = @"Select parental Control";
                            self.OK1.hidden = YES;
                            self.Cancel2.hidden = YES;
                            self.OK2.hidden = YES;
                            self.okButton.hidden = YES;
                        }
                        else{
                            NSLog(@"temp is getting Null");
                        }
                        
                        
                    }
                    else
                    {
                        NSLog(@"%@",error);
                    }
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                }
        }
            
 }else{
            [self.view endEditing:YES];
            _renameView.hidden = YES;
            _transparantView.hidden = YES;
            
            self.subTableView.hidden = YES;
            self.deleteTableView.hidden = YES;
            self.selectionView.hidden = YES;
            self.cancel1.hidden = YES;
            self.Cancel2.hidden = YES;
            self.okButton.hidden = YES;
            self.OK1.hidden = YES;
            self.OK2.hidden = YES;
            self.renameTableView.hidden = YES;
            self.renameView.hidden = YES;
            self.editTableView.hidden = YES;
            self.favEditTableView.hidden = YES;
            self.blockTableView.hidden = YES;
            self.parentalTableView.hidden  =YES;
            self.headingLabel.text = @"more";
            self.headingView.hidden = NO;
            
            
            
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                
                FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Live TV"];
                [firstVC reloadTableview];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                
                SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Radio"];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                
                ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                //        [firstVC setNavigationTitle:@"FAVOURITES"];
                [firstVC cancelButtonApi];
                
            }
            
            
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
            
        }
    }
    
    else if (alertView == deleteAlert) {
        if (buttonIndex == 1) {
            
            if (networkStatus == NotReachable) {
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                //            [alert show];
            }
            else{
                NSLog(@"Network %ld",(long)networkStatus);
            }
            @try {
                //        deleteFolderString = [deleteFolderString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/deleteFavouriteList1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                //        deleteFolderString = [deleteFolderString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                NSString *post = [NSString stringWithFormat:@"customerId=%@&listName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[deleteFolderString encode]];
                //        NSLog(@"post %@",post);
                
                NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                [request setHTTPBody:postData];
                
                NSURLResponse *response;
                NSError *error = nil;
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSDictionary *json_dict = (NSDictionary *)json_string;
                //        NSLog(@"json_dict\n%@",json_dict);
                //        NSLog(@"json_string\n%@",json_string);
                
                NSLog(@"%@",response);
                //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                NSLog(@" error is %@",error);
                
                if (!error) {
                    NSError *myError = nil;
                    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                    NSLog(@"%@",dictionary);
                    temp = [dictionary objectForKey:@"root"];
                    if (temp) {
                        NSDictionary *sDic = temp[0];
                        if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                            
                            
                            [self.navigationController.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                            //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                    [alert show];
                        }
                        else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToHome];
                        }
                        
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite Tv Channel List Does Not Exists" ]) {
                            
                            [self.navigationController.view makeToast:@"Favourite Tv Channel List Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                            
                            //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Favourite Tv Channel List Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                    [alert show];
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite List Deleted" ]) {
                            
                            [self.navigationController.view makeToast:@"Deleted Successfully" duration:2.0 position:CSToastPositionCenter style:style];
                            
                            //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Deleted Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                    [alert show];
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"deleted"];
                            [self.deleteTableView reloadData];
                            self.deleteTableView.hidden = YES;
                            self.renameView.hidden = NO;
                            //--------
                            self.mainTitle.hidden =YES;
                            self.mainTitle.text = @"More";
                            self.deletePopUp.hidden = YES;
                            _transparantView.hidden = YES;
                            self.headingView.hidden = YES;
                            self.selectionView.hidden = YES;
                            self.Cancel2.hidden = YES;
                            self.okButton.hidden = YES;
                            self.OK1.hidden = YES;
                            self.OK2.hidden = YES;
                            [self.view endEditing:YES];
                            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                                
                                FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                                [firstVC setNavigationTitle:@"Live TV"];
                                [firstVC reloadTableview];
                                
                            }
                            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                                
                                SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                                [firstVC setNavigationTitle:@"Radio"];
                                
                            }
                            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                                
                                ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                                //        [firstVC setNavigationTitle:@"FAVOURITES"];
                                [firstVC cancelButtonApi];
                                
                                
                            }
                            [self willMoveToParentViewController:nil];
                            [self.view removeFromSuperview];
                            [self removeFromParentViewController];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
                        }
                        else{
                            NSLog(@"temp is getting Null");
                        }
                    }
                    else
                    {
                        NSLog(@"%@",error);
                    }
                    
                }
                [self.view endEditing:YES];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        }
        else
        {
            deleteAlert.hidden = YES;
            _transparantView.hidden = YES;
            _renameView.hidden = YES;
            [self.view endEditing:YES];
            
            
            self.subTableView.hidden = YES;
            self.deleteTableView.hidden = YES;
            self.selectionView.hidden = YES;
            self.cancel1.hidden = YES;
            self.Cancel2.hidden = YES;
            self.okButton.hidden = YES;
            self.OK1.hidden = YES;
            self.OK2.hidden = YES;
            self.renameTableView.hidden = YES;
            self.renameView.hidden = YES;
            self.editTableView.hidden = YES;
            self.favEditTableView.hidden = YES;
            self.blockTableView.hidden = YES;
            self.parentalTableView.hidden  =YES;
            self.headingLabel.text = @"more";
            self.headingView.hidden = NO;
            
            
            
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
                
                FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Live TV"];
                [firstVC reloadTableview];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
                
                SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                [firstVC setNavigationTitle:@"Radio"];
                
            }
            if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
                
                ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
                //        [firstVC setNavigationTitle:@"FAVOURITES"];
                [firstVC cancelButtonApi];
                
            }
            
            
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
            
                      
        }
    }
    
}

- (IBAction)subViewCancelPressed:(id)sender {
    if (networkStatus == NotReachable) {
        
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
 
    //self.mainTitle.text = @"More";
    self.subTableView.hidden = YES;
    self.deleteTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.cancel1.hidden = NO;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.renameTableView.hidden = YES;
    self.renameView.hidden = YES;
    self.editTableView.hidden = YES;
    self.favEditTableView.hidden = YES;
    self.blockTableView.hidden = YES;
    self.parentalTableView.hidden  =YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = YES;
    editstatusArray = [[NSMutableArray alloc]init];
    
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        [firstVC reloadTableview];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        //        [firstVC setNavigationTitle:@"FAVOURITES"];
        [firstVC cancelButtonApi];
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
}


- (IBAction)cancel11Pressed:(id)sender {
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
   // self.renameView.hidden = NO;
    self.mainTitle.text = @"More";
    self.subTableView.hidden = YES;
    self.deleteTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.cancel1.hidden = NO;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.renameTableView.hidden = YES;
    self.renameView.hidden = YES;
    renameAlert.hidden = YES;
    self.editTableView.hidden = YES;
    self.favEditTableView.hidden = YES;
    self.blockTableView.hidden = YES;
    self.parentalTableView.hidden  =YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = YES;
    editstatusArray = [[NSMutableArray alloc]init];
    
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        [firstVC reloadTableview];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        //        [firstVC setNavigationTitle:@"FAVOURITES"];
        [firstVC cancelButtonApi];
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    
}


- (IBAction)calcelPINChange:(id)sender {
    self.pinChangeView.hidden = YES;
    _transparantView.hidden = YES;
    [self.view endEditing:YES];
    
    self.subTableView.hidden = YES;
    self.deleteTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.cancel1.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.renameTableView.hidden = YES;
    self.renameView.hidden = YES;
    self.editTableView.hidden = YES;
    self.favEditTableView.hidden = YES;
    self.blockTableView.hidden = YES;
    self.parentalTableView.hidden  =YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = NO;
    
    
    
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        [firstVC reloadTableview];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        //        [firstVC setNavigationTitle:@"FAVOURITES"];
        [firstVC cancelButtonApi];
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    

}


- (IBAction)renameViewCancel:(id)sender {
    self.subTableView.hidden = YES;
    self.deleteTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.cancel1.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.renameTableView.hidden = YES;
    self.renameView.hidden = YES;
    self.editTableView.hidden = YES;
    self.favEditTableView.hidden = YES;
    self.blockTableView.hidden = YES;
    self.parentalTableView.hidden  =YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = NO;
    
    
    
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        [firstVC reloadTableview];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        //        [firstVC setNavigationTitle:@"FAVOURITES"];
        [firstVC cancelButtonApi];
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    
}


- (IBAction)cancel1Pressed:(id)sender {
    
    self.renameView.hidden = NO;
    self.mainTitle.text = @"More";
    self.subTableView.hidden = YES;
    self.deleteTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.cancel1.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.renameTableView.hidden = YES;
    self.renameView.hidden = YES;
    self.editTableView.hidden = YES;
    self.favEditTableView.hidden = YES;
    self.blockTableView.hidden = YES;
    self.parentalTableView.hidden  =YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = NO;
    
    
    
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        [firstVC reloadTableview];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
//        [firstVC setNavigationTitle:@"FAVOURITES"];
        [firstVC cancelButtonApi];
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    
}

- (IBAction)changePINOk:(id)sender {
    
    oPINString = @"";
    nPINString = @"";
    rPINString = @"";
    
    oPINString = self.oldPINTextField.text;
    nPINString = self.N.text;
    rPINString = self.retype.text;
    self.pinChangeView.hidden = YES;
    _transparantView.hidden = YES;
    [self.view endEditing:YES];
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //        [alert show];
    }
    else
    {
        NSLog(@"Network %ld",(long)networkStatus);
    }
    if ([nPINString isEqualToString:rPINString]) {
        
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/changePIN1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&oldPIN=%@&newPIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],oPINString,nPINString];
            //        NSLog(@"post %@",post);
            
            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setHTTPBody:postData];
            
            NSURLResponse *response;
            NSError *error = nil;
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *json_dict = (NSDictionary *)json_string;
            NSLog(@"json_dict\n%@",json_dict);
            //        NSLog(@"json_string\n%@",json_string);
            
            NSLog(@"%@",response);
            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
            NSLog(@" error is %@",error);
            
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                NSLog(@"%@",dictionary);
                temp = [dictionary objectForKey:@"root"];
                if (temp) {
                    NSDictionary *sDic = temp[0];
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                        
                        
                        [self.navigationController.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                        //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN Change" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //                    [alert show];
                    }
                    
                    else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                    }
                    
                    
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                        
                        
                        [self.navigationController.view makeToast:@"Please Ckeck your PIN" duration:2.0 position:CSToastPositionCenter style:style];
                        //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN Change" message:@"Please Ckeck your PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //                    [alert show];
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Changed" ]) {
                        
                        [[NSUserDefaults standardUserDefaults] setObject:nPINString forKey:@"pin"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        [self.navigationController.view makeToast:@"PIN Changed Successfully" duration:2.0 position:CSToastPositionCenter style:style];
                        
                        //                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN Change" message:@"PIN Changed Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //                    [alert show];
                        
                    }
                    else{
                        NSLog(@"temp is getting Null");
                    }
                    
                    
                }
                else
                {
                    NSLog(@"%@",error);
                }
                
            }
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
       // [self ActionScheetDisplayMethod];
        NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 04.0 target: self
                                                          selector: @selector(callAfterFourSecond) userInfo: nil repeats: nil];
    }
    
    else{
        
        [self.navigationController.view makeToast:@"Please Check Your PIN" duration:2.0 position:CSToastPositionCenter style:style];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN Change" message:@"Please Check Your PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //        [alert show];
    }

}

- (IBAction)homePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)subTableViewOKPressed:(id)sender {
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
    self.renameView.hidden = YES;
    //self.mainTitle.text = @"More";
    self.subTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.cancel1.hidden = NO;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = YES;
    
    //    NSDictionary *selectedDict = [[NSDictionary alloc]init];
    @try {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/addTvChannelToList1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    
    
    NSString *joinedComponentsforChannel= [selectedObjectsArray componentsJoinedByString:@","];
    
//    textFieldString = [textFieldString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *post = [NSString stringWithFormat:@"customerId=%@&listName=%@&o&channelIds=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[textFieldString encode],joinedComponentsforChannel];
//    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSLog(@"%@",joinedComponentsforChannel);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
//    NSLog(@"json_dict\n%@",json_dict);
//    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
//    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite Tv Channel List Already Exists" ]) {
                selectedObjectsArray = [[NSMutableArray alloc]init];
                [self.subTableView reloadData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Favourite Tv Channel List Already Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToHome];
            }
            
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Limit Exceeded"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Favourite Tv Channel List Limit Exceeded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else{
                NSLog(@"your profile updated successfully");
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Added Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                selectedObjectsArray = [[NSMutableArray alloc]init];
                [self.subTableView reloadData];
            }
            
        }
    }
    else{
        NSLog(@"temp is getting Null");
    }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
    
    }
    
}
- (IBAction)OK11Pressed:(id)sender {
    if (editFav == YES) {
        
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
    self.renameView.hidden = YES;
    self.mainTitle.text = @"More";
    self.favEditTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.selectionNew.hidden = YES;
    self.mainNew.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.cancel1.hidden = NO;
    self.Cancel2.hidden = YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    
    
    //    NSDictionary *selectedDict = [[NSDictionary alloc]init];
        @try {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/editTvChannelToList1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    
    
    NSString *joinedComponentsforChannel= [selectedEditArray componentsJoinedByString:@","];
    
//    editFolderString = [editFolderString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *post = [NSString stringWithFormat:@"customerId=%@&listName=%@&o&channelIds=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[editFolderString encode],joinedComponentsforChannel];
//    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
//    NSLog(@"%@",joinedComponentsforChannel);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
//    NSLog(@"json_dict\n%@",json_dict);
//    NSLog(@"json_string\n%@",json_string);
    
//    NSLog(@"%@",response);
//    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
//        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Tv Channel Stored in  Favourite List " ]) {
                
           
                
                [self.navigationController.view makeToast:@"Edited Successfully" duration:2.0 position:CSToastPositionCenter style:style];
                
        
                
                
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"Edited Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
//                selectedEditArray = [[NSMutableArray alloc]init];
//                editIDarray = [[NSMutableArray alloc]init];
//                editstatusArray = [[NSMutableArray alloc]init];
//                favEditArray = [[NSMutableArray alloc]init];
//                [self.favEditTableView reloadData];
                
            }
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite List Does Not Exists" ]) {
                
         
                
                [self.navigationController.view makeToast:@"Favourite List Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
//                selectedEditArray = [[NSMutableArray alloc]init];
//                editIDarray = [[NSMutableArray alloc]init];
//                editstatusArray = [[NSMutableArray alloc]init];
//                favEditArray = [[NSMutableArray alloc]init];
//                [self.favEditTableView reloadData];
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"Favourite List Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
            }
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                
             
                
                [self.navigationController.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                
//                selectedEditArray = [[NSMutableArray alloc]init];
//                editIDarray = [[NSMutableArray alloc]init];
//                editstatusArray = [[NSMutableArray alloc]init];
//                favEditArray = [[NSMutableArray alloc]init];
//                [self.favEditTableView reloadData];
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
            }
            
            else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToHome];
            }
            else{
                NSLog(@"your profile updated successfully");
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
            }
            
        }
        else
        {
            
      
            
            [self.navigationController.view makeToast:@"You Can't remove All the Channels" duration:2.0 position:CSToastPositionCenter style:style];
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"You Can't remove All the Channels" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
        }
    }
    else{
        NSLog(@"temp is getting Null");
    }
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
       // [self ActionScheetDisplayMethod];
        NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 04.0 target: self
                                                          selector: @selector(callAfterFourSecond) userInfo: nil repeats: nil];

    }
    
    if (addFav == YES) {
        
        if (networkStatus == NotReachable) {
            
            [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
        }
        else{
            NSLog(@"Network %ld",(long)networkStatus);
        }
        self.renameView.hidden = YES;
        self.mainTitle.text = @"More";
        self.subTableView.hidden = YES;
        self.selectionNew.hidden = YES;
        self.mainNew.hidden = YES;
        self.selectionView.hidden = YES;
        self.cancel1.hidden = NO;
        self.Cancel2.hidden = YES;
        self.okButton.hidden = YES;
        self.OK1.hidden = YES;
        self.OK2.hidden = YES;
        self.headingLabel.text = @"more";
        self.headingView.hidden = YES;
        
        //    NSDictionary *selectedDict = [[NSDictionary alloc]init];
        
        @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/addTvChannelToList2",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
        
        
        
        NSString *joinedComponentsforChannel= [selectedObjectsArray componentsJoinedByString:@","];
        
//        textFieldString = [textFieldString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *post = [NSString stringWithFormat:@"customerId=%@&listName=%@&o&channelIds=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[textFieldString encode],joinedComponentsforChannel];
//        NSLog(@"post %@",post);
        
        NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        NSLog(@"%@",joinedComponentsforChannel);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setHTTPBody:postData];
        
        NSURLResponse *response;
        NSError *error = nil;
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
//        NSLog(@"json_dict\n%@",json_dict);
//        NSLog(@"json_string\n%@",json_string);
        
//        NSLog(@"%@",response);
//        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            NSLog(@"%@",dictionary);
            temp = [dictionary objectForKey:@"root"];
            if (temp) {
                
                NSDictionary *sDic = temp[0];
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite Tv Channel List Already Exists" ]) {
                    selectedObjectsArray = [[NSMutableArray alloc]init];
                    [self.subTableView reloadData];
                    
             
                    
                    [self.navigationController.view makeToast:@"Favorite TV Channel List Already Exists" duration:2.0 position:CSToastPositionCenter style:style];
                    
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Favourite Tv Channel List Already Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
                }
                else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Limit Exceeded"])
                {
                    
                   
                    
                    [self.navigationController.view makeToast:@"Favourite TV channel List Limit Exceede" duration:2.0 position:CSToastPositionCenter style:style];
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Favourite Tv Channel List Limit Exceeded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
                }
                else{
                    NSLog(@"your profile updated successfully");
           
                    
                    [self.navigationController.view makeToast:@"Added Successfully" duration:2.0 position:CSToastPositionCenter style:style];
                    
                    
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add Favourite" message:@"Added Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
                    
                    selectedObjectsArray = [[NSMutableArray alloc]init];
                    [self.subTableView reloadData];
                }
                
            }
        }
        else{
            NSLog(@"temp is getting Null");
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
     //[self ActionScheetDisplayMethod];
        NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 04.0 target: self
                                                          selector: @selector(callAfterFourSecond) userInfo: nil repeats: nil];
    }

    if (blockChan == YES) {
        
            if (networkStatus == NotReachable) {
                
                [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
                
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                [alert show];
            }
            else{
                NSLog(@"Network %ld",(long)networkStatus);
            }
            self.renameView.hidden = YES;
            self.mainTitle.text = @"More";
            self.blockTableView.hidden = YES;
            self.selectionNew.hidden = YES;
            self.mainNew.hidden = YES;
            self.selectionView.hidden = YES;
            self.Cancel2.hidden = YES;
            self.okButton.hidden = YES;
            self.OK1.hidden = YES;
            self.OK2.hidden = YES;
            self.cancel1.hidden = NO;
            self.Cancel2.hidden = YES;
            self.headingLabel.text = @"more";
            self.headingView.hidden = YES;
            
            //    NSDictionary *selectedDict = [[NSDictionary alloc]init];
        @try {
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/blockTvChannels1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
            
            [selectedBlockArray addObject:@"default"];
            
            NSString *joinedComponentsforChannel= [selectedBlockArray componentsJoinedByString:@","];
            
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&channelIds=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],joinedComponentsforChannel];
            NSLog(@"post %@",post);
            
            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSLog(@"%@",joinedComponentsforChannel);
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setHTTPBody:postData];
            
            NSURLResponse *response;
            NSError *error = nil;
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *json_dict = (NSDictionary *)json_string;
            NSLog(@"json_dict\n%@",json_dict);
            NSLog(@"json_string\n%@",json_string);
            
            NSLog(@"%@",response);
            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
            NSLog(@" error is %@",error);
            
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                NSLog(@"%@",dictionary);
                temp = [dictionary objectForKey:@"root"];
                if (temp) {
                    
                    NSDictionary *sDic = temp[0];
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Tv Channels Successfully Blocked" ]) {
                        selectedEditArray = [[NSMutableArray alloc]init];
                        editIDarray = [[NSMutableArray alloc]init];
                        editstatusArray = [[NSMutableArray alloc]init];
                        favEditArray = [[NSMutableArray alloc]init];
                        [self.favEditTableView reloadData];
                   
                        [self.navigationController.view makeToast:@"Blocked  Successfully" duration:2.0 position:CSToastPositionCenter style:style];
                        
//                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Block" message:@"Blocked Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                        [alert show];
                    }
                    
                    else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                    }
                    
                   else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                        selectedEditArray = [[NSMutableArray alloc]init];
                        editIDarray = [[NSMutableArray alloc]init];
                        editstatusArray = [[NSMutableArray alloc]init];
                        favEditArray = [[NSMutableArray alloc]init];
                        [self.favEditTableView reloadData];
                        
                   
                        [self.navigationController.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
//                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Block" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                        [alert show];
                    }
                    else{
                        NSLog(@"your profile updated successfully");
                        selectedEditArray = [[NSMutableArray alloc]init];
                        editIDarray = [[NSMutableArray alloc]init];
                        editstatusArray = [[NSMutableArray alloc]init];
                        favEditArray = [[NSMutableArray alloc]init];
                        [self.favEditTableView reloadData];
                    }
                    
                }
            }
            else{
                NSLog(@"temp is getting Null");
            }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
      }
    
    //[self ActionScheetDisplayMethod];
        NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 04.0 target: self
                                                          selector: @selector(callAfterFourSecond) userInfo: nil repeats: nil];

}
    
    
}

- (IBAction)OK1pressed:(id)sender {
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
   // self.renameView.hidden = NO;
    //self.mainTitle.text = @"More";
    self.favEditTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.cancel1.hidden = NO;
    self.Cancel2.hidden = YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    
    
    //    NSDictionary *selectedDict = [[NSDictionary alloc]init];
    @try {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/editTvChannelToList1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    
    
    NSString *joinedComponentsforChannel= [selectedEditArray componentsJoinedByString:@","];
    
//    editFolderString = [editFolderString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *post = [NSString stringWithFormat:@"customerId=%@&listName=%@&o&channelIds=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[editFolderString encode],joinedComponentsforChannel];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSLog(@"%@",joinedComponentsforChannel);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Tv Channel Stored in  Favourite List " ]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"Edited Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
                
            }
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Favourite List Does Not Exists" ]) {
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"Favourite List Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToHome];
            }
            else{
                NSLog(@"your profile updated successfully");
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
            }
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Edit" message:@"You Can't remove All the Channels" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else{
        NSLog(@"temp is getting Null");
    }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

- (IBAction)OK2pressed:(id)sender {
    
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
   // self.renameView.hidden = NO;
   // self.mainTitle.text = @"More";
    self.blockTableView.hidden = YES;
    self.selectionView.hidden = YES;
    self.Cancel2.hidden = YES;
    self.okButton.hidden = YES;
    self.OK1.hidden = YES;
    self.OK2.hidden = YES;
    self.cancel1.hidden = NO;
    self.Cancel2.hidden = YES;
    self.headingLabel.text = @"more";
    self.headingView.hidden = YES;
    
    //    NSDictionary *selectedDict = [[NSDictionary alloc]init];
    
    @try {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/blockTvChannels1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    [selectedBlockArray addObject:@"default"];
    
    NSString *joinedComponentsforChannel= [selectedBlockArray componentsJoinedByString:@","];
    
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&channelIds=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],joinedComponentsforChannel];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSLog(@"%@",joinedComponentsforChannel);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Tv Channels Successfully Blocked" ]) {
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Block" message:@"Blocked Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Block" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToHome];
            }
            else{
                NSLog(@"your profile updated successfully");
                selectedEditArray = [[NSMutableArray alloc]init];
                editIDarray = [[NSMutableArray alloc]init];
                editstatusArray = [[NSMutableArray alloc]init];
                favEditArray = [[NSMutableArray alloc]init];
                [self.favEditTableView reloadData];
            }
            
        }
    }
    else{
        NSLog(@"temp is getting Null");
    }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
