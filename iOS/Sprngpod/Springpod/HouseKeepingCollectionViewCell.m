//
//  HouseKeepingCollectionViewCell.m
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 18/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "HouseKeepingCollectionViewCell.h"

@implementation HouseKeepingCollectionViewCell
-(void)setHighlighted:(BOOL)highlighted
{
    if (highlighted)
    {
        self.layer.opacity = 0.6;
        // Here what do you want.
    }
    else{
        self.layer.opacity = 1.0;
        // Here all change need go back
    }
}


-(void)setSelected:(BOOL)selected
{
    if(selected) {
        
        [self.houseKeepingTitle setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
        
    }
    else {
        [self.houseKeepingTitle setTextColor:[UIColor whiteColor]];
        
    }
}


@end
