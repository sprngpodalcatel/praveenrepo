//
//  MD5Digest.h
//  Sprngpod
//
//  Created by Shrishail Diggi on 27/04/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
