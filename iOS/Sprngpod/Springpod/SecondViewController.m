
//  SecondViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "SecondViewController.h"
#import "RadioTableViewCell.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import "netWork.h"
#import "Flurry.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "LocationUpdate.h"
#import "UrlRequest.h"
#import "LastSeenIndex.h"



@interface SecondViewController ()
{
    NSString *metaString;
    NSString *lastRadioID;
    NSString *fetchRadioLanguage;
    NSMutableDictionary* radioReceivedData;
    NSString *RadioUrlRequest;
    
    
    NSString *lastSeenRadioFilterTypeValue;
    NSString *RadioFilterTypeValue;
    NSString *lastRadioLanguage;
   NSString *lastListenMalyalamRadioChannelId,*lastListenHindiRadioChannelId,*lastListenEnglishRadioChannelId;
    NSIndexPath *selectedIndexpath;
    NSMutableArray *temp, *temp1, *radioChannelNameArray, *radioChannelLogoArray, *radioChannelUrlArray,*backgroundimageArray,*indexArray,*channelIDArray,*lastListenStatusArray,*lastListenMalayalamStatusArray,*lastListenEnglishStatusArray,*lastListenHindiStatusArray,*radiolanguagefilterlistArray,*radioFilterlistArray;
    
    CGRect radioFrame;
    BOOL hindiPressed,malayalamPressed,languageoptionPressed;
    BOOL packageNotFound;
    BOOL tokenExpired;
     BOOL selectedGenreLanguage;
    NSMutableArray *fetchradioChannelLanguageArray;
    NSMutableArray *radioListArray;

    netWork *netReachability;
    NetworkStatus networkStatus;
    UISwipeGestureRecognizer *leftRecogniser;
    UISwipeGestureRecognizer *rightRecognoser;
    UITapGestureRecognizer *tapRecognizer;
    CSToastStyle *style;
    __weak IBOutlet UIActivityIndicatorView *loader;
    
    LocationUpdate *locationRefresh;
    
    NSTimer *playButtonTimer;
    
    UIBackgroundTaskIdentifier *backIdentifier;
}
@property (nonatomic,strong)AVPlayer*player;
@property (nonatomic,strong)AVPlayerViewController *AVPlayerController;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property (nonatomic) int currentValue;
@property (strong, nonatomic) UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIView *radioView;
@property (weak, nonatomic) IBOutlet UITableView *radioTableView;
@property (weak, nonatomic) IBOutlet UINavigationItem *radioNav;
@property (weak, nonatomic) IBOutlet UIButton *malayalamButton;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;
@property (weak, nonatomic) IBOutlet UIButton *hindiButton;
@property (weak, nonatomic) IBOutlet UILabel *metaDataLabel;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIView *playPauseView;

@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;

@property (weak, nonatomic) IBOutlet UIButton *addBannerButton;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentValue = 0;
    
    NSString *selectedFileName = @"";
    [[NSUserDefaults standardUserDefaults] setObject:selectedFileName forKey:@"selectedListName"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionExpiry:) name:@"sessionExpiry" object:nil];
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    temp = [[NSMutableArray alloc]init];
    temp1 = [[NSMutableArray alloc]init];
    radioChannelNameArray = [[NSMutableArray alloc]init];
    radioChannelLogoArray = [[NSMutableArray alloc]init];
    radioChannelUrlArray = [[NSMutableArray alloc]init];
    backgroundimageArray = [[NSMutableArray alloc]init];
    lastListenStatusArray = [[NSMutableArray alloc]init];
    lastListenEnglishStatusArray = [[NSMutableArray alloc]init];
    lastListenHindiStatusArray = [[NSMutableArray alloc]init];
    lastListenMalayalamStatusArray = [[NSMutableArray alloc]init];
    fetchradioChannelLanguageArray = [[NSMutableArray alloc]init];
    
    radioReceivedData = [[NSMutableDictionary alloc]init];
    indexArray = [[NSMutableArray alloc]init];
    lastRadioID = @"";
    NSLog(@"lastRadioID = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"lastRadio"]);
    lastRadioID = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastRadio"];
    lastRadioLanguage = @"";
    NSLog(@"lastRadioLanguage = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"lastListenRadioFilterTypeValue"]);
    lastRadioLanguage = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastSeenRadioFilterTypeValue"];
    
    
    if ([lastRadioLanguage isEqual:[NSNull null]]) {
        lastRadioLanguage = @"Malyalam";
        
    }
    else
    {
        lastRadioLanguage = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastSeenRadioFilterTypeValue"];
    }

    if ([lastRadioLanguage isEqualToString:@"Malyalam"]) {
        [self.languageButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.malayalamButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.hindiButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    }
    else if ([lastRadioLanguage isEqualToString:@"Hindi"])
    {
        [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.malayalamButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.hindiButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    }
    else{
        [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.malayalamButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.hindiButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    }
    lastListenHindiRadioChannelId = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastListenHindiRadioChannelId"];
    lastListenMalyalamRadioChannelId = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastListenMalyalamRadioChannelId"];
    lastListenEnglishRadioChannelId = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastListenEnglishRadioChannelId"];
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    tokenExpired = NO;
    [_playPauseButton setHidden:YES];
    
    self.radioTableView.backgroundView = [UIView new];
    self.radioTableView.backgroundView.backgroundColor = [UIColor clearColor];
    
    radioPlayerController = [[MPMoviePlayerController alloc] init];
    CGRect frame = CGRectMake(0, 0, 1, 1);
    radioPlayerController.view.frame = frame;
    [radioPlayerController setControlStyle:MPMovieControlStyleNone];
    [self.radioView addSubview:radioPlayerController.view];
    
    // **** AVPlayer Intinitialization ***** //
    
     _AVPlayerController = [[AVPlayerViewController alloc] init];
     _AVPlayerController.view.frame = frame;

    self.adBannerView
    .delegate = self;
    //self.adBannerView.adUnitID = @"ca-app-pub-5834410729019986/4549014756";
    self.adBannerView.adUnitID = @"ca-app-pub-9149503938080598/8140410483";
    self.adBannerView.rootViewController = self;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _addBannerButton.hidden = true;
    }
    else{
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &setCategoryErr];
    [[AVAudioSession sharedInstance] setActive: YES error: &activationErr];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    UIBackgroundTaskIdentifier newTaskId = UIBackgroundTaskInvalid;
    newTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    self.tabBarController.tabBar.shadowImage = [UIImage new];
    self.tabBarController.tabBar.translucent = YES;
    self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    
    
//    radioFrame = self.radioView.bounds;
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Radio";
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.radioNav.titleView = titleLabel;
//    [_metaDataLabel bringSubviewToFront:_backImage];
    
//    UILabel *radio
}
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
{
    _adBannerView.hidden = false;
    _addBannerButton.hidden = false;
}

- (void)adView:(GADBannerView* )adView didFailToReceiveAdWithError:(GADRequestError* )error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}
-(void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _addBannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        _addBannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
        
    }
    
}
-(void)adViewWillPresentScreen:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _addBannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        _addBannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self setNavigationTitle:@"Radio"];
    if (packageNotFound == YES) {
        
    }
    
    else if (tokenExpired == YES){
        
    }
    else{
//    NSURL *url = [NSURL URLWithString:[backgroundimageArray objectAtIndex:_currentValue]];
//    NSLog(@"backUrl =  %@",url);
//    [_backImage  sd_setImageWithURL:url];
//    _backgroundView = [[UIImageView alloc]init];
////WithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:url]]];
//    [_backgroundView sd_setImageWithURL:url];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inHomeView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inLiveTVView"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inRadioView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inFavouriteView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inOnDemandView"];
        
        
        
    [self callApiToFetchData];
    
    leftRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFromLeft)];
    leftRecogniser.delegate = self;
    [leftRecogniser setDirection:UISwipeGestureRecognizerDirectionLeft];
    [_playPauseView addGestureRecognizer:leftRecogniser];
    
    rightRecognoser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFromRight)];
    rightRecognoser.delegate = self;
    [rightRecognoser setDirection:UISwipeGestureRecognizerDirectionRight];
    [_playPauseView addGestureRecognizer:rightRecognoser];
        
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRadio:)];
        tapRecognizer.delegate = self;
        tapRecognizer.numberOfTapsRequired = 1;
        [_playPauseView addGestureRecognizer:tapRecognizer];
        [_playPauseView addSubview:self.playPauseButton];
        [_playPauseView bringSubviewToFront:self.playPauseButton];
        [self.playPauseButton setHidden:YES];
    
        locationRefresh = [[LocationUpdate alloc] init];
        [locationRefresh getCurrentLocation];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
        {
            _adBannerView.hidden = true;
            _addBannerButton.hidden = true;
        }
        else{
            _adBannerView.hidden = false;
            _addBannerButton.hidden = false;
            [self.adBannerView loadRequest:[GADRequest request]];
        }
        
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inHomeView"];
//    WithContentURL:[NSURL URLWithString:[ radioChannelUrlArray objectAtIndex:_currentValue]]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StreamMeta:) name:MPMoviePlayerTimedMetadataUpdatedNotification object:nil];
    
    
//    [radioPlayerController play];
    [_radioView bringSubviewToFront:_backImage];
    
    [_metaDataLabel bringSubviewToFront:_backImage];
    [_radioTableView reloadData];
    }
    
    
    [Flurry logEvent:@"Radio_Screen" withParameters:nil];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
}

#pragma mark UsageDetails
-(void)addingRadioUsageDetails
{
    //****** Getting the Add Radio Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *addRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        NSData *addRadioUsagePostData = [addRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addRadioUsagePostData length]];
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addRadioUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                
                NSLog(@"Radio Usage array----%@",radioUsageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@" Radio UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSString *serviceIdRN=[[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIdRN forKey:@"serviceIdRN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}

-(void)updateRadioUsageDetails
{
    //****** Getting the Update Radio Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *updateRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdRN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateRadioUsagePostData = [updateRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateRadioUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateRadioUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",radioUsageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"serviceIdRN"];
    });
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(void)sessionExpiry:(NSNotification *)notification{
 [radioPlayerController stop];
 }


-(void)StreamMeta:(NSNotification*)notification{
    @try {
        
    if ([[radioPlayerController timedMetadata]objectAtIndex:0]) {
        MPTimedMetadata *meta = [[radioPlayerController timedMetadata]objectAtIndex:0];
        if (meta.value) {
            metaString = meta.value;
            NSLog(@"MetaString = %@",metaString);
            self.metaDataLabel.text = metaString;
            
            if (selectedIndexpath != nil){
                [self reloadRowsAtIndexPaths:@[selectedIndexpath] withRowAnimation:UITableViewRowAnimationNone];
            }
            else{
                [self reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        
    }
    else{
        
    }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)tapOnRadio:(id) sender{
    if (self.playPauseButton.hidden) {
        if (radioPlayerController.playbackState == !MPMoviePlaybackStatePlaying) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
          playButtonTimer =  [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        [self.playPauseButton setHidden:NO];
    }
    else{
        if (radioPlayerController.playbackState == !MPMoviePlaybackStatePlaying) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
           playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
    }
}
- (IBAction)addButtonPressed:(id)sender {
    
    _adBannerView.hidden = true;
    _addBannerButton.hidden = true;
    
}




-(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)playPausePressed:(id)sender {
    
    if (radioPlayerController.playbackState == !MPMoviePlaybackStatePlaying) {
        NSString *stringFromArray = [radioChannelUrlArray objectAtIndex:selectedIndexpath.row];
        [radioPlayerController setContentURL:[NSURL URLWithString:stringFromArray]];
        [radioPlayerController play];
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self addingRadioUsageDetails];
        
        playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    }
    
    else{
        [radioPlayerController stop];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];

        
        [playButtonTimer invalidate];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.currentValue) {
        if(indexPath.row == self.currentValue) {
            [cell setSelected:YES animated:YES];
            [cell reloadInputViews];
        }
    }

}

-(void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    if (event.type == UIEventTypeRemoteControl) {
        switch (event.subtype) {
            case UIEventSubtypeRemoteControlPlay:
                case UIEventSubtypeRemoteControlPause:
                case UIEventSubtypeRemoteControlTogglePlayPause:
                if (radioPlayerController.playbackState == MPMoviePlaybackStatePlaying) {
                    [radioPlayerController pause];
                    [[MPMusicPlayerController applicationMusicPlayer]pause];
                    //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
                    [self updateRadioUsageDetails];

                }
            
                else{
                    [radioPlayerController play];
                    [[MPMusicPlayerController applicationMusicPlayer]play];
                    
                    //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
                    [self addingRadioUsageDetails];
                    
                }
            
                break;
            case UIEventSubtypeRemoteControlBeginSeekingForward:
                
            case UIEventSubtypeRemoteControlEndSeekingBackward:
            case UIEventSubtypeRemoteControlEndSeekingForward:
            case UIEventSubtypeRemoteControlPreviousTrack:
                [self handleSwipeFromRight];
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                [self handleSwipeFromLeft];
                radioPlayerController.currentPlaybackTime = 0;
                break;

                
            default:
                break;
        }
    }
}

-(void)goToHome{
    [radioPlayerController stop];
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}

-(void) becomeActive
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _addBannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        _addBannerButton.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
        
    }
    NSLog(@"ACTIVE");
    [_playPauseButton setHidden:YES];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [_player pause];
        [radioPlayerController stop];
        
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inLiveTVView"] == YES)
    {
        [_player pause];
        [radioPlayerController stop];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inRadioView"] == YES)
    {
        [radioPlayerController play];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inFavouriteView"] == YES)
    {
        [_player pause];
        [radioPlayerController stop];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inOnDemandView"] == YES)
    {
        [_player pause];
        [radioPlayerController stop];
    }
  
    else{
        
        [_player pause];
        [radioPlayerController stop];
    }
    
}

- (void) handleSwipeFromRight
{
    int selectedRow = (int)selectedIndexpath.row;
    if (selectedRow == 0) {
        
    }
    else{
        [self tableView:_radioTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndexpath.row - 1 inSection:0]];
    }
    
}

- (void) handleSwipeFromLeft
{
    int selectedRow = (int)selectedIndexpath.row;
    if (selectedRow == radioChannelNameArray.count - 1 ) {
        
    }
    else{
        [self tableView:_radioTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndexpath.row +1 inSection:0]];
    }
}



- (void)viewWillDisappear:(BOOL)animated {
    //BEFORE DOING SO CHECK THAT TIMER MUST NOT BE ALREADY INVALIDATED
    //Always nil your timer after invalidating so that
    //it does not cause crash due to duplicate invalidate
    [radioPlayerController stop];
}



- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    [self.radioTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [radioChannelNameArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RadioSegue" forIndexPath:indexPath];
    NSURL *url = [NSURL URLWithString:radioChannelLogoArray[indexPath.row]];
//    cell.RadioLogo.layer.cornerRadius  = 10;
    cell.RadioLogo.layer.masksToBounds = YES;
    cell.RadioLogo.layer.cornerRadius = 26.0;
    
    cell.backgroundColor = cell.contentView.backgroundColor;
    [cell.RadioLogo sd_setImageWithURL:url];
   // cell.RadioLogo.image = [UIImage imageWithData:[radioChannelLogoArray objectAtIndex:indexPath.row]];
    cell.RadioChannelName.text = radioChannelNameArray[indexPath.row];
    
        cell.metaLabel.text = metaString == nil ? @"" : metaString;
    cell.channelDetailsView.clipsToBounds = true;
    cell.channelDetailsView.layer.cornerRadius = 12;
    
    // Configure the cell...

    [cell.RadioChannelName setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]];
    cell.RadioChannelName.font = [UIFont fontWithName:@"Cabin" size:14];
    
    
    cell.metaLabel.hidden = YES;
    
    if (selectedIndexpath != nil)
    {
        if(indexPath.row == selectedIndexpath.row){
            [cell setSelected:YES animated:YES];
            [cell.RadioChannelName setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
            cell.RadioChannelName.font = [UIFont fontWithName:@"Cabin-Semibold" size:15];
            cell.metaLabel.hidden = NO;
        }
    }
    else if (indexPath.row == 0) {
        [cell setSelected:YES animated:YES];
        [cell.RadioChannelName setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
        cell.metaLabel.hidden = NO;
        cell.RadioChannelName.font = [UIFont fontWithName:@"Cabin-Semibold" size:15];
    }
    
    
    return cell;
}

- (IBAction)homePressed:(id)sender {

    [radioPlayerController stop];
    
    if (radioPlayerController.playbackState == MPMoviePlaybackStatePlaying)
    {
        [radioPlayerController stop];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
        
    }

    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backPressed:(id)sender {
    [radioPlayerController stop];
    
    if (radioPlayerController.playbackState == MPMoviePlaybackStatePlaying)
    {
        [radioPlayerController stop];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
        
    }

    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)malayalamPressed:(id)sender {
    
    _loader.hidden = NO;
    [_loader startAnimating];
    
    
    metaString = nil;
    malayalamPressed = YES;
    languageoptionPressed = NO;
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"malayalamPressed"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"languageoptionPressed"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hindiPressed"];
    
    
    hindiPressed = NO;
    
    lastRadioLanguage = @"malayalam";
    
    [self.malayalamButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.hindiButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [radioPlayerController stop];
    
    if (radioPlayerController.playbackState == MPMoviePlaybackStatePlaying)
    {
        [radioPlayerController stop];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
        
    }
    
    
    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to GTPL to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }
    
    
    else{
        @try {
            
            _loader.hidden = NO;
            [_loader startAnimating];
            
            RadioUrlRequest = [NSString stringWithFormat:@"?specificRadioChannelsList3=%@&language=Malyalam&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchRadioChannelList:) name:RadioUrlRequest object:nil];
            
            [UrlRequest getRequest:RadioUrlRequest view:self.view];
            
            
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    }
    
    NSString *url2 = @"lastListenRadioChannelFullDetails2";
    
    //NSString *RadioFilterType = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastListenRadioFilterType"];
    NSString *RadioFilterType = @"category";
    NSDictionary *dict = [[NSDictionary alloc]init];
    dict = [temp objectAtIndex:0];
    radioListArray = [dict valueForKey:@"radioChannels"];
    for (int i = 0; i < [radioListArray count]; i++)
    {
        NSString *lastSeenStatus =[[radioListArray objectAtIndex:i]objectForKey:@"lastListenStatus"];
        if ([lastSeenStatus isEqualToString:@"true"])
        {
            NSString *lastSeen = @"";
            lastSeen = [channelIDArray objectAtIndex:i];
            [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastRadio"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
    }
    
    
    NSString *post2 = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],RadioFilterType,lastRadioLanguage];
    NSLog(@"post %@",post2);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchLastSeenRadio:) name:url2 object:nil];
    
    [UrlRequest postRequest:url2 arguments:post2 view:self.view];
    
}

- (IBAction)languagePressed:(id)sender {
    
    _loader.hidden = NO;
    [_loader startAnimating];
    
    
    metaString = nil;
    languageoptionPressed = YES;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"malayalamPressed"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"languageoptionPressed"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hindiPressed"];
    
    
    hindiPressed = NO;
    malayalamPressed = NO;
    
    //lastRadioLanguage = @"English";
    
    
    [self.malayalamButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    [self.languageButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.hindiButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    
    
    @try {
        
        NSString *urlReque = [NSString stringWithFormat:@"%@?fetchCustomerRadioChannelLanguages=%@&countryCode=%@&MCC=%@&MNC=%@",[[SprngIPConf getSharedInstance]getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        
        NSURL *url = [NSURL URLWithString:urlReque];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (!(error))
        {
            radioReceivedData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
            radiolanguagefilterlistArray =[[radioReceivedData valueForKey:@"root"]valueForKey:@"LanguageList"];
            NSLog(@"%@", radiolanguagefilterlistArray);
            NSLog(@"%@",radioReceivedData);
        }
        else{
            NSLog(@"%@ Null Error",error);
        }
        
    } @catch (NSException *exception) {
        NSLog(@"%@ exception Error",exception);
    } @finally {
        NSLog(@"");
    }
    
    
    radioFilterlistArray=[[NSMutableArray alloc]init];
    
    [radiolanguagefilterlistArray valueForKey:@"LanguageList"];
    
    for (NSDictionary *dict1 in radiolanguagefilterlistArray)
    {
        [fetchradioChannelLanguageArray addObject:[dict1 objectForKey:@"language"]];
    }
    
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (int i = 0; i < [fetchradioChannelLanguageArray count]; i++)
    {
        [actionSheet  addButtonWithTitle:[fetchradioChannelLanguageArray objectAtIndex:i]];
        NSLog(@"%@",[fetchradioChannelLanguageArray objectAtIndex:i]);
    }
    [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    actionSheet.tag = 1;
    
}

- (IBAction)hindiPressed:(id)sender {
    
    metaString = nil;
    hindiPressed = YES;
    languageoptionPressed = NO;
    malayalamPressed = NO;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"malayalamPressed"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"languageoptionPressed"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hindiPressed"];
    
    lastRadioLanguage = @"Hindi";
    
    [self.malayalamButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.hindiButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];

    
    [radioPlayerController stop];
    
    if (radioPlayerController.playbackState == MPMoviePlaybackStatePlaying)
    {
        [radioPlayerController stop];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
        
    }
    
    
    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to GTPL to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else {
        
        @try {
            
            _loader.hidden = NO;
            [_loader startAnimating];
            
            
            
            RadioUrlRequest = [NSString stringWithFormat:@"?specificRadioChannelsList3=%@&language=Hindi&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchRadioChannelList:) name:RadioUrlRequest object:nil];
            
            [UrlRequest getRequest:RadioUrlRequest view:self.view];
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
    }
    
    NSString *url2 = @"lastListenRadioChannelFullDetails2";
    
    //NSString *RadioFilterType = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastListenRadioFilterType"];
    NSString *RadioFilterType = @"category";
    
    NSDictionary *dict = [[NSDictionary alloc]init];
    dict = [temp objectAtIndex:0];
    radioListArray = [dict valueForKey:@"radioChannels"];
    for (int i = 0; i < [radioListArray count]; i++)
    {
        NSString *lastSeenStatus =[[radioListArray objectAtIndex:i]objectForKey:@"lastListenStatus"];
        if ([lastSeenStatus isEqualToString:@"true"])
        {
            NSString *lastSeen = @"";
            lastSeen = [channelIDArray objectAtIndex:i];
            [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastRadio"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
    
    NSString *post2 = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],RadioFilterType,lastRadioLanguage];
    NSLog(@"post %@",post2);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchLastSeenRadio:) name:url2 object:nil];
    
    [UrlRequest postRequest:url2 arguments:post2 view:self.view];
}


-(void)setNavigationTitle:(NSString *)title
{
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = title;
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    
    self.radioNav.titleView = titleLabel;
}

-(void)fetchCustomerRadioChannelLanguageslist{
    
    @try {
        
        NSString *urlReque = [NSString stringWithFormat:@"%@?fetchCustomerRadioChannelLanguages=%@&countryCode=%@&MCC=%@&MNC=%@",[[SprngIPConf getSharedInstance]getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        
        NSURL *url = [NSURL URLWithString:urlReque];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (!(error))
        {
            radioReceivedData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
            radiolanguagefilterlistArray =[[radioReceivedData valueForKey:@"root"]valueForKey:@"LanguageList"];
            NSLog(@"%@", radiolanguagefilterlistArray);
            NSLog(@"%@",radioReceivedData);
        }
        else{
            NSLog(@"%@ Null Error",error);
        }
        
    } @catch (NSException *exception) {
        NSLog(@"%@ exception Error",exception);
    } @finally {
        NSLog(@"");
    }
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if (indexPath == selectedIndexpath) {
        
    }
    
    else{
    _metaDataLabel.text = @"";

    selectedIndexpath = indexPath;
    metaString = nil;
        [radioPlayerController stop];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    
    NSString *lastSeen = @"";
    lastSeen = [channelIDArray objectAtIndex:indexPath.row];
    [[NSUserDefaults standardUserDefaults] setObject:lastSeen forKey:@"saveLastRadio"];
    NSLog(@"lastSaveRadio =%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"]);
 
    
    self.currentValue = (int)indexPath.row;
    lastRadioID = nil;
//    lastRadioLanguage = nil;
    NSMutableArray *indexPathArr = [[NSMutableArray alloc] init];
    
    NSString *stringFromArray = [radioChannelUrlArray objectAtIndex:indexPath.row];
    NSURL *url = [NSURL URLWithString:[backgroundimageArray objectAtIndex:_currentValue]];
    
    NSLog(@"backUrl =  %@",url);
    [_backImage  sd_setImageWithURL:url];
    
    
    radioPlayerController = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:stringFromArray]];
    [self.radioTableView reloadRowsAtIndexPaths:[self.radioTableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StreamMeta:) name:MPMoviePlayerTimedMetadataUpdatedNotification object:nil];
    [radioPlayerController play];
        
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self addingRadioUsageDetails];
   
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    radioPlayerController.fullscreen = NO;
    [_playPauseButton setHidden:YES];
    
    [indexPathArr addObject:indexPath];
    
        NSString *url2 = @"lastListenRadioChannelFullDetails2";
        
        NSString *RadioFilterType = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastListenRadioFilterType"];
        
        NSString *post2 = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],RadioFilterType,[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedRadioLanguageFilterTypeValue"]];
        NSLog(@"post %@",post2);
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchLastSeenRadio:) name:url2 object:nil];
        
        [UrlRequest postRequest:url2 arguments:post2 view:self.view];
    }
    
                
}

-(void)fetchLastSeenRadio:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]){
            temp = [ [notification.userInfo objectForKey:@"dictionary"]objectForKey:@"root"];
            if (temp) {
                NSDictionary *sDic = temp[0];
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    NSLog(@"Customer Does Not Exist");
                    [self goToHome];
                }
                
                else if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [radioPlayerController stop];
                    [self goToHome];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Listen Radio Channel Details Stored Successfully" ]) {
                    NSLog(@"Last Listen Radio Channel Details Stored Successfully");
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(void) callApiToFetchData {
    @try {
        
        _loader.hidden = NO;
        [_loader startAnimating];
        
        lastSeenRadioFilterTypeValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastSeenRadioFilterTypeValue"];
        NSString *null = @"";
        NSString *All = @"All";
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"languageoptionPressed"] == YES)
        {
            RadioFilterTypeValue = [[NSUserDefaults standardUserDefaults]objectForKey:@"selectedRadioLanguageFilterTypeValue"];
            
            [self.languageButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.malayalamButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            
            [self.hindiButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            
            
        }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"hindiPressed"] == YES)
        {
            RadioFilterTypeValue = @"Hindi";
            
            [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.malayalamButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.hindiButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        }
        else if([[NSUserDefaults standardUserDefaults] boolForKey:@"malayalamPressed"] == YES)
        {
            RadioFilterTypeValue = @"Malyalam";
            
            
            [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.malayalamButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.hindiButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        }
        else
        {
            if ([lastRadioLanguage isEqual:[NSNull null]])
            {
                lastRadioLanguage = @"Malyalam";
                
            }
            else if ([lastSeenRadioFilterTypeValue isEqualToString:null])
            {
                RadioFilterTypeValue = null;
            }
            else if ([lastSeenRadioFilterTypeValue isEqualToString:All])
            {
                RadioFilterTypeValue = All;
                
            }
            else
            {
                //                [self.languageoptionButton setTitleColor:[UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0] forState:UIControlStateNormal];
                //                [self.gujaratButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                //                [self.hindiButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                
                RadioFilterTypeValue = lastSeenRadioFilterTypeValue;
                
            }
        }
        
        if (radioPlayerController.playbackState == MPMoviePlaybackStatePlaying)
        {
            [radioPlayerController stop];
            
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
            
        }
        RadioUrlRequest = [NSString stringWithFormat:@"?specificRadioChannelsList3=%@&language=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],RadioFilterTypeValue,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchRadioChannelList:) name:RadioUrlRequest object:nil];
        
        [UrlRequest getRequest:RadioUrlRequest view:self.view];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(void)fetchRadioChannelList:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
    
    
    temp =[[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            
    if(temp) {
        NSLog(@"temp = %@",temp);
        NSDictionary *dict = [[NSDictionary alloc]init];
        dict = temp[0];
        
        temp1 = [dict objectForKey:@"radioChannels"];
        
        if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
            tokenExpired = YES;
            [self goToHome];
        }
        else if ([[dict objectForKey:@"status"] isEqualToString:@"Package Not Found"]) {
            packageNotFound = YES;
            [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
            
        }
        
        else if ([[dict objectForKey:@"status"] isEqualToString:@"No Data Found"]){
            [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
        else{
            
            NSString *hindiLastListenID;
            NSString *malayalamLastListenID;
            NSString *englishLastListenID;
            NSString *languageLastListen;
            
            
            
            packageNotFound = NO;
            NSLog(@"temp1 = %@", temp1);
            radioChannelNameArray = [[NSMutableArray alloc]init];
            radioChannelLogoArray = [[NSMutableArray alloc]init];
            radioChannelUrlArray = [[NSMutableArray alloc]init];
            backgroundimageArray = [[NSMutableArray alloc]init];
            channelIDArray = [[NSMutableArray alloc] init];
            lastListenStatusArray = [[NSMutableArray alloc]init];
            
            channelIDArray = [[NSMutableArray alloc]init];
            NSLog(@"temp1count:%lu",(unsigned long)temp1.count);
            
            
            for (int i = 0; i < temp1.count; i++) {
                
                NSDictionary *newDict = temp1[i];
                [radioChannelNameArray addObject:[newDict objectForKey:@"channelName"]];
                NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance] getIpForImage]];
                
                imageUrl = [imageUrl stringByAppendingPathComponent:[newDict objectForKey:@"channelLogo"]];
                NSLog(@"imageurl = %@", imageUrl);
                
                [radioChannelLogoArray   addObject:imageUrl];
                NSString *imageUrl1 = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance] getIpForImage]];
                imageUrl1 = [imageUrl1 stringByAppendingPathComponent:[newDict objectForKey:@"backgroundImage"]];
                if ([[newDict objectForKey:@"backgroundImage"] isEqualToString:@""]) {
                    imageUrl1 = [imageUrl1 stringByAppendingString:@"none"];
                }
                [backgroundimageArray addObject:imageUrl1];
                
                [radioChannelUrlArray addObject:[newDict objectForKey:@"streamUrl"]];
                [channelIDArray addObject:[newDict objectForKey:@"id"]];
                [lastListenStatusArray addObject:[newDict objectForKey:@"lastListenStatus"]];
                
                if ([[newDict objectForKey:@"lastListenStatus"] isEqualToString:@"true"])
                {
                    
                    if ([[newDict objectForKey:@"language"] isEqualToString:@"Malyalam"])
                    {
                        malayalamLastListenID = [newDict objectForKey:@"id"];
                        languageLastListen = [newDict objectForKey:@"language"];
                        
                    }else if ([[newDict objectForKey:@"language"] isEqualToString:@"Hindi"])
                    {
                        hindiLastListenID = [newDict objectForKey:@"id"];
                        languageLastListen = [newDict objectForKey:@"language"];
                    }else if ([[newDict objectForKey:@"language"] isEqualToString:@"English"])
                    {
                        englishLastListenID = [newDict objectForKey:@"id"];
                        languageLastListen = [newDict objectForKey:@"language"];
                    }
                    
                }
                
                
                
                
            }
            
            
            
            [_radioTableView reloadData];
            
            self.currentValue = [LastSeenIndex getIndexSelectedChannel:lastListenStatusArray];
            selectedIndexpath = [NSIndexPath indexPathForRow:self.currentValue inSection:0];
            
            NSURL *url = [NSURL URLWithString:[backgroundimageArray objectAtIndex:_currentValue]];
            
            NSLog(@"backUrl =  %@",url);
            [_backImage  sd_setImageWithURL:url];
            
            
            radioPlayerController = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[ radioChannelUrlArray objectAtIndex:selectedIndexpath.row]]];
            [_playPauseButton setHidden:YES];
            
            
            [radioPlayerController play];
            
            if ([languageLastListen  isEqual: @"Malyalam"]) {
                
                //****** Getting the Add Radio Usage Details Of chaneels *****//
//                
                NSString *addRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],malayalamLastListenID,[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
                NSData *addRadioUsagePostData = [addRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSString *addRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addRadioUsagePostData length]];
                
                @try  {
                    NSString *str =[NSString stringWithFormat:@"%@/index.php/addRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
                    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *url = [NSURL URLWithString:str];
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setValue:addRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    [request setHTTPBody:addRadioUsagePostData];
                    
                    NSURLResponse *response;
                    NSError *error;
                    NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    
                    if (!(error))
                    {
                        NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                        NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                        
                        NSLog(@"Radio Usage array----%@",radioUsageArray);
                        
                        //****** Storing the UsageId In NSUserDefaults*******//
                        
                        NSString *usageId = [[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                        NSLog(@" Radio UsageId----%@",usageId);
                        [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        NSString *serviceIdRN=[[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                        [[NSUserDefaults standardUserDefaults]setObject:serviceIdRN forKey:@"serviceIdRN"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                    }
                    else{
                        NSLog(@"%@ Null Error",error);
                    }
                } @catch (NSException *exception) {
                    NSLog(@"%@ Exception Error",exception);
                } @finally {
                    NSLog(@"");
                }
                
            }else if ([languageLastListen  isEqual: @"Hindi"])
            {
                //****** Getting the Add Radio Usage Details Of chaneels *****//
                
                NSString *addRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],hindiLastListenID,[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
                NSData *addRadioUsagePostData = [addRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSString *addRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addRadioUsagePostData length]];
                
                @try  {
                    NSString *str =[NSString stringWithFormat:@"%@/index.php/addRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
                    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *url = [NSURL URLWithString:str];
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setValue:addRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    [request setHTTPBody:addRadioUsagePostData];
                    
                    NSURLResponse *response;
                    NSError *error;
                    NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    
                    if (!(error))
                    {
                        NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                        NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                        
                        NSLog(@"Radio Usage array----%@",radioUsageArray);
                        
                        //****** Storing the UsageId In NSUserDefaults*******//
                        
                        NSString *usageId = [[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                        NSLog(@" Radio UsageId----%@",usageId);
                        [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        NSString *serviceIdRN=[[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                        [[NSUserDefaults standardUserDefaults]setObject:serviceIdRN forKey:@"serviceIdRN"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                    }
                    else{
                        NSLog(@"%@ Null Error",error);
                    }
                } @catch (NSException *exception) {
                    NSLog(@"%@ Exception Error",exception);
                } @finally {
                    NSLog(@"");
                }
                
            }else if ([languageLastListen  isEqual: @"English"])
            {
                //****** Getting the Add Radio Usage Details Of chaneels *****//
                
                NSString *addRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],englishLastListenID,[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
                NSData *addRadioUsagePostData = [addRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSString *addRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addRadioUsagePostData length]];
                
                @try  {
                    NSString *str =[NSString stringWithFormat:@"%@/index.php/addRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
                    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *url = [NSURL URLWithString:str];
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setValue:addRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    [request setHTTPBody:addRadioUsagePostData];
                    
                    NSURLResponse *response;
                    NSError *error;
                    NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    
                    if (!(error))
                    {
                        NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                        NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                        
                        NSLog(@"Radio Usage array----%@",radioUsageArray);
                        
                        //****** Storing the UsageId In NSUserDefaults*******//
                        
                        NSString *usageId = [[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                        NSLog(@" Radio UsageId----%@",usageId);
                        [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        NSString *serviceIdRN=[[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                        [[NSUserDefaults standardUserDefaults]setObject:serviceIdRN forKey:@"serviceIdRN"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                    }
                    else{
                        NSLog(@"%@ Null Error",error);
                    }
                } @catch (NSException *exception) {
                    NSLog(@"%@ Exception Error",exception);
                } @finally {
                    NSLog(@"");
                }
//
            }
            
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            //[self addingRadioUsageDetails];
            
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        packageNotFound = YES;
        
    }
    
            
    }
        
    else{
            packageNotFound = YES;
        }

    }
    @catch(NSException *exception) {
        
    } @finally{
        
    }
    
    [_loader stopAnimating];
    [_loader hidesWhenStopped];

   
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 1){
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            selectedGenreLanguage = true;
            fetchRadioLanguage = [fetchradioChannelLanguageArray objectAtIndex:buttonIndex];
            //selectedradiolanguagefiltertypevalue = fetchRadioLanguage;
            [[NSUserDefaults standardUserDefaults]setObject:fetchRadioLanguage forKey:@"selectedRadioLanguageFilterTypeValue"];
            
            [self callApiToFetchData];
            
            NSString *url2 = @"lastListenRadioChannelFullDetails2";
            
            NSString *RadioFilterType = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastListenRadioFilterType"];
            
            NSString *post2 = [NSString stringWithFormat:@"customerId=%@&channelId=%@&filterType=%@&filterTypeValue=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],RadioFilterType,[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedRadioLanguageFilterTypeValue"]];
            NSLog(@"post %@",post2);
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchLastSeenRadio:) name:url2 object:nil];
            
            [UrlRequest postRequest:url2 arguments:post2 view:self.view];
            
        }else if (buttonIndex == actionSheet.cancelButtonIndex)
        {
            [_loader stopAnimating];
            [_loader hidesWhenStopped];
        }
    }
    [fetchradioChannelLanguageArray removeAllObjects];
}


@end
