//
//  SocialMediaViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 15/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "SocialMediaViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SprngIPConf.h"

@interface SocialMediaViewController ()
{
    NSMutableArray *temp;
    NSString *lastTvChannelID;
    NSString *userName;
    NSString *password;
    UIAlertView *deviceAlert;
    UIAlertView *socialAlert;
    
}

@property (weak, nonatomic) IBOutlet UINavigationBar *socialLogNav;
@property (weak, nonatomic) IBOutlet UITextField *fbUserID;
@property (weak, nonatomic) IBOutlet UITextField *typeTextField;
//@property (weak, nonatomic) IBOutlet UITextField *fbUserID;
@property (weak, nonatomic) IBOutlet UIImageView *fbProfilePic;
@property (weak, nonatomic) IBOutlet UITextField *deviceID;
@property (weak, nonatomic) IBOutlet UITextField *platFormTextField;

@end

@implementation SocialMediaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.socialLogNav setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.socialLogNav.shadowImage = [UIImage new];
    self.socialLogNav.translucent = YES;
    self.socialLogNav.backgroundColor = [UIColor clearColor];

    _fbUserID.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbUserID"];
    FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:_fbProfilePic.bounds];
    NSUUID *UUID = [[UIDevice currentDevice]identifierForVendor];
    NSString *ID;
    ID = [UUID UUIDString];
//    _deviceID.text = ID;
    _deviceID.text = @"231234";
    _typeTextField.text = @"facebook";
    _platFormTextField.text = @"iOS";
//    _platFormTextField.text = [[UIDevice currentDevice]systemName];
    [profilePictureview setPictureMode:FBSDKProfilePictureModeNormal];
    [profilePictureview setProfileID:_facebookId];
    [self.fbProfilePic addSubview:profilePictureview];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)loginPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/socialDeviceLoginCheck",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"loginId=%@&type=facebook&deviceId=%@&platform=%@",_fbUserID.text,_deviceID.text,_platFormTextField.text];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            NSDictionary *sDic = temp[0];
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"]) {
                NSLog(@"Home Screen");
                [self performSegueWithIdentifier:@"socialOutSegue" sender:self];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Does Not Exists" ]) {
                socialAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Social Media Id Does Not Exists" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Register",nil];
                [socialAlert show];
                
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Device Does Not Exists" ]) {
                deviceAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Device Does Not Exists" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Register",nil];
                [deviceAlert show];
                
            }
            else{
                NSLog(@"temp is getting Null");
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self performSegueWithIdentifier:@"socialReg" sender:self];
    }
    
}

@end
