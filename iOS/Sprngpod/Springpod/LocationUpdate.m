//
//  LocationUpdate.m
//  Springpod
//
//  Created by Shrishail Diggi on 26/02/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "LocationUpdate.h"

@implementation LocationUpdate

-(void)getCurrentLocation{
    // To check Authoorization Status for Location fetching...
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        NSLog(@"Message : %@",message);
        
        
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    
    
    //location checking...
    if ([CLLocationManager locationServicesEnabled]) {
        if (!self.locationManager) {
            self.locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [_locationManager startUpdatingLocation];
        }
    }
    //    [_locationManager startUpdatingLocation];
    
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    @try {
        
        currentLocation = [locations objectAtIndex:0];
        [_locationManager stopUpdatingLocation];
        CLGeocoder *geocoder = [[CLGeocoder alloc]init];
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 if (placemarks) {
                     if (placemarks.count >= 1) {
                         
                         CLPlacemark *placemark = [placemarks objectAtIndex:0];
                         
                         if (placemark) {
                             
                             NSLog(@"\nCurrent Location Detected\n");
                             if (placemark.ISOcountryCode && placemark.country ) {
                                 if (![placemark.ISOcountryCode isEqualToString:@""]  ) {
                                     NSString *Country = [[NSString alloc]initWithString:placemark.ISOcountryCode];
                                     [[NSUserDefaults standardUserDefaults] setObject:Country forKey:@"countryCode"];
                                 }
                                 if (![placemark.country isEqualToString:@""]) {
                                     NSString *countryName1 = [[NSString alloc]initWithString:placemark.country];
                                     [[NSUserDefaults standardUserDefaults] setObject:countryName1 forKey:@"countryName"];
                                 }
                                 
                             }
                             
                             if (_locationManager.location.coordinate.latitude && _locationManager.location.coordinate.longitude) {
                                 float lattitudeFromGoe = _locationManager.location.coordinate.latitude;
                                 float longitudeFromGeo = _locationManager.location.coordinate.longitude;
                                 
                                 [self callTimeZoneAPI:lattitudeFromGoe longitude:longitudeFromGeo];
                             }
                             
                             
                             
                         }
                         
                         
                     }
                     
                 }
                 
             }
             else
             {
                 NSLog(@"Geocode failed with error %@", error);
                 NSLog(@"\nCurrent Location Not Detected\n");
                 //return;
                 
             }
             
         }];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    //    });
    
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError: %@", error);
}


// to get Timezone from google API.
-(void)callTimeZoneAPI :(float)lattitude longitude:(float)longitude
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @try {
            NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/timezone/json?location=%f,%f&timestamp=00000&sensor=true",lattitude,longitude]]];
            
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                          returningResponse:&response
                                                                      error:&error];
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                NSInteger status =  [httpResponse statusCode];
                
                NSLog(@"status = %ld",(long)status);
                
                if(dictionary && status == 200) {
                    [[NSUserDefaults standardUserDefaults]setObject:@"Default" forKey:@"timeZone"];
                    if ([[dictionary allKeys] containsObject:@"timeZoneId"]) {
                        if ([dictionary objectForKey:@"timeZoneId"]) {
                            NSString *timeZoneID = [dictionary objectForKey:@"timeZoneId"];
                            [[NSUserDefaults standardUserDefaults]setObject:timeZoneID forKey:@"timeZone"];
                            NSLog (@"TimeZone From API = %@",timeZoneID);
                        }
                        
                    }
                    
                }
                else{
                    NSLog (@"Oh Sorry");
                    
                }
                
            }
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    });
}

@end
