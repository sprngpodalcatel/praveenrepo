//
//  NotificationViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 13/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"

@interface NotificationViewController ()
{
    NSArray *optionsArray;
}

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    optionsArray = [NSArray arrayWithObjects:@"Hello customer,you are now a registered member of SprngPod App",@"Hello Customer, Your new PIN is 1234, you can change the PIN in More Section",@"Your subscribed Package has Expired, Please remew through this Link : www.asianetmobiletv.com/login", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optionsArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell" forIndexPath:indexPath];
    cell.notfLabel.text = [optionsArray objectAtIndex:indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        NSString *passUrl=@"http://www.asianetmobiletv.com/login";
        [[NSUserDefaults standardUserDefaults]  setObject:passUrl forKey:@"MYWEBURL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self performSegueWithIdentifier:@"webView" sender:nil];
    }
}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
