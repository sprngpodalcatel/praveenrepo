//
//  MoreTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 09/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moreLabel;

@end
