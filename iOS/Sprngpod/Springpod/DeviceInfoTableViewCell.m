//
//  DeviceInfoTableViewCell.m
//  Sprngpod
//
//  Created by Shrishail Diggi on 25/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "DeviceInfoTableViewCell.h"

@implementation DeviceInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
