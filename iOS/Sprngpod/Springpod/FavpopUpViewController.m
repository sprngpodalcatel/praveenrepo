//
//  FavpopUpViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 03/02/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "FavpopUpViewController.h"
#import "favpopUpTableViewCell.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "SprngIPConf.h"
#import "UIView+Toast.h"
#import "netWork.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@interface FavpopUpViewController ()
{
    NSMutableArray *temp,*favouriteFolderNameArray,*temp1;
    NSString *selectedListName;
    netWork *netReachability;
    NetworkStatus networkStatus;
    CSToastStyle *style;
}
@property (weak, nonatomic) IBOutlet UITableView *favListTableView;
@property (weak, nonatomic) IBOutlet UIView *FavouriteView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *transparantView;

@end

@implementation FavpopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _cancelButton.tintColor = [UIColor blueColor];
    _transparantView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    
    self.titleLabel.layer.borderWidth = 1;
    self.titleLabel.layer.borderColor = [UIColor grayColor].CGColor;
    self.cancelButton.layer.borderWidth = 1;
    self.cancelButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.favListTableView.layer.borderWidth = 1;
    self.favListTableView.layer.borderColor = [UIColor grayColor].CGColor;
    [self callAPI];
    
    
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return favouriteFolderNameArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    favpopUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favPop" forIndexPath:indexPath];
    cell.favListLabel.text = favouriteFolderNameArray[indexPath.row];
    
    cell.favListLabel.text = favouriteFolderNameArray[indexPath.row];
    
    
    cell.favListLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
     _FavouriteView.backgroundColor = [UIColor clearColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedListName = favouriteFolderNameArray[indexPath.row];
    long listIndex = indexPath.row;
    NSString *listString = [@(listIndex) stringValue];
    [[NSUserDefaults standardUserDefaults] setObject:listString forKey:@"listIndex"];
    [[NSUserDefaults standardUserDefaults] setObject:selectedListName forKey:@"selectedListName"];
    [self.tabBarController setSelectedIndex:2];
    ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
//    UINavigationController *navVC = (UINavigationController *)self.parentVC;
//    ThirdViewController *firstVC = (ThirdViewController *)[navVC.viewControllers lastObject];
  
//    ThirdViewController *firstVC = (ThirdViewController *)[navVC.viewControllers objectAtIndex:0];

    [firstVC reloadtableView];
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    
}
- (IBAction)cancelPressed:(id)sender {
    
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
            
            FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            [firstVC setNavigationTitle:@"Live TV"];
//            [firstVC reloadTableview];
            
        }
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
            
            SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            [firstVC setNavigationTitle:@"Radio"];
            
        }
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
            
            ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            //        [firstVC setNavigationTitle:@"FAVOURITES"];
            [firstVC cancelButtonApi];
            
        }
        
        
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    
    
}

//***** More standardpopup change code*****//

-(void)callAPI
{
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        
        _FavouriteView.hidden = YES;
        
        _transparantView.hidden = YES;
        
     // [self.transparantView removeFromSuperview];
        
        
//        NSString *FavPop = [self.storyboard instantiateViewControllerWithIdentifier:@"FavpopUpViewControllerID"];
        // [self.transparantView removeFromSuperview];
       // [self dismissViewControllerAnimated:YES completion:nil];
        
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
            
            FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            [firstVC setNavigationTitle:@"Live TV"];
            //            [firstVC reloadTableview];
            
        }
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
            
            SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            [firstVC setNavigationTitle:@"Radio"];
            
        }
        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
            
            ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            //        [firstVC setNavigationTitle:@"FAVOURITES"];
            [firstVC cancelButtonApi];
            
        }
        
        
        [self willMoveToParentViewController:nil];
        //[self.view removeFromSuperview];
        [self.transparantView removeFromSuperview];
        [self removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
        
        
        
        
        
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
        
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchFavouriteTvChannelListNames1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
        [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                      returningResponse:&response
                                                                  error:&error];
        
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
        //    NSLog(@"json_dict\n%@",json_dict);
        //    NSLog(@"json_string\n%@",json_string);
        //
        //    NSLog(@"%@",response);
        //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger status =  [httpResponse statusCode];
            temp =[dictionary objectForKey:@"root"];
            //        NSLog(@"status = %ld",(long)status);
            favouriteFolderNameArray = [[NSMutableArray alloc]init];
            
            
            if(temp && status == 200) {
                //            NSLog(@"temp = %@",temp);
                NSDictionary *dict = [[NSDictionary alloc]init];
                dict = temp[0];
                
                
                if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                    
                }
                
                
                else if ([[dict objectForKey:@"status"] isEqualToString:@"Favourite Tv Channel List Does Not Exists"]) {
                    
                    [self.navigationController.view makeToast:@"Go to More to create favoruties list" duration:2.0 position:CSToastPositionCenter style:style];
                    
                }
                else{
                    temp1 = [dict objectForKey:@"listNames"];
                    //            NSLog(@"temp1 = %@", temp1);
                    //            NSLog(@"temp1 Coumt = %lu ",(unsigned long)temp1.count);
                    for (int i = 0; i < temp1.count; i++) {
                        
                        [favouriteFolderNameArray addObject:temp1[i]];
                        
                    }
                    [self.favListTableView reloadData];
                }
            }
            else{
                NSLog (@"Oh Sorry");
                [self.navigationController.view makeToast:@"Check Your UserId and Password" duration:2.0 position:CSToastPositionCenter style:style];
                //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //            [alert show];
                
            }
        }
        if (favouriteFolderNameArray.count == 0) {
            
            [self.navigationController.view makeToast:@"Go to More to create favoruties list" duration:2.0 position:CSToastPositionCenter style:style];
            
            
            //        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
            //
            //            FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            //            [firstVC setNavigationTitle:@"Live TV"];
            //            [firstVC reloadTableview];
            //
            //        }
            //        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
            //
            //            SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            //            [firstVC setNavigationTitle:@"Radio"];
            //
            //        }
            
            //        if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
            //
            //            ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
            //            //        [firstVC setNavigationTitle:@"FAVOURITES"];
            //            [firstVC cancelButtonApi];
            //
            //        }
            //
            //
            //        [self willMoveToParentViewController:nil];
            //        [self.view removeFromSuperview];
            //        [self removeFromParentViewController];
            
            
            //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Go to More to create favoruties list" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //        [alert show];
        }
        
    }
    
    
}

-(void)goToHome{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [self.view endEditing:YES];
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
