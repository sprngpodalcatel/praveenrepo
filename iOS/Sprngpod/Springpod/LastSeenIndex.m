//
//  LastSeenIndex.m
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 22/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "LastSeenIndex.h"

@implementation LastSeenIndex

+(int )getIndexSelectedChannel:(NSArray *)statusArray{
    __block int currentValue;
    [statusArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *status = (NSString *)obj;
        if([status isEqualToString:@"true"]) {
            currentValue = (int)idx;
            *stop = YES;
        } else {
            currentValue = 0;
        }
    }];
    return currentValue;
}

@end
