//
//  AWVViewController.h
//  Asianet Mobile TV Plus
//
//  Created by Pa'One' Nani on 01/12/18.
//  Copyright © 2018 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AWVViewController :UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *WebviewOn;
@property (strong, nonatomic) IBOutlet UIView *WebviewMAin;
- (IBAction)Back:(id)sender;




@end
