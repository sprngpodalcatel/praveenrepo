//
//  SettingsViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsTableViewCell.h"
#import "LoginViewController.h"
#import "Flurry.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"

@interface SettingsViewController ()
{
    NSArray *settingsArray;
    CSToastStyle *style;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *ipaddress = [NSString stringWithFormat:@"IPAddress : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]];
    NSString *countryName = [[NSUserDefaults standardUserDefaults]objectForKey:@"countryName"];
    if (countryName == NULL) {
        countryName = @"default";
        [[NSUserDefaults standardUserDefaults]setObject:countryName forKey:@"countryName"];
    }
    [[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"];
    NSString *countryCode = [NSString stringWithFormat:@"Country name is : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryName"]];
    if ([countryCode isEqualToString:@"Country name is : default"]) {
        countryCode = @"Country Name is : NA";
    }
    
    
    
    if ([ipaddress isEqualToString:@"IPAddress : 0.0.0.0"]) {
        ipaddress = @"IPAddress : NA";
    }
    settingsArray = [NSArray arrayWithObjects:@"Subscription Details",@"Active Device List",ipaddress,countryCode,@"App Version : 1.0.35", nil];
    
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    
    
    [Flurry logEvent:@"Settings_Screen" withParameters:nil];

    // Do any additional setup after loading the view.
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [settingsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell" forIndexPath:indexPath];
    cell.settingsLabel.text = [settingsArray objectAtIndex:indexPath.row];
    if (indexPath.row >= 2) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"subscriptionSegue" sender:self];
    }
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"devicesSegue" sender:self];
    }
    if (indexPath.row == 2) {
        NSString *ipaddress = [[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"];
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]);
        [self.navigationController.view makeToast:ipaddress duration:2.0 position:CSToastPositionCenter style:style];
        
//        [[[UIAlertView alloc]initWithTitle:@"IP Address" message:ipaddress delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    if (indexPath.row == 3) {
        NSString *countryCode = [NSString stringWithFormat:@"Your Present Country Name is : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryName"]];
        if ([countryCode isEqualToString:@"Your Present Country Name is : default"]) {
            countryCode = @"Your Present Country Name is : NA";
        }
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryName"]);
        [self.navigationController.view makeToast:countryCode duration:2.0 position:CSToastPositionCenter style:style];
//        [[[UIAlertView alloc]initWithTitle:@"Geo Location" message:countryCode delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)CancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)okPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation
My
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
