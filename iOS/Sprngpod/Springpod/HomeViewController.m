//
//  HomeViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 18/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "HomeViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Flurry.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "NotificationTableViewCell.h"
#import "SettingsTableViewCell.h"
#import "DeviceSettingsTableViewCell.h"
#import "DeviceInfoTableViewCell.h"
#import "SubscriptionTableViewCell.h"
#import "DefaultScreenTableViewCell.h"
#import "SprngIPConf.h"
#import "netWork.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "FirstViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "MD5Digest.h"
#import "NSString+AES.h"
#import "DeviceUID.h"
#import "LocationUpdate.h"
#import "AWVViewController.h"


@interface HomeViewController ()
{
    NSMutableArray *selectedIndexpathArray;
    NSArray *selectedIndexpathArray2;
    NSMutableArray *temp;
    NSMutableArray *deleteNotificationArray;
    NSMutableArray *deviceInfoArray,*basicInfoArray;
    
    NSString*lastlistenradiofiltertype;
    NSString*lastlistenradiofiltertypeValue;
    
    NSString*lastSeenTVfiltertype;
    NSString*lastSeenTVfiltertypeValue;
    


    netWork *netReachability;
    NetworkStatus networkStatus;
    NSArray *settingsArray,*notificationArray,*deviceInfoStatArray,*deviceModalArray,*deviceIDArray,*platFormArray,*OSVersionArray,*packageArray,*startDateArray,*endDateArray,*defaultScreenArray;
    NSString *lastSeenHindi,*lastSeenMalayalam,*lastSeenEnglish,*deviceID ,*notificationString , *notificationIds;
    NSString *rateString,*IPAddress,*lastSeenTVChannelLogoURL,*lastListenRadioChannelLanguage,*lastSeenTVUrl,*countryName,*promoVideoUrl,*defaultScreen,*lastListenRadioLogoUrl,*lastListenRadioUrl;
    NSString *selectedDefaultScreen;
    NSString *pinString;
    CSToastStyle *style;
    //    float lattitude,longitude;
    NSTimer *playButtonTimer;
    
    NSString *apiGeo;
    NSURL *tvLogoUrl,*promoLogoUrl,*radioLogoUrl;
    
    NSMutableArray *packageName,*startDate,*expiryDate,*deviceList,*DeviceID,*platform,*osVersion,*notifiactionzTitleArray,*notificationMsgArray,*notificationTimeArray,*notificationIdArray;
    NSMutableSet *expandedCells;
    NSIndexPath *defaultScreenIndex;
    
    UIAlertView *alert,*homepinviewalert;
    UIActivityIndicatorView *activityView;
    UIView *loadingView;
    UILabel *loadingLabel;
    
    UITapGestureRecognizer *tapRecogniser;
    LocationUpdate *locationRefresh;
    
    BOOL faqWebView,isMoreDataAvailable,rattingBool;
    int sessionTime,tokenRefreshTime,badgeNumber,from;
    
    NSInteger selectedRowsCount ;
    
    NSIndexPath *selectedIndex, *selectedIndexRow;
    
}

@property (nonatomic,strong)AVPlayer*player;
@property (nonatomic,strong)AVPlayerViewController *AVPlayerController;

@property(strong , nonatomic) NSTimer *loginTimer,*tokenRefreshTimer;

@property (nonatomic, retain) UIActivityIndicatorView * activityView;
@property (nonatomic, retain) UIView *loadingView;
@property (nonatomic, retain) UILabel *loadingLabel;

@property (weak, nonatomic) IBOutlet UIButton *notificatonButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *entertainButton;
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *defaultScreenBackButton;
@property (weak, nonatomic) IBOutlet UIButton *packageBackButton;
@property (weak, nonatomic) IBOutlet UIButton *deviceInfoBackBUtton;
@property (weak, nonatomic) IBOutlet UIButton *notificationBackButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsBackButton;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *OnDemandButton;
@property (weak, nonatomic) IBOutlet UINavigationBar *homeNav;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *logoutBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *entertainImage;
@property (weak, nonatomic) IBOutlet UIImageView *settingsCircle;
@property (weak, nonatomic) IBOutlet UIImageView *notificationCircle;
@property (weak, nonatomic) IBOutlet UIImageView *OnDemandCircle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIButton *star1;
@property (weak, nonatomic) IBOutlet UIButton *star2;
@property (weak, nonatomic) IBOutlet UIButton *star3;
@property (weak, nonatomic) IBOutlet UIButton *star4;
@property (weak, nonatomic) IBOutlet UIButton *star5;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UIImageView *lastSeenTvImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *MenuViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuCenterGapContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuBottomConstraint;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@property (weak, nonatomic) IBOutlet UITableView *notificationTableView;
@property (weak, nonatomic) IBOutlet UITableView *deviceInfoTableView;
@property (weak, nonatomic) IBOutlet UITableView *subscriptionTableView;
@property (weak, nonatomic) IBOutlet UITableView *defaultScreenTableView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIView *notificationBackground;
@property (weak, nonatomic) IBOutlet UIView *confirmViewDefaultScreen;
@property (weak, nonatomic) IBOutlet UIWebView *webViewInHome;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
@property (weak, nonatomic) IBOutlet UITextView *aboutUsPage;
@property (weak, nonatomic) IBOutlet UIButton *aboutUsBackButton;
@property (weak, nonatomic) IBOutlet UIWebView *FAQwebView;
@property (weak, nonatomic) IBOutlet UIImageView *AsianetLogo;
@property (weak, nonatomic) IBOutlet UIWebView *AboutUsWebView;
@property (weak, nonatomic) IBOutlet UIView *playPauseView;
@property (weak, nonatomic) IBOutlet UIView *pinView;
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UIButton *deleteNotificationButton;


@property (weak, nonatomic) IBOutlet UIButton *adButton;

@property (weak, nonatomic) IBOutlet GADBannerView *addBannerView;
@property (weak, nonatomic) IBOutlet UIImageView *topBackground;

@end

@implementation HomeViewController


@synthesize webView;
@synthesize activityView,loadingView,loadingLabel;

#pragma mark View Appearance Delegates

- (void)viewDidLoad {
    [super viewDidLoad];
    
     selectedRowsCount = 1;
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    deviceID = [DeviceUID uid];
    self.badgeLabel.hidden = YES;
    self.Walletbackbutton.hidden=YES;
    // Do any additional setup after loading the view.
//    settingsArray = [NSArray arrayWithObjects:@"Subscription Details",@"Active Device List",@"ipaddress",@"countryCode",@"App Version : 1.0.35", nil];
    [self.titleLabel sizeToFit];
    self.titleLabel.hidden = YES;
    self.titleLabel.text = @"Sprngpod";
    self.AsianetLogo.hidden = NO;
    self.titleImage.hidden = NO;
    self.logoutButton.hidden = NO;
    self.homeButton.hidden = YES;
    self.deleteNotificationButton.hidden = YES;
    self.notificationBackground.hidden = YES;
    self.confirmViewDefaultScreen.hidden = YES;
   // self.pinView.hidden = YES;
    _adButton.hidden = true;
    
    
 settingsArray = [NSArray arrayWithObjects:@"Default Screen",@"My Wallet",@"My Subscription",@"Device Information",@"My Account",@"FAQs",@"About Us", nil];
    notificationArray = [NSArray arrayWithObjects:@"Hello customer,you are now a registered member of SprngPod App",@"Hello Customer, Your new PIN is 1234, you can change the PIN in More Section",@"Your subscribed Package has Expired, Please remew through this Link : www.asianetmobiletv.com/login", nil];
    deviceInfoStatArray = [NSArray arrayWithObjects:@"IP Address",@"Location",@"App version",@"Device List", nil]; 
    deviceModalArray = [NSArray arrayWithObjects:@"9.0",@"9.2",@"9.3", nil];
    deviceIDArray = [NSArray arrayWithObjects:@"123",@"345",@"763", nil];
    platFormArray = [NSArray arrayWithObjects:@"abc",@"def",@"efg", nil];
    OSVersionArray = [NSArray arrayWithObjects:@"9.0",@"9.2",@"9.3", nil];
    packageArray = [NSArray arrayWithObjects:@"TV Package",@"Radio Package", nil];
    startDateArray = [NSArray arrayWithObjects:@"2016/01/01",@"2016/01/01", nil];
    endDateArray = [NSArray arrayWithObjects:@"2017/01/01",@"2016/01/01", nil];
    defaultScreenArray = [NSArray arrayWithObjects:@"Promo",@"Live TV",@"Radio", nil];
    
    deviceInfoArray = [[NSMutableArray alloc]init];
    basicInfoArray = [[NSMutableArray alloc]init];
    temp = [[NSMutableArray alloc]init];
    packageName = [[NSMutableArray alloc]init];
    startDate = [[NSMutableArray alloc]init];
    expiryDate = [[NSMutableArray alloc]init];
    DeviceID = [[NSMutableArray alloc]init];
    deviceList = [[NSMutableArray alloc]init];
    platform = [[NSMutableArray alloc]init];
    osVersion = [[NSMutableArray alloc]init];
    expandedCells = [[NSMutableSet alloc]init];
    notificationMsgArray = [[NSMutableArray alloc]init];
    notificationTimeArray = [[NSMutableArray alloc]init];
    notifiactionzTitleArray = [[NSMutableArray alloc]init];
    notificationIdArray = [[NSMutableArray alloc]init];
    deleteNotificationArray = [[NSMutableArray alloc]init];
    selectedIndexpathArray = [[NSMutableArray alloc]init];
    selectedIndexpathArray2 = [[NSArray alloc]init];
    
    
    self.notificationTableView.hidden = YES;
    self.settingsTableView.hidden = YES;
    self.notificationBackButton.hidden = YES;
    self.settingsBackButton.hidden = YES;
    self.deviceInfoBackBUtton.hidden = YES;
    self.deviceInfoTableView.hidden = YES;
    self.subscriptionTableView.hidden = YES;
    self.packageBackButton.hidden = YES;
    self.defaultScreenTableView.hidden = YES;
    self.defaultScreenBackButton.hidden = YES;
    self.webViewInHome.hidden = YES;
    self.aboutUsPage.hidden = YES;
    self.aboutUsBackButton.hidden = YES;
    self.FAQwebView.hidden = YES;
    self.AboutUsWebView.hidden = YES;
    
    
    [self.homeNav setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.homeNav.shadowImage = [UIImage new];
    self.homeNav.translucent = YES;
    self.homeNav.backgroundColor = [UIColor clearColor];
    self.homeNav.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self setNeedsStatusBarAppearanceUpdate];
    
    [self.logoutBarButton setImage:[[UIImage imageNamed:@"newLogout"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.logoutBarButton setTintColor:[UIColor clearColor]];
    _loader.hidden = YES;
    rateString = @"";
    
    self.ratingView.layer.cornerRadius = 13;
    _ratingView.hidden = YES;
    
    _AVPlayerController =[[AVPlayerViewController alloc]init];
    _AVPlayerController.view.frame = self.backImage.bounds;
    [_AVPlayerController.view bringSubviewToFront:_backImage];
    [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];

    
    if (self.view.frame.size.height == 480) {
    
        self.MenuViewTopConstraint.constant = -10;
        self.menuCenterGapContraint.constant = 10;
        
    }
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    
    
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 85, self.view.frame.size.height/4 - 85, 170, 170)];
    loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    loadingView.clipsToBounds = YES;
    loadingView.layer.cornerRadius = 10.0;
    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.frame = CGRectMake(65, 40, activityView.bounds.size.width, activityView.bounds.size.height);
    [loadingView addSubview:activityView];
    
    loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 115, 130, 22)];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textColor = [UIColor whiteColor];
    loadingLabel.adjustsFontSizeToFitWidth = YES;
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.text = @"Loading...";
    [loadingView addSubview:loadingLabel];
    
    
    [self.webViewInHome addSubview:loadingView];
    [self.FAQwebView addSubview:loadingView];
    [self.AboutUsWebView addSubview:loadingView];
    [activityView startAnimating];
    
    
    //**Google Ads Delegate Methods**//
    
    self.addBannerView.delegate = self;
    self.addBannerView.adUnitID = @"ca-app-pub-9149503938080598/8140410483";
    self.addBannerView.rootViewController = self;
    //[self.addBannerView loadRequest:[GADRequest request]];
   
    // ****** Removing Ads's For Paid Customers****** //
    
   
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources3=%@&countryCode=%@&MCC=%@&MNC=%@&deviceId=%@&appVersion=%@&platform=%@&deviceToken=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],deviceID,kAppVarsion,@"iOS",[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]]]];

    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        temp = [dictionary objectForKey:@"root"];
        NSDictionary *adsDictonary = temp[0];
        NSString *paidCustomer = [adsDictonary objectForKey:@"paidCustomer"];
        [[NSUserDefaults standardUserDefaults] setObject:paidCustomer forKey:@"paidCustomer"];
        
        if (temp) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
            {
                _addBannerView.hidden = true;
                _adButton.hidden = true;
            }
            else
            {
                [self.addBannerView loadRequest:[GADRequest request]];
                _addBannerView.hidden = false;
                _adButton.hidden = false;            }
        }
        //****** Storing the Unique Device Id In NSUserDefaults*******//
        NSString *uniqueDeviceId = [adsDictonary objectForKey:@"uniqueDeviceId"];
        [[NSUserDefaults standardUserDefaults]setObject:uniqueDeviceId forKey:@"uniqueDeviceId"];
        
        //****** Storing the lastSeenTVChannelId as serviceId to use as a parameter in Usage Details******//
        NSString *serviceId =[adsDictonary objectForKey:@"lastSeenTVChannelId"];
        NSLog(@"%@",serviceId);
        @try  {
            if (![serviceId isEqualToString:@"null"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:serviceId forKey:@"serviceId"];
                NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceId"]);
                
            }
            else{
                
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }

        
    }

    tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    tapRecogniser.delegate = self;
    tapRecogniser.numberOfTapsRequired = 1;
    [_playPauseView addGestureRecognizer:tapRecogniser];
    [_playPauseView addSubview:self.playPauseButton];
    [_playPauseView bringSubviewToFront:self.playPauseButton];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.notificationTableView addSubview:refreshControl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNotificationBadge) name:@"ReceivedNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNotificationBadge) name:UIApplicationDidBecomeActiveNotification object:nil];

   
}



// ****** Ads Delegate Methods ****** //

-(void)adViewWillPresentScreen:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _addBannerView.hidden = true;
        _adButton.hidden = true;
        
    }
    else{
        _addBannerView.hidden = false;
        _adButton.hidden = false;
        [self.addBannerView loadRequest:[GADRequest request]];
    }
    
}

- (void)adView:(GADBannerView* )adView didFailToReceiveAdWithError:(GADRequestError* )error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
{
    _addBannerView.hidden = false;
    _adButton.hidden = false;
}
- (IBAction)adButtonPressed:(id)sender {
    
    _addBannerView.hidden = true;
    _adButton.hidden = true;
    
}

-(void)adViewWillLeaveApplication:(GADBannerView *)bannerView{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _addBannerView.hidden = true;
        _adButton.hidden = true;
        
    }
    else{
        _addBannerView.hidden = false;
        _adButton.hidden = false;
        [self.addBannerView loadRequest:[GADRequest request]];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    [super viewDidAppear:YES];
    
    deleteNotificationArray =[[NSMutableArray alloc]init];
    selectedIndexpathArray = [[NSMutableArray alloc]init];
    // Capture flurry event summary
    [Flurry logEvent:@"Home_Screen" withParameters:nil];
    
    self.entertainImage.image = [UIImage imageNamed:@"btn4.png"];
    self.notificationCircle.image = [UIImage imageNamed:@"btn1.png"];
    self.settingsCircle.image = [UIImage imageNamed:@"btn3.png"];
    self.OnDemandCircle.image =[UIImage imageNamed:@"btn2.png"];

    
    _loader.hidden = YES;
    self.ratingView.layer.cornerRadius = 13;
    _ratingView.hidden = YES;
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inHomeView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inLiveTVView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inRadioView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inFavouriteView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inOnDemandView"];
    
    //to draw line on logout view...
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(2.0, 75.0)];
    [linePath addLineToPoint:CGPointMake(226.0, 75.0)];
    [linePath moveToPoint:CGPointMake(114.0, 75.0)];
    [linePath addLineToPoint:CGPointMake(114.0, 112.0)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [linePath CGPath];
    shapeLayer.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer.lineWidth = 1.0;
    shapeLayer.fillColor = [[[UIColor clearColor] colorWithAlphaComponent:0.8] CGColor];

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _addBannerView.hidden = true;
        _adButton.hidden = true;
        
    }
    else{
        _addBannerView.hidden = false;
        _adButton.hidden = false;
        [self.addBannerView loadRequest:[GADRequest request]];
    }

    
    locationRefresh = [[LocationUpdate alloc] init];
    [locationRefresh getCurrentLocation];
    
    [self.ratingView.layer addSublayer:shapeLayer];
//    [self.pinView.layer addSublayer:shapeLayer];
   
    if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying){
        self.lastSeenTvImageView.hidden = YES;
        self.playPauseButton.hidden = YES;
        _AVPlayerController.view.hidden = NO;
    }
    else{
        
 [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        self.lastSeenTvImageView.hidden = NO;
        self.playPauseButton.hidden = NO;
        _AVPlayerController.view.hidden = YES;
        
    [self newHomeScreenAPI];
    self.badgeLabel.layer.cornerRadius = 9;
    if (badgeNumber == 0) {
        [self.badgeLabel setHidden:YES];
        }
    else{
        self.badgeLabel.hidden = NO;
        self.badgeLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long) badgeNumber];
        }
    
    if ([defaultScreen isEqualToString:@"Promo"]) {
        [_lastSeenTvImageView sd_setImageWithURL:promoLogoUrl];
        defaultScreenIndex = [NSIndexPath indexPathForRow:0 inSection:1];
    }
    else if ([defaultScreen isEqualToString:@"LiveTV"])
    {
        [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
        defaultScreenIndex = [NSIndexPath indexPathForRow:1 inSection:1];
    }
    else if ([defaultScreen isEqualToString:@"Radio"])
    {
        [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
        defaultScreenIndex = [NSIndexPath indexPathForRow:2 inSection:1];
    }
    [_defaultScreenTableView reloadData];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromEntertainView"] == NO) {
//        if (_loginTimer) {
//            [_loginTimer invalidate];
//        }
//        else{
//        if (sessionTime < 1) {
//            
//        }
//        else{
//        _loginTimer = [NSTimer scheduledTimerWithTimeInterval:sessionTime target:self selector:@selector(goToRootView) userInfo:nil repeats:NO];
//        }
//        }
    }
 
_AVPlayerController.view.hidden = YES;
    
    if (!IPAddress || [IPAddress isEqualToString:@"0.0.0.0"] || [IPAddress isEqual:nil] || [IPAddress isEqual:[NSNull null]]) {
        IPAddress = @"NA";
    }
    apiGeo = @"NA";
    [[NSUserDefaults standardUserDefaults]setObject:apiGeo forKey:@"apiGeo"];
    
//    countryName = [[NSUserDefaults standardUserDefaults] objectForKey:@"countryName"];
    if (countryName == nil) {
        countryName = @"default";
    }
    basicInfoArray  = [NSMutableArray arrayWithObjects:IPAddress,[[NSUserDefaults standardUserDefaults] objectForKey:@"apiGeo"],kAppVarsion,@"", nil];
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
//    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
//    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DeviceTokenChanged"] == YES) {
        [self updateDeviceToken];
    }
    else{
        
    }
}
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
}

#pragma mark UsageDetails

-(void)addingChannelUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]];
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                //****** Storing the ServiceID In NSUserDefaults*******//
                
                NSString *serviceIDN=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIDN forKey:@"serviceIdN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}

-(void)updateChannelUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}



-(void)addingRadioUsageDetails
{
    //****** Getting the Add Radio Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *addRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        NSData *addRadioUsagePostData = [addRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addRadioUsagePostData length]];
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addRadioUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                
                NSLog(@"Radio Usage array----%@",radioUsageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@" Radio UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSString *serviceIdRN=[[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIdRN forKey:@"serviceIdRN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}

-(void)updateRadioUsageDetails
{
    //****** Getting the Update Radio Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *updateRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdRN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateRadioUsagePostData = [updateRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateRadioUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateRadioUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",radioUsageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"serviceIdRN"];
    });
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark After Active

-(void) becomeActive
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _addBannerView.hidden = true;
        _adButton.hidden = true;
        
    }
    else{
        _addBannerView.hidden = false;
        _adButton.hidden = false;
        [self.addBannerView loadRequest:[GADRequest request]];
    }

    NSLog(@"ACTIVE");

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [_player play];
        
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inLiveTVView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inFavouriteView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inOnDemandView"] == YES)
    {
        [_player pause];
    }
    else{
        
        [_player pause];
        
    }
    
//    self.lastSeenTvImageView.hidden = NO;
//    self.playPauseButton.hidden = NO;
//    _AVPlayerController.view.hidden = YES;
//    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    
}

-(void)timedOut{
    [_player pause];
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark TableView Delegates


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == _deviceInfoTableView) {
        if(indexPath.row < 4) {
            return 60.0;
        } else if (indexPath.row > 3) {
            if ([expandedCells containsObject:indexPath]) {
                return 140.0;
            }
            else{
                return 49.0;
            }
        }
    }
    
    if (tableView == _subscriptionTableView) {
        return 90;
    }
    if (tableView == _notificationTableView) {
        return [self heigtForCellwithString:[notificationMsgArray objectAtIndex:indexPath.row] withFont:[UIFont fontWithName:@"Roboto-Light" size:17]] + 55;
    }
    return 60;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.settingsTableView) {
        return [settingsArray count];
    }
    
    if (tableView == self.notificationTableView) {
        return notificationMsgArray.count;
    }
    
    if (tableView == self.deviceInfoTableView) {
        return deviceInfoStatArray.count + deviceList.count;
    }
    
    if (tableView == self.subscriptionTableView) {
        return 0;
    }
    
    if (tableView == self.defaultScreenTableView) {
        return  defaultScreenArray.count;
    }
    
    else
        return 5;
        
}
-(CGFloat )heigtForCellwithString:(NSString *)stringValue withFont:(UIFont*)font{
    CGSize constraint = CGSizeMake(self.view.frame.size.width-110,9999);
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGRect rect = [stringValue boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    return rect.size.height;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isMoreDataAvailable && indexPath.row == notificationMsgArray.count-1) {
        from += 10;
        [self viewAllAlertsAPI];
    }
}
-(void)Sharebutton{
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"sharecontent",  nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.notificationTableView) {
        
        NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell" forIndexPath:indexPath];
        
        if (cell == nil) {
            cell = [[NotificationTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"notificationCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
           // cell.Sharebutton.hidden=YES;
        }
        cell.tintColor = [UIColor redColor];
        cell.homeNofifLabel.text = [notificationMsgArray objectAtIndex:indexPath.row];
        cell.notifTitle.text = [notifiactionzTitleArray objectAtIndex:indexPath.row];
        if ([[notifiactionzTitleArray objectAtIndex:indexPath.row]isEqualToString:@"Promo"]) {
            cell.Sharebutton.hidden=NO;
            [cell.Sharebutton addTarget:self action:@selector(Sharebutton) forControlEvents:UIControlEventTouchUpInside];

          
        }else{
            cell.Sharebutton.hidden=YES;
        }
      
        cell.notifTime.text = [self timeToString:[notificationTimeArray objectAtIndex:indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ"];
        
        //-------------------------------------------------------------------------------------
        //****** Deletion of Multiple Notifications *******//
//        if([selectedIndexpathArray containsObject:indexPath])
//        {
//            NSLog(@"%lu",(unsigned long)[selectedIndexpathArray count]);
//            for (int i = 0; i < [selectedIndexpathArray count]; i++) {
//                [self.notificationTableView selectRowAtIndexPath:selectedIndexpathArray[i] animated:YES scrollPosition:UITableViewScrollPositionNone];
//                
//                NSLog(@"i----- %d",i);
//                
//                NSLog(@"%@",deleteNotificationArray);
//            }
//        }
        //-------------------------------------------------------------------------------------
        return cell;
    }
    
    if (tableView == self.settingsTableView) {
        self.TVIEW.hidden=true;
        SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell" forIndexPath:indexPath];
        cell.homeSettingsLabel.text = [settingsArray objectAtIndex:indexPath.row];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure25.png"]];
        return cell;
    }
    
    if (tableView == self.subscriptionTableView) {
        SubscriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subscription" forIndexPath:indexPath];
        //        cell.homePackageLabel.text = [packageName objectAtIndex:indexPath.row];
        //        cell.homePageStartDateLabel.text = [startDate objectAtIndex:indexPath.row];
        //        cell.homePageEndDateLabel.text = [expiryDate objectAtIndex:indexPath.row];
        return cell;
    }
    
    if (tableView == self.defaultScreenTableView) {
        DefaultScreenTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"defaultCell" forIndexPath:indexPath];
        cell.defaultLabel.text = [defaultScreenArray objectAtIndex:indexPath.row];
        if (indexPath.row == defaultScreenIndex.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        return cell ;
    }
    
    if (tableView == self.deviceInfoTableView) {
        
        if (indexPath.row < 4) {
            
            DeviceInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeviceInfo1" forIndexPath:indexPath];
            if(cell == nil) {
                cell = [[DeviceInfoTableViewCell alloc] init];
            }
            cell.deviceInfoLabel.text = [deviceInfoStatArray objectAtIndex:indexPath.row];
            if (basicInfoArray.count >0) {
                cell.basicInfo.text = [basicInfoArray objectAtIndex:indexPath.row];
            }
            
            return cell;
            
        }
        //
        if (indexPath.row > 3) {
            DeviceSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceSet" forIndexPath:indexPath];
            if(cell == nil || [cell isKindOfClass:[DeviceInfoTableViewCell class]]) {
                cell = [[DeviceSettingsTableViewCell alloc] init];
            }
            cell.deviceModel.text = [deviceList objectAtIndex:indexPath.row-4];
            cell.deviceID.text = [DeviceID objectAtIndex:indexPath.row-4];
            cell.platform.text = [platform objectAtIndex:indexPath.row-4];
            cell.OSversion.text = [osVersion objectAtIndex:indexPath.row-4];
            [cell.expationButton addTarget:self action:@selector(cellExpantionPressed:) forControlEvents:UIControlEventTouchUpInside];
            return  cell;
            
        }
        
    }
    
    return nil;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (tableView == self.notificationTableView) {
        
        
     
   // }
        
//        //****** Deletion of Multiple Notifications *******//
//        
//        if (selectedRowsCount == 1)
//        {
//            self.homeButton.hidden = YES;
//            self.deleteNotificationButton.hidden = NO;
//            tableView.allowsMultipleSelectionDuringEditing = true;
//            
//        }
//        else
//        {
//            [deleteNotificationArray addObject:notificationMsgArray[indexPath.row]];
//            
//            [selectedIndexpathArray addObject:indexPath];
//            
//            NSLog(@"selectedIndexpathArray %@",selectedIndexpathArray);
//            
//            if (selectedIndexpathArray.count > 0)
//            {
//                self.homeButton.hidden = YES;
//                self.deleteNotificationButton.hidden = NO;
//            }else
//            {
//            }
//            
//        }
        
        
    }
    
    if (tableView == self.defaultScreenTableView) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        
        
        if (indexPath.row == 0) {
            [_player pause];
            defaultScreenIndex = indexPath;
            defaultScreen = @"Promo";
            [_lastSeenTvImageView sd_setImageWithURL:promoLogoUrl];
            selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused)
            {
                _AVPlayerController.view.hidden = NO;
                _player = [AVPlayer playerWithURL:[NSURL URLWithString:promoVideoUrl]];
                
                _AVPlayerController.view.frame = self.backImage.bounds;
                [self addChildViewController:_AVPlayerController];
                [self.backImage addSubview:_AVPlayerController.view];
                _AVPlayerController.showsPlaybackControls = false;
                _AVPlayerController.player = _player;
                
                self.lastSeenTvImageView.hidden = YES;
                self.playPauseButton.hidden = YES;
                
                
                [_player play];
                //****** Getting the Add Channel Usage Details Of chaneels *****//
                //[self addingChannelUsageDetails];
                
            }
            else{
                _AVPlayerController.view.hidden = YES;
                _lastSeenTvImageView.hidden = NO;
                // [_player pause];
            }
            
        }
        
        if (indexPath.row == 1) {
           [_player pause];
            if (lastSeenTVUrl.length < 1) {
                
            }
            else{
                defaultScreenIndex = indexPath;
                defaultScreen = @"LiveTV";
                [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
                selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
                if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused)
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVBlockedStatus"] isEqualToString:@"true"]) {
                       // self.pinView.hidden = NO;
                        
                        homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                      message:@""
                                                                     delegate:self
                                                            cancelButtonTitle:@"Cancel"
                                                            otherButtonTitles:@"Ok",nil];
                        homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
                        
                        UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
                        pinTextField.delegate = self;
                        [pinTextField becomeFirstResponder];
                        pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                        pinTextField.placeholder = @"Enter PIN";
                        [homepinviewalert  show];
                        

                        
                    }
                    
                    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVParentalStatus"] isEqualToString:@"true"]){
                        //self.pinView.hidden = NO;
                        homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                      message:@""
                                                                     delegate:self
                                                            cancelButtonTitle:@"Cancel"
                                                            otherButtonTitles:@"Ok",nil];
                        homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
                        
                        UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
                        pinTextField.delegate = self;
                        [pinTextField becomeFirstResponder];
                        pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                        pinTextField.placeholder = @"Enter PIN";
                        [homepinviewalert  show];
                        

                        
                        
                        self.lastSeenTvImageView.hidden = NO;
                        self.playPauseButton.hidden = NO;
                        _AVPlayerController.view.hidden = YES;
                    }
                    
                    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVCountryAllowed"] isEqualToString:@"false"]){
                        [self.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
                    }
                    
                    else{
                        _AVPlayerController.view.hidden = NO;
                        _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
                        _AVPlayerController.view.frame = self.backImage.bounds;
                        [self addChildViewController:_AVPlayerController];
                        [self.backImage addSubview:_AVPlayerController.view];
                        _AVPlayerController.showsPlaybackControls = false;
                        _AVPlayerController.player = _player;
                        
                        self.lastSeenTvImageView.hidden = YES;
                        self.playPauseButton.hidden = YES;
                        
                        [_player play];
                        
                        //****** Getting the Add Channel Usage Details Of chaneels *****//
                        //[self addingChannelUsageDetails];
                    }
                }
                else{
                    _AVPlayerController.view.hidden = YES;
                    _lastSeenTvImageView.hidden = NO;
                    // [_player pause];
                    
                }
            }
            
        }
        
        if (indexPath.row == 2) {
            [_player pause];
            if (lastListenRadioUrl.length < 1) {
                
            }
            else{
                defaultScreenIndex = indexPath;
                defaultScreen = @"Radio";
                [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
                selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){
                    
                    
                    _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastListenRadioUrl]];
                    
                     self.lastSeenTvImageView.hidden = NO;
                     _AVPlayerController.view.hidden = YES;
                     self.playPauseButton.hidden = YES;
                    
                    [_player play];
                    
                    //****** Getting the Add Channel Usage Details Of chaneels *****//
                    //[self addingChannelUsageDetails];
                }
                else{
                    _AVPlayerController.view.hidden = YES;
                    _lastSeenTvImageView.hidden = NO;
                    
                    
                }
            }
        }
        
        if (indexPath.row == 3) {
            [self.view makeToast:@"Coming Soon..." duration:2.0 position:CSToastPositionCenter style:style];
            
        }
        
        [tableView reloadData];
    }
    
    if (tableView == self.settingsTableView) {
        if (indexPath.row==1) {
            self.titleLabel.text = @"My Wallet";
            self.TVIEW.hidden=false;
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php?walletPoints=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
            
            [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            NSLog(@"Saved Token = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
            NSLog(@"urlRequest : %@",urlRequest);
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                          returningResponse:&response
                                                                      error:&error];
            
            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *json_dict = (NSDictionary *)json_string;
            NSLog(@"json_dict\n%@",json_dict);
            NSLog(@"json_string\n%@",json_string);
            
            NSLog(@"%@",response);
            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
            NSLog(@" error is %@",error);
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                NSInteger status =  [httpResponse statusCode];
                
                NSLog(@"status = %ld",(long)status);
                if(dictionary && status == 200) {
                    NSDictionary *sDisc = [[NSArray alloc]init];
                    sDisc = [dictionary objectForKey:@"root"];
                    NSString * Walletpoints=[sDisc objectForKey:@"walletPoints"];
                    int walletponts=[Walletpoints intValue];
                    
                    NSString *value=[sDisc objectForKey:@"pointValue"];
                    //int ValuePoints=[value intValue];
                    [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Valuepoints"];
         
                  
                    self.Waleetpoins.text=[NSString stringWithFormat:@"%d points",walletponts];
                [[NSUserDefaults standardUserDefaults]setObject:self.Waleetpoins.text forKey:@"Walletpoints"];
                    self.settingsBackButton.hidden=YES;
                    self.Walletbackbutton.hidden=NO;
                
        }
        }
        }
        if (indexPath.row == 3) {
            [self callApiToFetchDeviceInfoData];
            self.titleLabel.text = @"Device Information";
            self.deviceInfoTableView.hidden =NO;
            self.deviceInfoBackBUtton.hidden  =NO;
            self.packageBackButton.hidden=YES;
            _defaultScreenBackButton.hidden=YES;
            _aboutUsBackButton.hidden=YES;
            
            
        }
        
        if (indexPath.row == 2) {
            self.titleLabel.text = @"Subscription Details";
            _defaultScreenBackButton.hidden=YES;
            
            
            
            self.webViewInHome.hidden = NO;
            faqWebView = NO;
            NSString *md5Customer = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
            //            NSString *md5Customer = @"SPAS10000004";
            
            [self.webViewInHome loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/subscription.php/?customerid=%@",md5Customer]]]];
            timer = [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
            [self.webViewInHome addSubview:loadingView];
            [activityView startAnimating];
            //self.subscriptionTableView.hidden = NO;
            self.packageBackButton.hidden = NO;
            
        }
        if (indexPath.row == 5) {
            self.titleLabel.text = @"FAQ";
            self.FAQwebView.hidden = NO;
            faqWebView = YES;
            [self.FAQwebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:kDevelopementFAQUrl]]]];
            timer = [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
            [self.FAQwebView addSubview:loadingView];
            [activityView startAnimating];
            // self.subscriptionTableView.hidden = NO;
            self.packageBackButton.hidden =NO;
            
        }
        if (indexPath.row == 0) {
            _addBannerView.hidden = true;
            _adButton.hidden = true;
            self.titleLabel.text = @"Default Screen";
            self.defaultScreenTableView.hidden = NO;
            self.defaultScreenBackButton.hidden = YES;
            self.confirmViewDefaultScreen.hidden = NO;
            
        }
        
        if (indexPath.row == 6) {
           
            self.titleLabel.text = @"About Us";
            self.aboutUsPage.hidden = NO;
            self.AboutUsWebView.hidden = NO;
            [self.AboutUsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:kDevelopmentAboutUsUrl]]]];
            timer = [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
            [self.AboutUsWebView addSubview:loadingView];
            [activityView startAnimating];
            self.aboutUsBackButton.hidden = NO;
            
            
            
        }
        
        if (indexPath.row == 4){
         
            [_player pause];
            
            if ([defaultScreen  isEqual: @"LiveTV"]) {
                //****** Getting the Update Channel Usage Details Of Chaneels *****//
                [self updateChannelUsageDetails];
            }else if ([defaultScreen  isEqual: @"Radio"])
            {
                //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
                [self updateRadioUsageDetails];
            }

            
            NSString *md5Customer = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
 
            NSString *passUrl= [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/account.php/?customerid=%@",md5Customer];
            [[NSUserDefaults standardUserDefaults]  setObject:passUrl forKey:@"MYWEBURL"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self performSegueWithIdentifier:@"webView" sender:nil];
        }
    }
    
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.defaultScreenTableView) {
        if (indexPath.row == defaultScreenIndex.row) {
            UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
            selectedCell.accessoryType = UITableViewCellAccessoryNone;
        }
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        selectedCell.accessoryType = UITableViewCellAccessoryNone;
        
        
    }
    
//    if (tableView == self.notificationTableView) {
//        
//        //****** Deletion of Multiple Notifications *******//
//        
//        [deleteNotificationArray removeObject:notificationMsgArray[indexPath.row]];
//        [selectedIndexpathArray removeObject:indexPath];
//        
//        if (selectedIndexpathArray.count == 0) {
//            self.homeButton.hidden = NO;
//            self.deleteNotificationButton.hidden = YES;
//        }else
//        {
//            
//        }
//    }
    
    
}



//****** Deletion of Multiple Notifications *******//

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (tableView == self.notificationTableView) {
//        // return UITableViewCellEditingStyleDelete;
//        
//        //****** Deletion of Multiple Notifications *******//
//        return 3;
//    }
//    return UITableViewCellEditingStyleNone;
//}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    notificationString=[notificationIdArray objectAtIndex:indexPath.row];
//    notificationIds = notificationString;

//    if (tableView == self.notificationTableView) {
//        
//        // ****** Deleting Notifications ***** //
//        
//        if (!(editingStyle == UITableViewCellEditingStyleDelete))
//        {
//            
//            [notificationMsgArray removeObjectAtIndex:indexPath.row];
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            
//            
//            NSString *notificationPost = [NSString stringWithFormat:@"customerId=%@&notificationIds=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"],notificationIds];
//            NSData *notificationPostData = [notificationPost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//            NSString *notificationPostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[notificationPostData length]];
//            @try  {
//                NSString *str =[NSString stringWithFormat:@"%@/index.php/deleteNotification",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
//                str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                NSURL *url = [NSURL URLWithString:str];
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//                [request setHTTPMethod:@"POST"];
//                [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
//                [request setValue:notificationPostLength forHTTPHeaderField:@"Content-Length"];
//                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//                [request setHTTPBody:notificationPostData];
//                
//                NSURLResponse *response;
//                NSError *error;
//                NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//                
//                if (!(error))
//                {
//                    NSDictionary *notificationDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
//                    NSMutableArray *notificationResultArray =[notificationDic valueForKey:@"root"];
//                    
//                    NSLog(@"Notification Result----> %@",notificationResultArray);
//                    [self.view makeToast:@"Notification Deleted Sucessfully" duration:2.0 position:CSToastPositionBottom style:style];
//                    
//                }
//                else{
//                    NSLog(@"%@ Null Error",error);
//                }
//            } @catch (NSException *exception) {
//                NSLog(@"%@ Exception Error",exception);
//            } @finally {
//                NSLog(@"");
//            }
//            
//            [tableView reloadData];
//        }
//        
//    }
    
    
//}


#pragma mark HomeScreen Actions

- (IBAction)cellExpantionPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    CGRect buttonFrame = [button convertRect:button.bounds toView:_deviceInfoTableView];
    NSIndexPath *indexpath = [_deviceInfoTableView indexPathForRowAtPoint:buttonFrame.origin];
    if ([expandedCells containsObject:indexpath]) {
        [expandedCells removeObject:indexpath];
    }
    else{
        
        [expandedCells addObject:indexpath];
        
    }
    
    [_deviceInfoTableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationNone];
}


- (IBAction)notificationBackButton:(id)sender {
    self.titleLabel.text = @"Sprngpod";
    self.AsianetLogo.hidden = NO;
    self.notificationBackground.hidden = YES;
    self.titleLabel.hidden = YES;
    self.titleImage.hidden = NO;
    self.logoutButton.hidden = NO;
    self.homeButton.hidden = YES;
    self.deleteNotificationButton.hidden = YES;
    self.notificationTableView.hidden = YES;
    self.notificationBackButton.hidden = YES;
    self.notificationCircle.image = [UIImage imageNamed:@"btn1.png"];
    self.settingsCircle.image = [UIImage imageNamed:@"btn3.png"];
     self.OnDemandCircle.image =[UIImage imageNamed:@"btn2.png"];
    
    [self.notificationTableView setEditing:NO animated:YES];
    [deleteNotificationArray removeAllObjects];
    [selectedIndexpathArray removeAllObjects];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _addBannerView.hidden = true;
        _adButton.hidden = true;
        
    }
    else{
        _addBannerView.hidden = false;
        _adButton.hidden = false;
        [self.addBannerView loadRequest:[GADRequest request]];
    }
    
    [self refreshNotificationBadge];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    from = 1;
    isMoreDataAvailable = true;
    notificationMsgArray = [[NSMutableArray alloc]init];
    notificationTimeArray = [[NSMutableArray alloc]init];
    notifiactionzTitleArray = [[NSMutableArray alloc]init];
     notificationIdArray = [[NSMutableArray alloc]init];
    [self viewAllAlertsAPI];
    [refreshControl endRefreshing];
}

- (IBAction)settingBackButton:(id)sender {
     _topBackground.image =[UIImage imageNamed:@"homeTop.png"];
    self.AsianetLogo.hidden = NO;
    self.titleLabel.text = @"Sprngpod";
    self.titleLabel.hidden = YES;
    self.titleImage.hidden = NO;
    self.logoutButton.hidden = NO;
    self.homeButton.hidden = YES;
//    self.webViewInHome.hidden =YES;
  //  self.deleteNotificationButton.hidden = YES;
    //self.deviceInfoTableView.hidden = YES;
    //self.defaultScreenTableView.hidden = YES;
   // self.FAQwebView.hidden =YES;
    //self.confirmViewDefaultScreen.hidden=YES;
   // self.TVIEW.hidden=YES;

    self.settingsTableView.hidden =YES;
    //self.subscriptionTableView.hidden=YES;
    
    self.settingsBackButton.hidden = YES;
    
    self.notificationCircle.image = [UIImage imageNamed:@"btn1.png"];
    self.settingsCircle.image = [UIImage imageNamed:@"btn3.png"];
    self.OnDemandCircle.image =[UIImage imageNamed:@"btn2.png"];
    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
    
}

-(void)loading{
    if (faqWebView) {
        if (!_FAQwebView.loading) {
            [loadingView removeFromSuperview];
            [activityView stopAnimating];
        }
        else{
            [self.webViewInHome addSubview:loadingView];
            [activityView startAnimating];
        }
    }
    else{
        if(!_webViewInHome.loading)
        {   [loadingView removeFromSuperview];
            [activityView stopAnimating];
        }
        else{
            [self.webViewInHome addSubview:loadingView];
            [activityView startAnimating];
        }
    }
}

- (IBAction)deviceInfoBackButton:(id)sender {
    self.titleLabel.text = @"Settings";
    
    self.deviceInfoTableView.hidden = YES;
    self.deviceInfoBackBUtton.hidden = YES;
    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
}

- (IBAction)aboutUsBackPressed:(id)sender {
    //self.aboutUsBackButton.hidden = NO;
    self.titleLabel.text = @"Settings";
    self.aboutUsBackButton.hidden = YES;
    self.aboutUsPage.hidden = YES;
    self.AboutUsWebView.hidden = YES;
    // self.aboutUsBackButton.hidden = YES;
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
}


- (IBAction)packageBackButton:(id)sender {
    if (faqWebView) {
        if ([self.FAQwebView canGoBack]) {
            [_FAQwebView goBack];
        }
        else{
            self.titleLabel.text = @"Settings";
            self.subscriptionTableView.hidden = YES;
            self.FAQwebView.hidden = YES;
            self.packageBackButton.hidden = YES;
        }
    }
    
    else{
//        if ([self.webViewInHome canGoBack]) {
//            [_webViewInHome goBack];
//        }
//        else{
            self.titleLabel.text = @"Settings";
            self.subscriptionTableView.hidden = YES;
            self.webViewInHome.hidden = YES;
             [_webViewInHome stopLoading];
             [self.webViewInHome endEditing:YES];
             self.packageBackButton.hidden = YES;
        
    }
    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
    
}

- (IBAction)defaultScreenBackButtonPressed:(id)sender {
    self.titleLabel.text = @"Settings";
    self.defaultScreenTableView.hidden = YES;
    self.defaultScreenBackButton.hidden = YES;
    self.confirmViewDefaultScreen.hidden = YES;
    
    _addBannerView.hidden = false;
    _adButton.hidden = false;


    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
}

-(void)callApiToFetchDeviceInfoData{
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?devicesList1=%@",[[SprngIPConf getSharedInstance]getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        temp =[dictionary objectForKey:@"root"];
        NSLog(@"status = %ld",(long)status);
        deviceList = [[NSMutableArray alloc]init];
        deviceIDArray = [[NSMutableArray alloc]init];
        platform = [[NSMutableArray alloc]init];
        
        
        if(temp && status == 200) {
            NSLog(@"temp = %@",temp);
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = temp[0];
            if ([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"]) {
                [deviceList addObject:@"No Device Found"];
                [DeviceID addObject:@"No Device Found"];
                [platform addObject:@"No Device Found"];
                [osVersion addObject:@"No Device Found"];
            }
            
           else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
               [self goToRootView];
                
            }
           else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
           else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
               [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
               [self goToRootView];
           }
            
            else
            {
                for (int i = 0; i < temp.count; i++) {
                    dict = temp[i];
                    [deviceList addObject:[dict objectForKey:@"deviceModel"]];
                    [DeviceID addObject:[dict objectForKey:@"deviceId"]];
                    [platform addObject:[dict objectForKey:@"platform"]];
                    [osVersion addObject:[dict objectForKey:@"osVersion"]];
                    [self.deviceInfoTableView reloadData];
                }
            }
        }
        
    }
    else{
        NSLog (@"Oh Sorry");
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        
    }
    
}

- (IBAction)onDemandPressed:(id)sender {
    
    [_player pause];
    self.OnDemandCircle.image =[UIImage imageNamed:@"btn2.png"];
    [self performSegueWithIdentifier:@"AWE" sender:self];
    //[self.navigationController pushViewController:detailViewController animated:YES];

}





- (IBAction)entertainPressed:(id)sender {
    if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying){
        [_player pause];

    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
}
    self.entertainImage.image = [UIImage imageNamed:@"btn4.png"];
    [self performSegueWithIdentifier:@"homeSegue" sender:self];
}
- (IBAction)notificationTouchDown:(id)sender {
    //self.notificationCircle.image = [UIImage imageNamed:@"notification_highlightWZ.png"];
}
- (IBAction)settingsTouchDown:(id)sender {
    //self.settingsCircle.image = [UIImage imageNamed:@"setting_highlightWZ.png"];
}


- (IBAction)entertainReleased:(id)sender {
    [_player pause];
    
    [self performSegueWithIdentifier:@"homeSegue" sender:self];
}
- (IBAction)rate1:(id)sender {
    rateString = @"1";
    rattingBool = false;
    NSLog(@"rate : %@",rateString);
    [[NSUserDefaults standardUserDefaults]setObject:rateString forKey:@"rating"];
    [_star1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star2 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    [_star3 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    [_star4 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    [_star5 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    
}
- (IBAction)rate2:(id)sender {
    rateString = @"2";
    rattingBool = false;
    NSLog(@"rate : %@",rateString);
    [[NSUserDefaults standardUserDefaults]setObject:rateString forKey:@"rating"];
    [_star1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star2 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star3 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    [_star4 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    [_star5 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
}

- (IBAction)rate3:(id)sender {
    rateString = @"3";
    rattingBool = true;
    if (rattingBool == true)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We are Honored !"
                                                        message:@"Please share with friends."
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK",nil];
        
        [alert show];

        // [_rattingCancelLBL setTitle:@"Share" forState:UIControlStateNormal];
        
    }
    
    
    
     NSLog(@"rate : %@",rateString);
    [[NSUserDefaults standardUserDefaults]setObject:rateString forKey:@"rating"];
    [_star1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star2 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star3 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star4 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    [_star5 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
}

- (IBAction)rate4:(id)sender {
    rateString = @"4";
    rattingBool = true;
    if (rattingBool == true) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We are Honored !"
                                                        message:@"Please share with friends."
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK",nil];
     
        [alert show];
       
        
    }
    
    
    
     NSLog(@"rate : %@",rateString);
    [[NSUserDefaults standardUserDefaults]setObject:rateString forKey:@"rating"];
    [_star1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star2 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star3 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star4 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star5 setImage:[UIImage imageNamed:@"emptyStar1.png"] forState:UIControlStateNormal];
    
}

- (IBAction)rate5:(id)sender {
    rateString = @"5";
    rattingBool = true;
    if (rattingBool == true) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We are Honored !"
                                                        message:@"Please share with friends."
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK",nil];
   
        [alert show];
        
        }
    
    
    
     NSLog(@"rate : %@",rateString);
    [[NSUserDefaults standardUserDefaults]setObject:rateString forKey:@"rating"];
    [_star1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star2 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star3 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star4 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [_star5 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (rattingBool == true) {
        if(buttonIndex == 0){
            if ([rateString isEqualToString:@""]) {
                
                [self.view makeToast:@"Select atleast one star" duration:2.0 position:CSToastPositionCenter style:style];
                
                //        [[[UIAlertView alloc]initWithTitle:@"Rate Us" message:@"No Star Selection Please do select a star" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else
            {
                if(_tokenRefreshTimer){
                    [_tokenRefreshTimer invalidate];
                    _tokenRefreshTimer = nil;
                }
                AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
                [appDel.window setRootViewController:rootView];
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
                
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/rateApp",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&rating=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]];
                    NSLog(@"post %@",post);
                    
                    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                    [request setHTTPBody:postData];
                    
                    NSURLResponse *response;
                    NSError *error = nil;
                    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSDictionary *json_dict = (NSDictionary *)json_string;
                    NSLog(@"json_dict\n%@",json_dict);
                    NSLog(@"json_string\n%@",json_string);
                    
                    NSLog(@"%@",response);
                    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                    NSLog(@" error is %@",error);
                    
                    if (!error) {
                        NSError *myError = nil;
                        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                        NSLog(@"%@",dictionary);
                        temp = [dictionary objectForKey:@"root"];
                        
                        if (temp) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                NSDictionary *sDic = temp[0];
                                
                                if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                                    [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                                    
                                }
                                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                                    [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                    [self goToRootView];
                                }
                                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                    NSLog(@"Customer Does Not Exist");
                                }
                                else if ([[sDic objectForKey:@"status"]isEqualToString:@"App Rated Successfully" ]) {
                                    NSLog(@"rating = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]);
                                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                                    //                    [[[UIAlertView alloc]initWithTitle:@"Thank You" message:@"We appreciate your feedback" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                                    
                                }
                                
                                else{
                                    NSLog(@"temp is getting Null");
                                }
                            });
                            
                        }
                        
                        
                        else
                        {
                            NSLog(@"%@",error);
                        }
                        
                    }
                    else{
                        
                    }
                    
                    
                });
                
            }
            
            
        }
        else if (buttonIndex == 1) {
            
            self.ratingView.hidden = true;
            NSString *text = @"Hey, Please check this new app i.e Asianet Mobile TV Plus, its FREE to download from Play store or iTune";
            NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/asianet-mobile-tv-plus/id1115146668"];
            UIImage *image = [UIImage imageNamed:@"asianetPlus.png"];
            
            
            UIActivityViewController *controller =
            [[UIActivityViewController alloc]
             initWithActivityItems:@[text,url,image]
             applicationActivities:nil];
            
            [controller setCompletionHandler:^(NSString *act, BOOL done)
             {
                 NSLog(@"act type %@",act);
                 //     NSString *ServiceMsg = nil;
                 //             if ( [act isEqualToString:UIActivityTypeMail] )ServiceMsg = @"Mail sent";
                 //             if ( [act isEqualToString:UIActivityTypeMessage] )ServiceMsg = @"SMS sended!";
                 //             if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"Post on facebook, ok!";
                 
                 if ( done )
                 {
                     if(_tokenRefreshTimer){
                         [_tokenRefreshTimer invalidate];
                         _tokenRefreshTimer = nil;
                     }
                     AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                     LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
                     [appDel.window setRootViewController:rootView];
                     
                     [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                     [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
                     
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         
                         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/rateApp",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                         
                         NSString *post = [NSString stringWithFormat:@"customerId=%@&rating=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]];
                         NSLog(@"post %@",post);
                         
                         NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                         
                         NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                         [request setHTTPMethod:@"POST"];
                         [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                         [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                         [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                         [request setHTTPBody:postData];
                         
                         NSURLResponse *response;
                         NSError *error = nil;
                         NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                         NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                         NSDictionary *json_dict = (NSDictionary *)json_string;
                         NSLog(@"json_dict\n%@",json_dict);
                         NSLog(@"json_string\n%@",json_string);
                         
                         NSLog(@"%@",response);
                         NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                         NSLog(@" error is %@",error);
                         
                         if (!error) {
                             NSError *myError = nil;
                             NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                             NSLog(@"%@",dictionary);
                             temp = [dictionary objectForKey:@"root"];
                             
                             if (temp) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     
                                     NSDictionary *sDic = temp[0];
                                     
                                     if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                                         [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                                         
                                     }
                                     else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                                         [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                         [self goToRootView];
                                     }
                                     else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                         NSLog(@"Customer Does Not Exist");
                                     }
                                     else if ([[sDic objectForKey:@"status"]isEqualToString:@"App Rated Successfully" ]) {
                                         NSLog(@"rating = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]);
                                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                                         //                    [[[UIAlertView alloc]initWithTitle:@"Thank You" message:@"We appreciate your feedback" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                                         
                                     }
                                     
                                     else{
                                         NSLog(@"temp is getting Null");
                                     }
                                 });
                                 
                             }
                             
                             
                             else
                             {
                                 NSLog(@"%@",error);
                             }
                             
                         }
                         else{
                             
                         }
                         
                         
                     });
                     
                     
                 }
                 else
                 {
                     
                     //                 UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sharing cancelled", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                     //                 [av show];
                     
                     if ([rateString isEqualToString:@""]) {
                         
                         [self.view makeToast:@"Select atleast one star" duration:2.0 position:CSToastPositionCenter style:style];
                         
                         //        [[[UIAlertView alloc]initWithTitle:@"Rate Us" message:@"No Star Selection Please do select a star" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                     }
                     else
                     {
                         if(_tokenRefreshTimer){
                             [_tokenRefreshTimer invalidate];
                             _tokenRefreshTimer = nil;
                         }
                         AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                         LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
                         [appDel.window setRootViewController:rootView];
                         
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                             
                             NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/rateApp",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                             
                             NSString *post = [NSString stringWithFormat:@"customerId=%@&rating=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]];
                             NSLog(@"post %@",post);
                             
                             NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                             
                             NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                             [request setHTTPMethod:@"POST"];
                             [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                             [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                             [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                             [request setHTTPBody:postData];
                             
                             NSURLResponse *response;
                             NSError *error = nil;
                             NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                             NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                             NSDictionary *json_dict = (NSDictionary *)json_string;
                             NSLog(@"json_dict\n%@",json_dict);
                             NSLog(@"json_string\n%@",json_string);
                             
                             NSLog(@"%@",response);
                             NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                             NSLog(@" error is %@",error);
                             
                             if (!error) {
                                 NSError *myError = nil;
                                 NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                                 NSLog(@"%@",dictionary);
                                 temp = [dictionary objectForKey:@"root"];
                                 
                                 if (temp) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         
                                         NSDictionary *sDic = temp[0];
                                         
                                         if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                                             [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                                             [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                                             [self goToRootView];
                                         }
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                             NSLog(@"Customer Does Not Exist");
                                         }
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"App Rated Successfully" ]) {
                                             NSLog(@"rating = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]);
                                             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                                             //                    [[[UIAlertView alloc]initWithTitle:@"Thank You" message:@"We appreciate your feedback" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                                             
                                         }
                                         
                                         else{
                                             NSLog(@"temp is getting Null");
                                         }
                                     });
                                     
                                 }
                                 
                                 
                                 else
                                 {
                                     NSLog(@"%@",error);
                                 }
                                 
                             }
                             else{
                                 
                             }
                             
                             
                         });
                         
                     }
                     
                     
                     // didn't succeed.
                 }
             }];
            [self presentViewController:controller animated:YES completion:nil];
            //    
        }
    }
    
    if (alertView == homepinviewalert) {
        if (buttonIndex == 0) {
            
                        [self.view endEditing:YES];
                        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
                        [self.playPauseButton setHidden:NO];
                        _AVPlayerController.view.hidden = YES;
                        _lastSeenTvImageView.hidden = NO;
            

            
        }else{
            
            @try {
                [self.view endEditing:YES];
                [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                pinString = [homepinviewalert textFieldAtIndex:0].text;
                // pinString = @"";
                // pinString = self.pinTextField.text;
                self.pinTextField.text = @"";
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/verifyPIN1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                
                NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                //        NSLog(@"post %@",post);
                
                NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                [request setHTTPBody:postData];
                
                NSURLResponse *response;
                NSError *error = nil;
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                //        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                //        NSDictionary *json_dict = (NSDictionary *)json_string;
                //        NSLog(@"json_dict\n%@",json_dict);
                //        NSLog(@"json_string\n%@",json_string);
                
                //        NSLog(@"%@",response);
                //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                NSLog(@" error is %@",error);
                
                if (!error) {
                    NSError *myError = nil;
                    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                    //            NSLog(@"%@",dictionary);
                    temp = [dictionary objectForKey:@"root"];
                    if (temp) {
                        NSDictionary *sDic = temp[0];
                        if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                            [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                            // self.pinView.hidden = NO;
                            homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                          message:@""
                                                                         delegate:self
                                                                cancelButtonTitle:@"Cancel"
                                                                otherButtonTitles:@"Ok",nil];
                            homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
                            
                            UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
                            pinTextField.delegate = self;
                            [pinTextField becomeFirstResponder];
                            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                            pinTextField.placeholder = @"Enter PIN";
                            [homepinviewalert  show];
                            self.lastSeenTvImageView.hidden = NO;
                            self.playPauseButton.hidden = NO;
                            _AVPlayerController.view.hidden = YES;
                            [self goToRootView];
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                            [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            // self.pinView.hidden = NO;
                            homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                          message:@""
                                                                         delegate:self
                                                                cancelButtonTitle:@"Cancel"
                                                                otherButtonTitles:@"Ok",nil];
                            homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
                            
                            UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
                            pinTextField.delegate = self;
                            [pinTextField becomeFirstResponder];
                            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                            pinTextField.placeholder = @"Enter PIN";
                            [homepinviewalert  show];
                            self.lastSeenTvImageView.hidden = NO;
                            self.playPauseButton.hidden = NO;
                            _AVPlayerController.view.hidden = YES;
                            [self goToRootView];
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                            [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToRootView];
                        }
                        
                        
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                            
                            [self.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                            // self.pinView.hidden = NO;
                            homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                          message:@""
                                                                         delegate:self
                                                                cancelButtonTitle:@"Cancel"
                                                                otherButtonTitles:@"Ok",nil];
                            homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
                            
                            UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
                            pinTextField.delegate = self;
                            [pinTextField becomeFirstResponder];
                            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                            pinTextField.placeholder = @"Enter PIN";
                            [homepinviewalert  show];
                            self.lastSeenTvImageView.hidden = NO;
                            self.playPauseButton.hidden = NO;
                            _AVPlayerController.view.hidden = YES;
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                            
                            //  [_player pause];
                            
                            self.lastSeenTvImageView.hidden = YES;
                            self.playPauseButton.hidden = YES;
                            _AVPlayerController.view.hidden = NO;
                            
                            //[_player play];
                            //****** Getting the Add Channel Usage Details Of chaneels *****//
                            //[self addingChannelUsageDetails];
                            
                            [self addChildViewController:_AVPlayerController];
                            [self.backImage addSubview:_AVPlayerController.view];
                            
                            _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
                            
                            _AVPlayerController.view.frame = self.backImage.bounds;
                            _AVPlayerController.player = _player;
                            [self.backImage addSubview:_AVPlayerController.view];
                            _AVPlayerController.showsPlaybackControls = false;
                            
                            [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
                            [_AVPlayerController.view bringSubviewToFront:_backImage];
                            
                            
                            [_player play];
                            
                            //****** Getting the Add Channel Usage Details Of chaneels *****//
                            //[self addingChannelUsageDetails];
                            
                        }
                        
                        
                        
                        
                        else{
                            NSLog(@"temp is getting Null");
                        }
                        
                        
                    }
                    else
                    {
                        NSLog(@"%@",error);
                    }
                    
                }
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }

            
            
        }
    }

}




- (IBAction)cancelRating:(id)sender {
    [_player pause];
    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
    
    if(_tokenRefreshTimer){
        [_tokenRefreshTimer invalidate];
        _tokenRefreshTimer = nil;
    }
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"thanksView"];
    self.ratingView.hidden = YES;
    
}

- (IBAction)ratingOKPressed:(id)sender {
    [_player pause];
    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }
    if ([rateString isEqualToString:@""]) {
        
        [self.view makeToast:@"Select atleast one star" duration:2.0 position:CSToastPositionCenter style:style];
        
    }
    else
    {
        if(_tokenRefreshTimer){
            [_tokenRefreshTimer invalidate];
            _tokenRefreshTimer = nil;
        }
        AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
        [appDel.window setRootViewController:rootView];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];

        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/rateApp",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
        
        NSString *post = [NSString stringWithFormat:@"customerId=%@&rating=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]];
        NSLog(@"post %@",post);
        
        NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setHTTPBody:postData];
        
        NSURLResponse *response;
        NSError *error = nil;
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *json_dict = (NSDictionary *)json_string;
        NSLog(@"json_dict\n%@",json_dict);
        NSLog(@"json_string\n%@",json_string);
        
        NSLog(@"%@",response);
        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
        NSLog(@" error is %@",error);
        
        if (!error) {
            NSError *myError = nil;
            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            NSLog(@"%@",dictionary);
            temp = [dictionary objectForKey:@"root"];
            
            if (temp) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                
                NSDictionary *sDic = temp[0];
                
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                    [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToRootView];
                    
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                    [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToRootView];
                }
               else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                   [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                   [self goToRootView];
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"App Rated Successfully" ]) {
                    NSLog(@"rating = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"rating"]);
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"thanksView"];
                    
                }
                
                else{
                    NSLog(@"temp is getting Null");
                }
                });
                
            }
            
            
            else
            {
                NSLog(@"%@",error);
            }
            
        }
        else{
            
        }
        
        
            });
        
    }
}

//- (IBAction)deleteNotificationPressed:(UIButton*)sender {
//    
//    // ****** Deleting Multiple Notifications ***** //
//    
//    selectedRowsCount = 0;
//    sender.selected = !sender.selected;
//    
//    [self.notificationTableView setEditing:sender.selected animated:YES];
//    
//    if (deleteNotificationArray.count)
//    {
//        
//        for (int i = 0; i<deleteNotificationArray.count ; i++)
//        {
//            selectedIndexRow =selectedIndexpathArray[i];
//            
//            [notificationMsgArray removeObjectAtIndex:selectedIndexRow.row];
//            
//            notificationString=[notificationIdArray objectAtIndex:selectedIndexRow.row];
//            
//            notificationIds = notificationString;
//            
//            [_notificationTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndexRow] withRowAnimation:UITableViewRowAnimationLeft];
//            
//            NSString *notificationPost = [NSString stringWithFormat:@"customerId=%@&notificationIds=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"],notificationIds];
//            NSData *notificationPostData = [notificationPost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//            NSString *notificationPostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[notificationPostData length]];
//            @try  {
//                NSString *str =[NSString stringWithFormat:@"%@/index.php/deleteNotification",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
//                str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                NSURL *url = [NSURL URLWithString:str];
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//                [request setHTTPMethod:@"POST"];
//                [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
//                [request setValue:notificationPostLength forHTTPHeaderField:@"Content-Length"];
//                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//                [request setHTTPBody:notificationPostData];
//                
//                NSURLResponse *response;
//                NSError *error;
//                NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//                
//                if (!(error))
//                {
//                    NSDictionary *notificationDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
//                    NSMutableArray *notificationResultArray =[notificationDic valueForKey:@"root"];
//                    
//                    NSLog(@"Notification Result----> %@",notificationResultArray);
//                    [self.view makeToast:@"Notification Deleted Sucessfully" duration:2.0 position:CSToastPositionBottom style:style];
//                    
//                }
//                else{
//                    NSLog(@"%@ Null Error",error);
//                }
//            } @catch (NSException *exception) {
//                NSLog(@"%@ Exception Error",exception);
//            } @finally {
//                NSLog(@"");
//            }
//            
//        }
//        [deleteNotificationArray removeAllObjects];
//        [selectedIndexpathArray removeAllObjects];
//        [self viewAllAlertsAPI];
//        [_notificationTableView reloadData];
//        
//        if (selectedIndexpathArray.count == 0) {
//            self.homeButton.hidden = NO;
//            self.deleteNotificationButton.hidden = YES;
//        }else
//        {
//            
//        }
//    }
//    
//}

- (IBAction)homePressed:(id)sender {
    self.AsianetLogo.hidden = NO;
    self.titleLabel.hidden = YES;
    self.titleLabel.text = @"Sprngpod";
    self.titleImage.hidden = NO;
    self.logoutButton.hidden = NO;
    self.homeButton.hidden = YES;
    
    self.deleteNotificationButton.hidden = YES;
    
    self.entertainImage.image = [UIImage imageNamed:@"btn4.png"];
    self.notificationCircle.image = [UIImage imageNamed:@"btn1.png"];
    self.settingsCircle.image = [UIImage imageNamed:@"btn3.png"];
    self.OnDemandCircle.image =[UIImage imageNamed:@"btn2.png"];
    
    self.confirmViewDefaultScreen.hidden = YES;
    self.notificationTableView.hidden = YES;
    self.settingsTableView.hidden = YES;
    self.notificationBackButton.hidden = YES;
    self.notificationBackground.hidden = YES;
    self.settingsBackButton.hidden = YES;
    self.deviceInfoBackBUtton.hidden = YES;
    self.deviceInfoTableView.hidden = YES;
    self.subscriptionTableView.hidden = YES;
    self.packageBackButton.hidden = YES;
    self.defaultScreenTableView.hidden = YES;
    self.defaultScreenBackButton.hidden = YES;
    self.webViewInHome.hidden = YES;
    self.aboutUsPage.hidden = YES;
    self.aboutUsBackButton.hidden = YES;
    self.FAQwebView.hidden = YES;
    self.AboutUsWebView.hidden = YES;
    
    if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying){
        [_player pause];
    
        if ([defaultScreen  isEqual: @"LiveTV"]) {
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }else if ([defaultScreen  isEqual: @"Radio"]){
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
        }
    }

}


- (IBAction)logotpressed:(id)sender {
    
    _loader.hidden = NO;
    _ratingView.hidden = NO;
    self.settingsButton.userInteractionEnabled = NO;
    self.entertainButton.userInteractionEnabled = NO;
    self.notificatonButton.userInteractionEnabled = NO;
    [self.loader startAnimating];
    FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
    FBSDKAccessToken.currentAccessToken = nil;
    [manager logOut];
    
    [[GIDSignIn sharedInstance] signOut];
    
    NSLog(@"lastHindi : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastHindi"]);
    NSLog(@"lastEnglish : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"lastSeenEnglish"]);
    NSLog(@"lastmalayalam : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"lastSeenMalayalam"]);
    
    lastSeenMalayalam = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastSeenMalayalam"];
    lastSeenHindi = [[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastHindi"];
    lastSeenEnglish = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastSeenEnglish"];
    
    if (lastSeenEnglish == NULL) {
        lastSeenEnglish = @"default";
    }
    if (lastSeenHindi == NULL) {
        lastSeenHindi = @"default";
    }
    if (lastSeenMalayalam == NULL) {
        lastSeenMalayalam = @"default";
    }
    NSLog(@"%@",lastSeenMalayalam);
    NSLog(@"%@",lastSeenHindi);
    NSLog(@"%@",lastSeenEnglish);
    
    
    if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying){
        [_player pause];
    
        
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }
    
}


//API to call needed data at HomeScreen.

-(void)homeScreenApi
{
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
    
    
    
     NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources1=%@&countryCode=%@&MCC=%@&MNC=%@&deviceId=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],deviceID]]];
    
    
    
    
    
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"]) {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            else{
                
                IPAddress = [dict objectForKey:@"ipAddress"];
                apiGeo = [dict objectForKey:@"countryName"];
                lastSeenTVUrl = [dict objectForKey:@"lastSeenTVChannelURL"];
                NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance]getIpForImage]];
                lastSeenTVChannelLogoURL =[dict objectForKey:@"lastSeenTVChannelLogo"];
                NSString *imageUrl4 = [imageUrl stringByAppendingString:lastSeenTVChannelLogoURL];
                tvLogoUrl = [NSURL URLWithString:imageUrl4];
                //                    [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
                lastListenRadioChannelLanguage = [dict objectForKey:@"lastListenRadioChannelLanguage"];
                lastListenRadioLogoUrl = [dict objectForKey:@"lastListenRadioChannelLogo"];
                NSString *imageUrl1 = [imageUrl stringByAppendingString:lastListenRadioLogoUrl];
                radioLogoUrl = [NSURL URLWithString:imageUrl1];
                //                        [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
                lastListenRadioUrl = [dict objectForKey:@"lastListenRadioChannelURL"];
                NSString *imageUrl2 = [dict objectForKey:@"promoLogo"];
                NSString *promoLogo = [imageUrl stringByAppendingString:imageUrl2];
                promoLogoUrl = [NSURL URLWithString:promoLogo];
                promoVideoUrl = [dict objectForKey:@"promoVideoURL"];
                defaultScreen = [dict objectForKey:@"settings"];
                NSNumber *badgNumber = [dict objectForKey:@"badge"];
                badgeNumber = [badgNumber intValue];
                
                NSNumber *number = [dict objectForKey:@"sessionTimeOut"];
                sessionTime = [number intValue];
                NSNumber *refreshTime = [dict objectForKey:@"tokenRefreshTime"];
                tokenRefreshTime = [refreshTime intValue];
                
                NSString *lastSeenTVBlockedStatus = [dict objectForKey:@"lastSeenTVBlockedStatus"];
                NSString *lastSeenTVCountryAllowed = [dict objectForKey:@"lastSeenTVCountryAllowed"];
                NSString *lastSeenTVParentalStatus = [dict objectForKey:@"lastSeenTVParentalStatus"];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVBlockedStatus forKey:@"lastSeenTVBlockedStatus"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVCountryAllowed forKey:@"lastSeenTVCountryAllowed"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVParentalStatus forKey:@"lastSeenTVParentalStatus"];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastListenRadioChannelLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults] setObject:apiGeo forKey:@"apiGeo"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [_loader stopAnimating];
                [_loader hidesWhenStopped];
                
                
                [_tokenRefreshTimer invalidate];
                if (tokenRefreshTime < 1) {
                    
                }
                else{
                    _tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:tokenRefreshTime target:self selector:@selector(RefreshToken) userInfo:nil repeats:YES];
                }
            }
            
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            
        }
        
    }
    
}

-(void)refreshNotificationBadge{
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchCustomerBadge=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            dispatch_async(dispatch_get_main_queue(), ^{
                
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"]) {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
//                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else{
                NSNumber *badgNumber = [dict objectForKey:@"badge"];
                badgeNumber = [badgNumber intValue];
                
                if (badgeNumber == 0) {
                    [self.badgeLabel setHidden:YES];
                }
                else{
                    self.badgeLabel.hidden = NO;
                    self.badgeLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long) badgeNumber];
                }
                
            }
            });
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [alert show];
            
        }
        
    }
    });
    
}

-(void)newHomeScreenAPI{
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources3=%@&countryCode=%@&MCC=%@&MNC=%@&deviceId=%@&appVersion=%@&platform=%@&deviceToken=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],deviceID,kAppVarsion,@"iOS",[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]]]];
    
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"Saved Token = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"]) {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else{
                
                IPAddress = [dict objectForKey:@"ipAddress"];
                apiGeo = [dict objectForKey:@"countryName"];
                lastSeenTVUrl = [dict objectForKey:@"lastSeenTVChannelURL"];
                NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance]getIpForImage]];
                lastSeenTVChannelLogoURL =[dict objectForKey:@"lastSeenTVChannelLogo"];
                NSString *imageUrl4 = [imageUrl stringByAppendingString:lastSeenTVChannelLogoURL];
                tvLogoUrl = [NSURL URLWithString:imageUrl4];
                
                //                    [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
                
                lastlistenradiofiltertype = [dict valueForKey:@"lastListenRadioFilterType"];
                
                lastlistenradiofiltertypeValue = [dict valueForKey:@"lastListenRadioFilterTypeValue"];
                
                
                lastSeenTVfiltertype = [dict valueForKey:@"lastSeenTVFilterType"];
                lastSeenTVfiltertypeValue = [dict valueForKey:@"lastSeenTVFilterTypeValue"];
                
                lastListenRadioChannelLanguage = [dict objectForKey:@"lastListenRadioChannelLanguage"];
                lastListenRadioLogoUrl = [dict objectForKey:@"lastListenRadioChannelLogo"];
                NSString *imageUrl1 = [imageUrl stringByAppendingString:lastListenRadioLogoUrl];
                radioLogoUrl = [NSURL URLWithString:imageUrl1];
                //                        [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
                lastListenRadioUrl = [dict objectForKey:@"lastListenRadioChannelURL"];
                NSString *imageUrl2 = [dict objectForKey:@"promoLogo"];
                NSString *promoLogo = [imageUrl stringByAppendingString:imageUrl2];
                promoLogoUrl = [NSURL URLWithString:promoLogo];
                promoVideoUrl = [dict objectForKey:@"promoVideoURL"];
                defaultScreen = [dict objectForKey:@"settings"];
                NSNumber *badgNumber = [dict objectForKey:@"badge"];
                badgeNumber = [badgNumber intValue];
                
                NSNumber *number = [dict objectForKey:@"sessionTimeOut"];
                sessionTime = [number intValue];
                NSNumber *refreshTime = [dict objectForKey:@"tokenRefreshTime"];
                tokenRefreshTime = [refreshTime intValue];
                
                NSString *lastSeenTVBlockedStatus = [dict objectForKey:@"lastSeenTVBlockedStatus"];
                NSString *lastSeenTVCountryAllowed = [dict objectForKey:@"lastSeenTVCountryAllowed"];
                NSString *lastSeenTVParentalStatus = [dict objectForKey:@"lastSeenTVParentalStatus"];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:lastSeenTVfiltertype forKey:@"lastSeenTVFilterType"];
                
                [[NSUserDefaults standardUserDefaults]setObject:lastSeenTVfiltertypeValue forKey:@"lastSeenTVFilterTypeValue"];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:lastlistenradiofiltertype forKey:@"lastSeenRadioFilterType"];
                
                [[NSUserDefaults standardUserDefaults]setObject:lastlistenradiofiltertypeValue forKey:@"lastSeenRadioFilterTypeValue"];
                

                                
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVBlockedStatus forKey:@"lastSeenTVBlockedStatus"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVCountryAllowed forKey:@"lastSeenTVCountryAllowed"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVParentalStatus forKey:@"lastSeenTVParentalStatus"];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastListenRadioChannelLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults] setObject:apiGeo forKey:@"apiGeo"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [_tokenRefreshTimer invalidate];
                if (tokenRefreshTime < 1) {
                    
                }
                else{
                    _tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:tokenRefreshTime target:self selector:@selector(RefreshToken) userInfo:nil repeats:YES];
                }
            }
            
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [alert show];
            
        }
        
    }
    
}



- (IBAction)settingsPressed:(id)sender {
    self.AsianetLogo.hidden = YES;
    self.settingsCircle.image = [UIImage imageNamed:@"grayCircle.png"];
     _topBackground.image =[UIImage imageNamed:@"settingsTop.png"];
    
    //    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchCountry=%@&MCC=%@&MNC=%@&customerId=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults] objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchCountry1=%@&MCC=%@&MNC=%@&customerId=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults] objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
                
            }
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else{
                IPAddress = [dict objectForKey:@"ipAddress"];
                apiGeo = [dict objectForKey:@"countryName"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults] setObject:apiGeo forKey:@"apiGeo"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
    }
    [_locationManager startUpdatingLocation];
    if (countryName == nil) {
        countryName = @"default";
    }
    basicInfoArray  = [NSMutableArray arrayWithObjects:IPAddress,[[NSUserDefaults standardUserDefaults] objectForKey:@"apiGeo"],kAppVarsion,@"", nil];
    
    //    [moviePlayerController.moviePlayer stop];
    //    [self performSegueWithIdentifier:@"settingsSegue" sender:self];
    self.titleLabel.text = @"Settings";
    self.titleImage.hidden = YES;
    self.logoutButton.hidden = YES;
    self.homeButton.hidden = NO;
    self.deleteNotificationButton.hidden = YES;
    self.titleLabel.hidden = NO;
    self.settingsTableView.hidden = NO;
    self.settingsBackButton.hidden = NO;
    self.notificationBackButton.hidden = YES;
    self.confirmViewDefaultScreen.hidden = YES;
    self.notificationBackground.hidden = YES;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _addBannerView.hidden = true;
        _adButton.hidden = true;
    }
    else{
        self.addBannerView.hidden = false;
        self.adButton.hidden = false;
        
        [self.addBannerView loadRequest:[GADRequest request]];
    }
    
    
}
- (IBAction)notificationPressed:(id)sender {
    
    //    [self performSegueWithIdentifier:@"notification" sender:self];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    self.AsianetLogo.hidden = YES;
    self.notificationCircle.image = [UIImage imageNamed:@"grayCircle.png"];
    self.titleLabel.text = @"Notifications";
    self.notificationBackground.hidden = NO;
    self.titleImage.hidden = YES;
    self.logoutButton.hidden = YES;
    self.titleLabel.hidden = NO;
    self.homeButton.hidden = NO;
    self.deleteNotificationButton.hidden = YES;
    self.notificationTableView.hidden = NO;
    self.notificationBackButton.hidden = NO;
    self.settingsBackButton.hidden = YES;
    _addBannerView.hidden = true;
    _adButton.hidden = true;
    
    from = 1;
    isMoreDataAvailable = true;
    notificationMsgArray = [[NSMutableArray alloc]init];
    notificationTimeArray = [[NSMutableArray alloc]init];
    notifiactionzTitleArray = [[NSMutableArray alloc]init];
    notificationIdArray = [[NSMutableArray alloc]init];
    
    [self.notificationTableView setEditing:NO animated:YES];
    [deleteNotificationArray removeAllObjects];
    [selectedIndexpathArray removeAllObjects];
    
    selectedRowsCount = 1;
    [self viewAllAlertsAPI];
}

- (IBAction)homeTVplayPressed:(id)sender {

    if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){


        self.lastSeenTvImageView.hidden = YES;
        self.playPauseButton.hidden = YES;
        
        
        _AVPlayerController.view.hidden = NO;
        
//        [self addChildViewController:_AVPlayerController];
//        _AVPlayerController.view.frame = self.backImage.bounds;
//        _AVPlayerController.player = _player;
//        [self.backImage addSubview:_AVPlayerController.view];
//        _AVPlayerController.showsPlaybackControls = false;
        
        [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
        [_AVPlayerController.view bringSubviewToFront:_backImage];


        if ([defaultScreen isEqualToString:@"Promo"]) {
            
            _player = [AVPlayer playerWithURL:[NSURL URLWithString:promoVideoUrl]];
            _AVPlayerController.view.frame = self.backImage.bounds;
            [self addChildViewController:_AVPlayerController];
            [self.backImage addSubview:_AVPlayerController.view];
            _AVPlayerController.player = _player;
            
            
            self.lastSeenTvImageView.hidden = YES;
            self.playPauseButton.hidden = YES;
            _AVPlayerController.view.hidden = NO;
            _AVPlayerController.showsPlaybackControls = false;
            
            [_player play];
            
            //****** Getting the Add Channel Usage Details Of chaneels *****//
            //[self addingChannelUsageDetails];
            
        }
    else if ([defaultScreen isEqualToString:@"LiveTV"]){
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVBlockedStatus"] isEqualToString:@"true"]) {
            //self.pinView.hidden = NO;
            
            homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                          message:@""
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Ok",nil];
            homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
            _pinTextField.delegate = self;
            UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
            [pinTextField becomeFirstResponder];
            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
            pinTextField.placeholder = @"Enter PIN";
            // pinviewAlert.view.tintColor = UIColor.orangeColor()
            // [[UIView appearance] setTintColor:[UIColor redColor]];
            [homepinviewalert show];
            
            self.lastSeenTvImageView.hidden = NO;
            self.playPauseButton.hidden = NO;
            _AVPlayerController.view.hidden = YES;
            
            
            self.lastSeenTvImageView.hidden = NO;
            self.playPauseButton.hidden = NO;
            _AVPlayerController.view.hidden = YES;
//             moviePlayerController.moviePlayer.repeatMode = MPMovieRepeatModeNone;
        }
        
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVParentalStatus"] isEqualToString:@"true"]){
           // self.pinView.hidden = NO;
            
            homepinviewalert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                          message:@""
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Ok",nil];
            homepinviewalert.alertViewStyle = UIAlertViewStylePlainTextInput;
            _pinTextField.delegate = self;
            UITextField *pinTextField = [homepinviewalert textFieldAtIndex:0];
            [pinTextField becomeFirstResponder];
            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
            pinTextField.placeholder = @"Enter PIN";
            // pinviewAlert.view.tintColor = UIColor.orangeColor()
            // [[UIView appearance] setTintColor:[UIColor redColor]];
            [homepinviewalert show];
            
            self.lastSeenTvImageView.hidden = NO;
            self.playPauseButton.hidden = NO;
            _AVPlayerController.view.hidden = YES;
            
            self.lastSeenTvImageView.hidden = NO;
            self.playPauseButton.hidden = NO;
            _AVPlayerController.view.hidden = YES;
        }
        
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVCountryAllowed"] isEqualToString:@"false"]){
            [self.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
        
        else{
            
            _AVPlayerController.view.hidden = NO;
            _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
            _AVPlayerController.view.frame = self.playPauseView.bounds;
            [self addChildViewController:_AVPlayerController];
            [self.backImage addSubview:_AVPlayerController.view];
            _AVPlayerController.player = _player;
            _AVPlayerController.showsPlaybackControls = false;
            [_player play];
            
            //****** Getting the Add Channel Usage Details Of chaneels *****//
            //[self addingChannelUsageDetails];
        }
    }
    
    else if ([defaultScreen isEqualToString:@"Radio"]){
        
        _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastListenRadioUrl]];
        
        _AVPlayerController.view.frame = self.playPauseView.bounds;
        [self addChildViewController:_AVPlayerController];
        [self.backImage addSubview:_AVPlayerController.view];
        _AVPlayerController.player = _player;
        
        self.playPauseButton.hidden = YES;
        self.lastSeenTvImageView.hidden = NO;
        _AVPlayerController.view.hidden = YES;
        [_player play];
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        //[self addingChannelUsageDetails];
    }
        playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    }
    
    else{
        [_player pause];
        
        [playButtonTimer invalidate];
        
        if ([defaultScreen  isEqual: @"LiveTV"]) {
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }else if ([defaultScreen  isEqual: @"Radio"])
        {
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
        }
         [playButtonTimer invalidate];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
        _AVPlayerController.view.hidden = YES;
        _lastSeenTvImageView.hidden = NO;
    }
    
}

-(void)tapOnView:(id) sender{
    if (_playPauseButton.hidden) {
        if (_AVPlayerController.player.timeControlStatus ==! AVPlayerTimeControlStatusPlaying) {

            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
           playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        _playPauseButton.hidden = NO;
        
    }
    else{
        
        if (_AVPlayerController.player.timeControlStatus ==! AVPlayerTimeControlStatusPlaying) {

            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
           playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
    }
}

-(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
//    [self homePressed:self];
}


- (IBAction)tappedOnMenuView:(id)sender {
    [_loader stopAnimating];
    _loader.hidden = YES;
    self.settingsButton.userInteractionEnabled = YES;
    self.entertainButton.userInteractionEnabled = YES;
    self.notificatonButton.userInteractionEnabled = YES;
    [self.ratingView setHidden:YES];

}

-(void)RefreshToken{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/refreshCustomerToken",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&deviceId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],deviceID];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            NSDictionary *sDic = temp[0];
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
                
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else{
                NSString *token = [sDic objectForKey:@"token"];
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}

-(void)viewAllAlertsAPI{
    long previousCount = notifiactionzTitleArray.count;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/viewAllAlerts",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&from=%d&limit=%@&countryCode=%@&MCC=%@&MNC=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],from,@"10",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            for (int i = 0; i < temp.count; i++) {
                NSDictionary *sDic = [[NSDictionary alloc] init];
                sDic = temp[i];
                
                
                
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                    [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToRootView];
                    
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                    [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToRootView];
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToRootView];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"No Data Found" ]) {
                    [self.view makeToast:@"No Notifications Available" duration:2.0 position:CSToastPositionCenter style:style];
                }
                
                else{
                    [notifiactionzTitleArray addObject:[sDic objectForKey:@"title"]];
                    [notificationMsgArray addObject:[sDic objectForKey:@"msg"]];
                    [notificationTimeArray addObject: [sDic objectForKey:@"time"]];
                    [notificationIdArray addObject:[sDic objectForKey:@"_id"]];
                    
                    
                }
            }
            if (notifiactionzTitleArray.count - previousCount != 10) {
                isMoreDataAvailable = false;
            }
            
            if([selectedIndexpathArray count])
            {
                NSLog(@"%lu",(unsigned long)[selectedIndexpathArray count]);
                for (int i = 0; i < [selectedIndexpathArray count]; i++) {
                    [self.notificationTableView selectRowAtIndexPath:selectedIndexpathArray[i] animated:YES scrollPosition:UITableViewScrollPositionNone];
                    
                    NSLog(@"i----- %d",i);
                    
                    NSLog(@"%@",deleteNotificationArray);
                }
            }
            
            
            [self.notificationTableView reloadData];
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}

-(NSString *)timeToString:(NSString *)date_rec format:(NSString *)format{
    
    //    NSLog(@"recived string is %@", date_rec);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *dateFromString = [dateFormatter dateFromString:date_rec];
    
    //    NSLog(@"%@", dateFromString);
    NSCalendarUnit units = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour| NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear;
    
    if ([dateFromString timeIntervalSinceNow] <= 0) {
        // if `date` is before "now" (i.e. in the past) then the components will be positive
        NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                       fromDate:dateFromString
                                                                         toDate:[NSDate date]
                                                                        options:0];
        //        NSLog(@"past %@", components);
        //        NSLog(@"present time is %@", [NSDate date]);
        if (components.year > 0) {
            if (components.year > 1) {
                return [NSString stringWithFormat:@"%ld Years ago", (long)components.year];
            }
            return [NSString stringWithFormat:@"%ld Year ago", (long)components.year];
        } else if (components.month > 0) {
            if (components.month > 1) {
                return [NSString stringWithFormat:@"%ld Months ago", (long)components.month];
            }
            return [NSString stringWithFormat:@"%ld Month ago", (long)components.month];
        } else if (components.weekOfYear > 0) {
            if (components.weekOfYear > 1) {
                return [NSString stringWithFormat:@"%ld Weeks ago", (long)components.weekOfYear];
            }
            return [NSString stringWithFormat:@"%ld Week ago", (long)components.weekOfYear];
        } else if (components.day > 0) {
            if (components.day > 1) {
                return [NSString stringWithFormat:@"%ld Days ago", (long)components.day];
            } else if(components.day == 1){
                return [NSString stringWithFormat:@"%ld Day ago", (long)components.day];
            }else{
                return @"Yesterday";
            }
        }else if(components.hour > 0){
            if (components.hour > 1) {
                return [NSString stringWithFormat:@"%ld Hours ago", (long)components.hour];
            }
            return [NSString stringWithFormat:@"%ld Hour ago", (long)components.hour];
        }else if(components.minute > 0){
            if (components.minute > 1) {
                return [NSString stringWithFormat:@"%ld Minutes ago", (long)components.minute];
            }
            return [NSString stringWithFormat:@"%ld Minute ago", (long)components.minute];
        }else if(components.second > 0){
            return @"Just now";
        }
    }else{
        NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                       fromDate:[NSDate date]
                                                                         toDate:dateFromString
                                                                        options:0];
        //        NSLog(@"future %@", components);
        //        NSLog(@"present time is %@", [NSDate date]);
        if (components.year > 0) {
            if (components.year > 1) {
                return [NSString stringWithFormat:@"%ld Years to go", (long)components.year];
            }
            return [NSString stringWithFormat:@"%ld Year to go", (long)components.year];
        } else if (components.month > 0) {
            if (components.month > 1) {
                return [NSString stringWithFormat:@"%ld Months to go", (long)components.month];
            }
            return [NSString stringWithFormat:@"%ld Month to go", (long)components.month];
        } else if (components.weekOfYear > 0) {
            if (components.weekOfYear > 1) {
                return [NSString stringWithFormat:@"%ld Weeks to go", (long)components.weekOfYear];
            }
            return [NSString stringWithFormat:@"%ld Week to go", (long)components.weekOfYear];
        } else if (components.day > 0) {
            if (components.day > 1) {
                return [NSString stringWithFormat:@"%ld Days to go", (long)components.day];
            } else if(components.day == 1){
                return [NSString stringWithFormat:@"%ld Days to go", (long)components.day];
            }else {
                return @"Tomorrow";
            }
        } else if(components.hour > 0){
            if (components.hour > 1) {
                return [NSString stringWithFormat:@"%ld Hours to go", (long)components.hour];
            }
            return [NSString stringWithFormat:@"%ld Hour to go", (long)components.hour];
        }else if(components.minute > 0){
            if (components.minute > 1) {
                return [NSString stringWithFormat:@"%ld Minutes to go", (long)components.minute];
            }
            return [NSString stringWithFormat:@"%ld Minute to go", (long)components.minute];
        }else if(components.second > 0){
            return @"Just now";
        }
        
    }
    return @"";
}

- (IBAction)defaultScreenConfirmPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/setCustomerSettings",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&settings=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],defaultScreen];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            NSDictionary *sDic = temp[0];
            
          
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Settings Updated" ]) {
                
                if ([defaultScreen isEqualToString:@"LiveTV"]) {
                    [self.view makeToast:@"Live TV Selected" duration:2.0 position:CSToastPositionCenter style:style];
                }
                
                else if ([defaultScreen isEqualToString:@"Radio"]){
                    [self.view makeToast:@"Radio Selected" duration:2.0 position:CSToastPositionCenter style:style];
                }
                else if ([defaultScreen isEqualToString:@"Promo"]){
                    [self.view makeToast:@"Promo Video Selected" duration:2.0 position:CSToastPositionCenter style:style];
                }
                
                [self defaultScreenBackButtonPressed:self];
            }
                        
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
    else{
        NSLog (@"Oh Sorry");
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
        
    }
}

-(void)goToRootView
{
    //    _loginTimer = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [_player pause];
    
    if ([defaultScreen  isEqual: @"LiveTV"]) {
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
    }else if ([defaultScreen  isEqual: @"Radio"])
    {
        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
        [self updateRadioUsageDetails];
    }

    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}

-(void)updateDeviceToken{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/updateDeviceToken",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
        NSString *post = [NSString stringWithFormat:@"customerId=%@&deviceId=%@&platform=%@&deviceToken=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"],@"iOS",[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]];
        
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData] forHTTPHeaderField:@"Content-Length"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setHTTPBody:postData];
        
        
        NSURLResponse *response;
        NSError *error;
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSLog(@"%@",response);
        if (response == NULL) {
            
        }
        if (!error) {
            NSError *myError = nil;
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger status = [httpResponse statusCode];
            
            temp = [dictionary objectForKey:@"root"];
            
            if (temp && status == 200) {
                NSLog(@"temp = %@",temp);
                NSDictionary *dict = [[NSDictionary alloc]init];
                dict = temp[0];
                
                if ([[dict objectForKey:@"status"] isEqualToString:@"Device Token Updated Successfully"]) {
                    NSLog(@"DeviceToken Updated");
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DeviceTokenChanged"];
                }
                
                else if ([[dict objectForKey:@"status"] isEqualToString:@"Some Problem Occurred"]){
                    
                    NSLog(@"Some Problem Occurred");
                }
                
                else if ([[dict objectForKey:@"status"] isEqualToString:@"Device Does Not Exists"]){
                    NSLog(@"Device Does Not Exists");
                }
                else if ([[dict objectForKey:@"status"] isEqualToString:@"Customer Does Not Exists"]){
                    [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToRootView];
                }
                else{
                    NSLog(@"Some Problem Occurred");
                }
            }
            
        }
        
        else{
            
        }
        
    });
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)WalletbackButtonAction:(UIButton *)sender {
    
    self.Walletbackbutton.hidden=YES;
    _TVIEW.hidden=YES;
    self.settingsTableView.hidden=NO;
    self.settingsBackButton.hidden=NO;
    
    
}

- (IBAction)ONDEMAN2:(id)sender {
    [_player pause];
    self.OnDemandCircle.image =[UIImage imageNamed:@"btn2.png"];
    [self performSegueWithIdentifier:@"HotelVODViewController" sender:self];
}
@end
