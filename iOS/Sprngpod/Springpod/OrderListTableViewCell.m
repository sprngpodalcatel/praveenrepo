//
//  OrderListTableViewCell.m
//  Asianet Mobile TV Plus
//
//  Created by Pa'One' Nani on 27/10/17.
//  Copyright © 2017 Xperio Labs Limited. All rights reserved.
//

#import "OrderListTableViewCell.h"

@implementation OrderListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
