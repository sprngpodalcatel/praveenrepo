//
//  OrderListTableViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by Pa'One' Nani on 27/10/17.
//  Copyright © 2017 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *orderName;
@property (strong, nonatomic) IBOutlet UILabel *orderCount;
@property (strong, nonatomic) IBOutlet UIButton *orderDecrementBtn;

@property (strong, nonatomic) IBOutlet UIButton *orderIncrementBtn;
@property (strong, nonatomic) IBOutlet UILabel *serialNumberLabel;

@end
