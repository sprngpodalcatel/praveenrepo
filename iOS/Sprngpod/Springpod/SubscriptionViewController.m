//
//  SubscriptionViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "SubscriptionViewController.h"
#import "SubscriptionTableViewCell.h"
#import "Flurry.h"
#import "SprngIPConf.h"

@interface SubscriptionViewController ()
{
    NSMutableArray *packageName,*startDate,*expiryDate;
    NSMutableArray *temp;
}
@property (weak, nonatomic) IBOutlet UITableView *subscriptionTableView;

@end

@implementation SubscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callApitoFetchData];
    
    [Flurry logEvent:@"Subscription_Screen" withParameters:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [packageName count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubscriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subscription" forIndexPath:indexPath];
    cell.packageLabel.text = [packageName objectAtIndex:indexPath.row];
    cell.startDateLabel.text = [startDate objectAtIndex:indexPath.row];
    cell.expiryLabel.text = [expiryDate objectAtIndex:indexPath.row];
    return cell;
}
-(void)callApitoFetchData
{
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?customerPackageDetails1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        temp =[dictionary objectForKey:@"root"];
        NSLog(@"status = %ld",(long)status);
        packageName = [[NSMutableArray alloc]init];
        startDate =[[NSMutableArray alloc]init];
        expiryDate = [[NSMutableArray alloc]init];
        
        
        if(temp && status == 200) {
            NSLog(@"temp = %@",temp);
            for (int i = 0; i < temp.count; i++) {
                NSDictionary *dict = [[NSDictionary alloc]init];
                dict = temp[i];
                [packageName addObject:[dict objectForKey:@"name"]];
                [startDate addObject:[dict objectForKey:@"startTime"]];
                [expiryDate addObject:[dict objectForKey:@"expirationTime"]];
                [self.subscriptionTableView reloadData];
            }
            
        }
        
    }
    else{
        NSLog (@"Oh Sorry");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }

}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
