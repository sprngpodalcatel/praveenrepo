//
//  AppDelegate.h
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"
#import <Google/SignIn.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>
{
    UIWindow *window;
    UINavigationController *navigationController;
}
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@end
//@property (strong, nonatomic) UIWindow *window;


//@end

