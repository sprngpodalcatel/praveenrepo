//
//  PODViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 09/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "PODViewController.h"
#import "PODTableViewCell.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "Flurry.h"
#import "SprngIPConf.h"

@interface PODViewController ()
{
    NSArray *podArray;
}
@property (weak, nonatomic) IBOutlet UITableView *podTableView;
@property (weak, nonatomic) IBOutlet UIView *podView;
@property (weak, nonatomic) IBOutlet UINavigationItem *podNav;

@end

@implementation PODViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    podArray = [NSArray arrayWithObjects:@"Movie on Demand",@"Music Video Demand",@"Free on Demand",@"Shopping on Demand", nil];
    self.podView.layer.cornerRadius = 5;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    self.tabBarController.tabBar.shadowImage = [UIImage new];
    self.tabBarController.tabBar.translucent = YES;
    self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops!!" message:@"The SPRNGPOD team is working hard to get you the best of On Demand , pls stay tuned" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    UILabel *theTitle = [alert valueForKey:@"_titleLabel"];
//    theTitle.textColor = [UIColor redColor];
//    theTitle.font = [UIFont fontWithName:@"Roboto_Light" size:22];

    
    [Flurry logEvent:@"VOD_Screen" withParameters:nil];
    
//    UILabel *titleLabel = [[UILabel alloc] init];
//    titleLabel.text = @"VOD";
//    titleLabel.textColor = [UIColor whiteColor];
//    [titleLabel sizeToFit];
//    self.podNav.titleView = titleLabel;
}

-(void)willPresentAlertView:(UIAlertView *)alertView
{
    UILabel *theTitle = [alertView valueForKey:@"_titleLabel"];
    theTitle.font = [UIFont fontWithName:@"Copperplate" size:18];
    [theTitle setTextColor:[UIColor whiteColor]];
    
//    UILabel *theBody = [alertView valueForKey:@"_bodyTextLabel"];
//    theBody.font = [UIFont fontWithName:@"Copperplate" size:15];
//    [theBody setTextColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)podDoSetup
{
    
}

-(void)setNavigationTitle:(NSString *)title
{
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
//    [[[UIAlertView alloc]initWithTitle:@"Oops!!" message:@"The SPRNGPOD team is working hard to get you the best of On Demand , pls stay tuned" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [podArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PODTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PODCell" forIndexPath:indexPath];
    cell.podLabel.text = [podArray objectAtIndex:indexPath.row];
    return cell;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
//        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        //        [firstVC setNavigationTitle:@"FAVOURITES"];
        
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
}

- (IBAction)alertCancelPressed:(id)sender {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
}

- (IBAction)alertOKPressed:(id)sender {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
}





- (IBAction)cancelPressed:(id)sender {
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
        
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [ThirdViewController class]]) {
        
//        ThirdViewController *firstVC = (ThirdViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
//        [firstVC setNavigationTitle:@"FAVOURITES"];
        
        
    }
    
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
