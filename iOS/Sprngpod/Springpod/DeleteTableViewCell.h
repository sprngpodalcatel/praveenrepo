//
//  DeleteTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 30/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deleteCell;

@end
