//
//  UrlRequesting.h
//  Sprngpod
//
//  Created by Shrishail Diggi on 08/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UrlRequesting : UIView

+(void)postRequest:(NSString *)urlString arguments:(NSString *)arguments;
+(void)getRequest:(NSString *)urlString;

@end
