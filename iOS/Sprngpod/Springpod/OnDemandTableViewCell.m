//
//  OnDemandTableViewCell.m
//  Sprngpod
//
//  Created by XperioLabs on 19/05/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "OnDemandTableViewCell.h"

@implementation OnDemandTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.plusButton resignFirstResponder];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if(selected) {

        self.descriptionText.hidden = NO;
        [self.channelNameLbl setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
        self.channelNameLbl.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];


        [self.descriptionText setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
        [self.descriptionText setTextColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0]];




        if (self.frame.size.height == 140)
        {
//            [self.oldPlusbutton setImage:[UIImage imageNamed:@"whiteMinus.png"] forState:UIControlStateNormal];
//            [self.channelNameLbl setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
             self.channelNameLbl.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];



            // [_descriptionText setFont:[UIFont fontWithName:@"Roboto-Light" size:11]];
            //  [_descriptionText setTextColor:[UIColor lightTextColor]];
        }
        else
        {
            [self.oldPlusbutton setImage:[UIImage imageNamed:@"whitePlus.png"] forState:UIControlStateNormal];
            [self.channelNameLbl setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
            self.channelNameLbl.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];

        }


    }

    else {

        if (self.frame.size.height == 140)
        {
            [self.descriptionText setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
             [self.descriptionText setTextColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0]];

            [self.oldPlusbutton setImage:[UIImage imageNamed:@"whiteMinus.png"] forState:UIControlStateNormal];
            [self.channelNameLbl setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]];
            self.channelNameLbl.font = [UIFont fontWithName:@"HelveticaNeue-Roman" size:14];

            // [_descriptionText setFont:[UIFont fontWithName:@"Roboto-Light" size:11]];
            //  [_descriptionText setTextColor:[UIColor lightTextColor]];
        }
        else
        {
            [self.descriptionText setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
            [self.descriptionText setTextColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0]];
            [self.oldPlusbutton setImage:[UIImage imageNamed:@"whitePlus.png"] forState:UIControlStateNormal];

        }
        [self.channelNameLbl setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]];
        self.channelNameLbl.font = [UIFont fontWithName:@"HelveticaNeue-Roman" size:14];
    }



   


    // Configure the view for the selected state
}

@end
