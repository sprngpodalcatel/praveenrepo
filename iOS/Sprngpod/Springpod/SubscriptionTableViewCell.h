//
//  SubscriptionTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *packageLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *expiryLabel;


@property (weak, nonatomic) IBOutlet UILabel *homePackageLabel;
@property (weak, nonatomic) IBOutlet UILabel *homePageStartDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *homePageEndDateLabel;


@end
