//
//  SplashViewController.m
//  Asianet Mobile TV Plus
//
//  Created by Sivakumar on 09/11/18.
//  Copyright © 2018 Xperio Labs Limited. All rights reserved.
//

#import "SplashViewController.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *animationArray=[NSArray arrayWithObjects:
                             [UIImage imageNamed:@"1(640x960).png"],
                             [UIImage imageNamed:@"2(640x960).png"],
                        [UIImage imageNamed:@"3(640x1136).png"],nil];
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //image.backgroundColor=[UIColor purpleColor];

   image.animationImages=animationArray;
    image.animationDuration=1.5;
    image.animationRepeatCount=0;
    [image startAnimating];
    [self.view addSubview:image];
   
    [self performSelector:@selector(Splash) withObject:nil afterDelay:1.0];
     //[image stopAnimating];
}

-(void)Splash{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"loginStatus"] == YES) {
        HomeViewController *hvc = [storyBoard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
        AppDelegate *delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        delegate.window.rootViewController = hvc;
    }
    
    else
    {
//splashViewController = [storyBoard instantiateViewControllerWithIdentifier:@"Splash"];
//self.window.rootViewController = splashViewController;
        AppDelegate *delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        LoginViewController *lvc = [storyBoard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
        delegate.window.rootViewController = lvc;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
