//
//  TabViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 09/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "TabViewController.h"
#import "FourthViewController.h"
#import "ThirdViewController.h"
#import "PODViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "FavpopUpViewController.h"
#import "netWork.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"
#import "LoginViewController.h"
#import "AppDelegate.h"


BOOL tabBool;
@interface TabViewController ()
{
    NSMutableArray *temp,*temp1;
    netWork *netReachability;
    NetworkStatus networkStatus;
    CSToastStyle *style;
    BOOL favListExist;
    
    NSString *selectedTabItem;
}

@property (strong , nonatomic) FourthViewController *fourthVC ;
@property (strong, nonatomic) PODViewController *podVC;
@property (strong , nonatomic) ThirdViewController *thirdVC;
@property (strong, nonatomic) FavpopUpViewController *favPopVC;
@property (weak , nonatomic) UIActivityIndicatorView *loader;
@property BOOL isThirdOrPODSelected;
@property BOOL isMOreSelected;
@property BOOL isPOPselected;
@property BOOL stopPortrait;//when ever there is a pop up view controller then parent view controller should not rotate.

@end

@implementation TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setDelegate:self];
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    _loader.hidden = YES;
    _loader.center = self.view.center;
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(0.0, 0.0)];
    [linePath addLineToPoint:CGPointMake(self.tabBar.frame.size.width, 0.0)];
    CAShapeLayer *shapelayer = [CAShapeLayer layer];
    shapelayer.path = [linePath CGPath];
    shapelayer.strokeColor = [[UIColor colorWithRed:(189.0/255.0) green:(195.0/255.0) blue:(199.0/255.0) alpha:1.0] CGColor];
    shapelayer.lineWidth = 1.0;
    shapelayer.fillColor = [[UIColor clearColor]CGColor];
    
    [self.tabBar.layer addSublayer:shapelayer];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
//    _isThirdOrPODSelected = false;
    // Do any additional setup after loading the view.
//        [[UITabBar appearance] setTintColor:[UIColor redColor]];
    
    
//    self.tabBar.backgroundImage = [UIImage imageNamed:@"tabbarBackGround.png"];
    
     selectedTabItem = @"LiveTV";
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
        
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UsageDetails

-(void)addingChannelUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]];
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSString *serviceIDN=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIDN forKey:@"serviceIdN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
    });
    
}

-(void)updateChannelUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}

-(void)addingRadioUsageDetails
{
    //****** Getting the Add Radio Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *addRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"saveLastRadio"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        NSData *addRadioUsagePostData = [addRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addRadioUsagePostData length]];
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addRadioUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                
                NSLog(@"Radio Usage array----%@",radioUsageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@" Radio UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSString *serviceIdRN=[[[radioUsageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIdRN forKey:@"serviceIdRN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}

-(void)updateRadioUsageDetails
{
    //****** Getting the Update Radio Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *updateRadioUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdRN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateRadioUsagePostData = [updateRadioUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateRadioUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateRadioUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateRadioUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateRadioUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateRadioUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *radioUsageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *radioUsageArray =[radioUsageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",radioUsageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"serviceIdRN"];
    });
}

-(void)addingFavChannelUsageDetails
{
    //****** Getting the Add Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"slectedFavID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]];
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"https://sprngpodapp.asianetmobiletv.com:8080/index.php/addUsageDetails"];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                //****** Storing the ServiceID In NSUserDefaults*******//
                
                NSString *serviceIDN=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIDN forKey:@"serviceIdN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}

-(void)updateFavChannelUsageDetails
{
    //****** Getting the Update Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"https://sprngpodapp.asianetmobiletv.com:8080/index.php/updateUsageDetails"];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}

-(void)addingVODUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Add Channel Usage Details Of chaneels *****//
        
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceProviderId=%@&contentId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"serviceProviderIdForUsageDetails"],[[NSUserDefaults standardUserDefaults]objectForKey:@"contentIdForUsageDetails"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addVODUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageIdForPoster"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}

-(void)updateVODUsageDetails
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //****** Getting the Update Channel Usage Details Of chaneels *****//
        
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceProviderId=%@&contentId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"serviceProviderIdForUsageDetails"],[[NSUserDefaults standardUserDefaults]objectForKey:@"contentIdForUsageDetails"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageIdForPoster"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateVODUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
}



-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item1
{
    if (item1.tag == 1) {
        
        if ([selectedTabItem isEqualToString:@"LiveTV"]) {
            
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
            
        }else if ([selectedTabItem isEqualToString:@"Radio"])
        {
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"VOD"])
        {
            //****** Getting the Update VOD Usage Details Of Radio Chaneels*****//
            [self updateVODUsageDetails];
        }else if([selectedTabItem isEqualToString:@"Fav"])
        {
            [self updateFavChannelUsageDetails];
        }else{
            
        }
        selectedTabItem = @"LiveTV";
        
        
    }else if (item1.tag == 2)
    {
        if ([selectedTabItem isEqualToString:@"LiveTV"]) {
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"Radio"])
        {
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"VOD"])
        {
            //****** Getting the Update VOD Usage Details Of Radio Chaneels*****//
            [self updateVODUsageDetails];
        }else if([selectedTabItem isEqualToString:@"Fav"])
        {
            [self updateFavChannelUsageDetails];
        }else{
            
        }
        selectedTabItem = @"Radio";
    }else if (item1.tag == 3)
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:selectedTabItem forKey:@"selectedTabItem"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if ([selectedTabItem isEqualToString:@"LiveTV"]) {
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"Radio"])
        {
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"VOD"])
        {
            //****** Getting the Update VOD Usage Details Of Radio Chaneels*****//
            [self updateVODUsageDetails];
        }else if([selectedTabItem isEqualToString:@"Fav"])
        {
            [self updateFavChannelUsageDetails];
        }else{
            
        }
        selectedTabItem = @"Fav";

        
    }else if (item1.tag == 4)
    {
        if ([selectedTabItem isEqualToString:@"LiveTV"]) {
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"Radio"])
        {
            //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
            [self updateRadioUsageDetails];
        }else if ([selectedTabItem isEqualToString:@"VOD"])
        {
            //****** Getting the Update VOD Usage Details Of Radio Chaneels*****//
            [self updateVODUsageDetails];
        }else if([selectedTabItem isEqualToString:@"Fav"])
        {
            [self updateFavChannelUsageDetails];
        }else{
            
        }
        selectedTabItem = @"VOD";
    }else if (item1.tag == 5)
    {
        
    }
}



- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    
    
    UINavigationController *navi = (UINavigationController *)viewController;

    if([[navi.viewControllers objectAtIndex:0] isKindOfClass:[FourthViewController class]])
    {
        NSLog(@"selected fourth view controller");
        
        if (!_isThirdOrPODSelected) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"stopRotation"] == NO) {
//            _isMOreSelected = true;
            _fourthVC = (FourthViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"FourthViewControllerID"];
            UIViewController *selectedVC = tabBarController.selectedViewController;
            NSLog(@"count %lu", (unsigned long)selectedVC.childViewControllers.count);
            
           
            
            if([[selectedVC.childViewControllers valueForKey:@"class"] containsObject:[FourthViewController class]] && selectedVC.childViewControllers.count >= 2) {
//                 [[selectedVC.childViewControllers objectAtIndex:1] removeFromParentViewController];

                return NO;
            }
            
            if (![[selectedVC.childViewControllers valueForKey:@"class"] containsObject:[FourthViewController class]]) {
                 [self setNavigationTitleforViewController:[((UINavigationController *)selectedVC).viewControllers objectAtIndex:0]];
                [selectedVC addChildViewController: _fourthVC];
                [selectedVC.view addSubview: _fourthVC.view];
                [selectedVC.view bringSubviewToFront:_fourthVC.view];
                [_fourthVC didMoveToParentViewController: viewController];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"stopRotation"];
//                _isMOreSelected = YES;
            }
        }
        }
        
        return NO;
    }
    
    else if ([[navi.viewControllers objectAtIndex:0]isKindOfClass:[ThirdViewController class]])
    {
        
        if (!_isMOreSelected) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"stopRotation"] == NO) {
            [self callAPI];
            if (favListExist) {
        _favPopVC = (FavpopUpViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"FavpopUpViewControllerID"];
//            _favPopVC.parentVC = viewController;
        UIViewController *selectedVC = tabBarController.selectedViewController;
            NSLog(@"count %lu", (unsigned long)selectedVC.childViewControllers.count);
        
        if ([[selectedVC.childViewControllers valueForKey:@"class"]containsObject:[FavpopUpViewController class]] && selectedVC.childViewControllers.count >= 2) {
            
            return NO;
        }
        
        if (![[selectedVC.childViewControllers valueForKey:@"class"] containsObject:[FavpopUpViewController class]]) {
            [self setNavigationTitleforViewController:[((UINavigationController *)selectedVC).viewControllers objectAtIndex:0]];
            [selectedVC addChildViewController: _favPopVC];
            [selectedVC.view addSubview: _favPopVC.view];
            [selectedVC.view bringSubviewToFront:_favPopVC.view];
            [_favPopVC didMoveToParentViewController: viewController];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"stopRotation"];
//            _isThirdOrPODSelected = YES;
        }
            }
            
            else{
                [self.view makeToast:@"Go to More to create favoruties list" duration:2.0 position:CSToastPositionCenter style:style];
            }
        }
    }
    return NO;
    
    
        
        
    }
//    else if ([[navi.viewControllers objectAtIndex:0]isKindOfClass:[ThirdViewController class]])
////        ThirdViewControllerID
//    {
//        NSLog(@"selected POD view controller");
//        if (!_isMOreSelected) {
//            //            _isThirdOrPODSelected = true;
//            _thirdVC = (ThirdViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ThirdViewControllerID"];
//            UIViewController *selectedVC = tabBarController.selectedViewController;
//            NSLog(@"count %lu",(unsigned long)selectedVC.childViewControllers.count);
//            
//            
//            if ([[selectedVC.childViewControllers valueForKey:@"class"]containsObject:[ThirdViewController class]]&& selectedVC.childViewControllers.count >= 2 ) {
//                
//                return NO;
//            }
//            
//            if (![[selectedVC.childViewControllers valueForKey:@"class"] containsObject:[ThirdViewController class]]) {
//                [self setNavigationTitleforViewController:[((UINavigationController *)selectedVC).viewControllers objectAtIndex:0]];
//                [selectedVC addChildViewController:_thirdVC];
//                [selectedVC.view addSubview:_thirdVC.view];
//                [selectedVC.view bringSubviewToFront:_thirdVC.view];
//                [_thirdVC didMoveToParentViewController:viewController];
//            }
//        }
//        return NO;
//    }
    
    
    //*commenting with sunil*//
    
//    else if ([[navi.viewControllers objectAtIndex:0]isKindOfClass:[PODViewController class]])
//    {
//        NSLog(@"selected POD view controller");
//        if (!_isMOreSelected) {
//            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"stopRotation"] == NO) {
////            _isThirdOrPODSelected = true;
//            _podVC = (PODViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PODViewControllerID"];
//            UIViewController *selectedVC = tabBarController.selectedViewController;
//            NSLog(@"count %lu",(unsigned long)selectedVC.childViewControllers.count);
//            
//            
//            if ([[selectedVC.childViewControllers valueForKey:@"class"]containsObject:[PODViewController class]]&& selectedVC.childViewControllers.count >= 2 ) {
//                
//                return NO;
//            }
//            
//            if (![[selectedVC.childViewControllers valueForKey:@"class"] containsObject:[PODViewController class]]) {
//                [self setNavigationTitleforViewController:[((UINavigationController *)selectedVC).viewControllers objectAtIndex:0]];
//                [selectedVC addChildViewController:_podVC];
//                [selectedVC.view addSubview:_podVC.view];
//                [selectedVC.view bringSubviewToFront:_podVC.view];
//                [_podVC didMoveToParentViewController:viewController];
//                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"stopRotation"];
//                
//            }
//        }
//        }
//        return NO;
//    }
//    
//    else if ([[navi.viewControllers valueForKey:@"class"] containsObject:[PODViewController class]])
//    {
//        return NO;
//    }
    
    //*commenting ending with sunil*//
    
    else if ([[navi.viewControllers valueForKey:@"class"] containsObject:[FourthViewController class]])
    {
        return NO;
    }
    
    else if ([[navi.viewControllers valueForKey:@"class"] containsObject:[FavpopUpViewController class]])
    {
        return NO;
    }
    
//    else if ([[navi.viewControllers valueForKey:@"class"] containsObject:[ThirdViewController class]])
//    {
//        return NO;
//    }
    
    //*comenting with sunil*//
//    if (_podVC != nil) {
//        [_podVC willMoveToParentViewController:nil];
//        [_podVC.view removeFromSuperview];
//        [_podVC removeFromParentViewController];
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
//        
//    }
    
    //*comenting ending with sunil*//
    
    if (_favPopVC != nil)
    {
        [_favPopVC willMoveToParentViewController:nil];
        [_favPopVC.view removeFromSuperview];
        [_favPopVC removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    }

    
//    if (_thirdVC != nil) {
//        [_thirdVC willMoveToParentViewController:nil];
//        [_thirdVC.view removeFromSuperview];
//        [_thirdVC removeFromParentViewController];
//    }
    
    if (_fourthVC != nil) {
        
        [_fourthVC willMoveToParentViewController:nil];
        [_fourthVC.view removeFromSuperview];
        [_fourthVC removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    }
    
    return YES;
}

//-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
////    if([viewController isKindOfClass:[UINavigationController class]]) {
////        if([[viewController.childViewControllers valueForKey:@"class"] containsObject:[ThirdViewController class]]) {
////            
////            
////        }
////        
////    }
//}


-(void)setNavigationTitleforViewController:(UIViewController *)viewController
{
    
//    if ([viewController isKindOfClass:[FirstViewController class]]) {
//        [((FirstViewController *)viewController) setNavigationTitle:@"MORE"];
//    }
//    
//    if ([viewController isKindOfClass:[SecondViewController class]]) {
//        [((SecondViewController *)viewController) setNavigationTitle:@"MORE"];
//    }
//    
//    if ([viewController isKindOfClass:[ThirdViewController class]]) {
//        [((ThirdViewController *)viewController) setNavigationTitle:@"MORE"];
//    }
//    
//    if ([viewController isKindOfClass:[PODViewController class]]) {
//        [((PODViewController *)viewController) setNavigationTitle:@"VOD"];
//    }
    
}




-(id<UIViewControllerAnimatedTransitioning>)tabBarController:(UITabBarController *)tabBarController animationControllerForTransitionFromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
//
//    
//    
////    
//    UINavigationController *naviTo = (UINavigationController *)toVC;
//    UIViewController *toController = naviTo.viewControllers[0];
    
//    NSLog(@"to child controller count %lu",toController.childViewControllers.count );

    
//
//    if([[naviTo.viewControllers objectAtIndex:0] isKindOfClass:[FourthViewController class]])
//    {
//        _fourthVC = (FourthViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"FourthViewControllerID"];
//        UINavigationController *navi = (UINavigationController *)fromVC;
//        UIViewController *selectedVC = navi.viewControllers[0];
//        
//        [selectedVC addChildViewController: _fourthVC];
//        _fourthVC.view.frame = CGRectMake(20, 72, 343, 513);
//        [selectedVC.view addSubview: _fourthVC.view];
//        [selectedVC.view bringSubviewToFront:_fourthVC.view];
//        [_fourthVC didMoveToParentViewController: selectedVC];
//        
//        [tabBarController setSelectedIndex:0];
//     //   [tabBarController setSelectedViewController:[tabBarController viewControllers][0]];
//
//    }
    
    _isThirdOrPODSelected = false;
    _isMOreSelected = false;
    _isPOPselected = false;
    
     UINavigationController *naviTo = (UINavigationController *)toVC;
    if([[naviTo.viewControllers valueForKey:@"class"] containsObject:[PODViewController class]] )
    {
        _isThirdOrPODSelected = true;
    }
    if([[naviTo.viewControllers valueForKey:@"class"] containsObject:[FourthViewController class]] )
    {
        _isMOreSelected = true;
    }
    
    if([[naviTo.viewControllers valueForKey:@"class"] containsObject:[ThirdViewController class]] )
    {
        _isPOPselected = true;
    }


    UINavigationController *navi = (UINavigationController *)fromVC;
    if([[navi.viewControllers valueForKey:@"class"] containsObject:[FourthViewController class]]) {
        _fourthVC = nil;
        [[fromVC.childViewControllers objectAtIndex:1] removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    }
    //*comenting with sunil*//
//    
//    if([[navi.viewControllers valueForKey:@"class"] containsObject:[PODViewController class]]) {
//        _podVC = nil;
//        [[fromVC.childViewControllers objectAtIndex:1] removeFromParentViewController];
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
//    }
    //*comenting ending with sunil*//
    
    if([[navi.viewControllers valueForKey:@"class"] containsObject:[FavpopUpViewController class]]) {
        _favPopVC = nil;
        [[fromVC.childViewControllers objectAtIndex:1] removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"stopRotation"];
    }
    
//
//    if([[navi.viewControllers valueForKey:@"class"] containsObject:[SecondViewController class]]) {
        // [navi.viewControllers objectAtIndex:0].title = @"More";
//        SecondViewController *secondVC = [navi.viewControllers objectAtIndex:0];
//        secondVC.title = @"More";
//    }
    
    return nil;
}

-(UIInterfaceOrientationMask)tabBarControllerSupportedInterfaceOrientations:(UITabBarController *)tabBarController
{
    if (tabBarController.selectedIndex == 1) {
        return UIInterfaceOrientationMaskPortrait;
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"stopRotation"] == YES) {
        return UIInterfaceOrientationMaskPortrait;
    }
    
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(void)callAPI
{
    _loader.hidden = NO;
    [_loader startAnimating];
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchFavouriteTvChannelListNames1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        temp =[dictionary objectForKey:@"root"];
        NSLog(@"status = %ld",(long)status);
       
        
        
        if(temp && status == 200) {
            NSLog(@"temp = %@",temp);
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = temp[0];
            
            
            if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToHome];
                
            }
            
            
            else if ([[dict objectForKey:@"status"] isEqualToString:@"Favourite Tv Channel List Does Not Exists"]) {
                favListExist = NO;
                
            }
            else{
                temp1 = [dict objectForKey:@"listNames"];
                if (temp1.count > 0) {
                    favListExist = YES;
                }
                else{
                    favListExist = NO;
                }
                
            }
        }
        else{
            NSLog (@"Oh Sorry");
            [self.navigationController.view makeToast:@"Check Your UserId and Password" duration:2.0 position:CSToastPositionCenter style:style];
            
        }
    }
    
}

-(void)goToHome{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [self.view endEditing:YES];
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
