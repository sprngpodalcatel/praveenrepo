//
//  ParentalTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 04/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *parentallabel;

@end
