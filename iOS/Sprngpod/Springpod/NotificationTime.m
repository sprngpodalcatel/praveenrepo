//
//  NotificationTime.m
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 19/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "NotificationTime.h"

@implementation NotificationTime

-(NSString *)timeToString:(NSString *)date_rec format:(NSString *)format{
    @try {
    
    //    NSLog(@"recived string is %@", date_rec);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *dateFromString = [dateFormatter dateFromString:date_rec];
    
    //    NSLog(@"%@", dateFromString);
    NSCalendarUnit units = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour| NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear;
    
    if ([dateFromString timeIntervalSinceNow] <= 0) {
        // if `date` is before "now" (i.e. in the past) then the components will be positive
        NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                       fromDate:dateFromString
                                                                         toDate:[NSDate date]
                                                                        options:0];
        //        NSLog(@"past %@", components);
        //        NSLog(@"present time is %@", [NSDate date]);
        if (components.year > 0) {
            if (components.year > 1) {
                return [NSString stringWithFormat:@"%ld Years ago", (long)components.year];
            }
            return [NSString stringWithFormat:@"%ld Year ago", (long)components.year];
        } else if (components.month > 0) {
            if (components.month > 1) {
                return [NSString stringWithFormat:@"%ld Months ago", (long)components.month];
            }
            return [NSString stringWithFormat:@"%ld Month ago", (long)components.month];
        } else if (components.weekOfYear > 0) {
            if (components.weekOfYear > 1) {
                return [NSString stringWithFormat:@"%ld Weeks ago", (long)components.weekOfYear];
            }
            return [NSString stringWithFormat:@"%ld Week ago", (long)components.weekOfYear];
        } else if (components.day > 0) {
            if (components.day > 1) {
                return [NSString stringWithFormat:@"%ld Days ago", (long)components.day];
            } else if(components.day == 1){
                return [NSString stringWithFormat:@"%ld Day ago", (long)components.day];
            }else{
                return @"Yesterday";
            }
        }else if(components.hour > 0){
            if (components.hour > 1) {
                return [NSString stringWithFormat:@"%ld Hours ago", (long)components.hour];
            }
            return [NSString stringWithFormat:@"%ld Hour ago", (long)components.hour];
        }else if(components.minute > 0){
            if (components.minute > 1) {
                return [NSString stringWithFormat:@"%ld Minutes ago", (long)components.minute];
            }
            return [NSString stringWithFormat:@"%ld Minute ago", (long)components.minute];
        }else if(components.second > 0){
            return @"Just now";
        }
    }else{
        NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                       fromDate:[NSDate date]
                                                                         toDate:dateFromString
                                                                        options:0];
        //        NSLog(@"future %@", components);
        //        NSLog(@"present time is %@", [NSDate date]);
        if (components.year > 0) {
            if (components.year > 1) {
                return [NSString stringWithFormat:@"%ld Years to go", (long)components.year];
            }
            return [NSString stringWithFormat:@"%ld Year to go", (long)components.year];
        } else if (components.month > 0) {
            if (components.month > 1) {
                return [NSString stringWithFormat:@"%ld Months to go", (long)components.month];
            }
            return [NSString stringWithFormat:@"%ld Month to go", (long)components.month];
        } else if (components.weekOfYear > 0) {
            if (components.weekOfYear > 1) {
                return [NSString stringWithFormat:@"%ld Weeks to go", (long)components.weekOfYear];
            }
            return [NSString stringWithFormat:@"%ld Week to go", (long)components.weekOfYear];
        } else if (components.day > 0) {
            if (components.day > 1) {
                return [NSString stringWithFormat:@"%ld Days to go", (long)components.day];
            } else if(components.day == 1){
                return [NSString stringWithFormat:@"%ld Days to go", (long)components.day];
            }else {
                return @"Tomorrow";
            }
        } else if(components.hour > 0){
            if (components.hour > 1) {
                return [NSString stringWithFormat:@"%ld Hours to go", (long)components.hour];
            }
            return [NSString stringWithFormat:@"%ld Hour to go", (long)components.hour];
        }else if(components.minute > 0){
            if (components.minute > 1) {
                return [NSString stringWithFormat:@"%ld Minutes to go", (long)components.minute];
            }
            return [NSString stringWithFormat:@"%ld Minute to go", (long)components.minute];
        }else if(components.second > 0){
            return @"Just now";
        }
        
    }
    return @"";
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
