//
//  SecondViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface SecondViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UITabBarControllerDelegate,CLLocationManagerDelegate,GADBannerViewDelegate,AVAudioPlayerDelegate>
{
    MPMoviePlayerController *radioPlayerController;
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
- (void)adView:(GADBannerView *)bannerView
didFailToReceiveAdWithError:(GADRequestError *)error;
- (void)adViewWillPresentScreen:(GADBannerView *)bannerView;
- (void)adViewDidDismissScreen:(GADBannerView *)bannerView;
- (void)adViewWillDismissScreen:(GADBannerView *)bannerView;
- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView;


@property (strong, nonatomic) CLLocationManager *locationManager;
-(void)setNavigationTitle:(NSString *)title;


//
@end

