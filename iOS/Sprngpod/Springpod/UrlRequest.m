//
//  UrlRequest.m
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 22/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "UrlRequest.h"
#import "SprngIPConf.h"
#import "UIView+Toast.h"

@implementation UrlRequest
CSToastStyle *style;

+(void)postRequest:(NSString *)urlString arguments:(NSString *)arguments view:(UIView *)view{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        NSLog(@"post Request with url : %@ \n arguments : %@", urlString, arguments);
        @try {
            
            style = [[CSToastStyle alloc] initWithDefaultStyle];
            style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
            style.messageColor = [UIColor whiteColor];
            style.messageAlignment = NSTextAlignmentCenter;
            style.backgroundColor = [UIColor grayColor];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],urlString]];
//            NSLog(@"postUrl %@",url);
            NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            request.HTTPMethod = @"POST";
            
             NSData *data =[[arguments stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"] dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                NSLog(@"error is %@", error);
                NSDictionary *dictionary;
                NSHTTPURLResponse *httpResponse;
                NSLog(@"Running on %@ thread", [NSThread currentThread]);
                if (!error) {
                    dictionary = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingAllowFragments error:&error];
//                    NSLog(@"string is %@",[NSString stringWithUTF8String:[data bytes]]);
                    
//                    NSLog(@"dictionary is %@", dictionary);
                    httpResponse = (NSHTTPURLResponse *)response;
                    
                    
                    if(dictionary){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[dictionary, httpResponse] forKeys:@[@"dictionary", @"response"]];
                            NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                            [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDic];
                        });
                        
                    }
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"Error: %@",error.userInfo);
                        
                        [view makeToast:[NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"NSLocalizedDescription"]] duration:2.0 position:CSToastPositionCenter style:style];
                        
                        NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[error, [NSNull null]] forKeys:@[@"error", @"dictionary"]];
                        NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                        [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDic];
                    });
                }
                
            }];
            
            // 5
            [uploadTask resume];
            
        }
        @catch(NSException *exception){
            NSLog(@"%@", exception.reason);
        }
    });
}

+(void)getRequest:(NSString *)urlString view:(UIView *)view{
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @try {
            
            style = [[CSToastStyle alloc] initWithDefaultStyle];
            style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
            style.messageColor = [UIColor whiteColor];
            style.messageAlignment = NSTextAlignmentCenter;
            style.backgroundColor = [UIColor grayColor];
            
            NSMutableURLRequest * url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],urlString]]];
            
            [url setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
//            NSLog(@"Token = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] );
//            NSLog(@"urlRequest : %@",url);
            
            NSURLSession *mySession = [NSURLSession sharedSession];
            [[mySession dataTaskWithRequest:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                NSLog(@"error is %@", error);
                NSDictionary *dictionary;
                NSHTTPURLResponse *httpResponse;
                NSLog(@"Running on %@ thread", [NSThread currentThread]);
                if (!error) {
                    dictionary = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingAllowFragments error:&error];
                    NSLog(@"string is %@",[NSString stringWithUTF8String:[data bytes]]);
                    
//                    NSLog(@"dictionary is %@", dictionary);
                    httpResponse = (NSHTTPURLResponse *)response;
                    
                    
                    if(dictionary){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[dictionary, httpResponse] forKeys:@[@"dictionary", @"response"]];
                            NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                            [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDic];
                        });
                        
                    }
                    
                    else{
                        NSLog(@"Null Dictionary");
                    }
                    
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //[error.userInfo objectForKey:@"NSLocalizedDescription"];
                        NSLog(@"Error: %@",error.userInfo);
                        
                        [view makeToast:[NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"NSLocalizedDescription"]] duration:2.0 position:CSToastPositionCenter style:style];
                        
                        NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[error, [NSNull null]] forKeys:@[@"error", @"dictionary"]];
                        NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                        
                        [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDic];
                    });
                }
            }] resume];
            
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.reason);
        }
    });
}


@end
