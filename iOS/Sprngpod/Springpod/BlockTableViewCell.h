//
//  BlockTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 03/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *blockLabel;

@end
