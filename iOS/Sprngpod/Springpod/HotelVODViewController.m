//
//  HotelVODViewController.m
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 20/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//
#import "HotelVODViewController.h"
#import "OnDemandTableViewCell.h"
#import "TourOnDemandTableViewCell.h"
#import "OnDemandCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"
#import "netWork.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "DeviceUID.h"
#import "TourVODCollectionViewCell.h"
#import "SmartHotelCollectionViewCell.h"
#import "HouseKeepingCollectionViewCell.h"
#import "Freshchat.h"
#import "Constants.h"
#import "OrderListTableViewCell.h"
#import "PointsViewcontrollwer.h"
#import "Minimarketcell.h"

@interface HotelVODViewController ()<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIActionSheetDelegate>

{
    NSString *urlString;
    
    NSString *string1;
    
    NSMutableArray *subFilters;
    NSMutableArray *genresubFilters;
    NSMutableArray *langsubFilters;
    
    NSMutableArray *cuisineSubFilters;
    NSMutableArray *drinkSubFilters;
    NSMutableArray *vegnonvegSubFilters;
   
    NSMutableArray *tourLibraryListArray;
    NSMutableArray *tourRoomServiceArray;
    
    NSMutableArray *houseKeepingArray;
    NSMutableArray *conciergeArray;
    
    NSMutableArray *roomServiceArray;
    NSMutableArray *selectedRoomServiceArray;
    NSDictionary *respeDict;
    NSIndexPath *selectedItemIndexPath;
    
    NSMutableArray *spaArray;
    NSMutableArray *spaGlobalArray;
    
    NSArray *channelNameArray;
    NSArray *bestOfAlbumArray;
    NSArray *bestOfmusicArray;
    NSArray *bestOfCountryArray;
    
    NSMutableArray *imagesArray;
    NSArray *descriptionArray;
    NSIndexPath *cellIndexPath;
    
    
    NSArray *collectionImagesArray;
    NSArray *valueArray;
    NSArray *priceArray;
    NSArray *languagesArray;
    NSArray *categoryArray;
    NSArray *genreArray;
    NSString *stringFromUrl;
    
    
    NSIndexPath *swipeIndex;
    NSString *lastTVid;
    NSTimer *playButtonTimer;
    UITapGestureRecognizer *tapRecogniser;
    NSIndexPath *selectedCellIndexPath;
    
    OnDemandTableViewCell *tableCell;
    netWork *netReachability;
    NetworkStatus networkStatus;
    CSToastStyle *style;
    
    NSMutableSet *expandedCells;
    NSMutableArray *temp,*channelUrlArray;
    NSString *rateString,*IPAddress,*lastListenRadioChannelLanguage,*lastSeenTVUrl,*countryName;
    NSString *deviceID,*basicInfoArray;
    float lattitude,longitude;
    NSMutableArray *packageName,*startDate,*expiryDate,*deviceList,*platform,*osVersion,*deletionStatusArray,*blockStatusArray,*countryAllowedStatusArray;
    NSIndexPath *blockIndexpath;
    
    NSIndexPath *selectedButtonIndex;
    
    
    float lastSlidedValue;
    
    float screenHeight;
    int sessionTime,tokenRefreshTime;
    
    BOOL seleted;
    
    BOOL packageNotFound;
    BOOL noDataFound; // To avoid crash in No data when low network.
    BOOL tokenExpired;
    BOOL cuisineTapped;
    BOOL drinkTapped;
    
    
    BOOL vegOrNonVegTapped;
    
    
    BOOL cuisineTapped1;
    BOOL drinkTapped2;
    
    
    BOOL vegOrNonVegTapped3;
    
    NSMutableData*reciveddata;
    NSString*get;
    
    NSMutableData*responsedata;
    
    
    NSMutableArray *_channelPosterArray2;
    NSMutableArray *_channelCategoryArray;
    NSMutableArray *_videoContentArray;
    NSMutableArray*subcontentArrAY;
    
    NSMutableArray *categoryArray1;
    
    NSDictionary *globalDic;
    
    
    NSString *param1;
    NSString *param2;
    NSString *param3;
    NSString *param4;
    NSString *param5;
    NSString *param6;
    
    //***** Order Display List Properties for FreshChat ******//
    NSMutableArray *orderNameListArray,*countArray,*serialNumberArray;
    NSString *orderName;
    NSMutableArray *orderDispalyListArray,*incrementValueArray;
    UIButton *orderCountIncrementBtn;
    UIButton *orderCountDecrementBtn;
    UILabel *OrderCountLbl;
    int countValue;
    int incrementedCountValue;
    int incrementedCount;
    int YPositionOfLabel;
    NSInteger profilePosition;
    NSString *incrementString, *decrementString;
    BOOL orderIncrementBool;
    BOOL orderDecrementBool;
    BOOL forPlayerStop;
    BOOL spaOrderBtnTapped;
    BOOL roomServiceOrderBtnTapped;
    BOOL houseKeepingCellTapped;
    BOOL concergeCellTapped;
    BOOL IncrementOrDecrementTapped;
    BOOL IncrementArrayBool;
    NSInteger countTagValue;
    NSInteger IncrementCounttagValue;
    
}


@property (strong, nonatomic) IBOutlet UILabel *salesOrderLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *hotelCollectionView;

@property (retain, nonatomic) NSURLConnection *connection;

@property(strong , nonatomic) NSTimer *loginTimer,*tokenRefreshTimer;
@property(strong, nonatomic) NSTimer *epgTimer;
@property (weak, nonatomic) IBOutlet UINavigationItem *ODNav;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playerViewHeight;
//@property (weak, nonatomic) IBOutlet UITableView *ODTableView;
@property (weak, nonatomic) IBOutlet UIView *tvView;
@property (weak, nonatomic) IBOutlet UICollectionView *ODCollectionView;

@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIView *playPauseView;


@property (nonatomic) int currentValue;

@property (weak, nonatomic) IBOutlet UIView *spaSelectedView;


@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UILabel *selectedName;
@property (weak, nonatomic) IBOutlet UITextView *DescriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *selectedPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *selectedDuration;
@property (weak, nonatomic) IBOutlet UIButton *selectedDiscriptionButton;

@property (strong) NSMutableArray *channelIDArray;
@property (strong) NSMutableArray *channelPosterArray;

@property (weak, nonatomic) IBOutlet UITableView *tourODTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *smrtHotelSegmentTap;


@property (weak, nonatomic) IBOutlet UIView *smrtHotelselectedView;


@property (weak, nonatomic) IBOutlet UIImageView *hotelSelectedItemImageView;


@property (weak, nonatomic) IBOutlet UILabel *hotelSelectedItemName;
@property (weak, nonatomic) IBOutlet UILabel *hotelSelectedItemPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *hotelVegNonItemNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *hotelSelectedDescriptionButton;
@property (weak, nonatomic) IBOutlet UITextView *hotelDescriptionTextView;

@property (weak, nonatomic) IBOutlet UILabel *hotelSpicyLbl;

@property (weak, nonatomic) IBOutlet UICollectionView *houseKeepingCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *concergeCollectionView;

@property (strong, nonatomic) IBOutlet UIView *smartHotelSiva;

@property (weak, nonatomic) IBOutlet UIView *menuView;


@property (weak, nonatomic) IBOutlet UIButton *cuisineBtn;


@property (weak, nonatomic) IBOutlet UIButton *drinksBtn;

@property (weak, nonatomic) IBOutlet UIButton *vegNonVegBtn;

@property (weak, nonatomic) IBOutlet UIView *firstPageView;
@property (weak, nonatomic) IBOutlet UIView *spaView;
@property (weak, nonatomic) IBOutlet UIView *concergeView;
@property (weak, nonatomic) IBOutlet UIView *houseKeepingView;

@property (weak, nonatomic) IBOutlet UIButton *smrtHotelRoderNowBtn;

@property (weak, nonatomic) IBOutlet UIButton *spaRorderNowBtn;
@property (weak, nonatomic) IBOutlet UIButton *spaStarBtn;
@property (weak, nonatomic) IBOutlet UIButton *smrtHotelStarBtn;

@property (weak, nonatomic) IBOutlet UIImageView *bottomImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopContsraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hotelcollectionviewTopConstraint;

//***** Order List Properties for FreshChat ---Sart---*****//
@property (strong, nonatomic) IBOutlet UIView *orderDisplayView;
@property (strong, nonatomic) IBOutlet UIScrollView *orderScrollView;
@property (strong, nonatomic) IBOutlet UIButton *orderCancleButton;
@property (strong, nonatomic) IBOutlet UIButton *orderConfrimButton;
@property (strong, nonatomic) IBOutlet UITableView *OrderListTableView;
//***** Order List Properties FreshChat ---End---*****//




@end

@implementation HotelVODViewController



- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    for (UIView *subview in actionSheet.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        }
    }
    
}


#pragma mark viewDidLoad




- (void)viewDidLoad {
    [super viewDidLoad];
    arr=[[NSMutableArray alloc]init];
    //customprice=[[NSMutableArray alloc]init];

    Custprice=[[NSMutableArray alloc]init];
    _orderDisplayView.hidden = true;
    _orderScrollView.contentSize = CGSizeMake(_orderScrollView.frame.size.width, 500);
    _orderScrollView.backgroundColor =[UIColor whiteColor];
    _orderScrollView.showsVerticalScrollIndicator = YES;
    orderNameListArray =[[NSMutableArray alloc]init];
    orderDispalyListArray =[[NSMutableArray alloc]init];
    countArray =[[NSMutableArray alloc]init];
    incrementValueArray=[[NSMutableArray alloc]init];
    serialNumberArray =[[NSMutableArray alloc]init];
    self.MinimarketCollview.delegate=self;
    self.MinimarketCollview.dataSource=self;
    countValue = 1;
    
    // Do any additional setup after loading the view.
    
    tourLibraryListArray = [[NSMutableArray alloc]init];
    cuisineSubFilters = [[NSMutableArray alloc]init];
    drinkSubFilters = [[NSMutableArray alloc]init];
    vegnonvegSubFilters = [[NSMutableArray alloc]init];
    roomServiceArray = [[NSMutableArray alloc]init];
    
    houseKeepingArray = [[NSMutableArray alloc]init];
    conciergeArray = [[NSMutableArray alloc]init];
    spaArray = [[NSMutableArray alloc]init];
    spaGlobalArray = [[NSMutableArray alloc]init];
    
    
    //  NSMutableDictionary *catgDict=[[NSMutableDictionary alloc]init];
    globalDic = [[NSDictionary alloc]init];
    categoryArray1 = [[NSMutableArray alloc]init];
    
    _channelIDArray = [[NSMutableArray alloc]init];
    //initialize the url that going to be fetched
    tourRoomServiceArray = [[NSMutableArray alloc]init];
    
    
    self.smrtHotelselectedView.hidden = true;
    
    self.hotelCollectionView.hidden = true;
    self.houseKeepingCollectionView.hidden = true;
    self.MinimarketCollview.hidden=YES;
    self.Minimarketmenu.hidden=YES;
    self.houseKeepingView.hidden = true;
    
    self.concergeCollectionView.hidden = true;
    self.concergeView.hidden = true;
    
    self.ODCollectionView.hidden = true;
    self.spaView.hidden = true;
    
    //self.smrtHotelSegmentTap.hidden = true;
    self.menuView.hidden = true;
    
    self.spaSelectedView.hidden = true;
    self.DescriptionTextView.textContainer.maximumNumberOfLines = 1;
    self.hotelDescriptionTextView.textContainer.maximumNumberOfLines = 1;
    
    _channelPosterArray2 = [[NSMutableArray alloc]init];
    _channelCategoryArray = [[NSMutableArray alloc]init];
    selectedRoomServiceArray = [[NSMutableArray alloc]init];
    respeDict = [[NSDictionary alloc]init];
    
    expandedCells = [[NSMutableSet alloc]init];
    
    //* Network connection Reachability
    
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    
    temp = [[NSMutableArray alloc]init];
    packageName = [[NSMutableArray alloc]init];
    startDate = [[NSMutableArray alloc]init];
    expiryDate = [[NSMutableArray alloc]init];
    
    deviceID = [DeviceUID uid];
    
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]} forState:UIControlStateSelected];
    UIFont *objFont = [UIFont fontWithName:@"Roboto-Light" size:15.0f];
    NSDictionary *dictAttributes = [NSDictionary dictionaryWithObject:objFont forKey:NSFontAttributeName];
    [[UISegmentedControl appearance] setTitleTextAttributes:dictAttributes forState:UIControlStateNormal];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    // Do any additional setup after loading the view.
    //    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    //    [button setImage:[UIImage imageNamed:@"homeNew.png"] forState:UIControlStateNormal];
    //    UIView *rightView = [[UIView alloc] initWithFrame:button.frame];
    
    //   UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightView];
    
    //    self.navigationController.navigationItem.rightBarButtonItem = right;
    //    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    //    self.tabBarController.tabBar.shadowImage = [UIImage new];
    //    self.tabBarController.tabBar.translucent = YES;
    //    self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
    //    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    
    UIBezierPath *linePath1 = [UIBezierPath bezierPath];
    [linePath1 moveToPoint:CGPointMake(8.0, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(232 , 82.0)];
    [linePath1 moveToPoint:CGPointMake(240.0/2, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(240.0/2, 117.0)];
    
    CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.path = [linePath1 CGPath];
    shapeLayer1.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer1.lineWidth = 1.0;
    shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
    
    self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    screenHeight = self.playerViewHeight.constant;
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"On Demand";
    //[titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.ODNav.titleView = titleLabel;
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"FS_Joey-Light" size:16.0], NSFontAttributeName,nil]];
    [_playPauseButton setHidden:YES];
    //    [_videoControlView setHidden:YES];
    
    
    tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTvView:)];
    tapRecogniser.delegate = self;
    tapRecogniser.numberOfTapsRequired = 1;
    [_playPauseView addGestureRecognizer:tapRecogniser];
    [_playPauseView addSubview:self.playPauseButton];
    [_playPauseView bringSubviewToFront:self.playPauseButton];
    
    
    [self homeScreenApi];
    [self httpGetRequest];
    
    moviePlayerController = [[MPMoviePlayerViewController alloc]init];
    moviePlayerController.view.hidden = NO;
    [moviePlayerController.view setFrame:self.tvView.bounds];
    [self.tvView addSubview:moviePlayerController.view];
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    [moviePlayerController.view bringSubviewToFront:_tvView];
    [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:lastSeenTVUrl]];
    [moviePlayerController.moviePlayer play];
    
    [self.spaSelectedView addSubview:self.selectedDiscriptionButton];
    
    _smrtHotelStarBtn.tag =0;
    _spaStarBtn.tag = 0;
    
    _hotelSelectedDescriptionButton.tag =0;
    _selectedDiscriptionButton.tag = 0;
    
    
    
}


-(void)httpGetRequest
{
    
    @try{
        
        
        NSString *tourStr =[NSString stringWithFormat:@"%@/index1.php/?fetchOnDemandLibrariesjone=abcd",[[SprngIPConf getSharedInstance]getIpForImage]];
        tourStr = [tourStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:tourStr];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         
         {
             if (!(connectionError))
             {
                 
                 NSError *connectionError = nil;
                 NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                 tourLibraryListArray = [receivedData objectForKey:@"root"];
                 
                 [_tourODTableView reloadData];
                 
                 NSLog(@"%@",tourLibraryListArray);
                 
             }
             else
             {
                 
             }
         }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    } @finally {
        NSLog(@"");
        
    }
    
}




#pragma mark SetNavigation


-(void)setNavigationTitle:(NSString *)title
{
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = title;
    [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.ODNav.titleView = titleLabel;
}


-(void) becomeActive
{
    NSLog(@"ACTIVE");
    //    [moviePlayerController.moviePlayer play];
    
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    [_playPauseButton setHidden:YES];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [moviePlayerController.moviePlayer pause];
    }
    else{
        [moviePlayerController.moviePlayer play];
    }
    
}

-(void)timedOut{
    [moviePlayerController.moviePlayer stop];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark searchBar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] == 0)
    {
        [searchBar performSelector:@selector(resignFirstResponder)
                        withObject:nil
                        afterDelay:0];
    }
}


#pragma mark playTV method

-(void)playTVWithValue
{
    
    moviePlayerController = [[MPMoviePlayerViewController alloc]init];
    [moviePlayerController.view setFrame: self.tvView.bounds];
    
    [self.tvView addSubview:moviePlayerController.view];
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    if (packageNotFound == YES) {
        
    }
    
    else if (tokenExpired == YES){
        
    }
    else if (noDataFound == YES);
    
    else if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]){
        [moviePlayerController.moviePlayer stop];
        [self.view endEditing:YES];
        [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
    }
    
    else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"])
    {
        [moviePlayerController.moviePlayer stop];
        [self.view endEditing:YES];
        [self.navigationController.view makeToast:@"This channel is not subscibed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
        
    }
    else
    {
        //    moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[ channelUrlArray objectAtIndex:_currentValue]]];
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
            //            [moviePlayerController.moviePlayer play];
        }
        else
        {
            
            NSString*vedioUrlfromArry=[globalDic objectForKey:@"fullContentUrl"];
            
            stringFromUrl = vedioUrlfromArry;
            self.currentValue = (int)cellIndexPath.row;
            NSLog(@"Url : %@",vedioUrlfromArry);
            [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:vedioUrlfromArry]];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            [_playPauseButton setHidden:YES];
            [moviePlayerController.moviePlayer play];
            
        }
        
    }
}



#pragma mark ViewDidAppear


-(void)viewDidAppear:(BOOL)animated
{
    
    [self httpGetRequest];
    _channelIDArray = [[NSMutableArray alloc]init];
    
    [self httpGetRequest];
    
    
    _channelPosterArray2=[[NSMutableArray alloc]init];
    
    
    _channelCategoryArray=[[NSMutableArray alloc]init];
    
    _videoContentArray=[[NSMutableArray alloc]init];
    
    //    [self.view bringSubviewToFront:self.activityInd];
    self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    self.playPauseButton.hidden = true;
    
    
    if (networkStatus == NotReachable) {
        [self.navigationController.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Netwotk Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //        [alert show];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWasShown:)
    //                                                 name:UIKeyboardDidShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillBeHidden:)
    //                                                 name:UIKeyboardWillHideNotification object:nil];
    
    //    [Flurry  setCrashReportingEnabled:YES];
    //    [Flurry startSession:@"VNVS4BP7SMYN4JFRW5RT"];
    //    NSException *e = [[NSException alloc]init];
    //    [Flurry logError:@"ERROR_NAME" message:@"ERROR_MESSAGE" exception:e];
    //    NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:@"Device ID",@"123445",@"OS version",@"9.2", nil];
    //    [Flurry logEvent:@"TV_Screen" withParameters:dictionary timed:YES];
    
    [self setNavigationTitle:@"On Demand"];
    //    _epgTimer = [NSTimer scheduledTimerWithTimeInterval:30*60 target:self selector:@selector(refreshTableData) userInfo:nil repeats:YES];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromEntertainView"];
    
    if ([CLLocationManager locationServicesEnabled]) {
        if (!self.locationManager) {
            self.locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [_locationManager startUpdatingLocation];
        }
    }
    [self homeScreenApi];
    [self httpGetRequest];
    //    [self playTVWithValue];
    //    [self reloadTableview];
    // [self.ODTableView reloadData];
    
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    if (forPlayerStop == YES) {
        [moviePlayerController.moviePlayer stop];
        
    }else
    {
        forPlayerStop= NO;
        [moviePlayerController.moviePlayer play];
    }
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    
}




-(void)sessionExpiry:(NSNotification *)notification{
    [moviePlayerController.moviePlayer stop];
}

- (void)viewDidDisappear:(BOOL)animated {
    //BEFORE DOING SO CHECK THAT TIMER MUST NOT BE ALREADY INVALIDATED
    //Always nil your timer after invalidating so that
    //it does not cause crash due to duplicate invalidate
    // self.segmentedTap.selected = false;
    // self.smrtHotelSegmentTap.selected = false;
    // self.menuView.hidden = false;
    
    
    
    if(_epgTimer)
    {
        [_epgTimer invalidate];
        _epgTimer = nil;
    }
    [moviePlayerController.moviePlayer stop];
}

#pragma mark screen Rotation

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.view layoutIfNeeded];
    if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"landscape");
        [self.navigationController setNavigationBarHidden:TRUE animated:FALSE];
        // self.tabBarController.tabBar.hidden = YES;
        
        NSLog(@" %f %f ",self.view.frame.size.width, self.view.frame.size.height);
        
        self.playerViewHeight.constant = MIN(self.view.frame.size.width, self.view.frame.size.height) ;
        
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        [moviePlayerController.view setFrame: self.tvView.bounds];
        
        
        //  [_searchBar removeFromSuperview];
        //   [_segmentedTap removeFromSuperview];
        //    [_ODTableView removeFromSuperview];
        [_ODCollectionView removeFromSuperview];
        
        [_hotelCollectionView removeFromSuperview];
        [_tourODTableView removeFromSuperview];
        [_bottomImageView removeFromSuperview];
        [_firstPageView removeFromSuperview];
        [_houseKeepingCollectionView removeFromSuperview];
        [_houseKeepingView removeFromSuperview];
        [_concergeCollectionView removeFromSuperview];
        [_concergeView removeFromSuperview];
        [_spaView removeFromSuperview];
        [_smrtHotelselectedView removeFromSuperview];
        [_spaSelectedView removeFromSuperview];
        //   [_smrtHotelSegmentTap removeFromSuperview];
        [_menuView removeFromSuperview];
        
        //        moviePlayerController.moviePlayer.view.frame = [[UIScreen mainScreen] bounds];
        //        moviePlayerController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        _pinView.frame = CGRectMake(moviePlayerController.moviePlayer.view.frame.size.width/2 - 120, 8, 240, 119);
        //         self.pinView.center = moviePlayerController.view.center;
        //        [[[[UIApplication sharedApplication] windows] lastObject] addSubview:blockAlert];
        //        [[[[UIApplication sharedApplication] windows] lastObject] addSubview:moviePlayerController.view];
        
        
        
        //        [[[UIApplication sharedApplication] keyWindow]addSubview:moviePlayerController.view];
        
        //        [[[UIApplication sharedApplication] keyWindow]addSubview:self.pinView];
    }
    else
    {
        NSLog(@"portrait");
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        //self.playerViewHeight.constant = (236/386.0) *  MIN(self.view.frame.size.width, self.view.frame.size.height);
        
        self.playerViewHeight.constant = screenHeight;
        [self.navigationController setNavigationBarHidden:false animated:FALSE];
        //   self.tabBarController.tabBar.hidden = false;
        [moviePlayerController.view setFrame: self.tvView.bounds];
        [self.view addSubview:_ODCollectionView];
        [self.view addSubview:_spaView];
        [self.view addSubview:_hotelCollectionView];
        
        [self.view addSubview:_houseKeepingCollectionView];
        [self.view addSubview:_houseKeepingView];
        [self.view addSubview:_concergeCollectionView];
        [self.view addSubview:_concergeView];
        [self.view addSubview:_tourODTableView];
        [self.view addSubview:_bottomImageView];
        [self.view addSubview:_firstPageView];
        // [self.view addSubview:_smrtHotelSegmentTap];
        [self.view addSubview:_menuView];
        [self.view addSubview:_spaSelectedView];
        [self.view addSubview:_smrtHotelselectedView];
        
        // [_ODTableView reloadData];
        
        //        [moviePlayerController.view removeFromSuperview];
        //        [self.pinView removeFromSuperview];
        //        [moviePlayerController.view setFrame: self.tvView.bounds];
        //        [self.pinView setFrame:self.tvView.bounds];
        //        [_tvView addSubview:moviePlayerController.view];
        //        [moviePlayerController.moviePlayer setFullscreen:NO];
        //        [_tvView addSubview:self.pinView];
        //         _pinView.frame = CGRectMake(self.tvView.frame.size.width/2 - 120, 8, 240, 119);
        //        self.pinView.center = _tvView.center;
        //        [moviePlayerController.view addSubview:self.pinView];
        
        
        //        [moviePlayerController.view bringSubviewToFront:self.pinView];
        
        
        //        [moviePlayerController.moviePlayer play];
    }
    
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
    
}




- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _OrderListTableView) {
        return [orderNameListArray count];
    }
    else{
        return [tourLibraryListArray count];
        
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _tourODTableView) {
        NSDictionary *responceDict = tourLibraryListArray[indexPath.row];
        
        TourOnDemandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourVODCell" forIndexPath:indexPath];
        cell.tag = indexPath.row;
        
        NSString *name = [responceDict objectForKey:@"libraryName"];
        cell.TourLibraryName.text = name;
        
        NSString *descriptionText = [responceDict objectForKey:@"libraryContent"];
        cell.tourDescriptionText.text = descriptionText;
        
        [cell.tourDescriptionText setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
        [cell.tourDescriptionText setTextColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0]];
        
        
        NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[responceDict objectForKey:@"libraryImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
//        cell.tourLogImageView.layer.masksToBounds = YES;
//        cell.tourLogImageView.layer.cornerRadius = 26.0;
//        cell.tourLogImageView.layer.borderWidth = 1.0;
        
        cell.channelDetailsView.clipsToBounds = true;
        cell.channelDetailsView.layer.cornerRadius = 12;
        
        [cell.tourLogImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
//        
//            if(indexPath.row == 4)
//            {
//        
//        
//                cell.tourLogImageView.layer.masksToBounds = YES;
//                cell.tourLogImageView.layer.cornerRadius = 26.0;
//                cell.tourLogImageView.layer.borderWidth = 1.0;
//                cell.tourLogImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://www.sprngpod.com/xperiolabsapi/tour_images/spa.jpg"]]];
//        
//                NSString *name = @"Bar";
//                cell.TourLibraryName.text = name;
//        
//        
//                NSString *descriptionText = @"Business consultancy for Service Providing to Glabal Encripation";
//                cell.tourDescriptionText.text = descriptionText;
//        
//                [cell.tourDescriptionText setFont:[UIFont fontWithName:@"Raleway-Light" size:11]];
//                [cell.tourDescriptionText setTextColor:[UIColor darkGrayColor]];
//        
//                 return  cell;
//        
//            }
        return  cell;
        
        
    }else if (tableView == _OrderListTableView)
    {
        
        OrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderListTableViewCell" forIndexPath:indexPath];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Sales Orders"];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        _salesOrderLabel.attributedText = [attributeString copy];
        cell.orderCount.tag = indexPath.row;
        countTagValue = cell.orderCount.tag;
        NSUInteger serialNumber =countTagValue+1;
        cell.serialNumberLabel.text =[NSString stringWithFormat:@"%lu.",(unsigned long)serialNumber];
        
        if (indexPath.row == profilePosition) {
            cell.orderName.text = [orderNameListArray objectAtIndex:indexPath.row];
            if (IncrementOrDecrementTapped == NO) {
                [incrementValueArray addObject:[NSNumber numberWithInt:(int)countValue]];
            }
            cell.orderIncrementBtn.layer.cornerRadius = 2;
            cell.orderIncrementBtn.clipsToBounds = YES;
            cell.orderDecrementBtn.layer.cornerRadius = 2;
            cell.orderDecrementBtn.clipsToBounds = YES;
            for (int i =0; i<[orderNameListArray count]; i++) {
                if (cell.orderCount.tag == profilePosition) {
                    if(orderIncrementBool == true){
                        cell.orderCount.text = incrementString;
                        IncrementCounttagValue = cell.orderCount.tag;
                        incrementedCount =[incrementString intValue];
                        [incrementValueArray replaceObjectAtIndex:profilePosition withObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(int)incrementedCount]]];
                    }else if (orderDecrementBool == true)
                    {
                        cell.orderCount.text = decrementString;
                        IncrementCounttagValue = cell.orderCount.tag;
                        incrementedCount =[decrementString intValue];
                        [incrementValueArray replaceObjectAtIndex:profilePosition withObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(int)incrementedCount]]];
                    }
                }else{
                    
                }
                
            }
        }else
        {
            cell.orderName.text = [orderNameListArray objectAtIndex:indexPath.row];
            if (IncrementOrDecrementTapped == NO) {
                [incrementValueArray addObject:[NSNumber numberWithInt:(int)countValue]];
            }
            cell.orderIncrementBtn.layer.cornerRadius = 2;
            cell.orderIncrementBtn.clipsToBounds = YES;
            cell.orderDecrementBtn.layer.cornerRadius = 2;
            cell.orderDecrementBtn.clipsToBounds = YES;
            if (cell.orderCount.tag == profilePosition) {
                if(orderIncrementBool == true){
                    cell.orderCount.text = incrementString;
                    IncrementCounttagValue = cell.orderCount.tag;
                    incrementedCount =[incrementString intValue];
                    [incrementValueArray replaceObjectAtIndex:profilePosition withObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(int)incrementedCount]]];
                }else if (orderDecrementBool == true)
                {
                    cell.orderCount.text = decrementString;
                    IncrementCounttagValue = cell.orderCount.tag;
                    incrementedCount =[decrementString intValue];
                    [incrementValueArray replaceObjectAtIndex:profilePosition withObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(int)incrementedCount]]];
                }
            }else{
                
            }
            
        }
        
        if (houseKeepingCellTapped == YES || concergeCellTapped == YES) {
            cell.orderDecrementBtn.hidden =YES;
            cell.orderIncrementBtn.hidden =YES;
            cell.orderCount.hidden=YES;
        }else{
            cell.orderDecrementBtn.hidden =NO;
            cell.orderIncrementBtn.hidden =NO;
            cell.orderCount.hidden=NO;
        }
        return cell;
        
    }
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        
        
        NSString *SpaStr =[NSString stringWithFormat:@"%@/index1.php/?fetchSPAPosters=input",[[SprngIPConf getSharedInstance]getIpForImage]];
        
        string1 = SpaStr;
        [[NSUserDefaults standardUserDefaults] setObject:SpaStr forKey:@"urlStr2"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        SpaStr = [SpaStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:SpaStr];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        
        @try {
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData4 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                     spaArray = [receivedData4 objectForKey:@"root"];
                     
                     NSDictionary *spaId = [spaArray valueForKey:@"spaId"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:spaId forKey:@"spaId"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     [_ODCollectionView reloadData];
                     NSLog(@"%@",spaArray);
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(spaEventToFire:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                     
                 }
                 else{
                     NSLog(@" %@ Null error ",connectionError);
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@" %@ exception error ",exception);
            
        } @finally {
            NSLog(@"");
        }
    }
    
    else if(indexPath.row == 1){
        
        param1 = @"false";
        param2 = @"Nr62WhRjzaLSJkvB";
        param3 = @"Nr62WhRjzaLSJkvB";
        param4 = @"Nr62WhRjzaLSJkvB";
        
        @try {
            NSString *post = [NSString stringWithFormat:@"retrive=%@&cuisine=%@&drinks=%@&vegnonveg=%@",param1,param2,param3,param4]; // <--here put the request parameters you used to get the response
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContent1",[[SprngIPConf getSharedInstance]getIpForImage]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     //  NSURLResponse *response;
                     //  NSError *error;
                     //  data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                     
                     NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                     
                     tourRoomServiceArray =[responseDic valueForKey:@"root"];
                     NSLog(@"%@", tourRoomServiceArray);
                     NSDictionary *roomId = [tourRoomServiceArray valueForKey:@"RoomServiceId"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:roomId forKey:@"RoomServiceIdKey"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     selectedButtonIndex = indexPath;
                     [_hotelCollectionView reloadData];
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(eventToFire1:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                 }
                 else
                 {
                     
                 }
             }];
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
        } @finally {
            NSLog(@"");
            
        }
        
        
        
        
    }
    else if (indexPath.row == 2) {
        
        @try {
            NSString *houseKeepingStr =[NSString stringWithFormat:@"%@/index1.php/?fetchHKPosters=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = houseKeepingStr;
            [[NSUserDefaults standardUserDefaults] setObject:houseKeepingStr forKey:@"urlStr3"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            houseKeepingStr = [houseKeepingStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:houseKeepingStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     
                     NSError *error = nil;
                     NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                     houseKeepingArray = [receivedData objectForKey:@"root"];
                     
                     NSDictionary *houseKeepingId = [houseKeepingArray valueForKey:@"hoseKeepingId"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:houseKeepingId forKey:@"hoseKeepingId"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     [_houseKeepingCollectionView reloadData];
                     
                     NSLog(@"%@",houseKeepingArray);
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire2:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                     
                 }
                 else
                 {
                     
                 }
                 
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
        
    }
    else if (indexPath.row == 3) {
//https://www.sprngpod.com/xperiolabsapi/index.php/?fetchBranchePosters=SPAS10001401&branchId=BRNC001&retrieve=false&branchProductName=Nr62WhRjzaLSJkvB&promotionsName=Nr62WhRjzaLSJkvB&brandName=Nr62WhRjzaLSJkvB
        
        self.MinimarketCollview.hidden=false;
        self.Minimarketmenu.hidden=false;
        @try {
            NSString *param0= [[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"];
            NSString *param1=@"BRNC001";
            NSString *param2=@"false";
            NSString *param3=@"Nr62WhRjzaLSJkvB";
            NSString *param4=@"Nr62WhRjzaLSJkvB";
            NSString *param5=@"Nr62WhRjzaLSJkvB";
            
             NSString *post = [NSString stringWithFormat:@"fetchBranchePosters=%@&branchId=%@&retrieve=%@&branchProductName=%@&promotionsName=%@&brandName=%@",param0,param1,param2,param3,param4,param5];
            
            NSString *conciergeStr =[NSString stringWithFormat:@"%@/index.php/?%@",[[SprngIPConf getSharedInstance]getIpForImage],post];
            
            string1 = conciergeStr;
            [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:conciergeStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];

            [request setHTTPMethod:@"GET"];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                     conciergeArray = [receivedData objectForKey:@"root"];
                    
                     
                     NSDictionary *conciergeIdImages = [conciergeArray valueForKey:@"branchImage"];
                     NSDictionary *branchProductName=[conciergeArray valueForKey:@"branchProductName"];
                      NSDictionary *price=[conciergeArray valueForKey:@"price"];
                     [[NSUserDefaults standardUserDefaults] setObject:conciergeIdImages forKey:@"branchImage"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                    
                     [self.MinimarketCollview reloadData];
                     
                     NSLog(@"%@",conciergeArray);
                     
            
                     
                     NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                 }
                 else{
                     
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
    }else{
        @try {
            NSString *conciergeStr =[NSString stringWithFormat:@"%@/index1.php/?fetchConciergePosters=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            
            string1 = conciergeStr;
            [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:conciergeStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"GET"];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                     conciergeArray = [receivedData objectForKey:@"root"];
                     
                     
                     NSDictionary *conciergeId = [conciergeArray valueForKey:@"conciergeID"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:conciergeId forKey:@"conciergeID"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     [_concergeCollectionView reloadData];
                     
                     NSLog(@"%@",conciergeArray);
                     
                     
                     NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(tourEventToFire3:) userInfo:nil repeats:NO];
                     
                     NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     NSString *name = [dic objectForKey:@"libraryName"];
                     [self setNavigationTitle:name];
                    // [self FilterDtaMiniMarket];
                 }
                 else{
                     
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
    }
}
- (void)spaEventToFire:(NSTimer*)timer0 {
    
    _firstPageView.hidden = true;
    _tourODTableView.hidden = true;
    _ODCollectionView.hidden = false;
    self.spaView.hidden = false;
    
}
-(void)eventToFire1:(NSTimer*)timer0{
    
    self.hotelCollectionView.hidden = false;
    
    self.ODCollectionView.hidden = true;
    self.spaView.hidden = true;
    
    _firstPageView.hidden = true;
    self.tourODTableView.hidden = true;
    // self.smrtHotelSegmentTap.hidden = false;
    self.menuView.hidden = false;
    
    
}
-(void)tourEventToFire2:(NSTimer*)timer0{
    
    
    self.hotelCollectionView.hidden = true;
    self.houseKeepingCollectionView.hidden = false;
    self.houseKeepingView.hidden = false;
    
    self.ODCollectionView.hidden = true;
    self.spaView.hidden = true;
    
    _firstPageView.hidden = true;
    self.tourODTableView.hidden = true;
    //self.smrtHotelSegmentTap.hidden = true;
    self.menuView.hidden = true;
    
    
}
-(void)tourEventToFire3:(NSTimer*)timer0{
    
    
    self.hotelCollectionView.hidden = true;
    self.concergeCollectionView.hidden = false;
    self.concergeView.hidden =false;
    
    self.ODCollectionView.hidden = true;
    self.spaView.hidden = true;
    
    _firstPageView.hidden = true;
    self.tourODTableView.hidden = true;
    // self.smrtHotelSegmentTap.hidden = true;
    self.menuView.hidden = true;
    
}

- (void)tourEventToFire0:(NSTimer*)timer0 {
    _firstPageView.hidden = true;
    _tourODTableView.hidden = true;
    // _smrtHotelSegmentTap.hidden = true;
    self.menuView.hidden = true;
    
}

- (void)eventToFire0:(NSTimer*)timer0 {
    
    self.ODCollectionView.hidden = false;
    self.spaView.hidden = false;
    //self.smrtHotelSegmentTap.hidden = true;
    self.menuView.hidden = true;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CGFloat kExpandedCellHeight = 140;
    CGFloat kNormalCellHeight = 69;
    
    
    if(selectedCellIndexPath != nil
       && [selectedCellIndexPath compare:indexPath] == NSOrderedSame)
    {
        
        [tableCell.plusButton setImage:[UIImage imageNamed:@"new"] forState:UIControlStateNormal];
        if ([expandedCells containsObject:indexPath]) {
            return kExpandedCellHeight;
        }
        else
        {
            return kNormalCellHeight;
        }
        
    }
    else if (tableView == _OrderListTableView) {
        kNormalCellHeight = 38;
        return kNormalCellHeight;
    }else
    {
        
        return kNormalCellHeight;
    }
    
    
}
#pragma mark CollectionView Delegate methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    if (collectionView == _hotelCollectionView) {
        return tourRoomServiceArray.count;
    }
    else if (collectionView == _houseKeepingCollectionView)
    {
        return houseKeepingArray.count;
    }
    else if (collectionView == _concergeCollectionView)
    {
        return conciergeArray.count;
    }
    else if (collectionView == _ODCollectionView)
    {
        return spaArray.count;
    }else{
        
        return conciergeArray.count;
    }
    
    return 0;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _hotelCollectionView)
    {
        
        NSDictionary *respDic = tourRoomServiceArray[indexPath.row];
        
        SmartHotelCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HotelCell" forIndexPath:indexPath];
        
        
        NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[respDic objectForKey:@"Image"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        [cell.smrtHotelImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
        
        
        
        NSString * RoomName = [respDic valueForKey:@"Name"];
        cell.smrtHotelNameLbl.text = RoomName;
        
        
        return cell;
        
    }
    
    else if (collectionView == _houseKeepingCollectionView)
    {
        
        
        NSDictionary *respDic = houseKeepingArray[indexPath.row];
        
        //  NSArray *array = [[NSArray alloc]initWithObjects:@"Digital TV (1).png",@"Iron.png",@"Laundry.png",@"Mini bar.png",@"Housekeeping.png",@"Free Water.png", nil];
        
        HouseKeepingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HouseKeepingCell" forIndexPath:indexPath];
        
        
        NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[respDic objectForKey:@"houseKeepingImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        cell.houseKeepingTitle.text=[respDic objectForKey:@"houseKeepingTitle"];
        
        
        [cell.houseKeepingImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
        
        //  cell.houseKeepingImageView.image = [UIImage imageNamed:array[indexPath.row]];
        return cell;
        
    }
    else if (collectionView == _concergeCollectionView)
    {
        
        
        NSDictionary *respDic = conciergeArray[indexPath.row];
        
        TourVODCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ConciergeCell" forIndexPath:indexPath];
        
        
        NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[respDic objectForKey:@"conciergeImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        [cell.tourVODImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
        cell.conciergeTitle.text = [respDic objectForKey:@"conciergerTitle"];
        
        return cell;
        
    }
    else if(collectionView == _ODCollectionView)
    {
        
        NSDictionary *resDic = spaArray[indexPath.row];
        
        OnDemandCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ODCollectionCell" forIndexPath:indexPath];
        
        NSString *imgURL = [NSString stringWithFormat:@"http://www.sprngpod.com/xperiolabsapi/%@",[resDic objectForKey:@"spaImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * RoomName = [resDic valueForKey:@"spaTitle"];
        cell.rateLable.text = RoomName;
        
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
        
        return cell;
        
    }else{
        
        
        
        NSDictionary *resDic = conciergeArray[indexPath.row];
        
        Minimarketcell *cell = [_MinimarketCollview dequeueReusableCellWithReuseIdentifier:@"ODCell" forIndexPath:indexPath];
        
        NSString *imgURL = [NSString stringWithFormat:@"http://www.sprngpod.com/xperiolabsapi/%@",[resDic objectForKey:@"branchImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
    NSString * RoomName = [resDic valueForKey:@"branchProductName"];
        cell.Titlelabel.text = RoomName;
        
        [cell.imageview1 sd_setImageWithURL:[NSURL URLWithString:encripString]];
        
        return cell;
        
        
        
        
    }
    
    
    
    return  nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==self.MinimarketCollview) {
        
//        if (drinkTapped == true)
//        {
//            NSMutableArray *drinkServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"DrinkRoomServiceIdKey"];
//            param1 = [drinkServiceID objectAtIndex:indexPath.row];
//        }else if (vegOrNonVegTapped == true)
//        {
//            NSMutableArray *vegOrNonVegServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"VegNonVegRoomServiceIdKey"];
//            param1 = [vegOrNonVegServiceID objectAtIndex:indexPath.row];
//
//        }else if (cuisineTapped == true)
//        {
//            NSMutableArray *CuisineServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"CuisineRoomServiceIdKey"];
//            param1 = [CuisineServiceID objectAtIndex:indexPath.row];
//        }else{
//            NSMutableArray *roomServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"RoomServiceIdKey"];
//            param1 = [roomServiceID objectAtIndex:indexPath.row];
//        }
        
      //  https://www.sprngpod.com/xperiolabsapi/index.php/?fetchProductDetails=SPAS10001401&branchProductId=BRNCPRO00106
        
        NSString *param0=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"];
        NSDictionary *param1=[conciergeArray objectAtIndex:indexPath.row];
        NSString *param2=[param1 objectForKey:@"branchProductId"];
        
        
        NSString *post = [NSString stringWithFormat:@"fetchProductDetails=%@&branchProductId=%@",param0,param2];
        
        NSString *conciergeStr =[NSString stringWithFormat:@"%@/index.php/?%@",[[SprngIPConf getSharedInstance]getIpForImage],post];
        
        string1 = conciergeStr;
        [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:conciergeStr];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        [request setHTTPMethod:@"GET"];
        
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if (!(connectionError))
             {
                 
                 NSError *error = nil;
                 NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                 
                 respeDict =[receivedData valueForKey:@"root"];
                 selectedButtonIndex = indexPath;

                 NSDictionary *roomImageDic = conciergeArray[indexPath.row];

                 //_hotelcollectionviewTopConstraint.constant = 128;
                 NSString * itemName = [respeDict valueForKey:@"branchProductName"];
                 NSString * itemPrice = [respeDict valueForKey:@"price"];
                 NSString * itemDescription = [respeDict valueForKey:@"branchDescription"];
// NSString * itemVegNonVeg = [respeDict valueForKey:@"Veg_NonVeg"];
// NSString * itemFlavour=[respeDict valueForKey:@"Flavour"];

                 orderName = itemName;

                 self.smrtHotelselectedView.hidden = false;
                 NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[respeDict objectForKey:@"branchImage"]];
                 NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                 [self.hotelSelectedItemImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];

                 self.hotelSelectedItemName.text =itemName;
                 if (itemDescription == nil || [itemDescription isKindOfClass:[NSNull class]]) {
                     itemDescription=@"";
                 }
                 self.hotelDescriptionTextView.text=itemDescription;
                 self.hotelSelectedItemPriceLbl.text = [NSString stringWithFormat:@"%@",itemPrice];
                 strprice=itemPrice;
                 self.hotelVegNonItemNameLbl.text = @"Duration";
                 self.hotelSpicyLbl.text = @"";
                 [self.smrtHotelselectedView addSubview:self.hotelDescriptionTextView];
                 [self.hotelDescriptionTextView setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
                 [self.hotelDescriptionTextView setTextColor:[UIColor grayColor]];
                 self.hotelDescriptionTextView.textContainer.maximumNumberOfLines = 40;
                 self.hotelDescriptionTextView.hidden = true;
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 // [self.MinimarketCollview reloadData];
                 
                 NSLog(@"%@",conciergeArray);
                 
                 
                 
                 //NSDictionary * dic = tourLibraryListArray[indexPath.row];
                 // NSString *name = [dic objectForKey:@"libraryName"];
                 //[self setNavigationTitle:name];
             }
             else{
                 
             }
         }];
   
        
}
    else if(collectionView == _ODCollectionView) {
        
        @try {
            NSMutableArray *spaID = [[NSUserDefaults standardUserDefaults] objectForKey:@"spaId"];
            
            param1 = [spaID objectAtIndex:indexPath.row];
            
            NSString *post = [NSString stringWithFormat:@"spaId=%@",param1];
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchSPAPosterDetails",[[SprngIPConf getSharedInstance]getIpForImage]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     //                     NSURLResponse *response;
                     //                     NSError *error;
                     //                     NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                     
                     NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                     NSLog(@"%@",responseDic);
                     
                     spaGlobalArray = [responseDic objectForKey:@"root"];
                     
                     
                     NSDictionary*posterDic = spaArray[indexPath.row];
                     NSString *imgURL = [NSString stringWithFormat:@"http://www.sprngpod.com/xperiolabsapi/%@",[posterDic objectForKey:@"spaImage"]];
                     NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                     
                     [self.selectedImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
                     
                     NSDictionary *dic = spaGlobalArray[0];
                     
                     _collectionViewTopContsraint.constant = 128;
                     
                     NSString * spaName = [dic valueForKey:@"spaTitle"];
                     NSString * Price = [dic objectForKey:@"price"];
                     NSString * duration = [dic objectForKey:@"duration"];
                     NSString * Discription=[dic valueForKey:@"sapDescription"];
                     
                     self.spaSelectedView.hidden = false;
                     self.DescriptionTextView.text = Discription;
                     self.selectedName.text = spaName;
                     self.selectedPriceLbl.text = Price;
                     strprice=Price;
//                     [Custprice addObject:strprice];
                     self.selectedDuration.text = [NSString stringWithFormat:@" %@ ",duration];
                     
                     //***** Storing the Name of the Order in a String*****//
                     orderName = spaName;
                     
                     [self.spaSelectedView addSubview:self.DescriptionTextView];
                     [self.DescriptionTextView setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
                     [self.DescriptionTextView setTextColor:[UIColor grayColor]];
                     self.DescriptionTextView.textContainer.maximumNumberOfLines = 10;
                     self.DescriptionTextView.hidden = true;
                     
                 }
                 else{
                     
                 }
             }];
            
            
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
    }
    else if (collectionView == _hotelCollectionView)
    {
        @try {
            
            if (drinkTapped == true)
            {
                NSMutableArray *drinkServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"DrinkRoomServiceIdKey"];
                param1 = [drinkServiceID objectAtIndex:indexPath.row];
            }else if (vegOrNonVegTapped == true)
            {
                NSMutableArray *vegOrNonVegServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"VegNonVegRoomServiceIdKey"];
                param1 = [vegOrNonVegServiceID objectAtIndex:indexPath.row];
                
            }else if (cuisineTapped == true)
            {
                NSMutableArray *CuisineServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"CuisineRoomServiceIdKey"];
                param1 = [CuisineServiceID objectAtIndex:indexPath.row];
            }else{
                NSMutableArray *roomServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"RoomServiceIdKey"];
                param1 = [roomServiceID objectAtIndex:indexPath.row];
            }
            
            
            //            NSMutableArray *roomServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"RoomServiceIdKey"];
            //            param1 = [roomServiceID objectAtIndex:indexPath.row];
            
            NSString *post = [NSString stringWithFormat:@"roomId=%@",param1]; // <--here put the request parameters you used to get the response
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContentDetails",[[SprngIPConf getSharedInstance]getIpForImage]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     //                     NSURLResponse *response;
                     //                     NSError *error;
                     //                     NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                     
                     NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
                     
                     
                     respeDict =[responseDic valueForKey:@"root"];
                     selectedButtonIndex = indexPath;
                     
                     NSDictionary *roomImageDic = tourRoomServiceArray[indexPath.row];
                     
                     _hotelcollectionviewTopConstraint.constant = 128;
                     
                     
                     NSString * itemName = [respeDict valueForKey:@"Name"];
                     NSString * itemPrice = [respeDict valueForKey:@"Price"];
                     strprice=itemPrice;
                     NSString * itemDescription = [respeDict valueForKey:@"Description"];
                     NSString * itemVegNonVeg = [respeDict valueForKey:@"Veg_NonVeg"];
                     NSString * itemFlavour=[respeDict valueForKey:@"Flavour"];
                     
                     orderName = itemName;
                     
                     self.smrtHotelselectedView.hidden = false;
                     NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[roomImageDic objectForKey:@"Image"]];
                     NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                   //  self.orderDisplayView.hidden=false;
                     [self.hotelSelectedItemImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
                     
                     self.hotelSelectedItemName.text =itemName;
                     self.hotelDescriptionTextView.text=itemDescription;
                     self.hotelSelectedItemPriceLbl.text = [NSString stringWithFormat:@"%@",itemPrice];
                     self.hotelVegNonItemNameLbl.text = [NSString stringWithFormat:@" %@ ,",itemVegNonVeg];
                     self.hotelSpicyLbl.text = itemFlavour;
                     [self.smrtHotelselectedView addSubview:self.hotelDescriptionTextView];
                     [self.hotelDescriptionTextView setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
                     [self.hotelDescriptionTextView setTextColor:[UIColor grayColor]];
                     self.hotelDescriptionTextView.textContainer.maximumNumberOfLines = 40;
                     self.hotelDescriptionTextView.hidden = true;
                     
                     
                 }
                 else{
                     
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"");
            
        } @finally {
            NSLog(@"");
        }
        
        
        
    }
    else if(collectionView == _houseKeepingCollectionView)
    {
        
        @try {
            NSMutableArray *hoseKeepingId = [[NSUserDefaults standardUserDefaults] objectForKey:@"hoseKeepingId"];
            
            param1 = [hoseKeepingId objectAtIndex:indexPath.row];
            
            NSString *post = [NSString stringWithFormat:@"hoseKeepingId=%@",param1];
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchHKPostersDetails",[[SprngIPConf getSharedInstance]getIpForImage]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     //                     NSURLResponse *response;
                     //                     NSError *error;
                     //                     NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                     
                     NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
                     //  NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
                     NSDictionary *Dic = [[NSDictionary alloc]init];
                     Dic = [responseDic valueForKey:@"root"];
                     NSArray *Array = [Dic valueForKey:@"houseKeepersPNo"];
                     
                     //***** Storing the Name of the Order in a String*****//
                     NSArray *orderTitleArray=[Dic valueForKey:@"houseKeepersTitle"];
                     orderName= [orderTitleArray objectAtIndex:0];
                     
                            spaOrderBtnTapped = NO;
                             roomServiceOrderBtnTapped = NO;
                              houseKeepingCellTapped=YES;
                              concergeCellTapped=NO;
                     
                             [orderNameListArray addObject:orderName];
                             NSLog(@"orderNameListArray: %@",orderNameListArray);
                             [countArray addObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:(int)countValue]]];
                             _orderDisplayView.hidden = NO;
                             [_OrderListTableView reloadData];
                             [moviePlayerController.moviePlayer stop];
                             _playPauseView.hidden = YES;
                     [self.smrtHotelRoderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];
                     
                     _OrderListTableView.hidden=YES;
                     _orderDisplayView.hidden=YES;
                     FreshchatMessage *userMessage = [[FreshchatMessage alloc] initWithMessage:salesOrders andTag:@"housekeeping"];
                    
                     [[Freshchat sharedInstance] sendMessage:userMessage];//send message api call
                     ConversationOptions *options = [ConversationOptions new];
                     [options filterByTags:@[@"housekeeping"] withTitle:@"House Keeping"];
                     
                     [[Freshchat sharedInstance] showConversations:self withOptions: options];
                     
                     //                     NSString *message = [Array objectAtIndex:0];
                     //                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"House Keeping" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     
                     //***** To bring up the Freshchat’s Conversations List or single conversation overlay at any point, you need to use -[[Freshchat sharedInstance] showConversations:] method  --START--*****//
                     
//                     ConversationOptions *options = [ConversationOptions new];
//                     [options filterByTags:@[@"Housekeeping"] withTitle:@"House Keeping"];
//                     [[Freshchat sharedInstance] showConversations:self withOptions: options];
//                      [[Freshchat sharedInstance] showConversations:self];
                     
                     //***** [[Freshchat sharedInstance] showConversations:] method --END--*****//

                     // [alert show];
                     
                 }
                 else{
                     
                 }
             }];
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        

        
    }
    else if (collectionView == _concergeCollectionView)
    {
        
        @try {
            NSMutableArray *conciergeID = [[NSUserDefaults standardUserDefaults] objectForKey:@"conciergeID"];
            
            param1 = [conciergeID objectAtIndex:indexPath.row];
            
            NSString *post = [NSString stringWithFormat:@"conciergeID=%@",param1];
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            NSString *str =[NSString stringWithFormat:@"%@/index1.php/?fetchConciergePosters=input",[[SprngIPConf getSharedInstance]getIpForImage]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     //                    NSURLResponse *response;
                     //                     NSError *error;
                     //                     NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                     
                     NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
                     
                     NSDictionary *Dic = [[NSDictionary alloc]init];
                     Dic = [responseDic valueForKey:@"root"];
                     NSArray *Array = [Dic valueForKey:@"conciergerPNo"];
                     
                     NSString *message = [Array objectAtIndex:0];
                     NSArray *orderTitleArray=[Dic valueForKey:@"conciergerTitle"];
                     NSInteger indexpath=indexPath.row;
                     orderName= [orderTitleArray objectAtIndex:indexPath.row];
                             spaOrderBtnTapped = NO;
                             roomServiceOrderBtnTapped = NO;
                             houseKeepingCellTapped=NO;
                             concergeCellTapped=YES;
                             [orderNameListArray addObject:orderName];
                             NSLog(@"orderNameListArray: %@",orderNameListArray);
                             [countArray addObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:(int)countValue]]];
                             _orderDisplayView.hidden = NO;
                             [_OrderListTableView reloadData];
                             [moviePlayerController.moviePlayer stop];
                             _playPauseView.hidden = YES;
                             [self.spaRorderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];

                     
                     _orderDisplayView.hidden=YES;
                     _OrderListTableView.hidden=YES;
                     FreshchatMessage *userMessage = [[FreshchatMessage alloc] initWithMessage:salesOrders andTag:@"concierge"];
                     [[Freshchat sharedInstance] sendMessage:userMessage];//send message api call
                     ConversationOptions *options = [ConversationOptions new];
                     [options filterByTags:@[@"concierge"] withTitle:@"Concierge"];
                     [[Freshchat sharedInstance] showConversations:self withOptions: options];
                     
                     // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Concierge" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     
                     //***** To bring up the Freshchat’s Conversations List or single conversation overlay at any point, you need to use -[[Freshchat sharedInstance] showConversations:] method  --START--*****//
                     
//                     ConversationOptions *options = [ConversationOptions new];
//                     [options filterByTags:@[@"Concierge"] withTitle:@"Concierge"];
//                     [[Freshchat sharedInstance] showConversations:self withOptions: options];
                     //[[Freshchat sharedInstance] showConversations:self];
                     
                     //***** [[Freshchat sharedInstance] showConversations:] method --END--*****//
                     
                     // [[ISafe shared] presentOn:self.navigationController];
                     
                     
                     //[alert show];
                 }
                 else{
                     
                     
                     if (drinkTapped == true)
                     {
                         NSMutableArray *drinkServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"DrinkRoomServiceIdKey"];
                         param1 = [drinkServiceID objectAtIndex:indexPath.row];
                     }else if (vegOrNonVegTapped == true)
                     {
                         NSMutableArray *vegOrNonVegServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"VegNonVegRoomServiceIdKey"];
                         param1 = [vegOrNonVegServiceID objectAtIndex:indexPath.row];
                         
                     }else if (cuisineTapped == true)
                     {
                         NSMutableArray *CuisineServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"CuisineRoomServiceIdKey"];
                         param1 = [CuisineServiceID objectAtIndex:indexPath.row];
                     }else{
                         NSMutableArray *roomServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"RoomServiceIdKey"];
                         param1 = [roomServiceID objectAtIndex:indexPath.row];
                     }
                     
                     
                     //            NSMutableArray *roomServiceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"RoomServiceIdKey"];
                     //            param1 = [roomServiceID objectAtIndex:indexPath.row];
                     
                     NSString *post = [NSString stringWithFormat:@"roomId=%@",param1]; // <--here put the request parameters you used to get the response
                     
                     
                     NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                     NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
                     
                     
                     NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContentDetails",[[SprngIPConf getSharedInstance]getIpForImage]];
                     str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                     NSURL *url = [NSURL URLWithString:str];
                     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                     [request setHTTPMethod:@"POST"];
                     [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                     [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                     [request setHTTPBody:postData];
                     
                     [NSURLConnection sendAsynchronousRequest:request
                                                        queue:[NSOperationQueue mainQueue]
                                            completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                      
                      {
                          if (!(connectionError))
                          {
                              
                              //                     NSURLResponse *response;
                              //                     NSError *error;
                              //                     NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                              
                              NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
                              
                              
                              respeDict =[responseDic valueForKey:@"root"];
                              selectedButtonIndex = indexPath;
                              
                              NSDictionary *roomImageDic = tourRoomServiceArray[indexPath.row];
                              
                              _hotelcollectionviewTopConstraint.constant = 128;
                              
                              
                              NSString * itemName = [respeDict valueForKey:@"Name"];
                              NSString * itemPrice = [respeDict valueForKey:@"Price"];
                              NSString * itemDescription = [respeDict valueForKey:@"Description"];
                              NSString * itemVegNonVeg = [respeDict valueForKey:@"Veg_NonVeg"];
                              NSString * itemFlavour=[respeDict valueForKey:@"Flavour"];
                              
                              orderName = itemName;
                              
                              self.smrtHotelselectedView.hidden = false;
                              NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[roomImageDic objectForKey:@"Image"]];
                              NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                              
                              [self.hotelSelectedItemImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
                              
                              self.hotelSelectedItemName.text =itemName;
                              self.hotelDescriptionTextView.text=itemDescription;
                              self.hotelSelectedItemPriceLbl.text = [NSString stringWithFormat:@"%@",itemPrice];
                              self.hotelVegNonItemNameLbl.text = [NSString stringWithFormat:@" %@ ,",itemVegNonVeg];
                              self.hotelSpicyLbl.text = itemFlavour;
                              [self.smrtHotelselectedView addSubview:self.hotelDescriptionTextView];
                              [self.hotelDescriptionTextView setFont:[UIFont fontWithName:@"Raleway-Light" size:12]];
                              [self.hotelDescriptionTextView setTextColor:[UIColor grayColor]];
                              self.hotelDescriptionTextView.textContainer.maximumNumberOfLines = 40;
                              self.hotelDescriptionTextView.hidden = true;
                              
                              
                          }
                          else{
                              
                          }
                      }];
                     
                     
                     
                     
                     
                 }
             }];
        } @catch (NSException *exception) {
            NSLog(@"");
        } @finally {
            NSLog(@"");
        }
        
        
        
    }
    
}


-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.spaSelectedView.hidden == true) {
        
        _collectionViewTopContsraint.constant = 0;
        
    }
    else if (self.smrtHotelselectedView.hidden == true){
        
        _hotelcollectionviewTopConstraint.constant = 0;
    }
    else
    {
        _collectionViewTopContsraint.constant = 128;
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == _ODCollectionView){
        
        if(IS_IPHONE_5)
        {
            if (self.collectionViewTopContsraint.constant == 128) {
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1+8);
            }
            else{
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
            }
        }
        else if (IS_IPHONE_6)
        {
            if (self.collectionViewTopContsraint.constant == 128) {
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-25);
            }
            else{
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
            }
        }
        else if (IS_IPHONE_6_PLUS)
        {
            if (self.collectionViewTopContsraint.constant == 128) {
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-48);
            }
            else{
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
            }
        }
        return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
        
    }else if (collectionView == _hotelCollectionView){
        
        if(IS_IPHONE_5)
        {
            if (self.hotelcollectionviewTopConstraint.constant == 128) {
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1+8);
            }
            else{
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
            }
        }
        else if (IS_IPHONE_6)
        {
            if (self.hotelcollectionviewTopConstraint.constant == 128) {
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-25);
            }
            else{
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
            }
        }
        else if (IS_IPHONE_6_PLUS)
        {
            if (self.hotelcollectionviewTopConstraint.constant == 128) {
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-48);
            }
            else{
                return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
            }
        }
        
    }
    return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
}




-(void)smartHotePostData{
    
    
    param1 = @"true";
    param2 = @"Nr62WhRjzaLSJkvB";
    param3 = @"Nr62WhRjzaLSJkvB";
    param4 = @"Nr62WhRjzaLSJkvB";
    
    NSString *post = [NSString stringWithFormat:@"retrive=%@&cuisine=%@&drinks=%@&vegnonveg=%@",param1,param2,param3,param4]; // <--here put the request parameters you used to get the response
    
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    @try {
        NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContent1",[[SprngIPConf getSharedInstance]getIpForImage]];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:str];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if (!(error))
        {
            roomServiceArray =[responseDic valueForKey:@"root"];
            NSLog(@"%@", roomServiceArray);
            
        }
        else{
            
        }
        
    } @catch (NSException *exception) {
        NSLog(@"");
    } @finally {
        NSLog(@"");
    }
    
    
    
}


#pragma mark HomeScreen API

-(void)homeScreenApi
{
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"])
            {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else{
                
                IPAddress = [dict objectForKey:@"ipAddress"];
                lastSeenTVUrl = [dict objectForKey:@"lastSeenTVChannelURL"];
                //                NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance]getIpForImage]];
                //                lastSeenTVChannelLogoURL =[dict objectForKey:@"lastSeenTVChannelLogo"];
                //                NSString *imageUrl4 = [imageUrl stringByAppendingString:lastSeenTVChannelLogoURL];
                //                tvLogoUrl = [NSURL URLWithString:imageUrl4];
                //                    [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
                lastListenRadioChannelLanguage = [dict objectForKey:@"lastListenRadioChannelLanguage"];
                //                lastListenRadioLogoUrl = [dict objectForKey:@"lastListenRadioChannelLogo"];
                //                NSString *imageUrl1 = [imageUrl stringByAppendingString:lastListenRadioLogoUrl];
                //                radioLogoUrl = [NSURL URLWithString:imageUrl1];
                //                        [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
                //                lastListenRadioUrl = [dict objectForKey:@"lastListenRadioChannelURL"];
                //                NSString *imageUrl2 = [dict objectForKey:@"promoLogo"];
                //                NSString *promoLogo = [imageUrl stringByAppendingString:imageUrl2];
                //                promoLogoUrl = [NSURL URLWithString:promoLogo];
                //                promoVideoUrl = [dict objectForKey:@"promoVideoURL"];
                //                defaultScreen = [dict objectForKey:@"settings"];
                
                NSNumber *number = [dict objectForKey:@"sessionTimeOut"];
                sessionTime = [number intValue];
                NSNumber *refreshTime = [dict objectForKey:@"tokenRefreshTime"];
                tokenRefreshTime = [refreshTime intValue];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastListenRadioChannelLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [_tokenRefreshTimer invalidate];
                if (tokenRefreshTime < 1) {
                    
                }
                else{
                    _tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:tokenRefreshTime target:self selector:@selector(RefreshToken) userInfo:nil repeats:YES];
                }
            }
            
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"Check Your UserId and Password" duration:2.0 position:CSToastPositionCenter style:style];
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [alert show];
            
        }
        
    }
    
    
    
}

-(void)tapOnTvView:(id) sender{
    
    if (self.playPauseButton.hidden) {
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        [self.playPauseButton setHidden:NO];
        
        
    }
    else{
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        
    }
}

-(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
}


#pragma mark RefreshToken

-(void)RefreshToken{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/refreshCustomerToken",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&deviceId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],deviceID];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                NSLog(@"Customer Does Not Exist");
            }
            
            else{
                NSString *token = [sDic objectForKey:@"token"];
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}



-(void)goToRootView
{
    //    _loginTimer = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [moviePlayerController.moviePlayer stop];
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}



#pragma mark BarButton Actions


- (IBAction)orderDecrementTaped:(id)sender {
    IncrementOrDecrementTapped = YES;
    IncrementArrayBool = YES;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.OrderListTableView];
    NSIndexPath *tappedIP = [self.OrderListTableView indexPathForRowAtPoint:buttonPosition];
    selectedItemIndexPath = tappedIP;
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:selectedItemIndexPath.row inSection:selectedItemIndexPath.section];
    
    profilePosition = newIndexPath.row;
    orderDecrementBool = true;
    orderIncrementBool = false;
    
    //for (int i=0; i<[orderNameListArray count]; i++) {
    if (countTagValue == profilePosition) {
        countValue =[[incrementValueArray objectAtIndex:profilePosition] intValue];
        countValue -= 1;
        incrementedCountValue = countValue;
        decrementString = [NSString stringWithFormat:@"%ld", (long)countValue];
    }else{
        incrementedCountValue =[[incrementValueArray objectAtIndex:profilePosition] intValue];
        incrementedCountValue-=1;
        decrementString = [NSString stringWithFormat:@"%ld", (long)incrementedCountValue];
    }
    //}
    
    [countArray replaceObjectAtIndex:profilePosition withObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(int)incrementedCountValue]]];
    
    [_OrderListTableView reloadData];
    
}

- (IBAction)orderIncrementTapped:(id)sender {
    IncrementOrDecrementTapped = YES;
    IncrementArrayBool = YES;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.OrderListTableView];
    NSIndexPath *tappedIP = [self.OrderListTableView indexPathForRowAtPoint:buttonPosition];
    selectedItemIndexPath = tappedIP;
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:selectedItemIndexPath.row inSection:selectedItemIndexPath.section];
    profilePosition = newIndexPath.row;
    
    orderDecrementBool = false;
    orderIncrementBool = true;
    
    if (countTagValue == profilePosition) {
        //*****Converting the ArrayValue to IntValue and storing in CountValue *****//
        countValue =[[incrementValueArray objectAtIndex:profilePosition] intValue];
        countValue += 1;
        incrementedCountValue = countValue;
        incrementString = [NSString stringWithFormat:@"%ld", (long)countValue];
    }else{
        incrementedCountValue =[[incrementValueArray objectAtIndex:profilePosition] intValue];
        incrementedCountValue+=1;
        incrementString = [NSString stringWithFormat:@"%ld", (long)incrementedCountValue];
    }
    //incrementedCountValue =[incrementString intValue];
    [countArray replaceObjectAtIndex:profilePosition withObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(int)incrementedCountValue]]];
    
    [_OrderListTableView reloadData];
    
    
    
}

- (IBAction)homeBarPressed:(id)sender {
    
    [moviePlayerController.moviePlayer stop];
    [self dismissViewControllerAnimated:YES completion:nil];
    vegOrNonVegTapped = false;
    drinkTapped = false;
    cuisineTapped = false;
    houseKeepingCellTapped = NO;
    concergeCellTapped = NO;
    
}

- (IBAction)backButtonPressed:(id)sender {
    CGRect newFrame = _smrtHotelselectedView.frame;
    CGRect newFrame1 = _spaSelectedView.frame;
    vegOrNonVegTapped = false;
    drinkTapped = false;
    cuisineTapped = false;
    houseKeepingCellTapped = NO;
    concergeCellTapped = NO;
    [orderNameListArray removeAllObjects];
    //    if (self.ODTableView.hidden == false)
    //    {
    //        self.tourODTableView.hidden = false;
    //        self.ODTableView.hidden = true;
    //        self.searchBar.hidden = true;
    //    }
    
    if(self.spaSelectedView.hidden == false)
    {
        self.spaSelectedView.hidden = true;
        self.ODCollectionView.hidden = false;
        _collectionViewTopContsraint.constant = 0;
        if (forPlayerStop == YES) {
            [moviePlayerController.moviePlayer stop];
            //_orderDisplayView.hidden = YES;
            
        }else
        {
            _playPauseView.hidden = NO;
            _orderDisplayView.hidden = YES;
            forPlayerStop= NO;
            [moviePlayerController.moviePlayer play];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            [orderNameListArray removeAllObjects];
        }
        
    }
    
    else if(self.smrtHotelselectedView.hidden == false)
    {
        if (forPlayerStop == YES) {
            [moviePlayerController.moviePlayer stop];
            //_orderDisplayView.hidden = YES;
            
        }else
        {
            _playPauseView.hidden = NO;
            _orderDisplayView.hidden = YES;
            forPlayerStop= NO;
            [moviePlayerController.moviePlayer play];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            [orderNameListArray removeAllObjects];
        }
        
        self.smrtHotelselectedView.hidden = true;
        if (self.MinimarketCollview.hidden==false) {
            
        }else{
        self.hotelCollectionView.hidden = false;
        _hotelcollectionviewTopConstraint.constant = 0;
    }
    }
    
    else if (self.hotelCollectionView.hidden == false)
    {
        if (forPlayerStop == YES) {
            [moviePlayerController.moviePlayer stop];
            //_orderDisplayView.hidden = YES;
            
        }else
        {
            _playPauseView.hidden = NO;
            _orderDisplayView.hidden = YES;
            forPlayerStop= NO;
            [moviePlayerController.moviePlayer play];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            [orderNameListArray removeAllObjects];
        }
        //self.smrtHotelSegmentTap.hidden = true;
        self.menuView.hidden = true;
        self.hotelCollectionView.hidden = true;
        _firstPageView.hidden = false;
        self.tourODTableView.hidden = false;
        _playPauseView.hidden = NO;
        _orderDisplayView.hidden = YES;
        forPlayerStop= NO;
        [moviePlayerController.moviePlayer play];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        [orderNameListArray removeAllObjects];
        
        
    }
    
    else if(self.ODCollectionView.hidden == false){
        self.ODCollectionView.hidden = true;
        self.spaView.hidden = true;
        _orderDisplayView.hidden = YES;
        _playPauseView.hidden = NO;
        _firstPageView.hidden = false;
        self.tourODTableView.hidden = false;
        forPlayerStop= NO;
        [moviePlayerController.moviePlayer play];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        [orderNameListArray removeAllObjects];
        
    }
    
    
    else if (self.houseKeepingCollectionView.hidden == false)
    {
        if (forPlayerStop == YES) {
            [moviePlayerController.moviePlayer stop];
            
        }else
        {
            _playPauseView.hidden = NO;
            _orderDisplayView.hidden = YES;
            forPlayerStop= NO;
            [moviePlayerController.moviePlayer play];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        }
        //self.smrtHotelSegmentTap.hidden = true;
        self.menuView.hidden = true;
        //           self.hotelCollectionView.hidden = true;
        
        self.houseKeepingCollectionView.hidden = true;
        self.houseKeepingView.hidden = true;
        _firstPageView.hidden = false;
        self.tourODTableView.hidden = false;
        _playPauseView.hidden = NO;
        _orderDisplayView.hidden = YES;
        forPlayerStop= NO;
        [moviePlayerController.moviePlayer play];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        
        
    }
    else if (self.concergeCollectionView.hidden == false)
    {
        if (forPlayerStop == YES) {
            [moviePlayerController.moviePlayer stop];
            
        }else
        {
            _playPauseView.hidden = NO;
            _orderDisplayView.hidden = YES;
            forPlayerStop= NO;
            [moviePlayerController.moviePlayer play];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        }
        //           self.smrtHotelSegmentTap.hidden =true;
        //           self.hotelCollectionView.hidden = true;
        self.concergeCollectionView.hidden = true;
        self.concergeView.hidden = true;
        
        _firstPageView.hidden = false;
        self.tourODTableView.hidden = false;
        _playPauseView.hidden = NO;
        _orderDisplayView.hidden = YES;
        forPlayerStop= NO;
        [moviePlayerController.moviePlayer play];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        
        
    }else if (self.MinimarketCollview.hidden==false){
        
        if (forPlayerStop == YES) {
            [moviePlayerController.moviePlayer stop];
            
        }else
        {
            _playPauseView.hidden = NO;
            _orderDisplayView.hidden = YES;
            forPlayerStop= NO;
            [moviePlayerController.moviePlayer play];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        }
        //self.smrtHotelSegmentTap.hidden = true;
        self.Minimarketmenu.hidden = true;
        //           self.hotelCollectionView.hidden = true;
        
        self.MinimarketCollview.hidden = true;
      //  self.houseKeepingView.hidden = true;
       // _firstPageView.hidden = false;
        //self.tourODTableView.hidden = false;
        _playPauseView.hidden = NO;
        //_orderDisplayView.hidden = YES;
        forPlayerStop= NO;
        [moviePlayerController.moviePlayer play];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        
    }
    
    else{
        //        if (self.ODCollectionView.hidden == true)
        //        {
        //            [moviePlayerController.moviePlayer stop];
        //            [self dismissViewControllerAnimated:YES completion:nil];
        //        }
        if (self.tourODTableView.hidden == false)
        {
            [moviePlayerController.moviePlayer stop];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            self.ODCollectionView.hidden = true;
            self.spaView.hidden = true;
            
            // self.segmentedTap.hidden = true;
            [self setNavigationTitle:@"Video On Demand"];
            //   self.searchBar.hidden = false;
            // self.ODTableView.hidden = false;
        }
        
        if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused)
        {
            [moviePlayerController.moviePlayer pause];
        }
    }
    
}

- (IBAction)playAndPausePressed:(id)sender
{
    if (moviePlayerController.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
        //   [moviePlayerController.moviePlayer setContentURL:[NSURL URLWithString:stringFromUrl]];
        [moviePlayerController.moviePlayer play];
        playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    }
    
    else{
        [moviePlayerController.moviePlayer pause];
        [playButtonTimer invalidate];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(actionSheet.tag == 1000){
        
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            NSMutableArray *cusineFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"cuisineArrayKey"];
            NSString *cusineName = [cusineFilerNameArray objectAtIndex:buttonIndex];
            
            
            NSString *drinksName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedDrinkSegmentButtonIndex"];
            NSString *vegNonVegName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedVegNonVegSegmentButtonIndex"];
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectDrinkSegment"]  isEqual: @"true"])
            {
                param3= drinksName;
                
            }else{
                param3 = @"Nr62WhRjzaLSJkvB";
            }
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectVegNonVegSegment"]  isEqual: @"true"])
            {
                param4 = vegNonVegName;
                
            }else{
                param4 = @"Nr62WhRjzaLSJkvB";
            }
            
            
            param1 = @"false";
            param2 = cusineName;
            
            @try {
                NSString *post = [NSString stringWithFormat:@"retrive=%@&cuisine=%@&drinks=%@&vegnonveg=%@",param1,param2,param3,param4]; // <--here put the request parameters you used to get the response
                
                
                NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
                
                
                NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContent1",[[SprngIPConf getSharedInstance]getIpForImage]];
                str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:str];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                
                
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                 
                 {
                     if (!(connectionError))
                     {
                         //                         NSURLResponse *response;
                         //                         NSError *error;
                         //                         NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                         
                         NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                         
                         tourRoomServiceArray =[responseDic valueForKey:@"root"];
                         NSLog(@"%@", tourRoomServiceArray);
                         //[_ODCollectionView reloadData];
                         
                         NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                         [dic setObject:[tourRoomServiceArray valueForKey:@"root"] forKey:@"status"];
                         NSDictionary *dict = [[NSDictionary alloc]init];
                         
                         dict = tourRoomServiceArray[0];
                         NSDictionary *roomId = [tourRoomServiceArray valueForKey:@"RoomServiceId"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:roomId forKey:@"CuisineRoomServiceIdKey"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         cuisineTapped = true;
                         drinkTapped = false;
                         vegOrNonVegTapped = false;
                         
                         if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                         {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                             [alert show];
                             
                         }
                         else{
                             [_hotelCollectionView reloadData];
                         }
                         
                         [[NSUserDefaults standardUserDefaults] setObject:cusineName forKey:@"SelectedCuisineSegmentButtonIndex"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"didSelectCuisineSegment"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         
                     }
                     else
                     {
                         
                     }
                 }];
                
            } @catch (NSException *exception) {
                NSLog(@"");
            } @finally {
                NSLog(@"");
            }
            
        }
        
    }
    else if(actionSheet.tag == 2000)
        
    {
        
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            NSString *cuisineName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCuisineSegmentButtonIndex"];
            
            NSMutableArray *drinkNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"drinkArrayKey"];
            NSString *drinkName = [drinkNameArray objectAtIndex:buttonIndex];
            
            
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectCuisineSegment"]  isEqual: @"true"])
            {
                param2 = cuisineName;
                
            }else{
                param2 = @"Nr62WhRjzaLSJkvB";
            }
            
            
            
            param1 = @"false";
            param3 = drinkName;
            param4 = @"Nr62WhRjzaLSJkvB";
            
            NSString *post = [NSString stringWithFormat:@"retrive=%@&cuisine=%@&drinks=%@&vegnonveg=%@",param1,param2,param3,param4]; // <--here put the request parameters you used to get the response
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            @try {
                NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContent1",[[SprngIPConf getSharedInstance]getIpForImage]];
                str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:str];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                 
                 {
                     if (!(connectionError))
                     {
                         
                         //                         NSURLResponse *response;
                         //                         NSError *error;
                         //                         NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                         
                         NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                         
                         tourRoomServiceArray =[responseDic valueForKey:@"root"];
                         NSLog(@"%@", tourRoomServiceArray);
                         //[_ODCollectionView reloadData];
                         
                         NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                         [dic setObject:[tourRoomServiceArray valueForKey:@"root"] forKey:@"status"];
                         NSDictionary *dict = [[NSDictionary alloc]init];
                         
                         dict = tourRoomServiceArray[0];
                         NSDictionary *roomId = [tourRoomServiceArray valueForKey:@"RoomServiceId"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:roomId forKey:@"DrinkRoomServiceIdKey"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         drinkTapped = true;
                         cuisineTapped = false;
                         vegOrNonVegTapped = false;
                         
                         if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                         {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                             [alert show];
                             
                         }
                         else{
                             [_hotelCollectionView reloadData];
                         }
                         
                         [[NSUserDefaults standardUserDefaults] setObject:drinkName forKey:@"SelectedDrinkSegmentButtonIndex"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"didSelectDrinkSegment"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         
                     }
                     else
                     {
                         
                     }
                 }];
                
            } @catch (NSException *exception) {
                NSLog(@"");
            } @finally {
                NSLog(@"");
            }
            
        }
        
        
    }
    
    else if (actionSheet.tag == 3000)
    {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            NSString *cuisineName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCuisineSegmentButtonIndex"];
            
            NSString *drinksName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedDrinkSegmentButtonIndex"];
            
            NSMutableArray *vegnonVegNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"vegNonVegArrayKey"];
            NSString *vegNonVegName = [vegnonVegNameArray objectAtIndex:buttonIndex];
            
            
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectCuisineSegment"]  isEqual: @"true"])
            {
                param2 = cuisineName;
                
            }else{
                param2 = @"Nr62WhRjzaLSJkvB";
            }
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectDrinkSegment"]  isEqual: @"true"]) {
                param3 = drinksName;
            }
            else{
                param3 =@"Nr62WhRjzaLSJkvB";
            }
            
            param1 = @"false";
            param4 = vegNonVegName;
            
            NSString *post = [NSString stringWithFormat:@"retrive=%@&cuisine=%@&drinks=%@&vegnonveg=%@",param1,param2,param3,param4]; // <--here put the request parameters you used to get the response
            
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            
            @try {
                NSString *str =[NSString stringWithFormat:@"%@/index1.php/fetchRoomServiceContent1",[[SprngIPConf getSharedInstance]getIpForImage]];
                str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:str];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                
                
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                 
                 {
                     if (!(connectionError))
                     {
                         //                         NSURLResponse *response;
                         //                         NSError *error;
                         //                         NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                         
                         NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                         
                         tourRoomServiceArray =[responseDic valueForKey:@"root"];
                         NSLog(@"%@", tourRoomServiceArray);
                         //[_ODCollectionView reloadData];
                         
                         NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                         [dic setObject:[tourRoomServiceArray valueForKey:@"root"] forKey:@"status"];
                         NSDictionary *dict = [[NSDictionary alloc]init];
                         
                         dict = tourRoomServiceArray[0];
                         NSDictionary *roomId = [tourRoomServiceArray valueForKey:@"RoomServiceId"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:roomId forKey:@"VegNonVegRoomServiceIdKey"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         vegOrNonVegTapped = true;
                         drinkTapped = false;
                         cuisineTapped = false;
                         
                         if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                         {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                             [alert show];
                             
                         }
                         else{
                             [_hotelCollectionView reloadData];
                         }
                         
                         [[NSUserDefaults standardUserDefaults] setObject:vegNonVegName forKey:@"SelectedVegNonVegSegmentButtonIndex"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"didSelectVegNonVegSegment"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                     }
                     
                     else{
                         
                     }
                 }];
                
            } @catch (NSException *exception) {
                NSLog(@"");
            } @finally {
                NSLog(@"");
            }
        }
    }else if (actionSheet.tag==100){
        
                    if (buttonIndex != actionSheet.cancelButtonIndex) {
                        NSMutableArray *cusineFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"productArrayKey"];
                        NSDictionary *cusineName = [cusineFilerNameArray objectAtIndex:buttonIndex];
        
                        NSString *param0=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"];
                        NSString *param1=@"BRNC001";
                        NSString *param2=@"false";
                        NSString *param3=[cusineName objectForKey:@"filterName"];
                        NSString *param4=@"Nr62WhRjzaLSJkvB";
                        NSString *param5=@"Nr62WhRjzaLSJkvB";
                        
                        NSString *post = [NSString stringWithFormat:@"fetchBranchePosters=%@&branchId=%@&retrieve=%@&branchProductName=%@&promotionsName=%@&brandName=%@",param0,param1,param2,param3,param4,param5];
                        
                        NSString *conciergeStr =[NSString stringWithFormat:@"%@/index.php/?%@",[[SprngIPConf getSharedInstance]getIpForImage],post];
                        
                        string1 = conciergeStr;
                        [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *url = [NSURL URLWithString:conciergeStr];
                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                        NSLog(@"token%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
                        
                        [request setHTTPMethod:@"GET"];
                        
                        
                        [NSURLConnection sendAsynchronousRequest:request
                                                           queue:[NSOperationQueue mainQueue]
                                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                         {
                             if (!(connectionError))
                             {
                                 
                                 NSError *error = nil;
                                 NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                                 proudutsfilte = [receivedData objectForKey:@"root"];
                                 
                                // conciergeArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"branchImage"];
                                 conciergeArray=proudutsfilte;
                                 // proudutsfilte=[conciergeArray valueForKey:@"productSubFilters"];
                                              //   NSDictionary *conciergeIdImages = [conciergeArray valueForKey:@"branchImage"];
                                 //                 NSDictionary *branchProductName=[conciergeArray valueForKey:@"branchProductName"];
                                 //                 NSDictionary *price=[conciergeArray valueForKey:@"price"];
                                 //[[NSUserDefaults standardUserDefaults]setValue:proudutsfilter forKey:@"productSubFilters"];
                                 // [[NSUserDefaults standardUserDefaults] setObject:proudutsfilter forKey:@"productSubFilters"];
                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                  [self.MinimarketCollview reloadData];
                                 
                                 NSLog(@"%@",conciergeArray);
                                 
                                 
                                 
                                 //NSDictionary * dic = tourLibraryListArray[indexPath.row];
                                 // NSString *name = [dic objectForKey:@"libraryName"];
                                 //[self setNavigationTitle:name];
                             }
                             else{
                                 
                             }
                         }];
                    
//                    } @catch (NSException *exception) {
//                        NSLog(@"");
//                    } @finally {
//                        NSLog(@"");
//                    }
                    //}
}
    
        
    }else if (actionSheet.tag==200){
        
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            NSMutableArray *cusineFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"brandArrayKey"];
            NSDictionary *cusineName = [cusineFilerNameArray objectAtIndex:buttonIndex];
            
            NSString *param0=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"];
            NSString *param1=@"BRNC001";
            NSString *param2=@"false";
            NSString *param5=[cusineName objectForKey:@"filterName"];
            NSString *param3=@"Nr62WhRjzaLSJkvB";
            NSString *param4=@"Nr62WhRjzaLSJkvB";
            
            NSString *post = [NSString stringWithFormat:@"fetchBranchePosters=%@&branchId=%@&retrieve=%@&branchProductName=%@&promotionsName=%@&brandName=%@",param0,param1,param2,param3,param4,param5];
            
            NSString *conciergeStr =[NSString stringWithFormat:@"%@/index.php/?%@",[[SprngIPConf getSharedInstance]getIpForImage],post];
            
            string1 = conciergeStr;
            [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:conciergeStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            NSLog(@"token%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
            
            [request setHTTPMethod:@"GET"];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                     proudutsfilte = [receivedData objectForKey:@"root"];
                     
                     // conciergeArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"branchImage"];
                     conciergeArray=proudutsfilte;
                     // proudutsfilte=[conciergeArray valueForKey:@"productSubFilters"];
                     //   NSDictionary *conciergeIdImages = [conciergeArray valueForKey:@"branchImage"];
                     //                 NSDictionary *branchProductName=[conciergeArray valueForKey:@"branchProductName"];
                     //                 NSDictionary *price=[conciergeArray valueForKey:@"price"];
                     //[[NSUserDefaults standardUserDefaults]setValue:proudutsfilter forKey:@"productSubFilters"];
                     // [[NSUserDefaults standardUserDefaults] setObject:proudutsfilter forKey:@"productSubFilters"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     [self.MinimarketCollview reloadData];
                     
                     NSLog(@"%@",conciergeArray);
                     
                     
                     
                     //NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     // NSString *name = [dic objectForKey:@"libraryName"];
                     //[self setNavigationTitle:name];
                 }
                 else{
                     
                 }
             }];
        }
    }else if (actionSheet.tag==30){
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            NSMutableArray *cusineFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"promotionArrayKey"];
            NSDictionary *cusineName = [cusineFilerNameArray objectAtIndex:buttonIndex];
            
            NSString *param0=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"];
            NSString *param1=@"BRNC001";
            NSString *param2=@"false";
            NSString *param4=[cusineName objectForKey:@"filterName"];
            NSString *param3=@"Nr62WhRjzaLSJkvB";
            NSString *param5=@"Nr62WhRjzaLSJkvB";
            
            NSString *post = [NSString stringWithFormat:@"fetchBranchePosters=%@&branchId=%@&retrieve=%@&branchProductName=%@&promotionsName=%@&brandName=%@",param0,param1,param2,param3,param4,param5];
            
            NSString *conciergeStr =[NSString stringWithFormat:@"%@/index.php/?%@",[[SprngIPConf getSharedInstance]getIpForImage],post];
            
            string1 = conciergeStr;
            [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:conciergeStr];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            NSLog(@"token%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
            
            [request setHTTPMethod:@"GET"];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 if (!(connectionError))
                 {
                     
                     NSError *error = nil;
                     NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                     proudutsfilte = [receivedData objectForKey:@"root"];
                     
                     // conciergeArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"branchImage"];
                     conciergeArray=proudutsfilte;
                     // proudutsfilte=[conciergeArray valueForKey:@"productSubFilters"];
                     //   NSDictionary *conciergeIdImages = [conciergeArray valueForKey:@"branchImage"];
                     //                 NSDictionary *branchProductName=[conciergeArray valueForKey:@"branchProductName"];
                     //                 NSDictionary *price=[conciergeArray valueForKey:@"price"];
                     //[[NSUserDefaults standardUserDefaults]setValue:proudutsfilter forKey:@"productSubFilters"];
                     // [[NSUserDefaults standardUserDefaults] setObject:proudutsfilter forKey:@"productSubFilters"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     [self.MinimarketCollview reloadData];
                     
                     NSLog(@"%@",conciergeArray);
                     
                     
                     
                     //NSDictionary * dic = tourLibraryListArray[indexPath.row];
                     // NSString *name = [dic objectForKey:@"libraryName"];
                     //[self setNavigationTitle:name];
                 }
                 else{
                     
                 }
             }];
        
    }
    
            
    }
        
    }


- (IBAction)playButtonAction:(id)sender {
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
    
    [self playTVWithValue];
}




- (IBAction)hotelDescriptionButtonPress:(id)sender {
    
    CGRect newFrame = _smrtHotelselectedView.frame;
    
    if (_hotelSelectedDescriptionButton.tag == 0)
    {
        _hotelCollectionView.hidden = true;
        [self.hotelSelectedDescriptionButton setImage:[UIImage imageNamed:@"plus-highlighted.png"] forState:UIControlStateNormal];
        newFrame.size.height = 500;
        self.hotelDescriptionTextView.hidden =false;
        [self.smrtHotelselectedView addSubview:_hotelDescriptionTextView];
        _smrtHotelselectedView.frame = newFrame;
        _hotelSelectedDescriptionButton.tag = 1;
    }
    else if(_hotelSelectedDescriptionButton.tag == 1){
        
        self.hotelCollectionView.hidden = false;
        [self.hotelSelectedDescriptionButton setImage:[UIImage imageNamed:@"Lplus.png"] forState:UIControlStateNormal];
        newFrame.size.height = 132;
        _smrtHotelselectedView.frame = newFrame;
        self.hotelDescriptionTextView.hidden =true;
        _hotelSelectedDescriptionButton.tag = 0;
        
    }
    
}


- (IBAction)spaSelectedPressed:(id)sender {
    
    
    CGRect newFrame = _spaSelectedView.frame;
    
    if (_selectedDiscriptionButton.tag == 0) {
        
        self.ODCollectionView.hidden = true;
        [self.selectedDiscriptionButton setImage:[UIImage imageNamed:@"plus-highlighted.png"] forState:UIControlStateNormal];
        newFrame.size.height = 500;
        self.DescriptionTextView.hidden =false;
        [self.spaSelectedView addSubview:_DescriptionTextView];
        _spaSelectedView.frame = newFrame;
        
        
        _selectedDiscriptionButton.tag = 1;
    }
    else if (_selectedDiscriptionButton.tag == 1)
    {
        self.ODCollectionView.hidden = false;
           // [self.selectedDiscriptionButton setImage:[UIImage imageNamed:@"Lplus.png"] forState:UIControlStateNormal];
        [self.selectedDiscriptionButton setImage:[UIImage imageNamed:@"Lplus.png"] forState:UIControlStateNormal];
        newFrame.size.height = 132;
        _spaSelectedView.frame = newFrame;
        self.DescriptionTextView.hidden =true;
        _selectedDiscriptionButton.tag = 0;
        
        
    }
    
}
- (IBAction)cuisinePressed:(id)sender {
    
    
    //  [self postdata2];
    [self smartHotePostData];
    
    [self.cuisineBtn setTitleColor:[UIColor colorWithRed:(141.0/255.0) green:(28.0/255.0) blue:(88.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.drinksBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.vegNonVegBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    cuisineSubFilters = [roomServiceArray valueForKey:@"criterionSubFilters"];
    NSDictionary *dict = roomServiceArray[0];
    NSMutableArray *array1 = [dict objectForKey: @"criterionSubFilters"];
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (NSString *title in array1) {
        [actionSheet addButtonWithTitle:title];
    }
    
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    [[NSUserDefaults standardUserDefaults] setObject:array1 forKey:@"cuisineArrayKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.smrtHotelSegmentTap setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]}  forState:UIControlStateSelected];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    actionSheet.tag = 1000;
    
}

- (IBAction)drinkBtnPressed:(id)sender {
    
    // [self postdata2];
    [self smartHotePostData];
    
    [self.cuisineBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.drinksBtn setTitleColor:[UIColor colorWithRed:(141.0/255.0) green:(28.0/255.0) blue:(88.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.vegNonVegBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    cuisineSubFilters = [roomServiceArray valueForKey:@"drinksSubFilters"];
    
    NSDictionary *dict = roomServiceArray[1];
    NSMutableArray *array2 = [dict objectForKey: @"drinksSubFilters"];
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (NSString *title in array2) {
        [actionSheet addButtonWithTitle:title];
    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    [[NSUserDefaults standardUserDefaults] setObject:array2 forKey:@"drinkArrayKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [self.smrtHotelSegmentTap setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]}  forState:UIControlStateSelected];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    
    actionSheet.tag = 2000;
}

- (IBAction)vegNonBtnPressed:(id)sender {
    
    //  [self postdata2];
    [self smartHotePostData];
    
    [self.cuisineBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.drinksBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.vegNonVegBtn setTitleColor:[UIColor colorWithRed:(141.0/255.0) green:(28.0/255.0) blue:(88.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    vegnonvegSubFilters = [roomServiceArray valueForKey:@"vegSubFilters"];
    
    NSDictionary *dict = roomServiceArray[2];
    NSMutableArray *array3 = [dict objectForKey: @"vegSubFilters"];
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (NSString *title in array3) {
        [actionSheet addButtonWithTitle:title];
    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    
    [[NSUserDefaults standardUserDefaults] setObject:array3 forKey:@"vegNonVegArrayKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.smrtHotelSegmentTap setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]}  forState:UIControlStateSelected];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    
    actionSheet.tag = 3000;
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)spaHeartButtonPressed:(id)sender {
    
    if (_spaStarBtn.tag == 0) {
        _spaStarBtn.tag = 1;
        
        [self.spaStarBtn setImage:[UIImage imageNamed:@"star-highlighted.png"] forState:UIControlStateNormal];
        // do action for button with tag 1
    } else if (_spaStarBtn.tag == 1) {
        _spaStarBtn.tag = 0;
        [self.spaStarBtn setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        // do action for button with tag 2
    } //
    
}
- (IBAction)smrtHotelHeartButtonPressed:(id)sender {
    
    if (_smrtHotelStarBtn.tag == 0) {
        _smrtHotelStarBtn.tag = 1;
        
        [self.smrtHotelStarBtn setImage:[UIImage imageNamed:@"star-highlighted.png"] forState:UIControlStateNormal];
        // do action for button with tag 1
    } else if (_smrtHotelStarBtn.tag == 1) {
        _smrtHotelStarBtn.tag = 0;
        [self.smrtHotelStarBtn setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        // do action for button with tag 2
    } //
    
}

- (IBAction)smrtHotelDollerPressed:(id)sender {
    IncrementOrDecrementTapped = NO;
    if (IncrementArrayBool == YES) {
        IncrementArrayBool = NO;
        countValue = 1;
    }else{
        incrementValueArray =[[NSMutableArray alloc]init];
    }
    spaOrderBtnTapped = NO;
    roomServiceOrderBtnTapped = YES;
    [orderNameListArray addObject:orderName];
    NSLog(@"orderNameListArray: %@",orderNameListArray);
    [countArray addObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:(int)countValue]]];
    _orderDisplayView.hidden = NO;
    [_OrderListTableView reloadData];
    [moviePlayerController.moviePlayer stop];
    _playPauseView.hidden = YES;
    //_smrtHotelselectedView.hidden=YES;
    
    //_smartHotelSiva.hidden=YES;

    
    if (self.hotelCollectionView.hidden==false) {
        [self.hotelCollectionView  reloadData];
        //  [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"roomServiceOrderBtnTapped"];
    }else if (self.MinimarketCollview.hidden==false){
       
      
        //[[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"MInIMARket"];
//        MInIMARket
//        roomServiceOrderBtnTapped
//        spaOrderBtnTapped
       
  
        [self.MinimarketCollview reloadData];
    }
   // [self.MinimarketCollview reloadData];
    //[[ISafe shared] presentOn:self.navigationController];
    
    [self.smrtHotelRoderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];
    //    NSString *message = [respeDict valueForKey:@"roomContactInfo"];
    /*
    IncrementOrDecrementTapped = NO;
    if (IncrementArrayBool == YES) {
        IncrementArrayBool = NO;
        countValue = 1;
    }else{
        incrementValueArray =[[NSMutableArray alloc]init];
    }
    spaOrderBtnTapped = NO;
    roomServiceOrderBtnTapped = YES;
   
    [arr addObject:orderName];
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arr];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    NSLog(@"orderNameListArray: %@",orderNameListArray);
    [orderNameListArray addObjectsFromArray:arrayWithoutDuplicates];
    [countArray addObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:(int)countValue]]];
    _orderDisplayView.hidden = NO;
    
    [_OrderListTableView reloadData];
    [moviePlayerController.moviePlayer stop];
    _playPauseView.hidden = YES;
    self.smrtHotelselectedView.hidden=true;

    //[[ISafe shared] presentOn:self.navigationController];
    
    [self.smrtHotelRoderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];
//
//    if (self.MinimarketCollview.hidden==false) {
//         [self.MinimarketCollview reloadData];
//    }else{
//        self.hotelCollectionView.hidden = false;
//        [self.hotelCollectionView reloadData];
//    }
  
    //    NSString *message = [respeDict valueForKey:@"roomContactInfo"];
    
    //    UIAlertView *smrtHotelAlert = [[UIAlertView alloc] initWithTitle:@"Room Service" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //    smrtHotelAlert.tag = 0;
    //    [smrtHotelAlert show];
  */
}


- (IBAction)spaDollerButtenPressed:(id)sender {
    IncrementOrDecrementTapped = NO;
    if (IncrementArrayBool == YES) {
        IncrementArrayBool = NO;
        countValue = 1;
    }else{
        incrementValueArray =[[NSMutableArray alloc]init];
    }
    spaOrderBtnTapped = YES;
    roomServiceOrderBtnTapped = NO;
    //[Custprice addObject:strprice];
    PriceCustomArr=[[NSMutableArray alloc]init];
    [orderNameListArray addObject:orderName];
//    [PriceCustomArr addObject:strprice];
    NSLog(@"orderNameListArray: %@",orderNameListArray);
    //NSIndexPath *indexpath;
//    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:orderNameListArray];
//    NSArray *arrayWithoutDuplicates = [orderedSet array];
    [countArray addObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:(int)countValue]]];
     //NSDictionary *resDic = spaArray[indexpath.row];
    _orderDisplayView.hidden = NO;
    [_OrderListTableView reloadData];
    [moviePlayerController.moviePlayer stop];
    _playPauseView.hidden = YES;
     [self.spaRorderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];
    self.spaSelectedView.hidden = true;
    self.ODCollectionView.hidden = false;
    [self.ODCollectionView reloadData];
    
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 0) {
        if (buttonIndex == 0) {
            [self.smrtHotelRoderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];
        }
        
    }
    else if (alertView.tag == 1){
        if (buttonIndex == 0) {
            [self.spaRorderNowBtn setImage:[UIImage imageNamed:@"order_btn.png"] forState:UIControlStateNormal];
            
        }
    }
}
- (IBAction)orderCancelAction:(id)sender {
    _playPauseView.hidden = NO;
    _orderDisplayView.hidden = YES;
    
    if (networkStatus == NotReachable) {
        [self.navigationController.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    self.playPauseButton.hidden = true;
    [moviePlayerController.moviePlayer setControlStyle:MPMovieControlStyleNone];
    [moviePlayerController.moviePlayer play];
    
    [orderNameListArray removeAllObjects];
    
}
- (IBAction)orderConfrimAction:(id)sender {
    //****** forPlayerStop BOOL Value is for stoping the player when close pressed in FreshChatSDK *****//
    forPlayerStop = YES;
    
    NSMutableString *completeOrder =[NSMutableString string];
salesOrders =[NSMutableString string];
    NSString *order;
    NSString *orderFinalCount;
    //NSString *serialNumber;
    
    if(houseKeepingCellTapped == YES || concergeCellTapped == YES)
    {
        for(int i = 0; i < [orderNameListArray count]; i++){
            order =[orderNameListArray objectAtIndex:i];
            
         
        }
        [salesOrders appendFormat:@"Sales Orders:\n %@", order];
        NSLog(@"SalesOrders %@",salesOrders);
    }else
    {
        [Custprice removeAllObjects];
        for(int i = 0; i < [orderNameListArray count]; i++){
            order =[orderNameListArray objectAtIndex:i];
            orderFinalCount=[countArray objectAtIndex:i];
         
            NSString *pricelist=strprice;

                pricelist = [pricelist stringByReplacingOccurrencesOfString:@"$" withString:@""];
         
            NSString *a = pricelist;
            NSInteger b = [a integerValue];
            NSString *c = orderFinalCount;
            NSInteger d = [c integerValue];
           NSString *totalprice=[NSString stringWithFormat: @"%ld", b*d];
     
            NSString *combined =[NSString stringWithFormat:@"%@ %@",order,orderFinalCount];
            [completeOrder appendFormat:@"%@\n", combined];
            NSLog(@"completeOrder %@",completeOrder);
            //NSString *pricestring =[PriceCustomArr objectAtIndex:i];
        [Custprice addObject:totalprice];
        }
        [salesOrders appendFormat:@"Sales Orders:\n %@", completeOrder];
        NSLog(@"SalesOrders %@",salesOrders);
 
    }
    
    //***** Calling different FreshChat with different UserMessages *****//
    
    if (roomServiceOrderBtnTapped == YES) {
        
    
        if (self.MinimarketCollview.hidden==false) {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"MInIMARket"];
             [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"roomServiceOrderBtnTapped"];
            
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"spaOrderBtnTapped"];
         
            
        }else{
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"roomServiceOrderBtnTapped"];
            
        
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"spaOrderBtnTapped"];
            
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"MInIMARket"];
        }

        PointsViewcontrollwer *socialVC = (PointsViewcontrollwer *)[self.storyboard instantiateViewControllerWithIdentifier:@"Wallet"];
        socialVC.CountARR=countArray;
        socialVC.ArrTitle=orderNameListArray;
        socialVC.Pricearr=Custprice;
        [self presentViewController:socialVC animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]setObject:salesOrders forKey:@"SalesOrder"];
        //***** [[Freshchat sharedInstance] showConversations:] method --END--*****/
    }else if (spaOrderBtnTapped == YES)
    {

        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"spaOrderBtnTapped"];
       [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"roomServiceOrderBtnTapped"];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"MInIMARket"];
       
        PointsViewcontrollwer *socialVC = (PointsViewcontrollwer *)[self.storyboard instantiateViewControllerWithIdentifier:@"Wallet"];
        socialVC.CountARR=countArray;
        socialVC.ArrTitle=orderNameListArray;
        socialVC.Pricearr=Custprice;
        [self presentViewController:socialVC animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]setObject:salesOrders forKey:@"SalesOrder"];
        
  
        
    }else if (houseKeepingCellTapped == YES)
    {
        //***** To bring up the Freshchat’s Conversations List or single conversation overlay at any point, you need to use -[[Freshchat sharedInstance] showConversations:] method  --START--*****//
        FreshchatMessage *userMessage = [[FreshchatMessage alloc] initWithMessage:salesOrders andTag:@"housekeeping"];
        [[Freshchat sharedInstance] sendMessage:userMessage];//send message api call
        ConversationOptions *options = [ConversationOptions new];
        [options filterByTags:@[@"housekeeping"] withTitle:@"House Keeping"];
        [[Freshchat sharedInstance] showConversations:self withOptions: options];
      // method --END--*****//
        
    }else if (concergeCellTapped == YES)
    {
//***** To bring up the Freshchat’s Conversations List or single conversation overlay at any point, you need to use -[[Freshchat sharedInstance] showConversations:] method  --START--*****//
        FreshchatMessage *userMessage = [[FreshchatMessage alloc] initWithMessage:salesOrders andTag:@"concierge"];
        [[Freshchat sharedInstance] sendMessage:userMessage];//send message api call
        ConversationOptions *options = [ConversationOptions new];
        [options filterByTags:@[@"concierge"] withTitle:@"Concierge"];
        [[Freshchat sharedInstance] showConversations:self withOptions: options];
//***** [[Freshchat sharedInstance] showConversations:] method --END--*****//
        
    }else{
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"MInIMARket"];
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"spaOrderBtnTapped"];
         [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"roomServiceOrderBtnTapped"];
        PointsViewcontrollwer *socialVC = (PointsViewcontrollwer *)[self.storyboard instantiateViewControllerWithIdentifier:@"Wallet"];
        socialVC.CountARR=countArray;
        socialVC.ArrTitle=orderNameListArray;
        socialVC.Pricearr=Custprice;
        [self presentViewController:socialVC animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]setObject:salesOrders forKey:@"SalesOrder"];
        
    }
}
-(void)SpaFreshchat{
    FreshchatMessage *userMessage = [[FreshchatMessage alloc] initWithMessage:salesOrders andTag:@"spa"];
    [[Freshchat sharedInstance] sendMessage:userMessage];//send message api call
    ConversationOptions *options = [ConversationOptions new];
    [options filterByTags:@[@"spa"] withTitle:@"Spa"];
    PointsViewcontrollwer *points=[[PointsViewcontrollwer alloc]init];
    [[Freshchat sharedInstance] showConversations:points withOptions: options];
}


- (IBAction)products:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self FilterDtaMiniMarket];
    });
    [self.cuisineBtn setTitleColor:[UIColor colorWithRed:(141.0/255.0) green:(28.0/255.0) blue:(88.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.drinksBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.vegNonVegBtn setTitleColor:[UIColor colorWithRed:(93.0/255.0) green:(93.0/255.0) blue:(93.0/255.0) alpha:1.0] forState:UIControlStateNormal];
   //cuisineSubFilters= [[NSUserDefaults standardUserDefaults]valueForKey:@"productSubFilters"];
   // cuisineSubFilters = [conciergeArray valueForKey:@"criterionSubFilters"];
    NSDictionary *dict = proudutsfilte[0];
  
    NSMutableArray *array1 = [dict objectForKey: @"productSubFilters"];
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (NSDictionary *title in array1) {
        [actionSheet addButtonWithTitle:[title objectForKey:@"filterName"]];
    }
    if (array1.count>0) {
      actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
        [[NSUserDefaults standardUserDefaults] setObject:array1 forKey:@"productArrayKey"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        // [self.smrtHotelSegmentTap setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]}  forState:UIControlStateSelected];
        
        [actionSheet showFromRect:button.frame inView:self.view animated:YES];
        actionSheet.tag =100;
    }
    
   
    
    
    
}

- (IBAction)brands:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self FilterDtaMiniMarket];
    });
   
    //cuisineSubFilters= [[NSUserDefaults standardUserDefaults]valueForKey:@"productSubFilters"];
    // cuisineSubFilters = [conciergeArray valueForKey:@"criterionSubFilters"];
    NSDictionary *dict = proudutsfilte[1];
    NSMutableArray *array1 = [dict objectForKey: @"brandsSubFilters"];
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (NSDictionary *title in array1) {
        [actionSheet addButtonWithTitle:[title objectForKey:@"filterName"]];
    }
    //cuisineArrayKey
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    [[NSUserDefaults standardUserDefaults] setObject:array1 forKey:@"brandArrayKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // [self.smrtHotelSegmentTap setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]}  forState:UIControlStateSelected];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    actionSheet.tag = 200;
}

- (IBAction)promotions:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self FilterDtaMiniMarket];
    });
   

    
    //cuisineSubFilters= [[NSUserDefaults standardUserDefaults]valueForKey:@"productSubFilters"];
    // cuisineSubFilters = [conciergeArray valueForKey:@"criterionSubFilters"];
    NSDictionary *dict = proudutsfilte[2];
    NSMutableArray *array1 = [dict objectForKey: @"promotionsSubFilters"];
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (NSDictionary *title in array1) {
        [actionSheet addButtonWithTitle:[title objectForKey:@"filterName"]];
    }
    //cuisineArrayKey
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
     [[NSUserDefaults standardUserDefaults] setObject:array1 forKey:@"promotionArrayKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // [self.smrtHotelSegmentTap setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]}  forState:UIControlStateSelected];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    actionSheet.tag = 30;
    
}
-(void)FilterDtaMiniMarket{
   // dispatch_async(dispatch_get_main_queue(), ^{
   
//
//
//    @try {
        NSString *param0=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerID"];
        NSString *param1=@"BRNC001";
        NSString *param2=@"true";
        NSString *param3=@"Nr62WhRjzaLSJkvB";
        NSString *param4=@"Nr62WhRjzaLSJkvB";
        NSString *param5=@"Nr62WhRjzaLSJkvB";
        
        NSString *post = [NSString stringWithFormat:@"fetchBranchePosters=%@&branchId=%@&retrieve=%@&branchProductName=%@&promotionsName=%@&brandName=%@",param0,param1,param2,param3,param4,param5];
        
        NSString *conciergeStr =[NSString stringWithFormat:@"%@/index.php/?%@",[[SprngIPConf getSharedInstance]getIpForImage],post];
        
        string1 = conciergeStr;
        [[NSUserDefaults standardUserDefaults] setObject:conciergeStr forKey:@"urlStr4"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        conciergeStr = [conciergeStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:conciergeStr];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        [request setHTTPMethod:@"GET"];
        
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if (!(connectionError))
             {
                  dispatch_async(dispatch_get_main_queue(), ^{

                 
                 NSError *error = nil;
                 NSDictionary *receivedData = [NSJSONSerialization
                                            JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                 proudutsfilte = [receivedData objectForKey:@"root"];
                 
                    

                 [[NSUserDefaults standardUserDefaults] synchronize];
                // [self.MinimarketCollview reloadData];
                 
                 NSLog(@"%@",conciergeArray);
                 
                });
                 
                 
             }
             else{
                 
             }
         }];
      
}
@end

        
