//
//  OnDemandCollectionViewCell.m
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 26/05/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "OnDemandCollectionViewCell.h"

@implementation OnDemandCollectionViewCell

-(void)setHighlighted:(BOOL)highlighted
{
    if (highlighted)
    {
        self.layer.opacity = 0.6;
        // Here what do you want.
    }
    else{
        self.layer.opacity = 1.0;
        // Here all change need go back
    }
}
//- (IBAction)buttonAction:(id)sender {
//    
//      [self.sdButton setImage:[UIImage imageNamed:@"backImage.jpg"] forState:UIControlStateSelected | UIControlStateHighlighted];
//}

-(void)setSelected:(BOOL)selected
{
    if(selected) {
        
        [self.rateLable setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
    }
    else {
        [self.rateLable setTextColor:[UIColor whiteColor]];
         
    }
}

@end
