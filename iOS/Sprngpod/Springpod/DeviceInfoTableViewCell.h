//
//  DeviceInfoTableViewCell.h
//  Sprngpod
//
//  Created by Shrishail Diggi on 25/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *basicInfo;

@end
