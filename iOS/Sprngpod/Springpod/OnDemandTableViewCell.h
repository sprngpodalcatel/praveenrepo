//
//  OnDemandTableViewCell.h
//  Sprngpod
//
//  Created by XperioLabs on 19/05/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnDemandTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *channelNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLog;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageLog;
@property (weak, nonatomic) IBOutlet UIButton *oldPlusbutton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UIView *channelDetailsView;

@end
