//
//  OnDemandCollectionViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 26/05/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnDemandCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *rateLable;

@property (weak, nonatomic) IBOutlet UIButton *sdButton;

@end
