//
//  TourViewController.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 11/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreData/CoreData.h>


@interface TourViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,CLLocationManagerDelegate,UIAlertViewDelegate,UIAlertViewDelegate>
{
    MPMoviePlayerViewController *moviePlayerController;
    AVPlayerViewController *moviewPlayer;
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
   
}


-(void)setNavigationTitle:(NSString *)title;
-(void)timedOut;



@property (strong, nonatomic) CLLocationManager *locationManager;

@end
