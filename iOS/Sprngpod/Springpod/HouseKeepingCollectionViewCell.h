//
//  HouseKeepingCollectionViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 18/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HouseKeepingCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *houseKeepingImageView;
@property (weak, nonatomic) IBOutlet UILabel *houseKeepingTitle;

@end
