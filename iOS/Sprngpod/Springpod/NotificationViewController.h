//
//  NotificationViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 13/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
