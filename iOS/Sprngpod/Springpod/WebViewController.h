//
//  WebViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 12/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
{
    NSTimer *timer;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UINavigationBar *webNav;

@end
