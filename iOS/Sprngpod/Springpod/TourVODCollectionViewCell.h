//
//  TourVODCollectionViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 14/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourVODCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *tourVODImageView;
@property (weak, nonatomic) IBOutlet UILabel *conciergeTitle;

@end
