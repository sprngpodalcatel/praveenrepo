//
//  DeviceViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end
