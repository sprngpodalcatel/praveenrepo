//
//  NormalDevRegViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 09/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "NormalDevRegViewController.h"
#import "SprngIPConf.h"

@interface NormalDevRegViewController ()
{
    NSMutableArray *temp;
    NSString *userName;
    NSString *password;
    NSString *lastTvChannelID;
    NSString *lastRadioID;
    NSString *lastRadioLanguage;
    NSString *deviceName;
    NSString *platform;
    NSString *osVersion;
    NSString *MAC;
    NSString *SCID;
    NSString *serialNumber;
    NSString *deviceModel;
    NSString *socialID;
    NSString *socialType;
    
    NSMutableArray *deviceList;
}
@property (weak, nonatomic) IBOutlet UINavigationBar *devNav;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *deviceID;

@end

@implementation NormalDevRegViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUUID *uuid = [[UIDevice currentDevice]identifierForVendor];
    NSString *UUID;
    UUID = [uuid UUIDString];
    _deviceID.text = UUID;
    deviceName = [[UIDevice currentDevice] name];
    [self.devNav setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.devNav.shadowImage = [UIImage new];
    self.devNav.translucent = YES;
    self.devNav.backgroundColor = [UIColor clearColor];
    platform = @"iOS";
    osVersion = [[UIDevice currentDevice]systemVersion];
    MAC = @"AFMAC06";
    SCID = @"1234";
    serialNumber = @"1";
    deviceModel = [[UIDevice currentDevice]model];
}
- (IBAction)regPressed:(id)sender {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/normalStoreDeviceDetails",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    /*
     loginName,password,deviceId,platform,osVersion,MAC,
     SCID,serialNumber,deviceModel,loginId,type
     */
    NSString *post = [NSString stringWithFormat:@"loginName=%@&password=%@&deviceId=%@&platform=%@&osVersion=%@&MAC=%@&SCID=%@&serialNumber=%@&deviceModel=%@",_userNameTextField.text,_passwordTextField.text,_deviceID.text,platform,osVersion,MAC,SCID,serialNumber,deviceModel];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            NSDictionary *sDic = temp[0];
            NSString *state = [sDic objectForKey:@"status"];
            NSLog(@"status = %@",state);
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"]) {
                userName = _userNameTextField.text;
                password = self.passwordTextField.text;
                lastTvChannelID  = [sDic objectForKey:@"lastSeenChannelId"];
                lastRadioID = [sDic objectForKey:@"lastListenRadioChannelId"];
                lastRadioLanguage = [sDic objectForKey:@"lastListenRadioChannelLanguage"];
                [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"usename"];
                [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] setObject:lastTvChannelID forKey:@"lastTV"];
                [[NSUserDefaults standardUserDefaults] setObject:lastRadioID forKey:@"lastRadio"];
                [[NSUserDefaults standardUserDefaults] setObject:lastRadioLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"%@",userName);

                [self performSegueWithIdentifier:@"home" sender:self];
            }
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Device Limit Crossed" ]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Maximum Device Limit Crossed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
            else{
                NSLog(@"temp is getting Null");
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
