//
//  Customcell.h
//  Asianet Mobile TV Plus
//
//  Created by Pa'One' Nani on 24/10/18.
//  Copyright © 2018 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Customcell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Titlelabel;
@property (weak, nonatomic) IBOutlet UILabel *QuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pricelabel;

@end
