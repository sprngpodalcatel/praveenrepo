//
//  PODViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 09/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PODViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
-(void)podDoSetup;
-(void)setNavigationTitle:(NSString *)title;
@property (strong, nonatomic) IBOutlet UIView *podBaseView;

@end
