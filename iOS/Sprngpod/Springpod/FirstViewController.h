//
//  FirstViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface FirstViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,GADBannerViewDelegate,GADInterstitialDelegate,UIActionSheetDelegate>

{

    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
- (void)adView:(GADBannerView *)bannerViewn didFailToReceiveAdWithError:(GADRequestError *)error;
- (void)adViewWillPresentScreen:(GADBannerView *)bannerView;
- (void)adViewDidDismissScreen:(GADBannerView *)bannerView;
- (void)adViewWillDismissScreen:(GADBannerView *)bannerView;
- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView;


-(void)setNavigationTitle:(NSString *)title;
-(void)reloadTableview;
//-(void)sessionTimeOut;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBar;

@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;

@end

