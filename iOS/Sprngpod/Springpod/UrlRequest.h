//
//  UrlRequest.h
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 22/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UrlRequest : UIView

+(void)postRequest:(NSString *)urlString arguments:(NSString *)arguments view:(UIView *)view;
+(void)getRequest:(NSString *)urlString view:(UIView *)view;


@end
