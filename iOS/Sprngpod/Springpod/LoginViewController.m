//
//  LoginViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//
#import "LoginViewController.h"
#import "FirstViewController.h"
#import "AppDelegate.h"
#import "netWork.h"
#import "SocialMediaViewController.h"
#import "HomeViewController.h"
#import "DeviceRegViewController.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <gethostuuid.h>
#import "PortMapper.h"
#import "Flurry.h"
#import "UIView+Toast.h"
#import "SprngIPConf.h"
#import "DeviceUID.h"
#import "LocationUpdate.h"
#import "TokenlessApiCall.h"
#import "WebViewController.h"

static NSString * const kClientID = @"412908284391-4lalhmetm6hm9un6iuvg7guf42uikaof.apps.googleusercontent.com";

@class GIDSignIn;


@interface LoginViewController ()
{
    NSMutableArray *temp;
    NSString *userName,*loginName;
    NSString *password;
    NSString *lastTvChannelID;
    NSString *lastRadioID;
    NSString *lastRadioLanguage;
    NSString *lastListenMalyalamRadioChannelId,*lastListenHindiRadioChannelId,*lastListenEnglishRadioChannelId;
    NSString *fbID,*socialUniqueId,*phoneNumber;
    NSString *deviceID;
    netWork *netReachability;
    NetworkStatus networkStatus;
    UIAlertView *socialAlert;
    UIAlertView *deviceAlert;
    NSString *userID;
    NSString *countryName;
    NSString *boolValue;
    BOOL rememberInteger;
    NSString *countryCode;
    NSString *deviceName;
    NSString *platform;
    NSString *osVersion;
    NSString *MAC;
    NSString *SCID;
    NSString *serialNumber;
    NSString *deviceModel,*deviceType;
    NSString *customerId,*timeZoneID;
    
    //    float lattitude,longitude;
    CSToastStyle *style;
    
    UIAlertView *thnakAlert;
    NSString *MCC;
    NSString *MNC;
    NSString *IPAddress;
    UIAlertView *deviceMaxAlert,*registrationalert,*socialalert;
    LocationUpdate *locationRefresh;
}

@property(strong , nonatomic) NSTimer *loginTimer;

@property(strong, nonatomic) FBSDKLoginManager *login ;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIImageView *rememberImageView;
@property (weak, nonatomic) IBOutlet UIImageView *forgotPassImageView;
@property (weak, nonatomic) IBOutlet UIView *thanksView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoPositionConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPositionConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sprngpodHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *showHideButton;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
- (IBAction)Register:(UIButton *)sender;

@end

@implementation LoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    boolValue = [[NSString alloc]init];
    _userIdTextField.borderStyle = UITextBorderStyleRoundedRect;

    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    _login = [[FBSDKLoginManager alloc]init];
    deviceID = [DeviceUID uid];
    [[NSUserDefaults standardUserDefaults] setObject:deviceID forKey:@"DeviceID"];
    //    deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    deviceName = [[UIDevice currentDevice] name];
    platform = @"iOS";
    osVersion = [[UIDevice currentDevice]systemVersion];
    MAC = @"default";
    SCID = @"default";
    serialNumber = @"default";
    deviceModel = [[UIDevice currentDevice]model];
    NSLog(@"%@",deviceModel);
    if ([deviceModel isEqualToString:@"iPhone"]) {
        deviceType = @"Phone";
    }
    else if ([deviceModel isEqualToString:@"iPad"])
    {
        deviceType = @"Tablet";
    }
    else
    {
        deviceType = @"Phone";
    }
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netInfo subscriberCellularProvider];
    NSString *mcc = [carrier mobileCountryCode];
    NSString *mnc = [carrier mobileNetworkCode];
    //    NSString *mnc = @"0";
    NSString *ccode = [carrier isoCountryCode];
    NSString *networkName = [carrier carrierName];
    if (mcc == NULL) {
        mcc = @"default";
    }
    if (mnc == NULL || [mnc isEqualToString:@"0"]) {
        mnc = @"zero";
    }
    MCC = mcc;
    MNC = mnc;
    NSLog(@"mcc : %@", MCC);
    NSLog(@"mnc : %@",MNC);
    NSLog(@"Country Code ISO : %@",ccode);
    NSLog(@"carrierName : %@",networkName);
    [[NSUserDefaults standardUserDefaults] setObject:MCC forKey:@"MCC"];
    [[NSUserDefaults standardUserDefaults] setObject:MNC forKey:@"MNC"];
    countryCode = @"default";
    [[NSUserDefaults standardUserDefaults] setObject:countryCode forKey:@"countryCode"];
    countryName = @"default";
    [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"countryName"];
    // self.thanksView.hidden = YES;
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    timeZoneID = [timeZone name];
    if (!timeZoneID) {
        timeZoneID = @"Default";
    }
    [[NSUserDefaults standardUserDefaults]setObject:timeZoneID forKey:@"timeZone"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    self.fbLogin.adjustsImageWhenHighlighted = NO;
    self.googleLogin.adjustsImageWhenHighlighted = NO;
    self.fbLogin.showsTouchWhenHighlighted = NO;
    self.googleLogin.showsTouchWhenHighlighted = NO;
    
    
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
    
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self setNeedsStatusBarAppearanceUpdate];
    
    [Flurry logEvent:@"Login_Event" withParameters:nil];
    
    // TODO(developer) Configure the sign-in button look/feel
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
    
    
    self.versionLabel.text = [NSString stringWithFormat:@"V%@",kAppVarsion ];
    _userIdTextField.delegate = self;
    _passwordTextField.delegate = self;
    if (self.view.frame.size.height == 480) {
        //        [_loginImage setHidden:YES];
        self.logoPositionConstraints.constant = -75;
        self.viewPositionConstraints.constant = 80;
        self.logoHeightConstraint.constant = 110;
        self.sprngpodHeightConstraint.constant = 30;
    }
    
    //    _userIdTextField.text = @"shridiggi";
    //    _passwordTextField.text = @"test123";
    temp = [[NSMutableArray alloc]init];
    _loader.hidden = YES;
    // Do any additional setup after loading the view.
    FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
    //    FBSDKAccessToken.currentAccessToken = nil;
    [manager logOut];
    [[GIDSignIn sharedInstance] signOut];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated {
    // view to display "Thank you" message only when they rate the app at Home Screen...
    BOOL thanksView = [[NSUserDefaults standardUserDefaults] boolForKey:@"thanksView"];
    
    if (thanksView == YES) {
        //self.thanksView.hidden = NO;
        thnakAlert = [[UIAlertView alloc] initWithTitle:@"Thank you" message:@"we appreciate you are feedback." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [thnakAlert show];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    // To remember the Login Credentials at First login...
    rememberInteger = [[NSUserDefaults standardUserDefaults] boolForKey:@"rememberStatus"];
    NSLog(@"%d",rememberInteger);
    NSLog(@"%d",[[NSUserDefaults standardUserDefaults] boolForKey:@"rememberStatus"]);
    if (rememberInteger == YES) {
        
        _userIdTextField.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"usename"];
        _passwordTextField.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        
    }
    else{
        
    }
    
    locationRefresh = [[LocationUpdate alloc] init];
    [locationRefresh getCurrentLocation];
    
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(0.0, 75.0)];
    [linePath addLineToPoint:CGPointMake(228.0, 75.0)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [linePath CGPath];
    shapeLayer.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer.lineWidth = 1.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    // Adding lines on thanksView...
    [self.thanksView.layer addSublayer:shapeLayer];
    
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *timeZoneName = [timeZone name];
    NSLog(@"timeZone NAme = %@",timeZoneName);
    CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netInfo subscriberCellularProvider];
    NSString *mcc = [carrier mobileCountryCode];
    NSString *mnc = [carrier mobileNetworkCode];
    //    NSString *mnc = @"0";
    NSString *ccode = [carrier isoCountryCode];
    NSString *networkName = [carrier carrierName];
    if (mcc == NULL) {
        mcc = @"default";
    }
    if (mnc == NULL || [mnc isEqualToString:@"0"]) {
        mnc = @"zero";
    }
    MCC = mcc;
    MNC = mnc;
    NSLog(@"mcc : %@", MCC);
    NSLog(@"mnc : %@",MNC);
    NSLog(@"Country Code ISO : %@",ccode);
    NSLog(@"carrierName : %@",networkName);
    
    //    [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"countryName"];
    
    [[NSUserDefaults standardUserDefaults] setObject:MCC forKey:@"MCC"];
    [[NSUserDefaults standardUserDefaults] setObject:MNC forKey:@"MNC"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    if (self.isMovingToParentViewController) {
        _loader.hidden = YES;
    }
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.view.frame.size.height <= 600) {
        int y = -180;
        //        [UIView beginAnimations:nil context:NULL];
        //        [UIView setAnimationDuration:0.25];
        self.view.frame = CGRectMake(0, y, self.view.frame.size.width, self.view.frame.size.height);
        //        [UIView commitAnimations];
    }
    else{
        
    }
    
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) becomeActive
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (IBAction)rememberPressed:(id)sender {
    rememberInteger = !rememberInteger;
    NSLog(@"rememberStatus : %d",rememberInteger);
    if (rememberInteger == YES) {
        [[NSUserDefaults standardUserDefaults]setBool:rememberInteger forKey:@"rememberStatus"];
        _rememberImageView.image = [UIImage imageNamed:@"remember2.png"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setBool:rememberInteger forKey:@"rememberStatus"];
        _rememberImageView.image = [UIImage imageNamed:@"remember.png"];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

//- (IBAction)thanksViewOKPressed:(id)sender {
//    self.thanksView.hidden = YES;
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"thanksView"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//}

- (IBAction)loginPressed:(id)sender
{
    
    _loader.hidden = NO;
    [self.loader startAnimating];
    
    
    if (countryCode == NULL) {
        countryCode = @"default";
        [[NSUserDefaults standardUserDefaults] setObject:countryCode forKey:@"countryCode"];
    }
    
    if (countryName == NULL) {
        countryName = @"default";
        [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"countryName"];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"IPA : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]);
    NSLog(@"MCC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"]);
    NSLog(@"MNC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]);
    NSLog(@"Country Code : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
    NSLog(@"Country Name : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
    
    
    loginName =@"";
    
    
    NSString *platform1 = [UIDevice currentDevice].model;
    
    NSLog(@"[UIDevice currentDevice].model: %@",platform1);
    NSLog(@"[UIDevice currentDevice].description: %@",[UIDevice currentDevice].description);
    NSLog(@"[UIDevice currentDevice].localizedModel: %@",[UIDevice currentDevice].localizedModel);
    NSLog(@"[UIDevice currentDevice].name: %@",[UIDevice currentDevice].name);
    
    NSLog(@"[UIDevice currentDevice].systemVersion: %@",[UIDevice currentDevice].systemVersion);
    NSLog(@"[UIDevice currentDevice].systemName: %@",[UIDevice currentDevice].systemName);
    
    NSLog(@"UUID : %@",deviceID);
    NSLog(@"username = %@", self.userIdTextField.text);
    NSLog(@"pwd = %@", self.passwordTextField.text);
    
    if (self.userIdTextField.text.length == 0 || self.passwordTextField.text.length == 0) {
        [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
        self.loader.hidden = YES;
        [self.loader stopAnimating];
    }
    
    else{
        
        @try {
            
            NSString *url = @"newNormalLogin1";
            
            NSString *post = [NSString stringWithFormat:@"loginName=%@&password=%@&deviceId=%@&platform=%@&osVersion=%@&MAC=%@&SCID=%@&serialNumber=%@&deviceModel=%@&deviceType=%@&layout=%@&deviceToken=%@&appVersion=%@",_userIdTextField.text,_passwordTextField.text,deviceID,platform,osVersion,MAC,SCID,serialNumber,deviceModel,deviceType,@"default",[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"],kAppVarsion];
            NSLog(@"post %@",post);
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchNormalLogin:) name:url object:nil];
            [TokenlessApiCall postRequestWithoutToken:url postKeys:post inView:self.view];
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
    }
    
}

-(void)fetchNormalLogin:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    @try {
        
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            NSDictionary *sDic = temp[0];
            NSString *state = [sDic objectForKey:@"status"];
            NSLog(@"status = %@",state);
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"]) {
                userName = self.userIdTextField.text;
                password = self.passwordTextField.text;
                lastTvChannelID  = [sDic objectForKey:@"lastSeenChannelId"];
                lastRadioID = [sDic objectForKey:@"lastListenRadioChannelId"];
                lastRadioLanguage = [sDic objectForKey:@"lastListenRadioChannelLanguage"];
                customerId = [sDic objectForKey:@"customerId"];
                loginName = [sDic objectForKey:@"loginName"];
                IPAddress = [sDic objectForKey:@"ipAddress"];
                lastListenMalyalamRadioChannelId = [sDic objectForKey:@"lastListenMalyalamRadioChannelId"];
                lastListenHindiRadioChannelId = [sDic objectForKey:@"lastListenHindiRadioChannelId"];
                lastListenEnglishRadioChannelId = [sDic objectForKey:@"lastListenEnglishRadioChannelId"];
                //                NSString *token = [sDic objectForKey:@"token"];
                //                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"usename"];
                [[NSUserDefaults standardUserDefaults] setObject:loginName forKey:@"loginName"];
                [[NSUserDefaults standardUserDefaults] setObject:customerId forKey:@"customerID"];
                [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] setObject:lastTvChannelID forKey:@"lastTV"];
                [[NSUserDefaults standardUserDefaults] setObject:lastRadioID forKey:@"lastRadio"];
                [[NSUserDefaults standardUserDefaults] setObject:lastListenMalyalamRadioChannelId forKey:@"lastListenMalyalamRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:lastListenEnglishRadioChannelId forKey:@"lastListenEnglishRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:lastListenHindiRadioChannelId forKey:@"lastListenHindiRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"rememberStatus"];
                
                
                NSLog(@"%@",userName);
                
                [self performSegueWithIdentifier:@"FirstSegue" sender:self];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginStatus"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromEntertainView"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                
                [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Please Check Your Username or Password" ]) {
                
                [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Deleted"]){
                [self.view makeToast:@"Customer Deleted" duration:4.0 position:CSToastPositionCenter style:style];
                
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:4.0 position:CSToastPositionCenter style:style];
                
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Device Limit Crossed" ]) {
                
                
                [self.view makeToast:@"Maximum Device Limit Crossed" duration:4.0 position:CSToastPositionCenter style:style];
                
            }
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [self.loader stopAnimating];
    [self.loader hidesWhenStopped];
}

- (IBAction)registerPressed:(id)sender {
    //    http://52.22.22.229/demoPortal/index1.html
    //    http://www.asianetmobiletv.com/register
    
    
}

- (IBAction)forgotPasswordPressed:(id)sender {
    NSString *passUrl=@"https://www.sprngpod.com/register/forgetpassword.html";
    [[NSUserDefaults standardUserDefaults]  setObject:passUrl forKey:@"MYWEBURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"webView" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UIViewController *vc = segue.destinationViewController;
    NSLog(@"%@",[vc class]);
    
}


- (IBAction)fbLoginPressed:(id)sender {
    NSLog(@"fb pressed");
    _loader.hidden = NO;
    [self.loader startAnimating];
    
    
    @try {
        if (countryCode == NULL) {
            countryCode = @"default";
            [[NSUserDefaults standardUserDefaults] setObject:countryCode forKey:@"countryCode"];
        }
        
        if (countryName == NULL) {
            countryName = @"default";
            [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"countryName"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"IPA : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]);
        NSLog(@"MCC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"]);
        NSLog(@"MNC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]);
        NSLog(@"Country Code : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
        NSLog(@"Country Name : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (networkStatus == NotReachable) {
            
            [self.view makeToast:@"No Network Available" duration:4.0 position:CSToastPositionCenter style:style];
            //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //        [alert show];
        }
        loginName = @"";
        
        FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
        [manager logOut];
        [[GIDSignIn sharedInstance] signOut];
        
        [_login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
            if (error) {
                NSLog(@"%@",error.description);
                [self.loader stopAnimating];
            }
            else if (result.isCancelled){
                NSLog(@"Cancelled");
                [self.loader stopAnimating];
            }
            
            else
            {
                
                NSLog(@"Email : %@",result);
                
                NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                [parameters setValue:@"id, name, email,first_name" forKey:@"fields"];
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     
                     if (!error) {
                         NSLog(@"fetched user:%@", result);
                         NSLog(@"email:%@",[result objectForKey:@"email"]);
                         NSLog(@"id : %@",[result objectForKey:@"id"]);
                         NSLog(@"First Name : %@",[result objectForKey:@"first_name"]);
                         fbID = @"";
                         fbID = [result objectForKey:@"email"];
                         if (fbID == NULL) {
                             fbID = @"default";
                         }
                         socialUniqueId = [result objectForKey:@"id"];
                         NSString *firstName = [result objectForKey:@"first_name"];
                         [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:@"fbName"];
                         [[NSUserDefaults standardUserDefaults] setObject:fbID forKey:@"fbUserID"];
                         [[NSUserDefaults standardUserDefaults] setObject:socialUniqueId forKey:@"socialUniqID"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         //                     if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]) {
                         
                         NSString *url = @"newSocialLogin4";
                         //                         socialLoginId,socialUniqueId,,deviceId,platform,osVersion,MAC,
                         //                         SCID,serialNumber,deviceModel,type,deviceType,layout
                         NSString *post = [NSString stringWithFormat:@"socialLoginId=%@&socialUniqueId=%@&deviceId=%@&platform=iOS&osVersion=%@&MAC=%@&SCID=%@&serialNumber=%@&deviceModel=%@&type=facebook&deviceType=%@&layout=%@&firstName=%@&countryCode=%@&MCC=%@&MNC=%@&deviceToken=%@&appVersion=%@",fbID,socialUniqueId,deviceID,osVersion,MAC,SCID,serialNumber,deviceModel,deviceType,@"default",firstName,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"],kAppVarsion];
                         NSLog(@"post %@",post);
                         
                         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFBLogin:) name:url object:nil];
                         
                         [TokenlessApiCall postRequestWithoutToken:url postKeys:post inView:self.view];
                         
                     }
                     
                     else{
                         NSLog(@"Error %@",error.userInfo);
                         [self.view makeToast:[NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"NSLocalizedDescription"]] duration:2.0 position:CSToastPositionCenter style:style];
                     }
                     
                 }];
                
            }
            
        }];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}
- (IBAction)googleLoginPressed:(id)sender {
    [[GIDSignIn sharedInstance] signIn];
}

-(void)fetchFBLogin:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    @try {
        
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            if (temp) {
                NSDictionary *sDic = temp[0];
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    
                    [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                    //
                }
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"]) {
                    NSLog(@"Home Screen");
                    lastTvChannelID  = [sDic objectForKey:@"lastSeenChannelId"];
                    lastRadioID = [sDic objectForKey:@"lastListenRadioChannelId"];
                    lastRadioLanguage = [sDic objectForKey:@"lastListenRadioChannelLanguage"];
                    loginName = [sDic objectForKey:@"loginName"];
                    userName = [sDic objectForKey:@"customerId"];
                    NSLog (@"customerId = %@", customerId);
                    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"customerID"];
                    [[NSUserDefaults standardUserDefaults] setObject:loginName forKey:@"loginName"];
                    lastListenMalyalamRadioChannelId = [sDic objectForKey:@"lastListenMalyalamRadioChannelId"];
                    lastListenHindiRadioChannelId = [sDic objectForKey:@"lastListenHindiRadioChannelId"];
                    lastListenEnglishRadioChannelId = [sDic objectForKey:@"lastListenEnglishRadioChannelId"];
                    IPAddress = [sDic objectForKey:@"ipAddress"];
                    //                    NSString *token = [sDic objectForKey:@"token"];
                    //                    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                    [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastTvChannelID forKey:@"lastTV"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastRadioID forKey:@"lastRadio"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastRadioLanguage forKey:@"lastRadioLanguage"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastListenMalyalamRadioChannelId forKey:@"lastListenMalyalamRadioChannelId"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastListenEnglishRadioChannelId forKey:@"lastListenEnglishRadioChannelId"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastListenHindiRadioChannelId forKey:@"lastListenHindiRadioChannelId"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    NSLog(@"%@",userName);
                    
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromEntertainView"];
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginStatus"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //                                 _loginTimer = [NSTimer scheduledTimerWithTimeInterval:2*60 target:self selector:@selector(goToRootView) userInfo:nil repeats:NO];
                    
                    HomeViewController *socialVC = (HomeViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
                    [self presentViewController:socialVC animated:YES completion:nil];
                    
                    
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Deleted"]){
                    [self.view makeToast:@"Customer Deleted" duration:4.0 position:CSToastPositionCenter style:style];
                    
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Incorrect EmailId"]){
                    [self.view makeToast:@"Incorrect EmailId" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Please Check Your Username or Password"]){
                    [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Customer Limit Crossed"]){
                    [self.view makeToast:@"Dear Customer You cannot Login, Due to Error Code 111, Kindly Contact Customer Care for more details support@asianetmobiletv.in" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                    [self.view makeToast:@"Customer Inactive" duration:4.0 position:CSToastPositionCenter style:style];
                }
                
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Already Exists"])
                {
                    [self.view makeToast:@"Social Media Id Already Exists" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Device Limit Crossed" ]) {
                    
                    
                    [self.view makeToast:@"Maximum Device Limit Crossed" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Does Not Exists" ]) {
                    
                    [self.view makeToast:@"Social Media Id Does Not Exists, please Register" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Device Does Not Exists" ]) {
                    
                    [self.view makeToast:@"Device Does Not Exists" duration:4.0 position:CSToastPositionCenter style:style];
                    
                }
                
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [_loader stopAnimating];
    [_loader hidesWhenStopped];
    
    
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == deviceMaxAlert) {
        if (buttonIndex == 1) {
            NSString *passUrl=@"http://sprngpodportal.asianetmobiletv.com/myAccount";
            [[NSUserDefaults standardUserDefaults]  setObject:passUrl forKey:@"MYWEBURL"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self performSegueWithIdentifier:@"webView" sender:nil];
        }
    }
    else if (alertView == registrationalert){
        
        
        if (buttonIndex == 1){
            socialalert = [[UIAlertView alloc] initWithTitle:@"Free Service"
                                                     message:@"To Enjoy select Live TV & Radio channels for 1 year, please click"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"Facebook",@"Google", nil];
            
            
            
            [socialalert show];
            
        }
        else if (buttonIndex == 2)
        {
            
            WebViewController *webpage =[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            
            // [self.navigationController pushViewController:webpage animated:YES];
            [self presentViewController:webpage animated:YES completion:nil];
            
        }
        
        
    }else if (alertView == socialalert){
        
        if (buttonIndex == 1) {
            
            NSLog(@"fb pressed");
            _loader.hidden = NO;
            [self.loader startAnimating];
            
            
            @try {
                if (countryCode == NULL) {
                    countryCode = @"default";
                    [[NSUserDefaults standardUserDefaults] setObject:countryCode forKey:@"countryCode"];
                }
                
                if (countryName == NULL) {
                    countryName = @"default";
                    [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"countryName"];
                }
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"IPA : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"]);
                NSLog(@"MCC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"]);
                NSLog(@"MNC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]);
                
                NSLog(@"Country Code : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
                NSLog(@"Country Name : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
                [[NSUserDefaults standardUserDefaults] synchronize];
                //        if (networkStatus == NotReachable) {
                //
                //            [self.view makeToast:@"No Network Available" duration:4.0 position:CSToastPositionCenter style:style];
                //            //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                //            //        [alert show];
                //        }
                loginName = @"";
                
                FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
                [manager logOut];
                
                [_login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
                    if (error) {
                        NSLog(@"%@",error.description);
                        [self.loader stopAnimating];
                    }
                    else if (result.isCancelled){
                        NSLog(@"Cancelled");
                        [self.loader stopAnimating];
                    }
                    
                    else
                    {
                        
                        NSLog(@"Email : %@",result);
                        
                        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                        [parameters setValue:@"id, name, email,first_name" forKey:@"fields"];
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                             
                             if (!error) {
                                 
                                 NSLog(@"fetched user:%@", result);
                                 NSLog(@"email:%@",[result objectForKey:@"email"]);
                                 NSLog(@"id : %@",[result objectForKey:@"id"]);
                                 NSLog(@"First Name : %@",[result objectForKey:@"first_name"]);
                                 fbID = @"";
                                 fbID = [result objectForKey:@"email"];
                                 if (fbID == NULL) {
                                     fbID = @"default";
                                 }
                                 socialUniqueId = [result objectForKey:@"id"];
                                 NSString *firstName = [result objectForKey:@"first_name"];
                                 [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:@"fbName"];
                                 [[NSUserDefaults standardUserDefaults] setObject:fbID forKey:@"fbUserID"];
                                 [[NSUserDefaults standardUserDefaults] setObject:socialUniqueId forKey:@"socialUniqID"];
                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                 
                                 //                     if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]) {
                                 
                                 NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/newSocialLogin3",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                                 
                                 //                         socialLoginId,socialUniqueId,,deviceId,platform,osVersion,MAC,
                                 //                         SCID,serialNumber,deviceModel,type,deviceType,layout
                                 
                                 NSString *post = [NSString stringWithFormat:@"socialLoginId=%@&socialUniqueId=%@&deviceId=%@&platform=iOS&osVersion=%@&MAC=%@&SCID=%@&serialNumber=%@&deviceModel=%@&type=facebook&deviceType=%@&layout=%@&firstName=%@&countryCode=%@&MCC=%@&MNC=%@&deviceToken=%@&appVersion=%@",fbID,socialUniqueId,deviceID,osVersion,MAC,SCID,serialNumber,deviceModel,deviceType,@"default",firstName,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"],kAppVarsion];
                                 NSLog(@"post %@",post);
                                 
                                 
                                 NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                                 
                                 NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                                 [request setHTTPMethod:@"POST"];
                                 [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                                 [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                                 [request setHTTPBody:postData];
                                 
                                 NSURLResponse *response;
                                 NSError *error = nil;
                                 NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                                 NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                                 NSDictionary *json_dict = (NSDictionary *)json_string;
                                 NSLog(@"json_dict\n%@",json_dict);
                                 NSLog(@"json_string\n%@",json_string);
                                 
                                 NSLog(@"%@",response);
                                 NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                                 NSLog(@" error is %@",error);
                                 if (!error) {
                                     NSError *myError = nil;
                                     NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                                     NSLog(@"%@",dictionary);
                                     temp = [dictionary objectForKey:@"root"];
                                     if (temp) {
                                         NSDictionary *sDic = temp[0];
                                         if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                             
                                             [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                                             //
                                         }
                                         if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"])
                                         {
                                             
                                             NSLog(@"Home Screen");
                                             lastTvChannelID  = [sDic objectForKey:@"lastSeenChannelId"];
                                             lastRadioID = [sDic objectForKey:@"lastListenRadioChannelId"];
                                             lastRadioLanguage = [sDic objectForKey:@"lastListenRadioChannelLanguage"];
                                             loginName = [sDic objectForKey:@"loginName"];
                                             userName = [sDic objectForKey:@"customerId"];
                                             [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"customerID"];
                                             [[NSUserDefaults standardUserDefaults] setObject:loginName forKey:@"loginName"];
                                             lastListenMalyalamRadioChannelId = [sDic objectForKey:@"lastListenMalyalamRadioChannelId"];
                                             lastListenHindiRadioChannelId = [sDic objectForKey:@"lastListenHindiRadioChannelId"];
                                             lastListenEnglishRadioChannelId = [sDic objectForKey:@"lastListenEnglishRadioChannelId"];
                                             IPAddress = [sDic objectForKey:@"ipAddress"];
                                             NSString *token = [sDic objectForKey:@"token"];
                                             [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                                             
                                             [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                                             [[NSUserDefaults standardUserDefaults] setObject:lastTvChannelID forKey:@"lastTV"];
                                             [[NSUserDefaults standardUserDefaults] setObject:lastRadioID forKey:@"lastRadio"];
                                             [[NSUserDefaults standardUserDefaults] setObject:lastRadioLanguage forKey:@"lastRadioLanguage"];
                                             [[NSUserDefaults standardUserDefaults] setObject:lastListenMalyalamRadioChannelId forKey:@"lastListenMalyalamRadioChannelId"];
                                             [[NSUserDefaults standardUserDefaults] setObject:lastListenEnglishRadioChannelId forKey:@"lastListenEnglishRadioChannelId"];
                                             [[NSUserDefaults standardUserDefaults] setObject:lastListenHindiRadioChannelId forKey:@"lastListenHindiRadioChannelId"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             NSLog(@"%@",userName);
                                             
                                             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromEntertainView"];
                                             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginStatus"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             //                                 _loginTimer = [NSTimer scheduledTimerWithTimeInterval:2*60 target:self selector:@selector(goToRootView) userInfo:nil repeats:NO];
                                             
                                             HomeViewController *socialVC = (HomeViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
                                             [self presentViewController:socialVC animated:YES completion:nil];
                                             
                                             
                                         }
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Deleted"]){
                                             [self.view makeToast:@"Customer Deleted" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Incorrect EmailId"]){
                                             [self.view makeToast:@"Incorrect EmailId" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Please Check Your Username or Password"]){
                                             [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Customer Limit Crossed"]){
                                             [self.view makeToast:@"Dear Customer You cannot Login, Due to Error Code 111, Kindly Contact Customer Care for more details support@asianetmobiletv.in" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                                             [self.view makeToast:@"Customer Inactive" duration:4.0 position:CSToastPositionCenter style:style];
                                         }
                                         
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Already Exists"])
                                         {
                                             [self.view makeToast:@"Social Media Id Already Exists" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Device Limit Crossed" ]) {
                                             
                                             
                                             [self.view makeToast:@"Maximum Device Limit Crossed" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Does Not Exists" ]) {
                                             DeviceRegViewController *deviceReg = (DeviceRegViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"DeviceRegViewControllerID"];
                                             [self presentViewController:deviceReg animated:YES completion:nil];
                                             
                                             [self.view makeToast:@"Social Media Id Does Not Exists, please Register" duration:4.0 position:CSToastPositionCenter style:style];
                                             
                                         }
                                         
                                         else if ([[sDic objectForKey:@"status"]isEqualToString:@"Device Does Not Exists" ]) {
                                             DeviceRegViewController *deviceReg = (DeviceRegViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"DeviceRegViewControllerID"];
                                             [self presentViewController:deviceReg animated:YES completion:nil];
                                             deviceAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Device Does Not Exists, please Register" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                             [deviceAlert show];
                                             
                                         }
                                         else{
                                             NSLog(@"temp is getting Null");
                                         }
                                         
                                         
                                     }
                                     else
                                     {
                                         NSLog(@"%@",error);
                                     }
                                     [self.loader stopAnimating];
                                 }
                                 
                                 else
                                 {
                                     NSLog(@"%@",error);
                                     [self.loader stopAnimating];
                                 }
                                 //                     }
                                 
                                 //                     else{
                                 //                         [self.view makeToast:@"Device Token not registered" duration:4.0 position:CSToastPositionCenter style:style];
                                 //
                                 //                     }
                                 
                                 
                                 //                         SocialMediaViewController *socialVC = (SocialMediaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewControllerID"];
                                 //                         socialVC.facebookId = result[@"id"];
                                 //                         [self presentViewController:socialVC animated:YES completion:nil];
                                 
                             }
                             
                         }];
                        
                    }
                    
                }];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            
        }
        else if (buttonIndex == 2)
        {
            
            [[GIDSignIn sharedInstance] signIn];
            
        }
        
    }
    else if (alertView == thnakAlert)
    {
        if (buttonIndex == 0) {
            thnakAlert.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"thanksView"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
    
}

-(void)GGLSignIn:(NSNotification *)notification{
    //    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ToggleAuthUINotification" object:nil];
    _loader.hidden = NO;
    [_loader startAnimating];
    
    if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
        NSDictionary *dictionary = [[NSDictionary alloc] init];
        dictionary = [notification.userInfo objectForKey:@"dictionary"];
        NSLog(@"GGL SignedIn user : %@",[dictionary objectForKey:@"email"]);
        NSLog(@"familyName : %@",[dictionary objectForKey:@"familyName"]);
        NSLog(@"fullName : %@",[dictionary objectForKey:@"fullName"]);
        NSLog(@"givenName : %@",[dictionary objectForKey:@"givenName"]);
        NSLog(@"userId : %@",[dictionary objectForKey:@"userId"]);
        
        
        NSString *email = [dictionary objectForKey:@"email"];
        NSString *firstName = [dictionary objectForKey:@"fullName"];
        NSString *googleUniqueID = [dictionary objectForKey:@"userId"];
        
        
        NSString *url = @"newSocialLogin4";
        //                         socialLoginId,socialUniqueId,,deviceId,platform,osVersion,MAC,
        //                         SCID,serialNumber,deviceModel,type,deviceType,layout
        NSString *post = [NSString stringWithFormat:@"socialLoginId=%@&socialUniqueId=%@&deviceId=%@&platform=iOS&osVersion=%@&MAC=%@&SCID=%@&serialNumber=%@&deviceModel=%@&type=google&deviceType=%@&layout=%@&firstName=%@&countryCode=%@&MCC=%@&MNC=%@&deviceToken=%@&appVersion=%@",email,googleUniqueID,deviceID,osVersion,MAC,SCID,serialNumber,deviceModel,deviceType,@"default",firstName,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"],kAppVarsion];
        NSLog(@"post %@",post);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchGGLLogin:) name:url object:nil];
        [TokenlessApiCall postRequestWithoutToken:url postKeys:post inView:self.view];
        
        //        HomeViewController *socialVC = (HomeViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
        //        [self presentViewController:socialVC animated:YES completion:nil];
        
        //        [[GIDSignIn sharedInstance] signOut];
        
    }
    
    
}

-(void)fetchGGLLogin:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    @try {
        
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            if (temp) {
                NSDictionary *sDic = temp[0];
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    
                    [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                    //
                }
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"]) {
                    NSLog(@"Home Screen");
                    lastTvChannelID  = [sDic objectForKey:@"lastSeenChannelId"];
                    lastRadioID = [sDic objectForKey:@"lastListenRadioChannelId"];
                    lastRadioLanguage = [sDic objectForKey:@"lastListenRadioChannelLanguage"];
                    loginName = [sDic objectForKey:@"loginName"];
                    userName = [sDic objectForKey:@"customerId"];
                    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"customerID"];
                    [[NSUserDefaults standardUserDefaults] setObject:loginName forKey:@"loginName"];
                    lastListenMalyalamRadioChannelId = [sDic objectForKey:@"lastListenMalyalamRadioChannelId"];
                    lastListenHindiRadioChannelId = [sDic objectForKey:@"lastListenHindiRadioChannelId"];
                    lastListenEnglishRadioChannelId = [sDic objectForKey:@"lastListenEnglishRadioChannelId"];
                    IPAddress = [sDic objectForKey:@"ipAddress"];
                    //                    NSString *token = [sDic objectForKey:@"token"];
                    //                    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastTvChannelID forKey:@"lastTV"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastRadioID forKey:@"lastRadio"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastRadioLanguage forKey:@"lastRadioLanguage"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastListenMalyalamRadioChannelId forKey:@"lastListenMalyalamRadioChannelId"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastListenEnglishRadioChannelId forKey:@"lastListenEnglishRadioChannelId"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastListenHindiRadioChannelId forKey:@"lastListenHindiRadioChannelId"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    NSLog(@"%@",userName);
                    
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromEntertainView"];
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginStatus"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //                                 _loginTimer = [NSTimer scheduledTimerWithTimeInterval:2*60 target:self selector:@selector(goToRootView) userInfo:nil repeats:NO];
                    
                    HomeViewController *socialVC = (HomeViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
                    [self presentViewController:socialVC animated:YES completion:nil];
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Deleted"]){
                    [self.view makeToast:@"Customer Deleted" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Incorrect EmailId"]){
                    [self.view makeToast:@"Incorrect EmailId" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Please Check Your Username or Password"]){
                    [self.view makeToast:@"Invalid Username / Password" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Customer Limit Crossed"]){
                    [self.view makeToast:@"Dear Customer You cannot Login, Due to Error Code 111, Kindly Contact Customer Care for more details support@asianetmobiletv.in" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                    [self.view makeToast:@"Customer Inactive" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                }
                
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Already Exists"])
                {
                    [self.view makeToast:@"Social Media Id Already Exists" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                    
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Device Limit Crossed" ]) {
                    
                    [self.view makeToast:@"Maximum Device Limit Crossed" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Does Not Exists" ]) {
                    
                    [self.view makeToast:@"Social Media Id Does Not Exists, please Register" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Device Does Not Exists" ]) {
                    
                    [self.view makeToast:@"Device Does Not Exists" duration:4.0 position:CSToastPositionCenter style:style];
                    [[GIDSignIn sharedInstance] signOut];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GGLSignIn:) name:@"ToggleAuthUINotification" object:nil];
                    
                }
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [_loader stopAnimating];
    [_loader hidesWhenStopped];
}





-(void)afterGet:(NSNotification *)Notification
{
    NSNotificationCenter *getNotiFy = [NSNotificationCenter defaultCenter];
    [getNotiFy removeObserver:self name:Notification.name object:nil];
    
    NSString *ipAddress = [Notification.userInfo objectForKey:@"query"];
    NSLog(@"IPA : %@",ipAddress);
    
    
}

- (IBAction)showHidePressed:(id)sender {
    if (_passwordTextField.text.length > 0) {
        _passwordTextField.secureTextEntry = !_passwordTextField.secureTextEntry;
        if ([self.showHideButton.currentTitle isEqualToString:@"Show"]) {
            [self.showHideButton setTitle:@"Hide" forState:UIControlStateNormal];
        }
        else{
            [self.showHideButton setTitle:@"Show" forState:UIControlStateNormal];
        }
    }
    else{
        [self.view makeToast:@"Enter password" duration:4.0 position:CSToastPositionCenter style:style];
    }
    
}

- (IBAction)Register:(UIButton *)sender {
    
    registrationalert = [[UIAlertView alloc] initWithTitle:@"Register"
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Free Service",@"Paid Service",nil];
    [registrationalert show];
    
//    NSString *passUrl=[NSString stringWithFormat:@"https://www.sprngpod.com/register/index.php"];
//    [[NSUserDefaults standardUserDefaults]  setObject:passUrl forKey:@"MYWEBURL"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    [self performSegueWithIdentifier:@"webView" sender:self];
    
}
@end
