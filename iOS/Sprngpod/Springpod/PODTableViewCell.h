//
//  PODTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 07/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PODTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *podLabel;

@end
