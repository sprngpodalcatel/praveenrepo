//
//  AppDelegate.m
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "HomeViewController.h"
#import "SprngIPConf.h"
//#import "LocationUpdate.h"
#import "ACTReporter.h"
#import "Freshchat.h"
#import "SplashViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
    [[UITabBar appearance] setUnselectedItemTintColor:[UIColor blackColor]];
    
 
   // [window makeKeyAndVisible];
    
    
 [GADMobileAds configureWithApplicationID:@"ca-app-pub-9149503938080598~4400476023"];
    
    //ca-app-pub-6757355035734715~2051768187
    //ca-app-pub-1319032657289195~5219627869
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    
    //To remove badge from icon after seeing the alert.
    
    if([UIApplication sharedApplication].applicationIconBadgeNumber != 0)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:[UIApplication sharedApplication].applicationIconBadgeNumber forKey:@"AlertsReceived"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSLog(@"Set 0 for AlertsReceived");
    NSLog(@"App Running..");
    
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    //    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[FBSDKApplicationDelegate sharedInstance]application:application didFinishLaunchingWithOptions:launchOptions];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
// SplashViewController *hvc = [storyBoard instantiateViewControllerWithIdentifier:@"Splashview"];
//    self.window.rootViewController=hvc;
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
   [GIDSignIn sharedInstance].delegate = self;
    
    
    
//    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"loginStatus"] == YES) {
//        HomeViewController *hvc = [storyBoard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
//        self.window.rootViewController = hvc;
//    }
//    
//    else
//    {
////        splashViewController = [storyBoard instantiateViewControllerWithIdentifier:@"Splash"];
////        self.window.rootViewController = splashViewController;
//        LoginViewController *lvc = [storyBoard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
//        self.window.rootViewController = lvc;
//    }
    
     [ACTConversionReporter reportWithConversionID:@"881994573" label:@"Mk9LCOzA9WcQzdbIpAM" value:@"1.00" isRepeatable:NO];
    
    //    [LocationUpdate getSharedInstance];
    
    [self initFreshchatSDK];
    
    return YES;
}

-(void) initFreshchatSDK {
    //***** Initialize Freshchat --START--*****//
    
    FreshchatConfig *config = [[FreshchatConfig alloc]initWithAppID:@"693f4d45-5ee3-4377-97ac-5f0359acbe01"
                                                          andAppKey:@"d1431788-84e9-4917-8b6d-9dd40bf4a1de"];
    
    [[Freshchat sharedInstance] initWithConfig:config];
    config.agentAvatarEnabled = YES;
    FAQOptions *options = [FAQOptions new];
    options.showFaqCategoriesAsGrid = YES;
    // set to NO to turn of showing an avatar for agents. to customize the avatar shown, use the theme file
    
    //***** Initialize Freshchat --END--*****//
    
}


-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return ([[FBSDKApplicationDelegate sharedInstance]application:application openURL:url sourceApplication:sourceApplication annotation:annotation]|| [[GIDSignIn sharedInstance] handleURL:url
                                                                                                                                                                           sourceApplication:sourceApplication
                                                                                                                                                                                  annotation:annotation] );
    
}

// [START signin_handler]
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error != nil) {
        NSLog(@"Looks like we got a sign in error %@",error);
    }
    else{
        // Perform any operations on signed in user here.
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        // [START_EXCLUDE]
        NSDictionary *statusText =
        [NSDictionary dictionaryWithObjects:@[userId,idToken,fullName,givenName,familyName,email] forKeys:@[@"userId",@"idToken",@"fullName",@"givenName",@"familyName",@"email"]];
        NSDictionary *myDictionary = [NSDictionary dictionaryWithObjects:@[statusText] forKeys:@[@"dictionary"]];
        
        //         NSLog(@"GGL User : %@",@[user.userID,user.authentication.idToken,user.profile.name,givenName,familyName,email,userId,idToken]);
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"ToggleAuthUINotification"
         object:nil
         userInfo:myDictionary];
    }
    // [END_EXCLUDE]
}
// [END signin_handler]


// This callback is triggered after the disconnect call that revokes data
// access to the user's resources has completed.
// [START disconnect_handler]

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.[START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
    NSLog(@"didDisconnect : %@",statusText);
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}

// [END disconnect_handler]



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //*****To enable Freshchat to send push notifications to the application --START--*****//
    [[Freshchat sharedInstance] setPushRegistrationToken:deviceToken];
    //*****To enable Freshchat to send push notifications to the application --END--*****//
    
    
    NSString *tokenInString = [[[deviceToken description]
                                stringByTrimmingCharactersInSet:[NSCharacterSet  characterSetWithCharactersInString:@"<>"]]
                               stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"Device Token %@",tokenInString);
    
    
    if (![tokenInString isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DeviceTokenChanged"];
        [[NSUserDefaults standardUserDefaults] setObject:tokenInString forKey:@"DeviceToken"];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DeviceTokenChanged"];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    if ([[Freshchat sharedInstance]isFreshchatNotification:userInfo]) {
        [[Freshchat sharedInstance]handleRemoteNotification:userInfo andAppstate:application.applicationState];
    }
    
    
    NSDictionary *aps = [userInfo objectForKey:@"aps"];
    NSString *alertString = [aps objectForKey:@"alert"];
    NSInteger badge = [[aps objectForKey:@"badge"] integerValue];
    
//    NSLog(@"Received notification details are %@ and %@ badge is %ld", aps ,alertString,(long)badge);
    
      if (application.applicationState == UIApplicationStateBackground) {
      UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = alertString;
        localNotification.alertLaunchImage = @"New Asianet Launch Screen (1036x1842).png";
        localNotification.fireDate = [NSDate date];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:@"Notification" message:@"You have recieved 1 Notification" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReceivedNotification" object:nil];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
