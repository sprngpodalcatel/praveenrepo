//
//  channelTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 28/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface channelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *channelDisplayLabel;

@end
