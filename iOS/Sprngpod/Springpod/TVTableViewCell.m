//
//  TVTableViewCell.m
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "TVTableViewCell.h"

@implementation TVTableViewCell

- (void)awakeFromNib {
    // Initialization code
//    [self setBackgroundColor:[UIColor darkGrayColor]];
    [super awakeFromNib];
    [self.plusButton resignFirstResponder];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected) {
        
        [self.channelNameLabel setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
        self.channelNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        //        [self.egpLabel setTextColor:[UIColor redColor]];
        //        [self.endTime setTextColor:[UIColor redColor]];
        //        [self.ratingLabel setTextColor:[UIColor redColor]];
        //        [self.descriptionTextView setTextColor:[UIColor redColor]];
        self.descriptionTextView.hidden = NO;
        self.descriptionLabel.hidden = NO;
        if (self.frame.size.height == 140) {
            [self.oldPlusButton setImage:[UIImage imageNamed:@"info-hilight.png"] forState:UIControlStateNormal];
        }
        else{
            [self.oldPlusButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
        }
        
        
    } else {
        [self.channelNameLabel setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]];
        self.channelNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Roman" size:14];

        
        //        [self.egpLabel setTextColor:[UIColor whiteColor]];
        //        [self.endTime setTextColor:[UIColor whiteColor]];
        //        [self.ratingLabel setTextColor:[UIColor whiteColor]];
        //        [self.descriptionTextView setTextColor:[UIColor whiteColor]];
        self.descriptionTextView.hidden = NO;
        self.descriptionLabel.hidden = YES;
        if (self.frame.size.height == 140) {
            [self.oldPlusButton setImage:[UIImage imageNamed:@"info-hilight.png"] forState:UIControlStateNormal];
        }
        else{
            [self.oldPlusButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
        }
        
    }
    // Configure the view for the selected state
}

@end
