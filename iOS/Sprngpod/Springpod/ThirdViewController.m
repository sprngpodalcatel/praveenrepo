    //
    //  ThirdViewController.m
    //  Springpod
    //
    //  Created by Shrishail Diggi on 07/11/15.
    //  Copyright © 2015 Appface. All rights reserved.
    //

    #import "ThirdViewController.h"
    #import "FavouriteTableViewCell.h"
    #import "FavTvTableViewCell.h"
    #import "UIImageView+WebCache.h"
    #import "FirstViewController.h"
    #import "SecondViewController.h"
    #import "netWork.h"
    #import "Flurry.h"
    #import "UIView+Toast.h"
    #import "SprngIPConf.h"
    #import "AppDelegate.h"
    #import "LoginViewController.h"
    #import "NSString+AES.h"
#import "LocationUpdate.h"
#import "UrlRequest.h"
#import "LastSeenIndex.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

    @interface ThirdViewController ()
    {

    NSIndexPath *selectedCellIndexPath;
    NSIndexPath *cellIndexPath;
    NSIndexPath *blockIndexpath;
    NSTimer *timer;
    NSMutableArray *favouriteFolderNameArray,*temp,*temp1;
    NSString *selectedListName,*pinString,*loginName,*newFolder,*newString;
    UIAlertView *blockAlert,*favouritepinviewAlert;
    NSMutableArray *tvProgArray,*tvLogoArray,*tvTimeArray,*tvtextFieldArray,*tvStartTimeArray,*tvEndTimeArray,*tvUrlArray,*tvRatingArray,*channelIDArray,*blockedStatusArray,*packageStatus,*geoLocationStatusArray,*lastSeenStatusArray,*countryAllowedStatusArray,*deletionStatusArray,*parentalStatusArray;
    FavTvTableViewCell *tableCell;
    netWork *netReachability;
    NetworkStatus networkStatus;
    NSMutableSet *expandedCells;
    CSToastStyle *style;
    NSIndexPath *swipeIndex;

    NSString *stringFromUrl;

    NSTimer *playButtonTimer;

    BOOL packageNotFound;
    BOOL noDataFound;
    BOOL tokenExpired;
        BOOL isPlayerLoaded;

    float screenHeight;

    UISwipeGestureRecognizer *leftRecogniser;
    UISwipeGestureRecognizer *rightRecogniser;
    UITapGestureRecognizer *tapRecognizer;
        
        LocationUpdate *locationRefresh;

    }

@property (nonatomic,strong)AVPlayer *player;
@property (nonatomic,strong)AVPlayerViewController *AVPlayerController;

    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *pinViewVerticalContraint;

    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *tvViewHeight;
    @property (nonatomic) int currentValue;
    @property (nonatomic) int previousInteger;
    @property (weak, nonatomic) IBOutlet UINavigationItem *favouriteNav;
    @property (weak, nonatomic) IBOutlet UITableView *favTvTableView;
    @property (weak, nonatomic) IBOutlet UITableView *listTableView;
    @property (weak, nonatomic) IBOutlet UIView *tvView;


@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *nowButton;

    @property (weak, nonatomic) IBOutlet UIView *cancelView;
    @property (weak, nonatomic) IBOutlet UIView *headingView;
    @property (weak, nonatomic) IBOutlet UIView *selectionTableview;
    @property (weak, nonatomic) IBOutlet UIView *pinView;
    @property (weak, nonatomic) IBOutlet UITextField *pinTextField;
    @property (strong , nonatomic) UIColor *appColor;
    @property (weak, nonatomic) IBOutlet UIImageView *nPreviuosImage;
    @property (weak, nonatomic) IBOutlet UIImageView *nNextImage;
    @property (weak, nonatomic) IBOutlet UIView *playPauseView;
    @property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIView *FavoriteViewMenu;
@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;
@property (weak, nonatomic) IBOutlet UIButton *adBannerButton;

    @end

    @implementation ThirdViewController

    - (void)viewDidLoad {
    [super viewDidLoad];
   // self.pinView.hidden = YES;
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
        self.nNextImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
        self.nPreviuosImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
        
        self.loader.hidden = YES;
        
        _AVPlayerController = [[AVPlayerViewController alloc]init];
        
    //    self.favTvTableView.hidden = NO;
    //    self.selectionTableview.hidden = NO;
    //    [self callApiTofetchData];
    //    [self callApiToFetchEPG];
    loginName = @"";
    loginName = [[NSUserDefaults standardUserDefaults]objectForKey:@"loginName"];
    temp = [[NSMutableArray alloc]init];
    temp1 = [[NSMutableArray alloc]init];

    tvProgArray = [[NSMutableArray alloc]init];
    tvLogoArray = [[NSMutableArray alloc]init];
    tvStartTimeArray = [[NSMutableArray alloc]init];
    tvEndTimeArray = [[NSMutableArray alloc]init];
    tvtextFieldArray = [[NSMutableArray alloc]init];
    tvUrlArray = [[NSMutableArray alloc]init];
    blockedStatusArray = [[NSMutableArray alloc]init];
    countryAllowedStatusArray = [[NSMutableArray alloc]init];
    deletionStatusArray = [[NSMutableArray alloc]init];
    parentalStatusArray = [[NSMutableArray alloc] init];
    packageStatus = [[NSMutableArray alloc] init];
    geoLocationStatusArray = [[NSMutableArray alloc]init];
    _appColor = [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0];

    expandedCells = [[NSMutableSet alloc]init];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionExpiry:) name:@"sessionExpiry" object:nil];

    tokenExpired = NO;
        isPlayerLoaded = NO;
    [_playPauseButton setHidden:YES];
//    [self callApiToFetchNowData];

    self.favTvTableView.backgroundView = [UIView new];
    self.favTvTableView.backgroundView.backgroundColor = [UIColor clearColor];
    //    _previousInteger = 0;
    self.headingView.layer.cornerRadius = 5;
    self.cancelView.layer.cornerRadius = 5;
    self.selectionTableview.layer.cornerRadius = 5;
    //self.pinView.layer.cornerRadius = 5;
    UIBezierPath *linePath1 = [UIBezierPath bezierPath];
    [linePath1 moveToPoint:CGPointMake(8.0, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(232 , 82.0)];
    [linePath1 moveToPoint:CGPointMake(240.0/2, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(240.0/2, 117.0)];

    CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.path = [linePath1 CGPath];
    shapeLayer1.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
    shapeLayer1.lineWidth = 1.0;
    shapeLayer1.fillColor = [[UIColor clearColor] CGColor];

    [self.pinView.layer addSublayer:shapeLayer1];
    self.tvViewHeight.constant = (236/387.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    screenHeight = self.tvViewHeight.constant;

    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.backgroundColor = [UIColor grayColor];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _pinTextField.inputAccessoryView = numberToolbar;
        
        self.adBannerView.delegate = self;
        //self.adBannerView.adUnitID = @"ca-app-pub-5834410729019986/4549014756";
        self.adBannerView.adUnitID = @"ca-app-pub-9149503938080598/8140410483";
        self.adBannerView.rootViewController = self;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
        {
            _adBannerView.hidden = true;
            _adBannerButton.hidden = true;
        }
        else{
            [self.adBannerView loadRequest:[GADRequest request]];
        }

        

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;

    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    self.tabBarController.tabBar.translucent = YES;
    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];

    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];
        
        
    selectedListName = [[NSUserDefaults standardUserDefaults]objectForKey:@"selectedListName"];
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = selectedListName;
    if (titleLabel.text.length > 10) {
        NSRange range = [titleLabel.text rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
        titleLabel.text = [titleLabel.text substringWithRange:range];
        titleLabel.text = [titleLabel.text stringByAppendingString:@"…"];
    }
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.favouriteNav.titleView = titleLabel;

    }

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _adBannerButton.hidden = true;
    }
    else{
        [self.adBannerView loadRequest:[GADRequest request]];
    }
}


- (void)adView:(GADBannerView* )adView didFailToReceiveAdWithError:(GADRequestError* )error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}
-(void)adViewWillPresentScreen:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _adBannerButton.hidden = true;
    }
    else{
        _adBannerView.hidden = false;
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
    
}

-(void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _adBannerButton.hidden = true;
    }
    else{
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
    
}

-(void)adViewWillDismissScreen:(GADBannerView *)bannerView
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"paidCustomer"] isEqualToString:@"Yes"])
    {
        _adBannerView.hidden = true;
        _adBannerButton.hidden = true;
    }
    else{
        
        [self.adBannerView loadRequest:[GADRequest request]];
    }
    
    
}



- (IBAction)adBannerButtonPressed:(id)sender
{
    
    _adBannerView.hidden = true;
    _adBannerButton.hidden = true;
    
}




    -(void)viewDidAppear:(BOOL)animated
    {
        _AVPlayerController = [[AVPlayerViewController alloc]init];
        self.nNextImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
        self.nPreviuosImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
        
    self.tvViewHeight.constant = (239/387.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    //    self.pinView.hidden = YES;
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Network Available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //        [alert show];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
        isPlayerLoaded = NO;
     [self callApiToFetchNowData];
        
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
    timer =  [NSTimer scheduledTimerWithTimeInterval:30*60 target:self selector:@selector(callApiToFetchNowData) userInfo:nil repeats:YES];
        
        locationRefresh = [[LocationUpdate alloc] init];
        [locationRefresh getCurrentLocation];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inHomeView"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inLiveTVView"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inRadioView"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inFavouriteView"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inOnDemandView"];

    [Flurry logEvent:@"FavPlay_Screen" withParameters:nil];
    //    self.favTvTableView.hidden = YES;
    //    self.favTvTableView.userInteractionEnabled = NO;
    //    self.listTableView.hidden = NO;
    //    self.cancelView.hidden = YES;
    //    self.headingView.hidden = YES;
    //    self.selectionTableview.hidden = NO;
    NSLog(@"%f\n%f\n%f\n%f\n",self.headingView.frame.origin.x,self.headingView.frame.origin.y,self.headingView.frame.size.width,self.headingView.frame.size.height);
    selectedListName = [[NSUserDefaults standardUserDefaults]objectForKey:@"selectedListName"];
    [self setNavigationTitle:selectedListName];
    //    [self callApiToFetchNowData];
    //    [self callApiTofetchData];
    }

    - (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    }

    - (void)viewDidDisappear:(BOOL)animated {
    //BEFORE DOING SO CHECK THAT TIMER MUST NOT BE ALREADY INVALIDATED
    //Always nil your timer after invalidating so that
    //it does not cause crash due to duplicate invalidate
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
     //[_player pause];
        [self.AVPlayerController.player pause];
    }

#pragma mark UsageDetails

-(void)addingChannelUsageDetails
{
    //****** Getting the Add Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"slectedFavID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]];
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"https://sprngpodapp.asianetmobiletv.com:8080/index.php/addUsageDetails"];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                //****** Storing the ServiceID In NSUserDefaults*******//
                
                NSString *serviceIDN=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIDN forKey:@"serviceIdN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}

-(void)updateChannelUsageDetails
{
    //****** Getting the Update Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"https://sprngpodapp.asianetmobiletv.com:8080/index.php/updateUsageDetails"];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}


    -(void)reloadtableView
    {

    loginName = @"";
    loginName = [[NSUserDefaults standardUserDefaults]objectForKey:@"loginName"];
    temp = [[NSMutableArray alloc]init];
    temp1 = [[NSMutableArray alloc]init];

    tvProgArray = [[NSMutableArray alloc]init];
    tvLogoArray = [[NSMutableArray alloc]init];
    tvStartTimeArray = [[NSMutableArray alloc]init];
    tvEndTimeArray = [[NSMutableArray alloc]init];
    tvtextFieldArray = [[NSMutableArray alloc]init];
    tvUrlArray = [[NSMutableArray alloc]init];
    blockedStatusArray = [[NSMutableArray alloc]init];
    countryAllowedStatusArray = [[NSMutableArray alloc]init];
    deletionStatusArray = [[NSMutableArray alloc] init];
    packageStatus = [[NSMutableArray alloc] init];
    geoLocationStatusArray = [[NSMutableArray alloc] init];
    parentalStatusArray = [[NSMutableArray alloc] init];

    expandedCells = [[NSMutableSet alloc]init];
    style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont fontWithName:@"FS_Joey-Light" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor grayColor];


    [self callApiToFetchNowData];
    [self.view setNeedsDisplay];
    //     _previousInteger = 0;

    selectedListName = [[NSUserDefaults standardUserDefaults]objectForKey:@"selectedListName"];
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = selectedListName;
    if (titleLabel.text.length > 10) {
        NSRange range = [titleLabel.text rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
        titleLabel.text = [titleLabel.text substringWithRange:range];
        titleLabel.text = [titleLabel.text stringByAppendingString:@"…"];
    }
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.favouriteNav.titleView = titleLabel;


    }




- (void) handleSwipeFromLeft: (id) sender
{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"landscape"] == YES)
    {
        int selectedRow = (int)cellIndexPath.row;
        
        if (selectedRow == tvProgArray.count-1) {
            
        }
        else{
            [self tableView:_favTvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row+1 inSection:0]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lastSwipeRight"];
        }
    }
    
    else
    {
        
    }
    
}
- (void) handleSwipeFromRight: (id) sender
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"landscape"] == YES)
    {
        // code for Landscope orientation
        int selectedRow = (int)cellIndexPath.row;
        if (selectedRow == 0) {
            
        }
        else{
            [self tableView:_favTvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row - 1 inSection:0]];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"lastSwipeRight"];
        }
    }
    else
    {
        
    }
}


    -(void)goToHome{
        [_player pause];
        
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
        
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
    }

    -(void) becomeActive
{
    NSLog(@"ACTIVE");
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    [_playPauseButton setHidden:YES];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [_player pause];
        
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inLiveTVView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inFavouriteView"] == YES)
    {
        [_player play];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inOnDemandView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inRadioView"] == YES)
    {
        [_player pause];
    }
    else{
        
        [_player pause];
        
    }
}

    -(BOOL)textFieldShouldReturn:(UITextField *)textField
    {
    [textField resignFirstResponder];
    return  YES;
    }

    -(void)cancelNumberPad{
    [_pinTextField resignFirstResponder];
    _pinTextField.text = @"";
    }

    -(void)doneWithNumberPad{
    //    NSString *numberFromTheKeyboard = numberTextField.text;
    [_pinTextField resignFirstResponder];
    }


    -(void)cancelButtonApi
    {
    if (selectedListName == nil) {
        
    }
    else
    {
        [self callApiBack];
    }


    }
    -(void)tapOnTvView:(id) sender{
    if (self.playPauseButton.hidden) {

        if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
           playButtonTimer =  [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        [self.playPauseButton setHidden:NO];
        
    }
    else{
            if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){

            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
           playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
    }
    }

    -(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
    }

    - (IBAction)playPausePressed:(id)sender {

        if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
        [self.AVPlayerController.player play];
            
            //****** Getting the Add Channel Usage Details Of chaneels *****//
            [self addingChannelUsageDetails];
            
        playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    }

    else{
        [_player pause];
        
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
        
        [playButtonTimer invalidate];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
    }
    }



    -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
    return 1;
    }

    -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
    if (tableView == self.favTvTableView) {
        return [tvProgArray count];
    }

    else
    return [favouriteFolderNameArray count];
    }


    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    if (tableView == self.favTvTableView) {
        FavTvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavTvCell" forIndexPath:indexPath];
        cell.programLabel.text = tvProgArray[indexPath.row];
        cell.backgroundColor = cell.contentView.backgroundColor;
        NSString *dateString = @"";
        if ([self timeReturn:tvStartTimeArray[indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ" ] && [self timeReturn:tvEndTimeArray[indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ"])
        {
            dateString = [NSString stringWithFormat:@"%@ to %@",[self timeReturn:tvStartTimeArray[indexPath.row ]  format:@"yyyy-MM-dd HH:mm:ssZ"],[self timeReturn:tvEndTimeArray[indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ"]];
        }
        if ([dateString isEqualToString:@""]) {
            dateString = @"NA to NA";
        }
        cell.timeLabel.text = dateString;
    //        cell.timeLabel.text = [NSString stringWithFormat:@"%@ to %@",[self timeReturn:tvStartTimeArray[indexPath.row ]  format:@"yyyy-MM-dd HH:mm:ssZ"],[self timeReturn:tvEndTimeArray[indexPath.row] format:@"yyyy-MM-dd HH:mm:ssZ"]];
        NSURL *url = [NSURL URLWithString:tvLogoArray[indexPath.row]];
        
        cell.tvLogoBG.layer.masksToBounds = YES;
        cell.tvLogoBG.layer.cornerRadius = 26.0;
        cell.tvLogoBG.layer.borderWidth = 1.0;

    //        cell.logoImageView.layer.cornerRadius = 10;
       // cell.tvLogoBG.layer.cornerRadius = 10;
    //        CALayer *borderLayer = [CALayer layer];
    //        CGRect borderFrame = CGRectMake(0, 0, (cell.tvLogoBG.frame.size.width), (cell.tvLogoBG.frame.size.height));
    //        [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    //        [borderLayer setFrame:borderFrame];
    //        [borderLayer setCornerRadius:10];
    //        [borderLayer setBorderWidth:0.1];
    //        [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    //        [cell.tvLogoBG.layer addSublayer:borderLayer];
        [cell.logoImageView sd_setImageWithURL:url];
        if ([blockedStatusArray[indexPath.row] isEqualToString:@"true"]) {
            cell.lockImage.image = [UIImage imageNamed:@"LTlock.png"];
        }
        else
            cell.lockImage.image = [UIImage imageNamed:@"new"];
        if (tvtextFieldArray[indexPath.row]) {
            cell.descriptionTextField.text = tvtextFieldArray[indexPath.row];
        }
        [cell.descriptionTextField setFont:[UIFont fontWithName:@"Raleway-Light" size:11]];
        [cell.descriptionTextField setTextColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0]];
        
        [cell.descriptionButton addTarget:self action:@selector(discriptionPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.descriptionTextField.hidden = YES;
        if (selectedCellIndexPath) {
            cell.descriptionTextField.hidden = NO;
        }
    //        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Rating %@",tvRatingArray[indexPath.row]]];
    //        [cell.ratingLabel setAttributedText:string];
        cell.ratingLabel.text = @"| PG";
        
    //        cell.descriptionTextField.textContainer.maximumNumberOfLines = 1;
        
        
        return cell;
    }
    if (tableView == self.listTableView) {
        FavouriteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavCell" forIndexPath:indexPath];
        cell.favouriteLabel.text = favouriteFolderNameArray[indexPath.row];
        return cell;
    }

    return nil;
    }

    
//    - (IBAction)pinCancelPressed:(id)sender {
//
//    self.pinView.hidden = YES;
//    self.pinTextField.text = @"";
//    [self.view endEditing:YES];
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"landscape"] == YES) {
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"lastSwipeRight"] == YES) {
//        int selectedRow = (int)swipeIndex.row;
//        if (selectedRow == tvLogoArray.count-1) {
//            
//        }
//        else{
//            [self tableView:_favTvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:swipeIndex.row+1 inSection:0]];
//        }
//    }
//
//    else{
//        int selectedRow = (int)swipeIndex.row;
//        if (selectedRow == 0) {
//            
//        }
//        else{
//            [self tableView:_favTvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:swipeIndex.row-1 inSection:0]];
//        }
//    }
//    }
//    else{
//        
//    }
//
//    }

    -(void)sessionExpiry:(NSNotification *)notification{
    [_player pause];
        
        //****** Getting the Update Channel Usage Details Of Chaneels *****//
        [self updateChannelUsageDetails];
        
    timer = nil;

    }


-(void)fetchVerifyPINFav:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            
            long row = [self.favTvTableView indexPathForSelectedRow].row;
            
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            if (temp) {
                NSDictionary *sDic = temp[0];
                
                if ([[sDic objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                    
                    [self.navigationController.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                    selectedCellIndexPath = nil;
                    [[_favTvTableView cellForRowAtIndexPath:cellIndexPath]setSelected:YES animated:YES];
                    blockIndexpath = nil;
                    [self.view endEditing:YES];
                    
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
            
                    [_player pause];
                    
                    //****** Getting the Update Channel Usage Details Of Chaneels *****//
                    [self updateChannelUsageDetails];
                    
                    selectedCellIndexPath = nil;
                    [[_favTvTableView cellForRowAtIndexPath:cellIndexPath]setSelected:NO animated:YES];
                    cellIndexPath = blockIndexpath;
                    [[_favTvTableView cellForRowAtIndexPath:cellIndexPath]setSelected:YES animated:YES];
                    blockIndexpath = nil;
                    NSString *stringFromArray = [tvUrlArray objectAtIndex:cellIndexPath.row];
                    NSString *selectedID = [channelIDArray objectAtIndex:cellIndexPath.row];
                    stringFromUrl = stringFromArray;
                    [[NSUserDefaults standardUserDefaults]setObject:selectedID forKey:@"slectedFavID"];
                    self.currentValue = (int)cellIndexPath.row;
                    NSLog(@"Url : %@",stringFromArray);
                    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                    [_playPauseButton setHidden:YES];
                    
                    _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                    
                    [self addChildViewController:_AVPlayerController];
                    [self.tvView addSubview:_AVPlayerController.view];
                    _AVPlayerController.view.frame = self.tvView.bounds;
                    _AVPlayerController.showsPlaybackControls = false;
                    _AVPlayerController.view.userInteractionEnabled = false;
                    _player.closedCaptionDisplayEnabled = NO;
                    _AVPlayerController.player = _player;
                    [_player play];
                    
                    //****** Getting the Add Channel Usage Details Of chaneels *****//
                    [self addingChannelUsageDetails];
                 
                    FavTvTableViewCell *cell = (FavTvTableViewCell *)[self.favTvTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                    cell.lockImage.image = [UIImage imageNamed:@"LTlock.png"];
                    
                    NSString *url1 = @"storeFavouriteTvChannelLastSeenDetails";
                    
                    NSString *post1 = [NSString stringWithFormat:@"customerId=%@&listName=%@&channelId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],[[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"]];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavLastSeen:) name:url1 object:nil];
                    
                    [UrlRequest postRequest:url1 arguments:post1 view:self.view];
                }
            }
        }
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    
    [self.loader stopAnimating];
    [self.loader hidesWhenStopped];

    
    
    
}




    - (void)keyboardWasShown:(NSNotification*)aNotification
    {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float diff = self.view.frame.size.height/2.0 - kbSize.height - self.pinView.frame.size.height/2.0;

    if (diff < 0) {
    //        self.pinViewVerticalContraint.constant = diff;
    }

    }

    // Called when the UIKeyboardWillHideNotification is sent
    - (void)keyboardWillBeHidden:(NSNotification*)aNotification
    {
    //    self.pinViewVerticalContraint.constant = 0;

    }

    - (IBAction)homePressed:(id)sender {
        
        if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying)
        {
             [_player pause];
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }

   
       
    [self dismissViewControllerAnimated:YES completion:nil];
    }
    - (IBAction)backPressed:(id)sender {
        if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPlaying)
        {
            [_player pause];
            //****** Getting the Update Channel Usage Details Of Chaneels *****//
            [self updateChannelUsageDetails];
        }
    [self dismissViewControllerAnimated:YES completion:nil];
    }

    - (void)viewWillDisappear:(BOOL)animated {
    //BEFORE DOING SO CHECK THAT TIMER MUST NOT BE ALREADY INVALIDATED
    //Always nil your timer after invalidating so that
    //it does not cause crash due to duplicate invalidate
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
        [_player pause];
    }

    -(void)setNavigationTitle:(NSString *)title
    {

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = title;
    if (titleLabel.text.length > 10) {
        NSRange range = [titleLabel.text rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
        titleLabel.text = [titleLabel.text substringWithRange:range];
        titleLabel.text = [titleLabel.text stringByAppendingString:@"…"];
    }
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];

    //
    //    if ([[self.childViewControllers valueForKey:@"class"] containsObject:[FourthViewController class]]) {
    //         titleLabel.text = @"MORE";
    //    }
    self.favouriteNav.titleView = titleLabel;
    }



    - (IBAction)discriptionPressed:(id)sender {

    UIButton *button = (UIButton *)sender;
    CGRect buttonFrame = [button convertRect:button.bounds toView:_favTvTableView];
    NSIndexPath *indexPath = [_favTvTableView indexPathForRowAtPoint:buttonFrame.origin];

    //    NSIndexPath *nowSelectedIndexPath = [_favTvTableView indexPathForSelectedRow];

    selectedCellIndexPath = indexPath;
    //    if (nowSelectedIndexPath.row == selectedCellIndexPath.row) {

    //        tableCell.descriptionTextField.hidden = NO;
    if ([expandedCells containsObject:indexPath]) {
        [expandedCells removeObject:indexPath];
    }
    else
    {
        [expandedCells addObject:indexPath];
    }

        [_favTvTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    //
    //    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"desc"];
    //    
    //    for (UITableViewCell *cell in self.favTvTableView.visibleCells) {
    //        if(cell.tag == indexPath.row) {
    //            for(UIButton *button in cell.subviews) {
    //                if([button isKindOfClass:[UIButton class]]) {
    //                    if(button.tag == indexPath.row) {
    //                        [button setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
    //                    }
    //                }
    //            }
    //        }
    //    }

    //        if (!selectedCellIndexPath.row == 0) {
    //            for (FavouriteTableViewCell *cell in [_favTvTableView visibleCells]) {
    //                [cell setSelected:NO animated:YES];
    //            }
    //        }
    //        [[_favTvTableView cellForRowAtIndexPath:selectedCellIndexPath] setSelected:YES animated:YES];
    //    }
    }

    - (IBAction)nNextPressed:(id)sender {

    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else if (noDataFound == YES){
        [self.navigationController.view makeToast:@"NO Data Found" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else{
        @try {
            
            self.loader.hidden = NO;
            [self.loader startAnimating];
            self.nNextImage.image = [UIImage imageNamed:@"leftarrow-highlighted.png"];
            self.nPreviuosImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
            
            [self.nextButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.nowButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            
    
            NSString *url = @"fetchAllOtherTvChannelFavouriteList3";

    //    _previousInteger++;
    NSString *joinedComponents = [tvEndTimeArray componentsJoinedByString:@","];
    NSString *joinedComponentsforChannel= [channelIDArray componentsJoinedByString:@","];


    NSString *post = [NSString stringWithFormat:@"customerId=%@&key=next&otherTime=%@&channelIds=%@&countryCode=%@&listName=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],joinedComponents,joinedComponentsforChannel,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[selectedListName encode],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
    NSLog(@"post %@",post);
            
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavAllotherEpg:) name:url object:nil];
            
        [UrlRequest postRequest:url arguments:post view:self.view];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
        
    }
    }

    - (IBAction)nPreviousPressed:(id)sender {


    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else if (noDataFound == YES){
        [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else{
        
        @try {
            
            self.loader.hidden = NO;
            [self.loader startAnimating];
            
            self.nNextImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
            self.nPreviuosImage.image = [UIImage imageNamed:@"rightarrow-highlighted.png"];
            
            
            
            [self.nextButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            [self.nowButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
            
            

            
    NSString *url = @"fetchAllOtherTvChannelFavouriteList3";

    NSString *joinedComponents = [tvStartTimeArray componentsJoinedByString:@","];
    NSString *joinedComponentsforChannel= [channelIDArray componentsJoinedByString:@","];


    NSString *post = [NSString stringWithFormat:@"customerId=%@&key=previous&otherTime=%@&channelIds=%@&countryCode=%@&listName=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],joinedComponents,joinedComponentsforChannel,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[selectedListName encode],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
    NSLog(@"post %@",post);
            
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavAllotherEpg:) name:url object:nil];
            
    [UrlRequest postRequest:url arguments:post view:self.view];

            
    NSLog(@"%@",joinedComponents);
    NSLog(@"%@",joinedComponentsforChannel);
          
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
        
    }
        
    }


-(void)fetchFavAllotherEpg:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            tvEndTimeArray = [[NSMutableArray alloc]init];
            tvStartTimeArray = [[NSMutableArray alloc]init];
            tvtextFieldArray    = [[NSMutableArray alloc]init];
            tvProgArray = [[NSMutableArray alloc]init];
            
            if (temp) {
                NSLog(@"your profile updated successfully");
                
                
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *insideDictionary = temp[i];
                    
                    if ([[insideDictionary objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                    }
                    
                    else{
                        [tvProgArray addObject:[insideDictionary objectForKey:@"eventName"]];
                        
                        if ([[insideDictionary objectForKey:@"eventDescription"]isEqualToString:@""]) {
                            NSString *emptystring = @"No Description Available";
                            [tvtextFieldArray addObject:emptystring];
                        }
                        
                        else{
                            [tvtextFieldArray addObject:[insideDictionary objectForKey:@"eventDescription"]];
                        }
                        if  ([[insideDictionary objectForKey:@"start"]isEqualToString:@""])
                        {
                            NSString *emptyString = @"default";
                            [tvStartTimeArray addObject:emptyString];
                            
                        }
                        else
                        {
                            [tvStartTimeArray addObject:[insideDictionary objectForKey:@"start"]];
                        }
                        
                        if ([[insideDictionary objectForKey:@"end"]isEqualToString:@""]) {
                            NSString *emptyString = @"default";
                            [tvEndTimeArray addObject:emptyString];
                        }
                        else{
                            [tvEndTimeArray addObject:[insideDictionary objectForKey:@"end"]];
                        }
                        
                        
                        [tvRatingArray addObject:[insideDictionary objectForKey:@"rating"]];
                        
                    }
                    
                }
                
                [self.favTvTableView reloadData];
            }
            FavTvTableViewCell *cell = (FavTvTableViewCell *)[self.favTvTableView cellForRowAtIndexPath:cellIndexPath];
            [cell setSelected:YES];
            
        }
    }
    @catch(NSException *exception) {
        
    } @finally{
        
    }
    
    [self.loader stopAnimating];
    [self.loader hidesWhenStopped];

}

    - (IBAction)nowPressed:(id)sender {

    _previousInteger = 0;
    FavTvTableViewCell *cell = (FavTvTableViewCell *)[self.favTvTableView cellForRowAtIndexPath:cellIndexPath];
    [cell setSelected:YES];
        self.nNextImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
        self.nPreviuosImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
        
        
        [self.nextButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.nowButton setTitleColor:[UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        
    @try {
        
        self.loader.hidden = NO;
        [self.loader startAnimating];

    NSDateFormatter *dateformate = [[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy/MM/dd"];
    NSString *date_String = [dateformate stringFromDate:[NSDate date]];
        
        NSString *urlParameter = [NSString stringWithFormat:@"%@&listName=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
        
        NSString * urlRequest = [NSString stringWithFormat:@"?fetchTvChannelsFavouriteList4=%@",urlParameter];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchTvChannelsFavList:) name:urlRequest object:nil];
        
        [UrlRequest getRequest:urlRequest view:self.view];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

    }


    - (IBAction)nextPressed:(id)sender {
        
        self.nNextImage.image = [UIImage imageNamed:@"darkleftArrow.png"];
        self.nPreviuosImage.image = [UIImage imageNamed:@"darkrightArrow.png"];
        [self.nextButton setTitleColor:[UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        [self.nowButton setTitleColor:[UIColor colorWithRed:(137.0/255.0) green:(137.0/255.0) blue:(137.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        
        
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else if (noDataFound == YES){
        [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else{
        @try {
            
            self.loader.hidden = NO;
            [self.loader startAnimating];
            
    [dateformate setDateFormat:@"yyyy/MM/dd"];

    NSString *date_String=[dateformate stringFromDate:[NSDate date]];
            
    NSString *urlParameter = [NSString stringWithFormat:@"%@&listName=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
            
    NSString * urlRequest = [NSString stringWithFormat:@"?fetchFirstNextTvChannelsFavouriteList4=%@", urlParameter];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavNext:) name:urlRequest object:nil];
   
            [UrlRequest getRequest:urlRequest view:self.view];
   
          
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    }


    }

-(void)fetchFavNext:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
           
            temp =[[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            tvProgArray = [[NSMutableArray alloc]init];
            tvRatingArray = [[NSMutableArray alloc]init];
            tvEndTimeArray = [[NSMutableArray alloc]init];
            tvStartTimeArray = [[NSMutableArray alloc]init];
            tvtextFieldArray = [[NSMutableArray alloc]init];
            deletionStatusArray = [[NSMutableArray alloc]init];
            countryAllowedStatusArray = [[NSMutableArray alloc]init];
            geoLocationStatusArray = [[NSMutableArray alloc] init];
            parentalStatusArray = [[NSMutableArray alloc] init];
            packageStatus = [[NSMutableArray alloc]init];
            
            if(temp) {
                NSLog(@"temp = %@",temp);
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *dict = [[NSDictionary alloc]init];
                    dict = temp[i];
                    
                    if ([[dict objectForKey:@"deletionStatus"]isEqualToString:@"true"]) {
                        
                    }
                    else if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        [self goToHome];
                    }
                    
                    else{
                        
                        if ([[dict objectForKey:@"eventDescription"]isEqualToString:@""]){
                            NSString *emptystring = @"No Description Available";
                            [tvtextFieldArray addObject:emptystring];
                        }
                        
                        else{
                            [tvtextFieldArray addObject:[dict objectForKey:@"eventDescription"]];
                        }
                        [tvProgArray addObject:[dict objectForKey:@"eventName"]];
                        if ([[dict objectForKey:@"end"] isEqualToString:@""]) {
                            NSString *emptyString = @"default";
                            [tvEndTimeArray addObject:emptyString];
                        }
                        else{
                            [tvEndTimeArray addObject:[dict objectForKey:@"end"]];
                        }
                        if ([[dict objectForKey:@"start"] isEqualToString:@""]) {
                            NSString *emptyString = @"default";
                            [tvStartTimeArray addObject:emptyString];
                        }
                        else{
                            [tvStartTimeArray addObject:[dict objectForKey:@"start"]];
                        }
                        [tvRatingArray addObject:[dict objectForKey:@"rating"]];
                        [deletionStatusArray addObject:[dict objectForKey:@"deletionStatus"]];
                        [parentalStatusArray addObject:[dict objectForKey:@"parentalStatus"]];
                        [countryAllowedStatusArray addObject:[dict objectForKey:@"countryAllowed"]];
                        [packageStatus addObject:[dict objectForKey:@"packageStatus"]];
                        [geoLocationStatusArray addObject:[dict objectForKey:@"geoLocationStatus"]];
                        
                        [self.favTvTableView reloadData];
                    }
                }
            }
            FavTvTableViewCell *cell = (FavTvTableViewCell *)[self.favTvTableView cellForRowAtIndexPath:cellIndexPath];
            [cell setSelected:YES];
            
        }
    }
    @catch(NSException *exception) {
        
    } @finally{
        
    }
    
    [self.loader stopAnimating];
    [self.loader hidesWhenStopped];

    
}




    -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
    CGFloat heightOfCell;

    if ([tableView isEqual:self.listTableView]) {
        heightOfCell = 44;
    }
    else if ([tableView isEqual:self.favTvTableView]){
        if(selectedCellIndexPath != nil
           && [selectedCellIndexPath compare:indexPath] == NSOrderedSame)
        {
            if ([expandedCells containsObject:indexPath]) {
                heightOfCell = 140;
            }
            else{
                heightOfCell = 69;
            }
        }
        else{
            heightOfCell = 69;
        }
    }
    return heightOfCell;
    }


    -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        if (tableView == self.favTvTableView) {
        NSLog(@"selectedRow = %ld",(long)indexPath.row);
            
            
    //        if ([packageStatus[indexPath.row] isEqualToString:@"false"]) {
    //            for (FavouriteTableViewCell *cell in [tableView visibleCells]) {
    //                [cell setSelected:NO animated:YES];
    //            }
    //            [[tableView cellForRowAtIndexPath:indexPath] setSelected:YES animated:YES];
    //            [moviePlayerController.moviePlayer stop];
    //            [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This Channel is not Subscribed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    //            
    //        }
             swipeIndex = indexPath;
            

            
            if ([deletionStatusArray[indexPath.row] isEqualToString:@"true"]) {
                
                [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
                [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                [self.view endEditing:YES];
                
                [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
    //            else if ([parentalStatusArray[indexPath.row] isEqualToString:@"true"]){
    //                blockIndexpath = indexPath;
    //                [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
    //                [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
    //                self.pinView.hidden = NO;
    //            }
            
            else if ([geoLocationStatusArray[indexPath.row] isEqualToString:@"false"]){
                [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
                [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                [self.view endEditing:YES];
                
                [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
            }

            
            else if ([packageStatus[indexPath.row] isEqualToString:@"false"]){
                [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
                [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                [self.view endEditing:YES];
                
                [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else if ([countryAllowedStatusArray[indexPath.row] isEqualToString:@"false"]){
                [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
                [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                [self.view endEditing:YES];
                
                [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else if ([countryAllowedStatusArray[indexPath.row] isEqualToString:@"false"]){
                [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
                [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
                [self.view endEditing:YES];
                
                [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
            }
        
       else if([blockedStatusArray[indexPath.row] isEqualToString:@"true"]) {
             blockIndexpath = indexPath;
             
             [[_favTvTableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
             [[_favTvTableView cellForRowAtIndexPath:cellIndexPath] setSelected:YES animated:YES];
           
           favouritepinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                              message:@""
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles:@"Ok",nil];
           favouritepinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
           
           UITextField *pinTextField = [favouritepinviewAlert textFieldAtIndex:0];
           pinTextField.delegate = self;
           
           [pinTextField becomeFirstResponder];
           pinTextField.keyboardType = UIKeyboardTypeNumberPad;
           pinTextField.placeholder = @"Enter PIN";
           
           
           [favouritepinviewAlert show];
           

            // self.pinView.hidden = NO;
           
           
           
    //            blockAlert = [[UIAlertView alloc]initWithTitle:@"Blocked" message:@"Enter PIN to Unblock" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK ", nil];
    //            blockAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    //            UITextField *textField = [blockAlert textFieldAtIndex:0];
    //            textField.delegate = self;
    //            textField.keyboardType = UIKeyboardTypeNumberPad;
    //            [blockAlert show];
        }
        
        else{
        selectedCellIndexPath = nil;
        cellIndexPath  = indexPath;
        NSMutableArray *avisiBleIndexpaths = [[NSMutableArray alloc]init];
        for (NSIndexPath *indexpath1 in tableView.indexPathsForVisibleRows) {
            if(indexpath1.row != indexPath.row)
                [avisiBleIndexpaths addObject:indexpath1];
        }
        
        [_favTvTableView reloadRowsAtIndexPaths:avisiBleIndexpaths withRowAnimation:UITableViewRowAnimationNone];
        
        
        //    if(!indexPath.row == 0) {
        for (FavouriteTableViewCell *cell in [tableView visibleCells]) {
            [cell setSelected:NO animated:YES];
        }
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:YES animated:YES];
        //    }
            if (![loginName isEqualToString:@""]) {
                if ([packageStatus[indexPath.row] isEqualToString:@"false"]) {
                   // [moviePlayerController.moviePlayer stop];
                    [_player pause];
                    
                    [self.navigationController.view makeToast:@"This Channel is not Subscribed" duration:2.0 position:CSToastPositionCenter style:style];
    //                    [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This Channel is not Subscribed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                }
                else{
                    
                    [_player pause];
                    
                    //****** Getting the Update Channel Usage Details Of Chaneels *****//
                  [self updateChannelUsageDetails];
                    
                    NSString *selectedID = [channelIDArray objectAtIndex:indexPath.row];
                    [[NSUserDefaults standardUserDefaults]setObject:selectedID forKey:@"slectedFavID"];
                    NSString *stringFromArray = [tvUrlArray objectAtIndex:indexPath.row];
                    stringFromUrl = stringFromArray;
                    self.currentValue = (int)indexPath.row;
                    NSLog(@"Url : %@",stringFromArray);
                    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                    [_playPauseButton setHidden:YES];
                  _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                    
                    [self addChildViewController:_AVPlayerController];
                    [self.tvView addSubview:_AVPlayerController.view];
                    
                    _AVPlayerController.view.frame = self.tvView.bounds;
                    _AVPlayerController.showsPlaybackControls = false;
                    _AVPlayerController.view.userInteractionEnabled = false;
                    _player.closedCaptionDisplayEnabled = NO;
                    _AVPlayerController.player = _player;
                    
                    [_player play];
                    
                    //****** Getting the Adding Channel Usage Details Of Chaneels *****//
                    [self addingChannelUsageDetails];

                    
                        NSString *url1 = @"storeFavouriteTvChannelLastSeenDetails";
                        
                        NSString *post1 = [NSString stringWithFormat:@"customerId=%@&listName=%@&channelId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],[[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"]];
                        NSLog(@"post %@",post1);
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavLastSeen:) name:url1 object:nil];
                        
                        [UrlRequest postRequest:url1 arguments:post1 view:self.view];

                        }
                }
            
            else
            {
                [_player pause];
                
                //****** Getting the Update Channel Usage Details Of Chaneels *****//
               [self updateChannelUsageDetails];
                
                NSString *selectedID = [channelIDArray objectAtIndex:indexPath.row];
                [[NSUserDefaults standardUserDefaults]setObject:selectedID forKey:@"slectedFavID"];

                NSString *stringFromArray = [tvUrlArray objectAtIndex:indexPath.row];
                self.currentValue = (int)indexPath.row;
                NSLog(@"Url : %@",stringFromArray);
                stringFromUrl = stringFromArray;
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                
                [_playPauseButton setHidden:YES];
                
                _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                [self addChildViewController:_AVPlayerController];
                [self.tvView addSubview:_AVPlayerController.view];
                
                _AVPlayerController.view.frame = self.tvView.bounds;
                _AVPlayerController.showsPlaybackControls = false;
                _AVPlayerController.view.userInteractionEnabled = false;
                _player.closedCaptionDisplayEnabled = NO;
                _AVPlayerController.player = _player;

                [_player play];
                
                //****** Getting the Adding Channel Usage Details Of Chaneels *****//
                [self addingChannelUsageDetails];
                [_playPauseButton setHidden:YES];
                NSString *url1 = @"storeFavouriteTvChannelLastSeenDetails";
                    
                    NSString *post1 = [NSString stringWithFormat:@"customerId=%@&listName=%@&channelId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],[[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"]];
                    NSLog(@"post %@",post1);
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavLastSeen:) name:url1 object:nil];
                
                [UrlRequest postRequest:url1 arguments:post1 view:self.view];
                
                
            }
            
            
        }
    }

    if (tableView == self.listTableView)
    {
        self.listTableView.hidden = YES;
        self.cancelView.hidden = YES;
        self.headingView.hidden = YES;
        self.selectionTableview.hidden = YES;
        self.currentValue = 0;
        cellIndexPath = 0;
        selectedListName = nil;
        selectedListName = favouriteFolderNameArray[indexPath.row];
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = selectedListName;
        if (titleLabel.text.length > 10) {
            NSRange range = [titleLabel.text rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
            titleLabel.text = [titleLabel.text substringWithRange:range];
            titleLabel.text = [titleLabel.text stringByAppendingString:@" …"];
        }
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        self.favouriteNav.titleView = titleLabel;
        [self callApiToFetchNowData];
    }
    }

-(void)fetchFavLastSeen:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            
            temp = [[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            if (temp) {
                NSDictionary *sDic = temp[0];
                if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                    NSLog(@"Customer Does Not Exist");
                }
                
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                    [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                    [self goToHome];
                }
                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Seen Stored Successfully" ]) {
                    NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"]);
                }
                
        }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}

    -(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
    {
    if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft || toInterfaceOrientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"Landscape");
        
        [self.navigationController setNavigationBarHidden:TRUE animated:FALSE];
        self.tabBarController.tabBar.hidden = YES;
        self.tvViewHeight.constant = MIN(self.view.frame.size.width, self.view.frame.size.height) ;
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
       
        _AVPlayerController.view.frame = self.tvView.bounds;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"landscape"];
        
    }
    else{
        NSLog(@"Portrait");
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    //        self.tvViewHeight.constant = (236/386.0) *  MIN(self.view.frame.size.width, self.view.frame.size.height);
        self.tvViewHeight.constant = screenHeight;
        [self.navigationController setNavigationBarHidden:false animated:FALSE];
        self.tabBarController.tabBar.hidden = false;
        
        
        _AVPlayerController.view.frame = self.tvView.bounds;
       
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"landscape"];
        [_AVPlayerController.player play];

        }
    }

    -(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
    {
        
        if (alertView == favouritepinviewAlert) {
            if (buttonIndex == 1) {
                
                @try {
                    //self.pinView.hidden = YES;
                    _AVPlayerController.view.hidden = NO;
                    selectedCellIndexPath = nil;
                    [self.view endEditing:YES];
                    pinString = [favouritepinviewAlert textFieldAtIndex:0].text;
                    // pinString = @"";
                    // pinString = self.pinTextField.text;
                    self.pinTextField.text = @"";
                    _loader.hidden = NO;
                    [self.loader startAnimating];
                    
                    NSString *url = @"verifyPIN1";
                    
                    NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                    NSLog(@"post %@",post);
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchVerifyPINFav:) name:url object:nil];
                    
                    [UrlRequest postRequest:url arguments:post view:self.view];
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
            }else{
                pinString = [favouritepinviewAlert textFieldAtIndex:0].text;
                self.pinTextField.text = @"";
                [self.view endEditing:YES];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"landscape"] == YES) {
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"lastSwipeRight"] == YES) {
                        int selectedRow = (int)swipeIndex.row;
                        if (selectedRow == tvLogoArray.count-1) {
                            
                        }
                        else{
                            [self tableView:_favTvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:swipeIndex.row+1 inSection:0]];
                        }
                    }
                    
                    else{
                        int selectedRow = (int)swipeIndex.row;
                        if (selectedRow == 0) {
                            
                        }
                        else{
                            [self tableView:_favTvTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:swipeIndex.row-1 inSection:0]];
                        }
                    }
                }
                else{
                    
                }
                
            }
        }
        
    if (alertView == blockAlert) {
        if (buttonIndex == 1) {
            selectedCellIndexPath = nil;
            UITextField *pin = (UITextField *)[alertView textFieldAtIndex:0];
            pinString = @"";
            pinString = pin.text;
            
            long row = [self.favTvTableView indexPathForSelectedRow].row;
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/verifyPIN1",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
            
            NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
            NSLog(@"post %@",post);
            
            NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:postData];
            
            NSURLResponse *response;
            NSError *error = nil;
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //            NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //            NSDictionary *json_dict = (NSDictionary *)json_string;
    //            NSLog(@"json_dict\n%@",json_dict);
    //            NSLog(@"json_string\n%@",json_string);
            
            NSLog(@"%@",response);
            NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
            NSLog(@" error is %@",error);
            
            if (!error) {
                NSError *myError = nil;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                NSLog(@"%@",dictionary);
                temp = [dictionary objectForKey:@"root"];
                if (temp) {
                    NSDictionary *sDic = temp[0];
                    if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                        
                        [self.navigationController.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
    //                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PIN" message:@"Enter Correct PIN" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        selectedCellIndexPath = nil;
                        [[_favTvTableView cellForRowAtIndexPath:cellIndexPath]setSelected:YES animated:YES];
                        blockIndexpath = nil;
                        [self.view endEditing:YES];
    //                        [alert show];
                    }
                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                    
                        [_player pause];
                        
                        //****** Getting the Update Channel Usage Details Of Chaneels *****//
                        [self updateChannelUsageDetails];
                        
                        selectedCellIndexPath = nil;
                        [[_favTvTableView cellForRowAtIndexPath:cellIndexPath]setSelected:NO animated:YES];
                        cellIndexPath = blockIndexpath;
                        [[_favTvTableView cellForRowAtIndexPath:cellIndexPath]setSelected:YES animated:YES];
                        blockIndexpath = nil;
                        NSString *stringFromArray = [tvUrlArray objectAtIndex:cellIndexPath.row];
                        
                        NSString *selectedID = [channelIDArray objectAtIndex:cellIndexPath.row];
                        [[NSUserDefaults standardUserDefaults]setObject:selectedID forKey:@"slectedFavID"];
                        self.currentValue = (int)cellIndexPath.row;
                        NSLog(@"Url : %@",stringFromArray);
                        stringFromUrl = stringFromArray;

                        _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];
                        [self.AVPlayerController.player play];
                        
                        //****** Getting the Adding Channel Usage Details Of Chaneels *****//
                        [self addingChannelUsageDetails];
                        
                        FavTvTableViewCell *cell = (FavTvTableViewCell *)[self.favTvTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                        cell.lockImage.image = [UIImage imageNamed:@"lock.png"];
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/storeFavouriteTvChannelLastSeenDetails",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
                            
                            NSString *post1 = [NSString stringWithFormat:@"customerId=%@&listName=%@&channelId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],selectedListName,[[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"]];
                            NSLog(@"post %@",post1);
                            
                            NSData *postData1 =[post1 dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                            
                            NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url1];
                            [request1 setHTTPMethod:@"POST"];
                            [request1 setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                            [request1 setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData1.length] forHTTPHeaderField:@"Content-Length"];
                            [request1 setHTTPBody:postData1];
                            
                            NSURLResponse *response1;
                            NSError *error1 = nil;
                            NSData *receivedData1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
    //                            NSString *json_string1 = [[NSString alloc] initWithData:receivedData1 encoding:NSUTF8StringEncoding];
    //                            NSDictionary *json_dict1 = (NSDictionary *)json_string1;
    //                            NSLog(@"json_dict\n%@",json_dict1);
    //                            NSLog(@"json_string\n%@",json_string1);
                            
                            NSLog(@"%@",response1);
                            NSLog(@"received Data is : %@  Class is : %@",receivedData1,[response1 class]);
                            NSLog(@" error is %@",error1);
                            
                            if (!error1) {
                                NSError *myError = nil;
                                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData1 options:NSJSONReadingMutableLeaves error:&myError];
                                NSLog(@"%@",dictionary);
                                temp = [dictionary objectForKey:@"root"];
                                if (temp) {
                                    NSDictionary *sDic = temp[0];
                                    if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                                        NSLog(@"Customer Does Not Exist");
                                    }
                                    else if ([[sDic objectForKey:@"status"]isEqualToString:@"Last Seen Stored Successfully" ]) {
                                        NSLog(@"lastTv = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"slectedFavID"]);
                                    }
                                    
                                    else{
                                        NSLog(@"temp is getting Null");
                                    }
                                    
                                    
                                }
                                else
                                {
                                    NSLog(@"%@",error1);
                                }
                                
                            }
                        });
                    }
                    
                    else{
                        NSLog(@"temp is getting Null");
                    }
                    
                    
                }
                else
                {
                    NSLog(@"%@",error);
                }
                
            }
        }
    }


    }

    -(NSString *)timeReturn:(NSString *)date_rec format:(NSString *)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *dateFromString = [dateFormatter dateFromString:date_rec];

    //    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    //    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
     [dateFormatter setDateFormat:@"HH:mm"];
    NSString *formattedTimeString = [dateFormatter stringFromDate:dateFromString];
    NSLog(@"formattedDateString: %@", formattedTimeString);
    return  formattedTimeString;
    }

    - (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.favTvTableView) {
        if(indexPath.row == 0 && ![tableView indexPathsForSelectedRows] && !selectedCellIndexPath && !cellIndexPath){
            [cell setSelected:YES animated:YES];
    }
        else if (cellIndexPath){
            if (cellIndexPath.row == indexPath.row) {
                [cell setSelected:YES animated:YES];
            }
        }
        if (blockIndexpath == indexPath) {
            [cell setSelected:NO animated:YES];
        }
        
    }
    }

    //-(void)callApiTofetchData
    //{
    //    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://52.22.22.229/index.php/?fetchFavouriteTvChannelListNames1=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    //    
    //    NSURLResponse * response = nil;
    //    NSError * error = nil;
    //    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
    //                                                  returningResponse:&response
    //                                                              error:&error];
    //    
    //    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //    NSDictionary *json_dict = (NSDictionary *)json_string;
    //    NSLog(@"json_dict\n%@",json_dict);
    //    NSLog(@"json_string\n%@",json_string);
    //    
    //    NSLog(@"%@",response);
    //    //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    //    NSLog(@" error is %@",error);
    //    if (!error) {
    //        NSError *myError = nil;
    //        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
    //        
    //        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    //        NSInteger status =  [httpResponse statusCode];
    //        temp =[dictionary objectForKey:@"root"];
    //        NSLog(@"status = %ld",(long)status);
    //        favouriteFolderNameArray = [[NSMutableArray alloc]init];
    //        
    //        
    //        if(temp && status == 200) {
    //            NSLog(@"temp = %@",temp);
    //            NSDictionary *dict = [[NSDictionary alloc]init];
    //            dict = temp[0];
    //            temp1 = [dict objectForKey:@"listNames"];
    //           
    //            NSLog(@"temp1 = %@", temp1);
    //            NSLog(@"temp1 Coumt = %lu ",(unsigned long)temp1.count);
    //            for (int i = 0; i < temp1.count; i++) {
    //               
    //                [favouriteFolderNameArray addObject:temp1[i]];
    //                
    //            }
    //            [self.listTableView reloadData];
    //        }
    //        else{
    //            NSLog (@"Oh Sorry");
    //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            [alert show];
    //            
    //        }
    //    }
    //    if (favouriteFolderNameArray.count == 0) {
    //        [self.tabBarController setSelectedIndex:0];
    //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Go to More to create favoruties list" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        [alert show];
    //    }
    //    
    //}

    - (IBAction)cancelPressed:(id)sender {
    self.listTableView.hidden = YES;
    self.cancelView.hidden = YES;
    self.headingView.hidden = YES;
    self.selectionTableview.hidden = YES;
        [_player pause];
       //    [self.tabBarController setSelectedIndex:0];

    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [FirstViewController class]]) {
        
        FirstViewController *firstVC = (FirstViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Live TV"];
    //        [firstVC reloadTableview];
        
    }
    if ([[((UINavigationController *)self.parentViewController).viewControllers valueForKey:@"class" ] containsObject: [SecondViewController class]]) {
        
        SecondViewController *firstVC = (SecondViewController *)[((UINavigationController *)self.parentViewController).viewControllers objectAtIndex:0];
        [firstVC setNavigationTitle:@"Radio"];
        
    }

    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];


    }

    -(void) callApiBack
    {

    if (packageNotFound == YES) {
        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else if (noDataFound == YES){
        [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
    }

    else{
        
        
        @try {
            
            self.loader.hidden = NO;
            [self.loader startAnimating];
            
    NSString * urlRequest = [NSString stringWithFormat:@"?fetchFavouriteTvChannelListNames1=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFavLists:) name:urlRequest object:nil];
                [UrlRequest getRequest:urlRequest view:self.view];
        
    }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
    }

    }

-(void)fetchFavLists:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
    temp =[[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
    
    favouriteFolderNameArray = [[NSMutableArray alloc]init];
    
    
    if(temp) {
        NSLog(@"temp = %@",temp);
        NSDictionary *dict = [[NSDictionary alloc]init];
        dict = temp[0];
        
        if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
            [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
            [self goToHome];
            
        }
        else{
            temp1 = [dict objectForKey:@"listNames"];
            
            NSLog(@"temp1 = %@", temp1);
            NSLog(@"temp1 Coumt = %lu ",(unsigned long)temp1.count);
            for (int i = 0; i < temp1.count; i++) {
                
                [favouriteFolderNameArray addObject:temp1[i]];
                
            }
        }
        
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"deleted"] == YES) {
        NSString *favName = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedListName"];
        long listIndex = [favouriteFolderNameArray indexOfObject:favName];
        NSString *listString = [@(listIndex) stringValue];
        [[NSUserDefaults standardUserDefaults] setObject:listString forKey:@"listIndex"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"deleted"];
    }
    else{
        
    }
    
    newString = [[NSUserDefaults standardUserDefaults] objectForKey:@"listIndex"];
    int newIndex = [newString intValue];
    newFolder = [favouriteFolderNameArray objectAtIndex:newIndex];
    
    if ([newFolder isEqualToString:selectedListName]) {
        
        [_loader stopAnimating];
        [_loader hidesWhenStopped];
        
    }
    else
    {
        selectedListName = newFolder;
        [[NSUserDefaults standardUserDefaults] setObject:selectedListName forKey:@"selectedListName"];
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = selectedListName;
        if (titleLabel.text.length > 10) {
            NSRange range = [titleLabel.text rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
            titleLabel.text = [titleLabel.text substringWithRange:range];
            titleLabel.text = [titleLabel.text stringByAppendingString:@"…"];
        }
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        self.favouriteNav.titleView = titleLabel;
        
        
        NSDateFormatter *dateformate = [[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy/MM/dd"];
        NSString *date_String = [dateformate stringFromDate:[NSDate date]];
        selectedListName = [[NSUserDefaults standardUserDefaults]objectForKey:@"selectedListName"];
        //    selectedListName = [selectedListName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString *urlParameter = [NSString stringWithFormat:@"%@&listName=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
        
        
        NSString * urlRequest1 = [NSString stringWithFormat:@"?fetchTvChannelsFavouriteList4=%@",urlParameter];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchTvChannelsFavList:) name:urlRequest1 object:nil];
        
        [UrlRequest getRequest:urlRequest1 view:self.view];
        
        
    }
    }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

}

-(void) callApiToFetchNowData {

    @try {
        
        self.loader.hidden = NO;
        [self.loader startAnimating];

    NSDateFormatter *dateformate = [[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy/MM/dd"];
    NSString *date_String = [dateformate stringFromDate:[NSDate date]];
    selectedListName = [[NSUserDefaults standardUserDefaults]objectForKey:@"selectedListName"];
        
        NSString *urlParameter = [NSString stringWithFormat:@"%@&listName=%@&date=%@&countryCode=%@&MCC=%@&MNC=%@&ipAddress=%@&timeZone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[selectedListName encode],date_String,[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"iPA"],[[NSUserDefaults standardUserDefaults]objectForKey:@"timeZone"]];
        
    NSString * urlRequest = [NSString stringWithFormat:@"?fetchTvChannelsFavouriteList4=%@",urlParameter];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchTvChannelsFavList:) name:urlRequest object:nil];
        
        [UrlRequest getRequest:urlRequest view:self.view];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

    }


-(void)fetchTvChannelsFavList:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    @try {
        if ([notification.userInfo objectForKey:@"dictionary"] != [NSNull null]) {
            temp =[[notification.userInfo objectForKey:@"dictionary"] objectForKey:@"root"];
            favouriteFolderNameArray = [[NSMutableArray alloc]init];
            tvRatingArray = [[NSMutableArray alloc]init];
            tvUrlArray = [[NSMutableArray alloc]init];
            tvLogoArray = [[NSMutableArray alloc]init];
            tvProgArray = [[NSMutableArray alloc]init];
            tvStartTimeArray = [[NSMutableArray alloc]init];
            tvEndTimeArray = [[NSMutableArray alloc]init];
            tvtextFieldArray = [[NSMutableArray alloc]init];
            channelIDArray = [[NSMutableArray alloc]init];
            blockedStatusArray = [[NSMutableArray alloc]init];
            packageStatus = [[NSMutableArray alloc]init];
            geoLocationStatusArray = [[NSMutableArray alloc]init];
            lastSeenStatusArray = [[NSMutableArray alloc]init];
            
            if(temp) {
                NSLog(@"temp = %@",temp);
                
                for (int i = 0; i < temp.count; i++) {
                    NSDictionary *dict = temp[i];
                    
                    
                    if ([[dict objectForKey:@"status"] isEqualToString:@"Token Expired"]) {
                        [self.navigationController.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                        tokenExpired = YES;
                        [self goToHome];
                    }
                    
                    else if ([[dict objectForKey:@"status"] isEqualToString:@"Package Not Found"]) {
                        packageNotFound = YES;
                        [self.navigationController.view makeToast:@"Currently No Package Available, Subscribe to Asianet to avail Uninterrupted service" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    
                    else if ([[dict objectForKey:@"status"] isEqualToString:@"No Data Found"]) {
                        noDataFound = YES;
                        [self.navigationController.view makeToast:@"No Data Found" duration:2.0 position:CSToastPositionCenter style:style];
                        
                    }
                    
                    else{
                        packageNotFound = NO;
                        noDataFound = NO;
                        NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance] getIpForImage]];
                        
                        if ([[dict objectForKey:@"deletionStatus"]isEqualToString:@"true"]) {
                            
                        }
                        else{
                            imageUrl = [imageUrl stringByAppendingPathComponent:[dict objectForKey:@"channelLogo"]];
                            NSLog(@"imageurl = %@", imageUrl);
                            [tvLogoArray addObject:imageUrl];
                            [tvUrlArray addObject:[dict objectForKey:@"streamUrl"]];
                            [tvProgArray addObject:[dict objectForKey:@"eventName"]];
                            if ([[dict objectForKey:@"start"] isEqualToString:@""]) {
                                NSString *emptyString = @"default";
                                [tvStartTimeArray addObject:emptyString];
                            }
                            else{
                                [tvStartTimeArray addObject:[dict objectForKey:@"start"]];
                            }
                            if ([[dict objectForKey:@"end"] isEqualToString:@""]) {
                                NSString *emptyString = @"default";
                                [tvEndTimeArray addObject:emptyString];
                            }
                            else{
                                [tvEndTimeArray addObject:[dict objectForKey:@"end"]];
                            }
                            [blockedStatusArray addObject:[dict objectForKey:@"blockedStatus"]];
                            [tvRatingArray addObject:[dict objectForKey:@"rating"]];
                            [channelIDArray addObject:[dict objectForKey:@"channelId"]];
                            [lastSeenStatusArray addObject:[dict objectForKey:@"lastSeenStatus"]];
                            [deletionStatusArray addObject:[dict objectForKey:@"deletionStatus"]];
                            [parentalStatusArray addObject:[dict objectForKey:@"parentalStatus"]];
                            [countryAllowedStatusArray addObject:[dict objectForKey:@"countryAllowed"]];
                            [packageStatus addObject:[dict objectForKey:@"packageStatus"]];
                            [geoLocationStatusArray addObject:[dict objectForKey:@"geoLocationStatus"]];
                            if ([[dict objectForKey:@"eventDescription"]isEqualToString:@""]) {
                                NSString *emptystring = @"No Description Available";
                                [tvtextFieldArray addObject:emptystring];
                            }
                            
                            
                            else{
                                [tvtextFieldArray addObject:[dict objectForKey:@"eventDescription"]];
                            }
                        }
                    }
                }
                
                [self.favTvTableView reloadData];
                [self.view setNeedsDisplay];
                
                if (isPlayerLoaded == NO) {
                    self.currentValue = [LastSeenIndex getIndexSelectedChannel:lastSeenStatusArray];
                    cellIndexPath = [NSIndexPath indexPathForRow:self.currentValue inSection:0];
                    [self playTVwithValue];
                }
            }
        }
    }
    @catch(NSException *exception) {
        
    } @finally{
        
    }
    
    [self.loader stopAnimating];
    [self.loader hidesWhenStopped];
}

-(void)playTVwithValue{
    
    if(AVPlayerController){
        AVPlayerController = nil;
    }
    
    isPlayerLoaded = YES;
    
    leftRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFromLeft:         )];
    leftRecogniser.delegate = self  ;
    [leftRecogniser setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.playPauseView addGestureRecognizer:leftRecogniser];
    
    rightRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFromRight:)];
    rightRecogniser.delegate = self ;
    [rightRecogniser setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.playPauseView addGestureRecognizer:rightRecogniser];
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTvView:)];
    tapRecognizer.delegate = self;
    tapRecognizer.numberOfTapsRequired = 1;
    [_playPauseView addGestureRecognizer:tapRecognizer];
    [_playPauseView addSubview:self.playPauseButton];
    [_playPauseView bringSubviewToFront:self.playPauseButton];
    [self.playPauseButton setHidden:YES];
    
    [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    if (packageNotFound == YES) {
        
    }
    
    else if (noDataFound == YES){
        
    }
    else{
        
        if (![loginName isEqualToString:@""]) {
            if ([packageStatus[cellIndexPath.row] isEqualToString:@"false"]) {
                [_player pause];
                
                //****** Getting the Update Channel Usage Details Of Chaneels *****//
                [self updateChannelUsageDetails];
                
                [self.navigationController.view makeToast:@"This Channel is not Subscribed" duration:2.0 position:CSToastPositionCenter style:style];
              }
            else
            {
                if([blockedStatusArray[cellIndexPath.row] isEqualToString:@"true"]) {
                    blockIndexpath = cellIndexPath;
                    _AVPlayerController.view.hidden = YES;
                   // self.pinView.hidden = NO;
                    
                    favouritepinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                       message:@""
                                                                      delegate:self
                                                             cancelButtonTitle:@"Cancel"
                                                             otherButtonTitles:@"Ok",nil];
                    favouritepinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
                    
                    UITextField *pinTextField = [favouritepinviewAlert textFieldAtIndex:0];
                    pinTextField.delegate = self;
                    
                    [pinTextField becomeFirstResponder];
                    pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                    pinTextField.placeholder = @"Enter PIN";
                    
                    
                    [favouritepinviewAlert show];
                    
                    [self.view endEditing:YES];
                    [_player pause];
                    
                }
                
                else if ([geoLocationStatusArray[cellIndexPath.row] isEqualToString:@"false"]){
                    [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
                    [_player pause];
                }
                
                
                else if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]) {
                    
                    [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
                    [self.view endEditing:YES];
                    [_player pause];
                }
                
                //                 else if ([parentalStatusArray [cellIndexPath.row] isEqualToString:@"true"]){
                //                     blockIndexpath = cellIndexPath;
                //                     self.pinView.hidden = NO;
                //
                //                 }
                
                
                else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"]){
                    
                    [self.view endEditing:YES];
                    [_player pause];
                    
                    //****** Getting the Update Channel Usage Details Of chaneels *****//
                    [self updateChannelUsageDetails];
                    
                    [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
                }
                
                
                else{
                    NSString *selectedID = [channelIDArray objectAtIndex:cellIndexPath.row];
                    [[NSUserDefaults standardUserDefaults]setObject:selectedID forKey:@"slectedFavID"];
                    NSString *stringFromArray = [tvUrlArray objectAtIndex:cellIndexPath.row];
                    self.currentValue = (int)cellIndexPath.row;
                    NSLog(@"Url : %@",stringFromArray);
                    stringFromUrl = stringFromArray;
                   
                    _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];

                    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                    
                    [self addChildViewController:_AVPlayerController];
                    [self.tvView addSubview:_AVPlayerController.view];
                    
                    _AVPlayerController.view.frame = self.tvView.bounds;
                    _AVPlayerController.showsPlaybackControls = false;
                    _AVPlayerController.view.userInteractionEnabled = false;
                    _player.closedCaptionDisplayEnabled = NO;
                    _AVPlayerController.player = _player;
                    [_playPauseButton setHidden:YES];
                    [_player play];

                    
                    //****** Getting the Adding Channel Usage Details Of Chaneels *****//
                    [self addingChannelUsageDetails];
                }
            }
            
            
        }
        
        else if (tokenExpired == YES){
            
        }
        else
        {
            if([blockedStatusArray[cellIndexPath.row] isEqualToString:@"true"]) {
                blockIndexpath = cellIndexPath;
                _AVPlayerController.view.hidden = YES;
                //self.pinView.hidden = NO;
                favouritepinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                   message:@""
                                                                  delegate:self
                                                         cancelButtonTitle:@"Cancel"
                                                         otherButtonTitles:@"Ok",nil];
                favouritepinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
                
                UITextField *pinTextField = [favouritepinviewAlert textFieldAtIndex:0];
                pinTextField.delegate = self;
                
                [pinTextField becomeFirstResponder];
                pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                pinTextField.placeholder = @"Enter PIN";
                
                
                [favouritepinviewAlert show];
                
                [self.view endEditing:YES];
                [_player pause];
                
                //****** Getting the Update Channel Usage Details Of chaneels *****//
                 [self updateChannelUsageDetails];
            }
            
            if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]) {
                [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            //                else if ([parentalStatusArray[cellIndexPath.row] isEqualToString:@"true"]){
            //                    blockIndexpath = cellIndexPath;
            //                    self.pinView.hidden = NO;
            //                }
            
            else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"]){
                
                [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
            }
            else{
                NSString *selectedID = [channelIDArray objectAtIndex:cellIndexPath.row];
                [[NSUserDefaults standardUserDefaults]setObject:selectedID forKey:@"slectedFavID"];
                NSString *stringFromArray = [tvUrlArray objectAtIndex:cellIndexPath.row];
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",stringFromArray);
                stringFromUrl = stringFromArray;

                _player = [AVPlayer playerWithURL:[NSURL URLWithString:stringFromArray]];

                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [self addChildViewController:_AVPlayerController];
                [self.tvView addSubview:_AVPlayerController.view];
                
                _AVPlayerController.view.frame = self.tvView.bounds;
                _AVPlayerController.showsPlaybackControls = false;
                _AVPlayerController.view.userInteractionEnabled = false;
                _player.closedCaptionDisplayEnabled = NO;
                _AVPlayerController.player = _player;
                
                
                [_player play];
                
                //****** Getting the Adding Channel Usage Details Of Chaneels *****//
                [self addingChannelUsageDetails];
            }
        }
    }
        
        
        
        
    }


    @end
