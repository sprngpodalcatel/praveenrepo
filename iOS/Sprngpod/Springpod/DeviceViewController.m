//
//  DeviceViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "DeviceViewController.h"
#import "DeviceSettingsTableViewCell.h"
#import "SprngIPConf.h"


@interface DeviceViewController ()
{
    NSMutableArray *deviceList,*temp,*deviceIDArray,*platform;
}
@property (weak, nonatomic) IBOutlet UITableView *deviceTableView;

@end

@implementation DeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callApitofetchdata];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return [deviceList count];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeviceSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceSet" forIndexPath:indexPath];
   
    cell.deviceLabel.text = [deviceList objectAtIndex:indexPath.row];
    cell.uuidlabel.text = [deviceIDArray objectAtIndex:indexPath.row];
    cell.platformLabel.text = [platform objectAtIndex:indexPath.row];
    
    return cell;
}
-(void)callApitofetchdata
{
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?devicesList1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
//    NSLog(@"json_dict\n%@",json_dict);
//    NSLog(@"json_string\n%@",json_string);
    
//    NSLog(@"%@",response);
    //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        temp =[dictionary objectForKey:@"root"];
        NSLog(@"status = %ld",(long)status);
        deviceList = [[NSMutableArray alloc]init];
        deviceIDArray = [[NSMutableArray alloc]init];
        platform = [[NSMutableArray alloc]init];
        
        
        if(temp && status == 200) {
//            NSLog(@"temp = %@",temp);
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = temp[0];
            if ([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"]) {
                [deviceList addObject:@"No Device Found"];
                [deviceIDArray addObject:@"No Device Found"];
                [platform addObject:@"No Device Found"];
            }
            else
            {
                for (int i = 0; i < temp.count; i++) {
                    dict = temp[i];
                    [deviceList addObject:[dict objectForKey:@"deviceModel"]];
                    [deviceIDArray addObject:[dict objectForKey:@"deviceId"]];
                    [platform addObject:[dict objectForKey:@"platform"]];
                    [self.deviceTableView reloadData];
                }
            }
        }
        
    }
    else{
        NSLog (@"Oh Sorry");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
