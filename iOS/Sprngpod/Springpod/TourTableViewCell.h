//
//  TourTableViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 11/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourTableViewCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *titleNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *ImageLbl;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIView *channelDetailsView;


@end
