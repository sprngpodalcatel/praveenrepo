//
//  SocialMediaViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 15/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialMediaViewController : UIViewController<UIAlertViewDelegate>


@property (strong , nonatomic) NSString *facebookId;


@end
