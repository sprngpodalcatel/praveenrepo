//
//  UrlRequesting.m
//  Sprngpod
//
//  Created by Shrishail Diggi on 08/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "UrlRequesting.h"

@implementation UrlRequesting

// Post Request API...

+(void)postRequest:(NSString *)urlString arguments:(NSString *)arguments{
//    NSLog(@"post Request with url : %@ \n arguments : %@", urlString , arguments);
    @try {
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        NSData *data = [arguments dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData *data, NSURLResponse *response , NSError *error){
            NSLog(@"error is %@",error);
            NSDictionary *dictionary;
            NSHTTPURLResponse *httpResponse;
            NSLog(@"Running on %@ thread",[NSThread currentThread]);
            
            if (!error) {
                dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//                NSLog(@"string is %@",[NSString stringWithUTF8String:[data bytes]]);
//                NSLog(@"dictionary is %@",dictionary);
                httpResponse = (NSHTTPURLResponse *)response;
                
                if (dictionary) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *myDict = [NSDictionary dictionaryWithObject:@[dictionary,httpResponse] forKey:@[@"dictionary",@"response"]];
                        NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                        [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDict];
                    });
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@",[error.userInfo objectForKey:@"NSLocalizedDescription"]);
                });
            }
            
            
        }];
        [uploadTask resume];
    }
    @catch (NSException *exception) {
        
    }
   
}

//Get Request API...

+(void)getRequest:(NSString *)urlString{
   @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]];
//        NSLog(@"%@", url);
       
        //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        //    [request setHTTPMethod:@"GET"];
        //    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        //    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
        // {
        
        NSURLSession *mySession = [NSURLSession sharedSession];
        [[mySession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSLog(@"error is %@", error);
            NSDictionary *dictionary;
            NSHTTPURLResponse *httpResponse;
            NSLog(@"Running on %@ thread", [NSThread currentThread]);
            if (!error) {
                dictionary = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingAllowFragments error:&error];
//                NSLog(@"string is %@",[NSString stringWithUTF8String:[data bytes]]);
                
//                NSLog(@"dictionary is %@", dictionary);
                httpResponse = (NSHTTPURLResponse *)response;
                
                
                if(dictionary){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[dictionary, httpResponse] forKeys:@[@"dictionary", @"response"]];
                        NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                        [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDic];
                    });
                    
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //[error.userInfo objectForKey:@"NSLocalizedDescription"];
                    UIAlertView *al = [[UIAlertView alloc] initWithTitle:[error.userInfo objectForKey:@"NSLocalizedDescription"] message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [al show];
                    NSDictionary *myDic = [NSDictionary dictionaryWithObjects:@[error] forKeys:@[@"error"]];
                    NSNotificationCenter *myCenter = [NSNotificationCenter defaultCenter];
                    [myCenter postNotificationName:[NSString stringWithFormat:@"%@",urlString] object:self userInfo:myDic];
                });
            }
        }] resume];
       
       
       
        
    }
    @catch (NSException *exception) {
        NSLog(@"get failed ");
    }
}
@end
