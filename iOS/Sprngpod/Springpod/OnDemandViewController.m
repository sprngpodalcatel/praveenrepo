//
//  OnDemandViewController.m
//  Sprngpod
//
//  Created by XperioLabs on 19/05/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "OnDemandViewController.h"
#import "OnDemandTableViewCell.h"
#import "OnDemandCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "SprngIPConf.h"
#import "UIView+Toast.h"
#import "netWork.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "DeviceUID.h"
#import "Constants.h"
#import "Flurry.h"


@interface OnDemandViewController ()<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIActionSheetDelegate>
{
    
    
    NSString *urlString;
    NSString*serviceProviderID;
    NSMutableArray *subFilters;
    NSMutableArray *genresubFilters;
    NSMutableArray *langsubFilters;
    NSArray *channelNameArray;
    
    NSArray *bestOfAlbumArray;
    NSArray *bestOfmusicArray;
    NSArray *bestOfCountryArray;
    
    NSMutableArray *imagesArray;
    NSArray *descriptionArray;
    NSIndexPath *cellIndexPath;
     
     
    NSArray *collectionImagesArray;
    NSArray *valueArray;
    NSArray *priceArray;
    NSArray *languagesArray;
    NSArray *categoryArray;
    NSArray *genreArray;
    NSString *stringFromUrl;
    NSIndexPath *swipeIndex;
    NSString *lastTVid;
    NSTimer *playButtonTimer;
    UITapGestureRecognizer *tapRecogniser;
    NSIndexPath *selectedCellIndexPath;
    
    OnDemandTableViewCell *tableCell;
    netWork *netReachability;
    NetworkStatus networkStatus;
    CSToastStyle *style;
    UIAlertView *ODpinviewAlert;
    NSMutableSet *expandedCells;
    NSMutableArray *temp,*channelUrlArray;
    NSString *rateString,*IPAddress,*lastListenRadioChannelLanguage,*lastSeenTVUrl,*countryName;
    NSString *deviceID,*basicInfoArray;
     NSString*pinString;
    
    float lattitude,longitude;
    NSMutableArray *packageName,*startDate,*expiryDate,*deviceList,*platform,*osVersion,*deletionStatusArray,*blockStatusArray,*countryAllowedStatusArray;
    NSIndexPath *blockIndexpath;
    
    NSIndexPath *selectedButtonIndex;
    
    
    float lastSlidedValue;
    
    float screenHeight;
    int sessionTime,tokenRefreshTime;

    BOOL seleted;
    BOOL posterSeleted;
     
     BOOL packageNotFound;
     BOOL noDataFound; // To avoid crash in No data when low network.
     BOOL tokenExpired;
     BOOL isPlayerLoaded;
    
    BOOL pinAtfirstTime;
    BOOL pinOnSelection;
    
    UISwipeGestureRecognizer *leftRecognizer;
    UISwipeGestureRecognizer *rightRecognizer;
    UITapGestureRecognizer *tapRecognizer;
    CGPoint startPosition;
    
    NSMutableData*reciveddata;
    NSString*get;
    NSMutableDictionary *receivedData1;

    NSMutableData*responsedata;
    NSMutableArray *categoryArray1;
    NSMutableArray *_channelPosterArray2;
    NSMutableArray *_channelCategoryArray;
    NSMutableArray *_videoContentArray;
    NSMutableArray*subcontentArrAY;
    NSMutableArray*packagetypeArray;
    NSMutableArray*producerArray;
    NSMutableArray*languageArray;
    NSMutableArray*actorArray;
    NSMutableArray*directorArray;
    NSMutableArray*packageArray;
    NSMutableArray*VODTypeArray;
    NSMutableArray*musicArray;
    NSDictionary *globalDic;
    NSMutableArray*ResponseArray;

    
    NSString *param0;
    NSString *param1;
    NSString *param2;
    NSString *param3;
    NSString *param4;
    NSString *param5;
    NSString *param6;
    NSString *param7;
    NSString *param8;
    
    NSString *param9;
    NSString *param10;
    NSString *param11;
    NSString *param12;
    
    NSString *contentIdForUsageDetails;
    NSString *serviceProviderIdForUsageDetails;
    NSString *state;
    
    NSString *categoryselectedActshtBtnIndex;
    NSString *genereselectedActshtBtnIndex;
    NSString *languageselectedActshtBtnIndex;
    
    NSString *categoryfilterName;
    NSString *genreFilterName;
    NSString *languageFilterName;
    
    NSString *scrollType;
    NSString *statusOfData;
    
    int from;
    int to;
    
    int firstFrom;
    int firstTo;

    

 }

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property (nonatomic,strong)AVPlayer*player;
@property (nonatomic,strong)AVPlayerViewController *AVPlayerController;


@property (retain, nonatomic) NSURLConnection *connection;
@property(nonatomic) NSInteger *cancelButtonIndex;

@property(strong , nonatomic) NSTimer *loginTimer,*tokenRefreshTimer;
@property(strong, nonatomic) NSTimer *epgTimer;
@property (weak, nonatomic) IBOutlet UINavigationItem *ODNav;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playerViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *ODTableView;
@property (weak, nonatomic) IBOutlet UIView *tvView;
@property (weak, nonatomic) IBOutlet UICollectionView *ODCollectionView;

@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIView *playPauseView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic) int currentValue;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedTap;

@property (weak, nonatomic) IBOutlet UIView *selectedView;

@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;


@property (weak, nonatomic) IBOutlet UIButton *trailerButton;


@property (weak, nonatomic) IBOutlet UIButton *oldPlusButton;

@property (weak, nonatomic) IBOutlet UITextView *selectedDescriptionText;
@property (weak, nonatomic) IBOutlet UILabel *eventNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventLanguageLbl;

@property (weak, nonatomic) IBOutlet UILabel *rattingLbl;
@property (weak, nonatomic) IBOutlet UILabel *costLbl;

@property (weak, nonatomic) IBOutlet UILabel *actorNameLbl;

@property (weak, nonatomic) IBOutlet UILabel *directorNameLbl;

@property (strong) NSMutableArray *selectedchannelIDArray;
@property (strong) NSMutableArray *defaultchannelIDArray;

@property (strong) NSMutableArray *channelPosterArray;
@property (weak, nonatomic) IBOutlet UIView *hidesubtitlesview;

@property (strong, nonatomic) IBOutlet UIView *hideFullScreenView;
@property (weak, nonatomic) IBOutlet UIButton *subtitleshidebtn;

@property (weak, nonatomic) IBOutlet UIButton *FullScreenBtn;
@property (weak, nonatomic) IBOutlet UIView *VODMenuView;

@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *genreButton;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;


@property (weak, nonatomic) IBOutlet UIView *VODSeparatedView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopContsraint;



@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UIView *PinView;

@property (weak, nonatomic) IBOutlet UIView *selectedViewHightConstraint;


@end



@implementation OnDemandViewController


#pragma mark NSManagedObjectContext

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
     for (UIView *subview in actionSheet.subviews) {
          if ([subview isKindOfClass:[UIButton class]]) {
               UIButton *button = (UIButton *)subview;
               [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
          }
     }
     
}
#pragma mark viewDidLoad
 - (void)viewDidLoad {
     
     _AVPlayerController.view.hidden = YES;
     _AVPlayerController = [[AVPlayerViewController alloc] init];
     
     statusOfData = @"Data Found";
    globalDic = [[NSDictionary alloc]init];
     categoryArray1 = [[NSMutableArray alloc]init];
     _selectedchannelIDArray = [[NSMutableArray alloc]init];
     _defaultchannelIDArray = [[NSMutableArray alloc]init];
     
     tokenExpired = NO;
     noDataFound = NO;
     isPlayerLoaded = NO;
     posterSeleted = NO;
     
     _loader.hidden = NO;
     [_loader startAnimating];
     [self.view bringSubviewToFront:_loader];
     
    self.ODCollectionView.hidden = true;
    //self.segmentedTap.hidden = true;
     self.VODMenuView.hidden = true;

    self.SearchBar.hidden = false;
     self.VODSeparatedView.hidden = false;
    self.selectedView.hidden = true;
    self.selectedDescriptionText.textContainer.maximumNumberOfLines = 1;
    
    _channelPosterArray2 = [[NSMutableArray alloc]init];
    _channelCategoryArray = [[NSMutableArray alloc]init];
    [_ODCollectionView reloadData];
    expandedCells = [[NSMutableSet alloc]init];
    
    //* Network connection Reachability
    
    netReachability = [netWork reachabilityForInternetConnection];
    networkStatus = [netReachability currentReachabilityStatus];
    
    //* Arrays for Ondemand last seenTV
    
    temp = [[NSMutableArray alloc]init];
    packageName = [[NSMutableArray alloc]init];
    startDate = [[NSMutableArray alloc]init];
    expiryDate = [[NSMutableArray alloc]init];
    deviceID = [DeviceUID uid];
     
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(253.0/255.0) green:(69.0/255.0) blue:(86.0/255.0) alpha:1.0]} forState:UIControlStateSelected];
    UIFont *objFont = [UIFont fontWithName:@"Roboto-Light" size:15.0f];
    NSDictionary *dictAttributes = [NSDictionary dictionaryWithObject:objFont forKey:NSFontAttributeName];
    [[UISegmentedControl appearance] setTitleTextAttributes:dictAttributes forState:UIControlStateNormal];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    

     // Do any additional setup after loading the view.
     
     UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
     [button setImage:[UIImage imageNamed:@"homeNew.png"] forState:UIControlStateNormal];
     UIView *rightView = [[UIView alloc] initWithFrame:button.frame];
     
     UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightView];
     
     self.navigationController.navigationItem.rightBarButtonItem = right;
     [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
     self.tabBarController.tabBar.shadowImage = [UIImage new];
     self.tabBarController.tabBar.translucent = YES;
     self.tabBarController.tabBar.backgroundColor = [UIColor clearColor];
     self.tabBarController.view.backgroundColor = [UIColor clearColor];
     
     UIBezierPath *linePath1 = [UIBezierPath bezierPath];
     [linePath1 moveToPoint:CGPointMake(8.0, 82.0)];
     [linePath1 addLineToPoint:CGPointMake(232 , 82.0)];
     [linePath1 moveToPoint:CGPointMake(240.0/2, 82.0)];
     [linePath1 addLineToPoint:CGPointMake(240.0/2, 117.0)];
     
     CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
     shapeLayer1.path = [linePath1 CGPath];
     shapeLayer1.strokeColor = [[[UIColor grayColor] colorWithAlphaComponent:0.8] CGColor];
     shapeLayer1.lineWidth = 1.0;
     shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
    
    self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    screenHeight = self.playerViewHeight.constant;
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"On Demand";
    //    [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.ODNav.titleView = titleLabel;
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"FS_Joey-Light" size:16.0], NSFontAttributeName,nil]];
    [_playPauseButton setHidden:YES];
    
    tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTvView:)];
    tapRecogniser.delegate = self;
    tapRecogniser.numberOfTapsRequired = 1;
    [_playPauseView addGestureRecognizer:tapRecogniser];
    [_playPauseView addSubview:self.playPauseButton];
    [_playPauseView bringSubviewToFront:self.playPauseButton];
     
     NSString *selectedFileName = @"";
     //self.PinView.layer.cornerRadius = 5;
     self.PinView.hidden = YES;

     [[NSUserDefaults standardUserDefaults] setObject:selectedFileName forKey:@"selectedListName"];
     [[NSNotificationCenter defaultCenter]addObserver:self
                                             selector:@selector(becomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionExpiry:) name:@"sessionExpiry" object:nil];
     
     [self.PinView.layer addSublayer:shapeLayer1];
     UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
     numberToolbar.barStyle = UIBarStyleBlackTranslucent;
     numberToolbar.backgroundColor = [UIColor grayColor];
     numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
     [numberToolbar sizeToFit];
     _pinTextField.inputAccessoryView = numberToolbar;
     
    //[self homeScreenApi];
    [self mainPost];
     //***** AVPlayer integration  ************//
//      _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
//     _AVPlayerController = [[AVPlayerViewController alloc]
//                            init];
//     _AVPlayerController.showsPlaybackControls = false;
//     
//     [self addChildViewController:_AVPlayerController];
//     [self.tvView addSubview:_AVPlayerController.view];
//     
//     _AVPlayerController.view.frame = self.tvView.bounds;
//     
//     _AVPlayerController.player = _player;
//    _player.closedCaptionDisplayEnabled = false;
//     
//     self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
//    [_player play];
     
     //****** Getting the Add Channel Usage Details Of chaneels *****//
    // [self addingChannelUsageDetails];
     
     state = @"LiveTV";


     
      _oldPlusButton.tag = 0;
}

#pragma mark TextFeild Delegate Methods


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float diff = self.view.frame.size.height/2.0 - kbSize.height - self.PinView.frame.size.height/2.0;
    
    if (diff < 0) {
        //        self.pinVerticalConstraint.constant = diff;
    }
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //    self.pinVerticalConstraint.constant = 0;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)cancelNumberPad{
    [_pinTextField resignFirstResponder];
    _pinTextField.text = @"";
}

-(void)doneWithNumberPad{
    //    NSString *numberFromTheKeyboard = numberTextField.text;
    [_pinTextField resignFirstResponder];
}


#pragma mark UsageDetails


-(void)addingChannelUsageDetails
{
    //****** Getting the Add Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&countryCode=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"saveLastTV"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"]];
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSString *serviceIDN=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceId"];
                [[NSUserDefaults standardUserDefaults]setObject:serviceIDN forKey:@"serviceIdN"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                
                
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
    });
    
    
}

-(void)updateChannelUsageDetails
{
    //****** Getting the Update Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"serviceIdN"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageId"]];
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}
-(void)addingVODUsageDetails
{
    //****** Getting the Add Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *addUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceProviderId=%@&contentId=%@&countryCode=%@&MCC=%@&MNC=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"serviceProviderIdForUsageDetails"],[[NSUserDefaults standardUserDefaults]objectForKey:@"contentIdForUsageDetails"],[[NSUserDefaults standardUserDefaults] objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]];
        
        NSData *addUsagePostData = [addUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *addUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[addUsagePostData length]];
        
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/addVODUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:addUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:addUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                
                NSLog(@"Usage array----%@",usageArray);
                
                //****** Storing the UsageId In NSUserDefaults*******//
                
                //** Storing the UsageId , Service&Content ProviderID From Add Usage Details Response ***//
                
                NSString *usageId = [[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"usageId"];
                NSLog(@"UsageId----%@",usageId);
                
                NSString *currentServiceProviderIdForUsageDetails =[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"serviceProviderId"];
                NSLog(@"currentServiceProviderIdForUsageDetails----%@",currentServiceProviderIdForUsageDetails);
                
                NSString *currentContentIdForUsageDetails=[[[usageDic objectForKey:@"root"]objectForKey:@"data"]objectForKey:@"contentId"];
                NSLog(@"currentContentIdForUsageDetails----%@",currentContentIdForUsageDetails);
                
                [[NSUserDefaults standardUserDefaults]setObject:usageId forKey:@"usageIdForPoster"];
                [[NSUserDefaults standardUserDefaults]setObject:currentServiceProviderIdForUsageDetails forKey:@"currentServiceProviderIdForUsageDetails"];
                [[NSUserDefaults standardUserDefaults]setObject:currentContentIdForUsageDetails forKey:@"currentContentIdForUsageDetails"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                //** Storing the UsageId , Service&Content ProviderID From Add Usage Details Response ***//
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}
-(void)updateVODUsageDetails
{
    //****** Getting the Update Channel Usage Details Of chaneels *****//
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *updateUsagePost = [NSString stringWithFormat:@"customerId=%@&uniqueDeviceId=%@&serviceProviderId=%@&contentId=%@&usageId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueDeviceId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"currentServiceProviderIdForUsageDetails"],[[NSUserDefaults standardUserDefaults]objectForKey:@"currentContentIdForUsageDetails"],[[NSUserDefaults standardUserDefaults]objectForKey:@"usageIdForPoster"]];
        
        
        NSData *updateUsagePostData = [updateUsagePost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *updateUsagePostLength = [NSString stringWithFormat:@"%lu", (unsigned long)[updateUsagePostData length]];
        @try  {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/updateVODUsageDetails",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:updateUsagePostLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:updateUsagePostData];
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                NSDictionary *usageDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                NSMutableArray *usageArray =[usageDic valueForKey:@"root"];
                NSLog(@"Usage array----%@",usageArray);
                
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    });
    
}
- (IBAction)Fullscreenaction:(id)sender {
  [self.view makeToast:@"Please rotate fullscreen mode" duration:2.0 position:CSToastPositionCenter style:style];  
 
}
- (IBAction)subtitlesactionbtnpressed:(id)sender {
    
   [self.view makeToast:@"Subtitles are not available" duration:2.0 position:CSToastPositionCenter style:style]; 
    
    
}



-(void)hideFullScreen{

}

//nsurl connection deligate methods
-(void)didSuccessfullResponceData:(NSDictionary *)responceDict{
    
//    _channelIDArray=[responceDict valueForKey:@"root"];
//    NSDictionary *serviceproviderId=[_channelIDArray valueForKey:@"serviceProviderId"];
//    [[NSUserDefaults standardUserDefaults] setObject:serviceproviderId forKey:@"serviceproviderKey1"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    [_ODTableView reloadData];
}

#pragma mark SetNavigation
-(void)setNavigationTitle:(NSString *)title
{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = title;
    [titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin.ttf" size:12]];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.ODNav.titleView = titleLabel;
}
-(void) becomeActive
{
    NSLog(@"ACTIVE");
    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
    [_playPauseButton setHidden:YES];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inHomeView"] == YES) {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inLiveTVView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inFavouriteView"] == YES)
    {
        [_player pause];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inOnDemandView"] == YES)
    {
        [_player play];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inRadioView"] == YES)
    {
        [_player pause];
    }
    else{
        
        [_player pause];
        
    }
}

-(void)timedOut{
    AVPlayerViewController *AVPlayerController = [[AVPlayerViewController alloc] init];
    AVPlayerController.player = _player;
    [_player pause];

}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark searchBar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
     [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     [searchBar resignFirstResponder];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
     [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
     if ([searchText length] == 0)
     {
          [searchBar performSelector:@selector(resignFirstResponder)
                          withObject:nil
                          afterDelay:0];
     }
}
-(void)mainPost{
    
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    
    param1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
    
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@",param1]; // <--here put the request parameters you used to get the response
    
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    @try {
        NSString *str =[NSString stringWithFormat:@"%@/index.php/fetchAllVODLibraries2",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:str];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (!(error))
        {
            
            NSDictionary *receivedData1 = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
            receivedData1 = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
            _defaultchannelIDArray =[[receivedData1 objectForKey:@"root"]valueForKey:@"defaultServiceProviders"];
            
            
            _selectedchannelIDArray = [[receivedData1 objectForKey:@"root"]valueForKey:@"selectedServiceProviders"];
            
            
            
            
            NSDictionary *channelserviceproviderId = [_selectedchannelIDArray valueForKey:@"serviceProviderId"];
            
            NSDictionary *defaultserviceproviderId = [_defaultchannelIDArray valueForKey:@"serviceProviderId"];
            
            [[NSUserDefaults standardUserDefaults] setObject:channelserviceproviderId forKey:@"channelserviceproviderkey"];
            [[NSUserDefaults standardUserDefaults] setObject:defaultserviceproviderId forKey:@"defaultserviceproviderkey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [_ODTableView reloadData];
            NSLog(@"%@",_selectedchannelIDArray);
            NSLog(@"%@",_defaultchannelIDArray);
        }
        
        else{
            NSLog(@"%@ Null Error",error);
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"%@ exception Error",exception);
    } @finally {
        NSLog(@"");
    }
    
    
    
}

#pragma mark playTV method

-(void)playTVWithValue
{
     //AVPlayerViewController *AVPlayerController = [[AVPlayerViewController alloc] init];
    _playPauseView.hidden=NO;
    
     self.hideFullScreenView.hidden=false;
  [self.tvView bringSubviewToFront:self.hideFullScreenView];
    
//if (packageNotFound == YES) {
//          
//     }
//     
//     else if (tokenExpired == YES){
//          
//     }
//     else if (noDataFound == YES);
//     
//     else if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]){
//            [_player pause];
//         
//         //***** Updation Of VOD Usage Details ******//
//         
//         if ([state isEqualToString:@"LiveTV"])
//         {
//             [self updateChannelUsageDetails];
//         }else if ([state isEqualToString:@"VOD"])
//         {
//             [self updateVODUsageDetails];
//         }
//
//          [self.view endEditing:YES];
//          [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
//     }
//     
//               else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"])
//               {
//                    [_player pause];
//                   
//                   //***** Updation Of VOD Usage Details ******//
//                   
//                   if ([state isEqualToString:@"LiveTV"])
//                   {
//                       [self updateChannelUsageDetails];
//                   }else if ([state isEqualToString:@"VOD"])
//                   {
//                       [self updateVODUsageDetails];
//                   }
//
//                    [self.view endEditing:YES];
//                    [self.navigationController.view makeToast:@"This channel is not subscibed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
//               }
//         else
//           {
//
//               if([[globalDic objectForKey:@"SubscriptionType"]isEqualToString:@"Unsubscribed"])
//                   
//               {
//                   [_ODCollectionView reloadData];
//                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"you are not Subscribed to this Video" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//                   [alert show];
//                   
//               }
//               else{
//               
//              NSString*vedioUrlfromArry=[globalDic objectForKey:@"fullContentUrl"];
//               stringFromUrl = vedioUrlfromArry;
//               self.currentValue = (int)cellIndexPath.row;
//               NSLog(@"Url : %@",vedioUrlfromArry);
//            
//                   _player = [AVPlayer playerWithURL:[NSURL URLWithString:vedioUrlfromArry]];
//                   [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
//                   [_playPauseButton setHidden:YES];
//                    AVPlayerViewController *AVPlayerController = [[AVPlayerViewController alloc] init];
//                   [self addChildViewController:_AVPlayerController];
//                   [self.tvView addSubview:_AVPlayerController.view];
//                   
//                   _AVPlayerController.view.frame = self.tvView.bounds;
//                   _AVPlayerController.player = _player;
//                   _AVPlayerController.showsPlaybackControls = YES;
//                   _player.closedCaptionDisplayEnabled = false;
//                   [_player play];
//                   
//                   //***** Adding Of VOD Usage Details ******//
//                   [self addingVODUsageDetails];
//
//
//                   
//               }
////          }
//          
//     }
    
    isPlayerLoaded = YES;
    
    [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    
    if (packageNotFound == YES) {
        
    }
    
    else if (tokenExpired == YES){
        
    }
    else if (noDataFound == YES){
        
    }
    
    else{
        if ([blockStatusArray[cellIndexPath.row] isEqualToString:@"true"]) {
            
            [_player pause];
            
            //****** Getting the Update Channel Usage Details Of chaneels *****//
             [self updateChannelUsageDetails];
            
            
            [self.view endEditing:YES];
            _AVPlayerController.view.hidden = YES;
            pinAtfirstTime = YES;
            pinOnSelection = NO;
            //self.PinView.hidden = NO;
            ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];
            ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
            
            UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
            pinTextField.delegate = self;
            [pinTextField becomeFirstResponder];
            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
            pinTextField.placeholder = @"Enter PIN";
            // pinviewAlert.view.tintColor = UIColor.orangeColor()
            // [[UIView appearance] setTintColor:[UIColor redColor]];
            [ODpinviewAlert show];
        }
        
        else if ([deletionStatusArray[cellIndexPath.row] isEqualToString:@"true"]){
            
            [_player pause];
            
            //****** Getting the Update Channel Usage Details Of chaneels *****//
             [self updateChannelUsageDetails];
            
            [self.view endEditing:YES];
            [self.navigationController.view makeToast:@"This channel is Deleted" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
        else if ([countryAllowedStatusArray[cellIndexPath.row] isEqualToString:@"false"])
        {
            [_player pause];
            
            //****** Getting the Update Channel Usage Details Of chaneels *****//
               [self updateChannelUsageDetails];
            _AVPlayerController.view.hidden = YES;
            [self.view endEditing:YES];
            [self.navigationController.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
        }
        else
        {
            if([[globalDic objectForKey:@"SubscriptionType"]isEqualToString:@"Unsubscribed"])
            {
                [_ODCollectionView reloadData];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"you are not Subscribed to this Video" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                
            }
            if (_AVPlayerController.player.status == AVPlayerTimeControlStatusPlaying) {
   
            }
            else
            {
            [_player pause];
            NSString *stringFromArray = [channelUrlArray objectAtIndex:cellIndexPath.row];
                               stringFromUrl = stringFromArray;
                               self.currentValue = (int)cellIndexPath.row;
                _AVPlayerController.view.hidden = NO;
                NSString *vedioUrlfromArry=[globalDic objectForKey:@"fullContentUrl"];
                stringFromUrl = vedioUrlfromArry;
                self.currentValue = (int)cellIndexPath.row;
                NSLog(@"Url : %@",vedioUrlfromArry);
                // NSLog(@"Url : %@",stringFromArray);
                
                _player = [AVPlayer playerWithURL:[NSURL URLWithString:vedioUrlfromArry]];
                _AVPlayerController = [[AVPlayerViewController alloc] init];
                [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
                [_playPauseButton setHidden:YES];
                [self addChildViewController:_AVPlayerController];
                [self.tvView addSubview:_AVPlayerController.view];
                
                _AVPlayerController.view.frame = self.tvView.bounds;
                _AVPlayerController.player = _player;
                
                _AVPlayerController.showsPlaybackControls = YES;
                _player.closedCaptionDisplayEnabled = false;
                self.playPauseView.hidden = YES;
                
                    [_player play];
                
                //****** Getting the Add Channel Usage Details Of chaneels *****//
                // ***** Adding Of VOD Usage Details ******//
                [self addingVODUsageDetails];
                
                lastTVid = nil;
            }
            
            
        }
    }

}
#pragma mark ViewDidAppear
-(void)viewDidAppear:(BOOL)animated
{
    [Flurry logEvent:@"VOD_Screen" withParameters:nil];
    posterSeleted = NO;
    _selectedchannelIDArray = [[NSMutableArray alloc]init];
    _defaultchannelIDArray = [[NSMutableArray alloc]init];
    _channelPosterArray2=[[NSMutableArray alloc]init];
    _channelCategoryArray=[[NSMutableArray alloc]init];
    _videoContentArray=[[NSMutableArray alloc]init];
    // self.pinView.hidden = YES;
    _AVPlayerController.view.hidden = YES;
     self.playPauseView.hidden = NO;
    _loader.hidden = NO;
    [_loader startAnimating];
    [self.view bringSubviewToFront:_loader];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inHomeView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inLiveTVView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inRadioView"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inFavouriteView"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inOnDemandView"];

    //     _AVPlayerController.player = _player;
    
    _channelPosterArray2=[[NSMutableArray alloc]init];
    _channelCategoryArray=[[NSMutableArray alloc]init];
    _videoContentArray=[[NSMutableArray alloc]init];
    
    languageArray=[[NSMutableArray alloc]init];
    actorArray=[[NSMutableArray alloc]init];
    directorArray=[[NSMutableArray alloc]init];
    packageArray=[[NSMutableArray alloc]init];
    producerArray =[[NSMutableArray alloc]init];
    VODTypeArray =[[NSMutableArray alloc]init];
    musicArray = [[NSMutableArray alloc]init];
    
    self.playerViewHeight.constant = (118/193.0) * [[UIScreen mainScreen]bounds].size.width + 11;
    self.playPauseButton.hidden = true;
    // [self.view addSubview:self.selectedView];
    
    if (networkStatus == NotReachable) {
        [self.navigationController.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    [self setNavigationTitle:@"On Demand"];
    //    _epgTimer = [NSTimer scheduledTimerWithTimeInterval:30*60 target:self selector:@selector(refreshTableData) userInfo:nil repeats:YES];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromEntertainView"];
    
    if ([CLLocationManager locationServicesEnabled]) {
        if (!self.locationManager)
        {
            self.locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [_locationManager startUpdatingLocation];
        }
    }
    //[self homeScreenApi];
    [self newHomeScreenAPI];
    [self mainPost];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVBlockedStatus"] isEqualToString:@"true"]) {
        
        _AVPlayerController.view.hidden = YES;
        
        //self.PinView.hidden = NO;
        
        ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok",nil];
        ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
        pinTextField.delegate = self;
        [pinTextField becomeFirstResponder];
        pinTextField.keyboardType = UIKeyboardTypeNumberPad;
        pinTextField.placeholder = @"Enter PIN";
        [ODpinviewAlert show];

        
        [self.view endEditing:YES];
    }
    
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVParentalStatus"] isEqualToString:@"true"]){
        
        _AVPlayerController.view.hidden = YES;
       // self.PinView.hidden = NO;
        
        ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok",nil];
        ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
        pinTextField.delegate = self;
        [pinTextField becomeFirstResponder];
        pinTextField.keyboardType = UIKeyboardTypeNumberPad;
        pinTextField.placeholder = @"Enter PIN";
        [ODpinviewAlert show];

               //   self.lastSeenTvImageView.hidden = NO;
        self.playPauseButton.hidden = NO;
        [self.view endEditing:YES];
        
    }
    
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVCountryAllowed"] isEqualToString:@"false"]){
        [self.view makeToast:@"This channel is not subscribed for current geo location" duration:2.0 position:CSToastPositionCenter style:style];
    }
    
    else{
        _AVPlayerController.view.hidden = NO;
        self.PinView.hidden = YES;
        
        
        
        _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
        [self addChildViewController:_AVPlayerController];
        [self.tvView addSubview:_AVPlayerController.view];
        _AVPlayerController.view.frame = self.tvView.bounds;
        _AVPlayerController.player = _player;
        
        _player.closedCaptionDisplayEnabled = false;
        _AVPlayerController.showsPlaybackControls = false;
        self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        _loader.hidden = YES;
        [_loader hidesWhenStopped];
        
        [_player play];
        
        
        
        
        //        //****** Getting the Add Channel Usage Details Of chaneels *****//
            [self addingChannelUsageDetails];
    }
    
    
    
    //***** Adding Of TV Channel Usage Details ******//
      [self addingChannelUsageDetails];
    isPlayerLoaded = NO;
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    _oldPlusButton.tag = 0;
    
}
-(void)sessionExpiry:(NSNotification *)notification{
    [_player pause];
    
    //***** Updation Of VOD Usage Details ******//
    [self updateVODUsageDetails];
     _epgTimer = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    //BEFORE DOING SO CHECK THAT TIMER MUST NOT BE ALREADY INVALIDATED
    //Always nil your timer after invalidating so that
    //it does not cause crash due to duplicate invalidate
    //self.segmentedTap.selected = false;
    self.VODMenuView.hidden = true;
    self.VODSeparatedView.hidden = false;
    self.ODTableView.hidden = false;
    self.ODCollectionView.hidden = true;
    self.selectedView.hidden = true;
    
    if(_epgTimer)
    {
        [_epgTimer invalidate];
        _epgTimer = nil;
    }
    [_player pause];
    
    //***** Updation Of VOD Usage Details ******//
    
    if ([state isEqualToString:@"LiveTV"])
    {
        [self updateChannelUsageDetails];
    }else if ([state isEqualToString:@"VOD"])
    {
        [self updateVODUsageDetails];
    }

}

#pragma mark screen Rotation

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.view layoutIfNeeded];
    if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"landscape");
        [self.navigationController setNavigationBarHidden:TRUE animated:FALSE];
        self.tabBarController.tabBar.hidden = YES;
        
       
      //  [_searchBar removeFromSuperview];
       [_VODSeparatedView removeFromSuperview];
        // [_segmentedTap removeFromSuperview];
        [_VODMenuView removeFromSuperview];
        [_ODTableView removeFromSuperview];
        [_ODCollectionView removeFromSuperview];
        [_selectedView removeFromSuperview];
        
        [self.view layoutIfNeeded];
        NSLog(@" %f %f ",self.view.frame.size.width, self.view.frame.size.height);
        self.playerViewHeight.constant = MIN(self.view.frame.size.width, self.view.frame.size.height) ;
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        [_AVPlayerController.view setFrame: self.tvView.bounds];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"landscape"];

    }
    else
    {
        NSLog(@"portrait");
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [self.view layoutIfNeeded];
        self.playerViewHeight.constant = (236/386.0) *  MIN(self.view.frame.size.width, self.view.frame.size.height);
        // self.playerViewHeight.constant = screenHeight;
        [self.navigationController setNavigationBarHidden:false animated:FALSE];
        self.tabBarController.tabBar.hidden = false;
        [_AVPlayerController.view setFrame: self.tvView.bounds];
        
        
        //  [self.view addSubview:_segmentedTap];
        [self.view addSubview:_VODMenuView];
        
        // [self.view addSubview:_searchBar];
        [self.view addSubview:_VODSeparatedView];
        [self.view addSubview:_ODCollectionView];
        [self.view addSubview:_ODTableView];
        [self.view addSubview:self.selectedView];
        [self.view addSubview:self.PinView];
        
        [_AVPlayerController.view setFrame: self.tvView.bounds];
        
        [_tvView addSubview:_AVPlayerController.view];
        //***** Adding Of VOD Usage Details ******//
        
        if ([state isEqualToString:@"LiveTV"])
        {
           [self addingChannelUsageDetails];
        }else if ([state isEqualToString:@"VOD"])
        {
            [self addingVODUsageDetails];
        }
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"landscape"];
    }
    
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_selectedchannelIDArray.count == 0) {
        return [_defaultchannelIDArray count];
    }else{
        return [_selectedchannelIDArray count];
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectedchannelIDArray.count == 0) {
        
        [_defaultchannelIDArray count];
        
        NSDictionary *responceDict = _defaultchannelIDArray[indexPath.row];
        OnDemandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ODCell" forIndexPath:indexPath];
        cell.tag = indexPath.row;
        
        cell.channelNameLbl.tag = indexPath.row;
        NSString *name = [responceDict objectForKey:@"serviceProviderName"];
        NSString *discription = [responceDict objectForKey:@"serviceProviderContentType"];
        cell.channelNameLbl.text = name;
        NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[responceDict objectForKey:@"serviceProviderImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
//        cell.imageViewLog.layer.masksToBounds = YES;
//        cell.imageViewLog.layer.cornerRadius = 26.0;
//        cell.imageViewLog.layer.borderWidth = 1.0;
        cell.channelDetailsView.clipsToBounds = true;
        cell.channelDetailsView.layer.cornerRadius = 12;
        
        //set UI webimage cache to store image in cache it will fastly while view loading time.
        
        [cell.imageViewLog sd_setImageWithURL:[NSURL URLWithString:encripString]];
        [cell.descriptionText setText:discription];
        [cell.descriptionText setFont:[UIFont fontWithName:@"Roboto-Light" size:11]];
        [cell.descriptionText setTextColor:[UIColor lightTextColor]];
        
        NSLog(@"%@",cell.descriptionText);
        if (cell.frame.size.height == 140) {
            
            cell.descriptionText.textContainer.maximumNumberOfLines = 5;
            [cell.descriptionText setText:discription];
        }
        return  cell;
        
        
    }else{
        
        [_selectedchannelIDArray count];
        NSDictionary *responceDict = _selectedchannelIDArray[indexPath.row];
        OnDemandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ODCell" forIndexPath:indexPath];
        cell.tag = indexPath.row;
        cell.channelNameLbl.tag = indexPath.row;
        NSString *name = [responceDict objectForKey:@"serviceProviderName"];
        NSString *discription = [responceDict objectForKey:@"serviceProviderContentType"];
        cell.channelNameLbl.text = name;
        NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[responceDict objectForKey:@"serviceProviderImage"]];
        NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
//        cell.imageViewLog.layer.masksToBounds = YES;
//        cell.imageViewLog.layer.cornerRadius = 26.0;
//        cell.imageViewLog.layer.borderWidth = 1.0;
        
        cell.channelDetailsView.clipsToBounds = true;
        cell.channelDetailsView.layer.cornerRadius = 12;
        //set UI webimage cache to store image in cache it will fastly while view loading time.
        
        [cell.imageViewLog sd_setImageWithURL:[NSURL URLWithString:encripString]];
        [cell.descriptionText setText:discription];
        [cell.descriptionText setFont:[UIFont fontWithName:@"Roboto-Light" size:11]];
        [cell.descriptionText setTextColor:[UIColor lightTextColor]];
        
        NSLog(@"%@",cell.descriptionText);
        if (cell.frame.size.height == 140) {
            
            cell.descriptionText.textContainer.maximumNumberOfLines = 5;
            [cell.descriptionText setText:discription];
        }
        return cell;
    }
    }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectedchannelIDArray.count == 0) {
        
        NSMutableArray *defaultserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultserviceproviderkey"];
        serviceProviderID =[defaultserviceProviderID objectAtIndex:indexPath.row];
        
        
        param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
        param1 = serviceProviderID;
        param2 = @"false";
        param3 = @"Nr62WhRjzaLSJkvB";
        param4 = @"Nr62WhRjzaLSJkvB";
        param5 = @"Nr62WhRjzaLSJkvB";
        param6 = @"Nr62WhRjzaLSJkvB";
        
        from =1;
        to =100;
        
        //        firstFrom = from;
        //        firstTo = to;
        
        NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%i&to=%i",param0,param1,param2,param3,param4,param5,param6,from,to];
        // <--here put the request parameters you used to get the response
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        @try {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     
                     NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                     
                     
                     
                     _channelPosterArray2 =[responseDic valueForKey:@"root"];
                     NSLog(@"%@", _channelPosterArray2);
                     
                     
                     packagetypeArray=[[NSMutableArray alloc]init];
                     packagetypeArray= [[_channelPosterArray2 valueForKey:@"packageType"]objectAtIndex:0];
                     
                     selectedButtonIndex = indexPath;
                     NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                     [dic setObject:[_channelPosterArray2 valueForKey:@"root"] forKey:@"status"];
                     
                     NSDictionary *dict = [[NSDictionary alloc]init];
                     dict = _channelPosterArray2[0];
                     
                     if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                         [alert show];
                     }
                     else{
                         
                         
                         [_ODCollectionView reloadData];
                         
                         
                         NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(eventToFire0:) userInfo:nil repeats:NO];
                         //  selectedButtonIndex = indexPath;
                         NSMutableDictionary *contentID = [_channelPosterArray2 valueForKey:@"contentId"];
                         NSMutableDictionary *serviceProviderID1 = [_channelPosterArray2 valueForKey:@"serviceProviderId"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:contentID forKey:@"posterContentID"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:serviceProviderID1 forKey:@"serviceproviderKey2"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                     }
                     
                     if (_selectedchannelIDArray.count == 0) {
                         NSDictionary*dict1 = _defaultchannelIDArray[indexPath.row];
                         NSString *name = [dict1 objectForKey:@"serviceProviderName"];
                         [self setNavigationTitle:name];
                     }else{
                         NSDictionary*dict1 = _selectedchannelIDArray[indexPath.row];
                         NSString *name = [dict1 objectForKey:@"serviceProviderName"];
                         [self setNavigationTitle:name];
                         
                     }
                 }
                 else{
                     NSLog(@"%@ Null Error",connectionError);
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
    }else{
        
        NSMutableArray *channelserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelserviceproviderkey"];
        serviceProviderID =[channelserviceProviderID objectAtIndex:indexPath.row];
        
        param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
        param1 = serviceProviderID;
        param2 = @"false";
        param3 = @"Nr62WhRjzaLSJkvB";
        param4 = @"Nr62WhRjzaLSJkvB";
        param5 = @"Nr62WhRjzaLSJkvB";
        param6 = @"Nr62WhRjzaLSJkvB";
        
        from =1;
        to =100;
        
        //    firstFrom = from;
        //    firstTo = to;
        
        NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%i&to=%i",param0,param1,param2,param3,param4,param5,param6,from,to];
        // <--here put the request parameters you used to get the response
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        @try {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             
             {
                 if (!(connectionError))
                 {
                     
                     
                     NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                     
                     _channelPosterArray2 =[responseDic valueForKey:@"root"];
                     NSLog(@"%@", _channelPosterArray2);
                     
                     
                     packagetypeArray=[[NSMutableArray alloc]init];
                     packagetypeArray= [[_channelPosterArray2 valueForKey:@"packageType"]objectAtIndex:0];
                     
                     selectedButtonIndex = indexPath;
                     NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                     [dic setObject:[_channelPosterArray2 valueForKey:@"root"] forKey:@"status"];
                     
                     NSDictionary *dict = [[NSDictionary alloc]init];
                     dict = _channelPosterArray2[0];
                     
                     if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                         [alert show];
                     }
                     else{
                         
                         [_ODCollectionView reloadData];
                         
                         
                         NSTimer *timer0 = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(eventToFire0:) userInfo:nil repeats:NO];
                         //  selectedButtonIndex = indexPath;
                         NSMutableDictionary *contentID = [_channelPosterArray2 valueForKey:@"contentId"];
                         NSMutableDictionary *serviceProviderID1 = [_channelPosterArray2 valueForKey:@"serviceProviderId"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:contentID forKey:@"posterContentID"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:serviceProviderID1 forKey:@"serviceproviderKey2"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                     }
                     
                     if (_selectedchannelIDArray.count == 0) {
                         NSDictionary*dict1 = _defaultchannelIDArray[indexPath.row];
                         NSString *name = [dict1 objectForKey:@"serviceProviderName"];
                         [self setNavigationTitle:name];
                     }else{
                         NSDictionary*dict1 = _selectedchannelIDArray[indexPath.row];
                         NSString *name = [dict1 objectForKey:@"serviceProviderName"];
                         [self setNavigationTitle:name];
                         
                     }
                 }
                 else{
                     NSLog(@"%@ Null Error",connectionError);
                 }
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
    }
}
- (void)eventToFire0:(NSTimer*)timer0 {
    self.ODCollectionView.hidden = false;
    self.ODTableView.hidden = true;
  //  self.segmentedTap.hidden= false;
    self.VODMenuView.hidden = false;
    //self.SearchBar.hidden = true;
    
    self.VODSeparatedView.hidden = true;
    
  }

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat kExpandedCellHeight = 140;
    CGFloat kNormalCellHeight = 69;
    
   if(selectedCellIndexPath != nil
       && [selectedCellIndexPath compare:indexPath] == NSOrderedSame)
    {
        [tableCell.plusButton setImage:[UIImage imageNamed:@"new"] forState:UIControlStateNormal];
        if ([expandedCells containsObject:indexPath]) {
            return kExpandedCellHeight;
        }
        else
        {
            return kNormalCellHeight;
        }
        
    }
    else
    {
        
        return kNormalCellHeight;
    }
  
}

#pragma mark CollectionView Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _channelPosterArray2.count;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *responseDic = _channelPosterArray2[indexPath.row];
    OnDemandCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ODCollectionCell" forIndexPath:indexPath];
    NSString *value = [[responseDic objectForKey:@"packageType"]objectAtIndex:0];
    cell.rateLable.text = value;
    NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[responseDic objectForKey:@"contentImageFileName"]];
    NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //set UI webimage cache to store image in cache it will fastly while view loading time.
[cell.imageView sd_setImageWithURL:[NSURL URLWithString:encripString]];

return cell;
}
//NSLog(@"MCC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"]);
//NSLog(@"MNC : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"]);
//NSLog(@"Country Code : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]);
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *contentID = [[NSUserDefaults standardUserDefaults] objectForKey:@"posterContentID"];
    NSMutableArray *allServiceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"serviceproviderKey2"];

    NSMutableArray *categreContentID = [[NSUserDefaults standardUserDefaults] objectForKey:@"CategryContentID"];
    NSMutableArray *genreContentID = [[NSUserDefaults standardUserDefaults] objectForKey:@"GenreContentID"];
    NSMutableArray *languageContentID = [[NSUserDefaults standardUserDefaults] objectForKey:@"LanguageContentID"];
    NSString *languageFilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLanguageSegmentButtonIndex"];
    
    param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
    
    if (posterSeleted == NO) {
  param1 = [allServiceProviderID objectAtIndex:indexPath.row];
        param2 = [contentID objectAtIndex:indexPath.row];
        param9 = @"Nr62WhRjzaLSJkvB";
        
    }
    else
    {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectCategorySegment"]  isEqual: @"true"])
        {
            NSMutableArray *serviceProviderID2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"CategryserviceProviderID"];
            param1 = [serviceProviderID2 objectAtIndex:indexPath.row];
            param2 = [categreContentID objectAtIndex:indexPath.row];
            param9 = @"Nr62WhRjzaLSJkvB";
        }
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectGenreSegment"]  isEqual: @"true"])
        {
            NSMutableArray *serviceProviderID2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"GenreServiceProviderID"];
            param1 = [serviceProviderID2 objectAtIndex:indexPath.row];
            param2 = [genreContentID objectAtIndex:indexPath.row];
            param9 = @"Nr62WhRjzaLSJkvB";
        }
        
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectLanguageSegment"]  isEqual: @"true"])
        {
            NSMutableArray *serviceProviderID2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"languageServiceProviderID"];
            param1 = [serviceProviderID2 objectAtIndex:indexPath.row];
            param2 = [languageContentID objectAtIndex:indexPath.row];
            param9 = languageFilterName;
            
            
        }
        
        
    }
    
    param10 = [[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"];
    param11 = [[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"];
    param12 = [[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&contentId=%@&language=%@&countryCode=%@&MCC=%@&MNC=%@",param0,param1,param2,param9,param10,param11,param12];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    @try
    {

    NSString *str =[NSString stringWithFormat:@"%@/index.php/videoContent2",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:str];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         
         {
             if (!(connectionError))
             {

    

    
    NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];

    globalDic = [responseDic valueForKey:@"root"];
    //_videoContentArray = [responseDic valueForKey:@"root"];
                 
                 if([[globalDic objectForKey:@"status"]isEqualToString:@"No Allowed Packages"])
                     
                 {
                     [_ODCollectionView reloadData];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     [alert show];
                     
                 }
                 else if([[globalDic objectForKey:@"status"]isEqualToString:@"No Data Found"])
                 {
                     [_ODCollectionView reloadData];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     [alert show];
                    
                 }
                 else if([[globalDic objectForKey:@"status"]isEqualToString:@"Token Expired"])
                 {
                     [_ODCollectionView reloadData];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Token Expired" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     [alert show];
                     
                 }
                 else{
                 
    
        languageArray=[globalDic valueForKey:@"language"];
                     
                     
        actorArray=[globalDic valueForKey:@"Actor"];
                     
                     
        directorArray=[globalDic valueForKey:@"Director"];
                     
                     
        packageArray=[globalDic valueForKey:@"packageType"];
                     
                     
                     
        producerArray =[globalDic valueForKey:@"Producer"];
                     
                     
        VODTypeArray =[globalDic valueForKey:@"VODType"];
                     
                     
        musicArray = [globalDic valueForKey:@"music"];

                     
      _collectionViewTopContsraint.constant = 125;
                     
                     
     cellIndexPath  = indexPath;
    
    NSDictionary*posterDic = _channelPosterArray2[indexPath.row];
    NSString *imgURL = [NSString stringWithFormat:@"https://www.sprngpod.com/xperiolabsapi/%@",[posterDic objectForKey:@"contentImageFileName"]];
    NSString *encripString =[imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * eventName = [globalDic valueForKey:@"titleName"];
    NSString * eventLanguage = [languageArray objectAtIndex:0];
    NSString * ratting = [globalDic valueForKey:@"parentalRating"];
    NSString * cost = [packageArray objectAtIndex:0];
    NSString * actorNameLbl=[actorArray objectAtIndex:0];
    NSString * directorNameLbl=[directorArray objectAtIndex:0];
    NSString * SelectedDiscription=[globalDic valueForKey:@"extendedDescription"];
    self.currentValue = (int)cellIndexPath.row;
    self.actorNameLbl.text =actorNameLbl;
    self.directorNameLbl.text =directorNameLbl;
    self.selectedDescriptionText.text=SelectedDiscription;
    self.eventNameLbl.text = eventName;
    self.eventLanguageLbl.text = eventLanguage;
    self.rattingLbl.text = [NSString stringWithFormat:@"|  %@  |",ratting];
    self.costLbl.text = cost;
    [self.selectedImageView sd_setImageWithURL:[NSURL URLWithString:encripString]];
    [self.selectedView addSubview:self.selectedDescriptionText];
    [self.selectedDescriptionText setFont:[UIFont fontWithName:@"Roboto-Light" size:11]];
                 [self.selectedDescriptionText setTextColor:[UIColor grayColor]];
    self.selectedDescriptionText.textContainer.maximumNumberOfLines = 40;
    self.selectedDescriptionText.hidden = true;
    self.selectedView.hidden = false;
                     
                 }
    }
    else{
        NSLog(@"%@ Null Error",connectionError);
    }
}];

} @catch (NSException *exception) {
    NSLog(@"%@ Exception Error",exception);
} @finally {
    NSLog(@"");
}


}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (self.selectedView.hidden == true) {
        
        

        
        _collectionViewTopContsraint.constant = 0;
    }
    else
    {
        _collectionViewTopContsraint.constant = 125;
       
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(IS_IPHONE_5)
  {
    if (self.collectionViewTopContsraint.constant ==125) {
        
        return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1+8);
        }
        else{
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
        }
   }
    else if (IS_IPHONE_6)
    {
        if (self.collectionViewTopContsraint.constant == 125) {
            
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-25);
        }
        else{
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
        }
    }
    else if (IS_IPHONE_6_PLUS)
    {
    if (self.collectionViewTopContsraint.constant == 125) {
        
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-48);
        }
        else{
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);//14
        }
    }
    else if (IS_IPHONE_7)
    {
        if (self.collectionViewTopContsraint.constant == 125) {
          
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-25);
        }
        else{
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
        }
    }
    else if (IS_IPHONE_7_PLUS)
    {
        if (self.collectionViewTopContsraint.constant == 125) {
            
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/1-48);
        }
        else{
            return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);//14
        }
    }

   return CGSizeMake(collectionView.frame.size.width/3-5, collectionView.frame.size.height/2-14);
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView == ODpinviewAlert)
    {
        if (buttonIndex == 0) {
            
            [self.view endEditing:YES];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
            [self.playPauseButton setHidden:NO];
            _AVPlayerController.view.hidden = YES;
            _loader.hidden = YES;
            [_loader hidesWhenStopped];
            
        }
        else
        {
            _AVPlayerController.view.hidden = NO;
            [self.view endEditing:YES];
            
            @try {
                [self.view endEditing:YES];
                [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                pinString = [ODpinviewAlert textFieldAtIndex:0].text;
                // pinString = @"";
                // pinString = self.pinTextField.text;
                self.pinTextField.text = @"";
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/verifyPIN1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
                
                NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
                //        NSLog(@"post %@",post);
                
                NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
                [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
                [request setHTTPBody:postData];
                
                NSURLResponse *response;
                NSError *error = nil;
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                //        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
                //        NSDictionary *json_dict = (NSDictionary *)json_string;
                //        NSLog(@"json_dict\n%@",json_dict);
                //        NSLog(@"json_string\n%@",json_string);
                
                //        NSLog(@"%@",response);
                //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
                NSLog(@" error is %@",error);
                
                if (!error) {
                    NSError *myError = nil;
                    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
                    //            NSLog(@"%@",dictionary);
                    temp = [dictionary objectForKey:@"root"];
                    if (temp) {
                        NSDictionary *sDic = temp[0];
                        if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                            [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                            ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                        message:@""
                                                                       delegate:self
                                                              cancelButtonTitle:@"Cancel"
                                                              otherButtonTitles:@"Ok",nil];
                            ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
                            
                            UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
                            pinTextField.delegate = self;
                            [pinTextField becomeFirstResponder];
                            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                            pinTextField.placeholder = @"Enter PIN";
                            [ODpinviewAlert show];
                            
                            //  self.PinView.hidden = NO;
                            
                            //         self.lastSeenTvImageView.hidden = NO;
                            self.playPauseButton.hidden = NO;
                            _AVPlayerController.view.hidden = YES;
                            [self goToRootView];
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                            [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                            
                            ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                        message:@""
                                                                       delegate:self
                                                              cancelButtonTitle:@"Cancel"
                                                              otherButtonTitles:@"Ok",nil];
                            ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
                            
                            UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
                            pinTextField.delegate = self;
                            [pinTextField becomeFirstResponder];
                            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                            pinTextField.placeholder = @"Enter PIN";
                            [ODpinviewAlert show];
                            
                            // self.PinView.hidden = NO;
                            //          self.lastSeenTvImageView.hidden = NO;
                            self.playPauseButton.hidden = NO;
                            _AVPlayerController.view.hidden = YES;
                            [self goToRootView];
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                            [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                            [self goToRootView];
                        }
                        
                        
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
                            
                            [self.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
                            
                            ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                                        message:@""
                                                                       delegate:self
                                                              cancelButtonTitle:@"Cancel"
                                                              otherButtonTitles:@"Ok",nil];
                            ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
                            
                            UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
                            pinTextField.delegate = self;
                            [pinTextField becomeFirstResponder];
                            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
                            pinTextField.placeholder = @"Enter PIN";
                            
                            
                            [ODpinviewAlert show];
                            
                            // self.PinView.hidden = NO;
                            //       self.lastSeenTvImageView.hidden = NO;
                            self.playPauseButton.hidden = NO;
                            _AVPlayerController.view.hidden = YES;
                        }
                        else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
                            
                            //  [_player pause];
                            
                            //                    if ([defaultScreen  isEqual: @"LiveTV"]) {
                            //                        //****** Getting the Update Channel Usage Details Of Chaneels *****//
                            //                        //[self updateChannelUsageDetails];
                            //                    }else if ([defaultScreen  isEqual: @"Radio"])
                            //                    {
                            //                        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
                            //                        //[self updateRadioUsageDetails];
                            //                    }
                            //
                            
                            // self.lastSeenTvImageView.hidden = YES;
                            self.playPauseButton.hidden = YES;
                            _AVPlayerController.view.hidden = NO;
                            
                            //[_player play];
                            //****** Getting the Add Channel Usage Details Of chaneels *****//
                             [self addingChannelUsageDetails];
                            
                            _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
                            _AVPlayerController = [[AVPlayerViewController alloc] init];
                            _AVPlayerController.view.frame = self.tvView.bounds;
                            [self addChildViewController:_AVPlayerController];
                            [self.tvView addSubview:_AVPlayerController.view];
                            _AVPlayerController.player = _player;
                            _AVPlayerController.showsPlaybackControls = false;
                            
                            [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
                            [_AVPlayerController.view bringSubviewToFront:_tvView];
                            
                            _loader.hidden = YES;
                            [_loader hidesWhenStopped];
                            
                            [_player play];
                            
                            
                            
                            //****** Getting the Add Channel Usage Details Of chaneels *****//
                             [self addingChannelUsageDetails];
                            
                        }
                        
                        
                        
                        
                        else{
                            NSLog(@"temp is getting Null");
                        }
                        
                        
                    }
                    else
                    {
                        NSLog(@"%@",error);
                    }
                    
                }
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        }
    }
}


#pragma mark PostData

-(void)postdata2{
    
    if (_selectedchannelIDArray.count == 0) {
        
        NSMutableArray *defaultserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultserviceproviderkey"];
        serviceProviderID =[defaultserviceProviderID objectAtIndex:selectedButtonIndex.row];
        
        param1 = serviceProviderID;
        
        param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
        param2 = @"true";
        param3 = @"Nr62WhRjzaLSJkvB";
        param4 = @"Nr62WhRjzaLSJkvB";
        param5 = @"Nr62WhRjzaLSJkvB";
        param6 = @"Nr62WhRjzaLSJkvB";
        param7 =@"1";
        NSInteger vaue1 =[param7 intValue];
        param8 =@"10";
        NSInteger vaue2 =[param8 intValue];
        
        NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%ld&to=%ld",param0,param1,param2,param3,param4,param5,param6,vaue1,vaue2]; // <--here put the request parameters you used to get the response
        
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        @try
        {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                
                
                NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                _channelCategoryArray =[responseDic valueForKey:@"root"];
                NSLog(@"%@", _channelCategoryArray);
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
            
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
    }
    
    
    else{
        
        NSMutableArray *channelserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelserviceproviderkey"];
        serviceProviderID =[channelserviceProviderID objectAtIndex:selectedButtonIndex.row];
        //    NSMutableArray *serviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"serviceproviderKey1"];
        param1 = serviceProviderID;
        
        param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
        param2 = @"true";
        param3 = @"Nr62WhRjzaLSJkvB";
        param4 = @"Nr62WhRjzaLSJkvB";
        param5 = @"Nr62WhRjzaLSJkvB";
        param6 = @"Nr62WhRjzaLSJkvB";
        param7 =@"1";
        NSInteger vaue1 =[param7 intValue];
        param8 =@"10";
        NSInteger vaue2 =[param8 intValue];
        
        NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%ld&to=%ld",param0,param1,param2,param3,param4,param5,param6,vaue1,vaue2]; // <--here put the request parameters you used to get the response
        
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        @try
        {
            NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
            str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            
            NSURLResponse *response;
            NSError *error;
            NSData *responseData= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (!(error))
            {
                
                
                NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                _channelCategoryArray =[responseDic valueForKey:@"root"];
                NSLog(@"%@", _channelCategoryArray);
            }
            else{
                NSLog(@"%@ Null Error",error);
            }
            
        } @catch (NSException *exception) {
            NSLog(@"%@ Exception Error",exception);
        } @finally {
            NSLog(@"");
        }
        
    }
}

#pragma mark HomeScreen API
    
-(void)newHomeScreenAPI{
        
    if (networkStatus == NotReachable) {
        
        [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
//    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources1=%@&countryCode=%@&MCC=%@&MNC=%@&deviceId=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],deviceID]]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources1=%@&countryCode=%@&MCC=%@&MNC=%@&deviceId=%@&appVersion=%@&platform=%@&deviceToken=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MCC"],[[NSUserDefaults standardUserDefaults]objectForKey:@"MNC"],deviceID,kAppVarsion,@"iOS",[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]]]];

    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"Saved Token = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"]) {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
                [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else{
                
                IPAddress = [dict objectForKey:@"ipAddress"];
               // apiGeo = [dict objectForKey:@"countryName"];
                lastSeenTVUrl = [dict objectForKey:@"lastSeenTVChannelURL"];
                //NSString *imageUrl = [NSString stringWithFormat:@"%@/",[[SprngIPConf getSharedInstance]getIpForImage]];
              //  lastSeenTVChannelLogoURL =[dict objectForKey:@"lastSeenTVChannelLogo"];
            //    NSString *imageUrl4 = [imageUrl stringByAppendingString:lastSeenTVChannelLogoURL];
              //  tvLogoUrl = [NSURL URLWithString:imageUrl4];
                //                    [_lastSeenTvImageView sd_setImageWithURL:tvLogoUrl];
                lastListenRadioChannelLanguage = [dict objectForKey:@"lastListenRadioChannelLanguage"];
              //  lastListenRadioLogoUrl = [dict objectForKey:@"lastListenRadioChannelLogo"];
            //    NSString *imageUrl1 = [imageUrl stringByAppendingString:lastListenRadioLogoUrl];
//radioLogoUrl = [NSURL URLWithString:imageUrl1];
              //  //                        [_lastSeenTvImageView sd_setImageWithURL:radioLogoUrl];
          //      lastListenRadioUrl = [dict objectForKey:@"lastListenRadioChannelURL"];
                NSString *imageUrl2 = [dict objectForKey:@"promoLogo"];
               // NSString *promoLogo = [imageUrl stringByAppendingString:imageUrl2];
             //   promoLogoUrl = [NSURL URLWithString:promoLogo];
             //   promoVideoUrl = [dict objectForKey:@"promoVideoURL"];
             //   defaultScreen = [dict objectForKey:@"settings"];
             //   NSNumber *badgNumber = [dict objectForKey:@"badge"];
             //   badgeNumber = [badgNumber intValue];
                
                NSNumber *number = [dict objectForKey:@"sessionTimeOut"];
                sessionTime = [number intValue];
                NSNumber *refreshTime = [dict objectForKey:@"tokenRefreshTime"];
                tokenRefreshTime = [refreshTime intValue];
                
                NSString *lastSeenTVBlockedStatus = [dict objectForKey:@"lastSeenTVBlockedStatus"];
                NSString *lastSeenTVCountryAllowed = [dict objectForKey:@"lastSeenTVCountryAllowed"];
                NSString *lastSeenTVParentalStatus = [dict objectForKey:@"lastSeenTVParentalStatus"];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVBlockedStatus forKey:@"lastSeenTVBlockedStatus"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVCountryAllowed forKey:@"lastSeenTVCountryAllowed"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVParentalStatus forKey:@"lastSeenTVParentalStatus"];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastListenRadioChannelLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults] setObject:lastSeenTVBlockedStatus forKey:@"lastSeenTVBlockedStatus"];
                
                // [[NSUserDefaults standardUserDefaults] setObject:apiGeo forKey:@"apiGeo"];
                [[NSUserDefaults standardUserDefaults]synchronize];

                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [_tokenRefreshTimer invalidate];
                if (tokenRefreshTime < 1) {
                    
                }
                else{
                    _tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:tokenRefreshTime target:self selector:@selector(RefreshToken) userInfo:nil repeats:YES];
                }
            }
            
            
            
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"No Network Available" duration:2.0 position:CSToastPositionCenter style:style];
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [alert show];
            
        }
        
    }
    
    
    }


-(void)homeScreenApi
{
     if (networkStatus == NotReachable) {
        [self.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"%ld",(long)networkStatus);
    }
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?fetchHomeScreenResources=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    [urlRequest setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSLog(@"urlRequest : %@",urlRequest);
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        
        NSLog(@"status = %ld",(long)status);
        
        if(dictionary && status == 200) {
            NSArray *sDisc = [[NSArray alloc]init];
            sDisc = [dictionary objectForKey:@"root"];
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = sDisc[0];
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Deleted"])
        {
                [self.view makeToast:@"Customer Deleted" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                
            }
            if ([[dict objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            
            else if ([[dict objectForKey:@"status"]isEqualToString:@"Customer Inactive"]){
                [self.view makeToast:@"Customer Inactive" duration:2.0 position:CSToastPositionCenter style:style];
            }
            
            else{
                
                IPAddress = [dict objectForKey:@"ipAddress"];
                lastSeenTVUrl = [dict objectForKey:@"lastSeenTVChannelURL"];
              lastListenRadioChannelLanguage = [dict objectForKey:@"lastListenRadioChannelLanguage"];
               NSNumber *number = [dict objectForKey:@"sessionTimeOut"];
                sessionTime = [number intValue];
                NSNumber *refreshTime = [dict objectForKey:@"tokenRefreshTime"];
                tokenRefreshTime = [refreshTime intValue];
                
                [[NSUserDefaults standardUserDefaults] setObject:lastListenRadioChannelLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults]setObject:IPAddress forKey:@"iPA"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [_tokenRefreshTimer invalidate];
                if (tokenRefreshTime < 1) {
                    
                }
                else{
                    _tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:tokenRefreshTime target:self selector:@selector(RefreshToken) userInfo:nil repeats:YES];
                }
            }
        }
        else{
            NSLog (@"Oh Sorry");
            
            [self.view makeToast:@"Check Your UserId and Password" duration:2.0 position:CSToastPositionCenter style:style];
        }
        
    }
}

-(void)tapOnTvView:(id) sender{
    
    if (self.playPauseButton.hidden) {
        if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        [self.playPauseButton setHidden:NO];
        
        
    }
    else{
        if(_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused){

            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
        }
        
    }
}

-(void)hidePlayPause{
    [self.playPauseButton setHidden:YES];
}
#pragma mark RefreshToken

-(void)RefreshToken{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/refreshCustomerToken",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&deviceId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],deviceID];
    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
    NSLog(@"json_dict\n%@",json_dict);
    NSLog(@"json_string\n%@",json_string);
    
    NSLog(@"%@",response);
    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            
            NSDictionary *sDic = temp[0];
            
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
                [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
                
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
                [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
                [self goToRootView];
            }
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                NSLog(@"Customer Does Not Exist");
            }
            
            else{
                NSString *token = [sDic objectForKey:@"token"];
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
}



-(void)goToRootView
{
 
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"sessionExpiry" object:self userInfo:nil];
    [_player pause];
    
    //***** Updation Of VOD Usage Details ******//
    
    if ([state isEqualToString:@"LiveTV"])
    {
       [self updateChannelUsageDetails];
    }else if ([state isEqualToString:@"VOD"])
    {
        [self updateVODUsageDetails];
    }

    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LoginViewController *rootView = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [appDel.window setRootViewController:rootView];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginStatus"];
    NSLog(@"hello I am Back");
}



#pragma mark BarButton Actions

- (IBAction)descriptionPressed:(id)sender {
    
    
    UIButton *button = (UIButton *)sender;
    CGRect buttonFrame = [button convertRect:button.bounds toView:_ODTableView];
    NSIndexPath *indexPath = [_ODTableView indexPathForRowAtPoint:buttonFrame.origin];
   
    selectedCellIndexPath = indexPath;
 
    
    if ([expandedCells containsObject:indexPath]) {
        [expandedCells removeObject:indexPath];
    }
    else
    {
        [expandedCells addObject:indexPath];
    }
  
    [_ODTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
}


- (IBAction)homeBarPressed:(id)sender {
    [_player pause];
    
    //***** Updation Of VOD Usage Details ******//
    
    if ([state isEqualToString:@"LiveTV"])
    {
        [self updateChannelUsageDetails];
    }else if ([state isEqualToString:@"VOD"])
    {
        [self updateVODUsageDetails];
    }

    
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"didSelectCategorySegment"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"didSelectGenreSegment"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"didSelectLanguageSegment"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedGenreSegmentButtonIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedLanguageSegmentButtonIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedCategorySegmentButtonIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    posterSeleted = NO;

    
}

- (IBAction)backButtonPressed:(id)sender {
    
    
    if (self.ODCollectionView.hidden == true)
    {
        [_player pause];
        
        //***** Updation Of VOD Usage Details ******//
        
        if ([state isEqualToString:@"LiveTV"])
        {
          [self updateChannelUsageDetails];
        }else if ([state isEqualToString:@"VOD"])
        {
            [self updateVODUsageDetails];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } else if(self.selectedView.hidden == false)
    {
        //***** Updation Of VOD Usage Details ******//
        
        if ([state isEqualToString:@"LiveTV"])
        {
          [self updateChannelUsageDetails];
        }else if ([state isEqualToString:@"VOD"])
        {
            [self updateVODUsageDetails];
        }
        
         self.selectedView.hidden = true;
        _collectionViewTopContsraint.constant = 0;
    }
    else
    {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"didSelectCategorySegment"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"didSelectGenreSegment"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"didSelectLanguageSegment"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedGenreSegmentButtonIndex"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedLanguageSegmentButtonIndex"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedCategorySegmentButtonIndex"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.ODCollectionView.hidden = true;
        //self.segmentedTap.hidden = true;
        self.VODMenuView.hidden = true;
        [self setNavigationTitle:@"On Demand"];
        self.SearchBar.hidden = false;
        self.VODSeparatedView.hidden = false;
        self.ODTableView.hidden = false;
        posterSeleted = NO;
    }

    if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
        [_player pause];
        
        //***** Updation Of VOD Usage Details ******//
        
        if ([state isEqualToString:@"LiveTV"])
        {
            [self updateChannelUsageDetails];
        }else if ([state isEqualToString:@"VOD"])
        {
            [self updateVODUsageDetails];
        }
    }
    


}

- (IBAction)playAndPausePressed:(id)sender
{
    if (_AVPlayerController.player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
        
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"lastSeenTVBlockedStatus"] isEqualToString:@"true"]) {
            
            _AVPlayerController.view.hidden = YES;
            
            //self.PinView.hidden = NO;
            
            ODpinviewAlert = [[UIAlertView alloc] initWithTitle:@"Enter PIN"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];
            ODpinviewAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
            
            UITextField *pinTextField = [ODpinviewAlert textFieldAtIndex:0];
            pinTextField.delegate = self;
            [pinTextField becomeFirstResponder];
            pinTextField.keyboardType = UIKeyboardTypeNumberPad;
            pinTextField.placeholder = @"Enter PIN";
            
            [ODpinviewAlert show];
            
            [self.view endEditing:YES];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
            
        }else
        {
            _player.closedCaptionDisplayEnabled = NO;
            [_player play];
            playButtonTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hidePlayPause) userInfo:nil repeats:NO];
            [self.playPauseButton setImage:[UIImage imageNamed:@"HomePausebutton.png"] forState:UIControlStateNormal];
        }
        
    }
    
    else{
        _player.closedCaptionDisplayEnabled = NO;
        [_player pause];
        [playButtonTimer invalidate];
        [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
        [self.playPauseButton setHidden:NO];
    }
}


- (void)PosterFilterforCategory{
    param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
    param1 = serviceProviderID;
    
        NSString *selectedGenreFilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedGenreSegmentButtonIndex"];
    NSString *selectedLanguageFilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLanguageSegmentButtonIndex"];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectGenreSegment"]  isEqual: @"true"])
    {
        param4 = selectedGenreFilterName;
        
    }
    
    else {
        param4 = @"Nr62WhRjzaLSJkvB";
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectLanguageSegment"]  isEqual: @"true"])
        
    {
        param5 = selectedLanguageFilterName;
        
    }
    else {
        param5 = @"Nr62WhRjzaLSJkvB";
        
    }
    
    param2 = @"false";
    
    param3 = categoryfilterName;
    
    param6 = @"Nr62WhRjzaLSJkvB";
    
    param7 =@"1";
    NSInteger vaue1 =[param7 intValue];
    param8 =@"100";
    NSInteger vaue2 =[param8 intValue];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%ld&to=%ld",param0,param1,param2,param3,param4,param5,param6,(long)vaue1,(long)vaue2];
    // <--here put the request parameters you used to get the response
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    @try {
        
        NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
        
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:str];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         
         {
             if (!(connectionError))
             {
                 
                 NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                 _channelPosterArray2 =[responseDic valueForKey:@"root"];
                 NSLog(@"%@", _channelPosterArray2);
                 NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                 [dic setObject:[_channelPosterArray2 valueForKey:@"root"] forKey:@"status"];
                 
                 
                 
                 NSDictionary *dict = [[NSDictionary alloc]init];
                 dict = _channelPosterArray2[0];
                 
                 if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                     
                 {
                     [_ODCollectionView reloadData];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     [alert show];
                     
                 }
                 else{
                     [_ODCollectionView reloadData];
                     
                     NSMutableDictionary *categryContentID = [_channelPosterArray2 valueForKey:@"contentId"];
                     NSMutableDictionary *catrgryserviceProviderID = [_channelPosterArray2 valueForKey:@"serviceProviderId"];
                     [[NSUserDefaults standardUserDefaults] setObject:catrgryserviceProviderID forKey:@"CategryserviceProviderID"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     [[NSUserDefaults standardUserDefaults] setObject:categryContentID forKey:@"CategryContentID"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     
                 }
                 [[NSUserDefaults standardUserDefaults] setObject:categoryfilterName forKey:@"SelectedCategorySegmentButtonIndex"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"didSelectCategorySegment"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
             }
             else{
                 NSLog(@"%@ Null Error",connectionError);
             }
         }];
        
    } @catch (NSException *exception) {
        NSLog(@"%@ Exeption Null Error",exception);
    } @finally {
        NSLog(@"");
    }
}

- (void)PosterFilterforGenre{
    
    param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
    param1 = serviceProviderID;
    
    NSString *selectedcategoryfilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCategorySegmentButtonIndex"];
    
    NSString *selectedlanguageFilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLanguageSegmentButtonIndex"];
    
    param2 = @"false";
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectCategorySegment"]  isEqual: @"true"])
    {
        param3 = selectedcategoryfilterName;
    }
    
    else {
        param3 = @"Nr62WhRjzaLSJkvB";
        
    }
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectLanguageSegment"]  isEqual: @"true"])
        
    {
        param5 = selectedlanguageFilterName;
    }
    
    else {
        param5 = @"Nr62WhRjzaLSJkvB";
        
    }
    
    param4 = genreFilterName;
    param6 = @"Nr62WhRjzaLSJkvB";
    
    param7 =@"1";
    NSInteger vaue1 =[param7 intValue];
    param8 =@"100";
    NSInteger vaue2 =[param8 intValue];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%ld&to=%ld",param0,param1,param2,param3,param4,param5,param6,vaue1,vaue2]; // <--here put the request parameters you used to get the response
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    @try
    {
        NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:str];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if (!(connectionError))
             {
                 
                 NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                 _channelPosterArray2 =[responseDic valueForKey:@"root"];
                 
                 NSLog(@"%@", _channelPosterArray2);
                 
                 NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                 
                 [dic setObject:[_channelPosterArray2 valueForKey:@"root"] forKey:@"status"];
                 
                 NSDictionary *dict = [[NSDictionary alloc]init];
                 dict = _channelPosterArray2[0];
                 
                 if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                     
                 {
                     [_ODCollectionView reloadData];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     [alert show];
                 }
                 else{
                     [_ODCollectionView reloadData];
                     
                     NSMutableDictionary *genreContentID = [_channelPosterArray2 valueForKey:@"contentId"];
                     
                     NSMutableDictionary *genreserviceProviderID = [_channelPosterArray2 valueForKey:@"serviceProviderId"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:genreserviceProviderID forKey:@"GenreServiceProviderID"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:genreContentID forKey:@"GenreContentID"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     
                 }
                 
                 [[NSUserDefaults standardUserDefaults] setObject:genreFilterName forKey:@"SelectedGenreSegmentButtonIndex"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"didSelectGenreSegment"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             else{
                 NSLog(@"%@ Null Error",connectionError);
             }
         }];
        
    } @catch (NSException *exception) {
        NSLog(@"%@ Exception Error",exception);
    } @finally {
        NSLog(@"");
    }
    
}

- (void)PosterFilterforLanguage{
    
    param0 = [[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"];
    param1 = serviceProviderID;
    
    
    NSString *slectedCategoryFilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCategorySegmentButtonIndex"];
    
    NSString *slectedGenreFilterName = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedGenreSegmentButtonIndex"];
    
    
    param2 = @"false";
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectCategorySegment"]  isEqual: @"true"]){
        
        param3 = slectedCategoryFilterName;
    }
    else {
        param3 = @"Nr62WhRjzaLSJkvB";
    }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"didSelectGenreSegment"]  isEqual: @"true"])
    {
        param4 = slectedGenreFilterName;
    }
    
    else {
        param4 = @"Nr62WhRjzaLSJkvB";
    }
    param5 = languageFilterName;
    
    param6 = @"Nr62WhRjzaLSJkvB";
    
    param7 =@"1";
    NSInteger vaue1 =[param7 intValue];
    param8 =@"100";
    NSInteger vaue2 =[param8 intValue];
    
    NSString *post = [NSString stringWithFormat:@"customerId=%@&serviceProviderId=%@&retrieve=%@&categoryName=%@&genres=%@&language=%@&searchString=%@&from=%ld&to=%ld",param0,param1,param2,param3,param4,param5,param6,vaue1,vaue2];
    // <--here put the request parameters you used to get the response
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    @try
    {
        NSString *str =[NSString stringWithFormat:@"%@/index.php/listOfAllVideos1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:str];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if (!(connectionError))
             {
                 
                 NSDictionary *responseDic= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&connectionError];
                 _channelPosterArray2 =[responseDic valueForKey:@"root"];
                 NSLog(@"%@", _channelPosterArray2);
                 
                 NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                 [dic setObject:[_channelPosterArray2 valueForKey:@"root"] forKey:@"status"];
                 NSDictionary *dict = [[NSDictionary alloc]init];
                 dict = _channelPosterArray2[0];
                 if([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"])
                 {
                     [_ODCollectionView reloadData];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data to Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                     [alert show];
                 }
                 else{
                     [_ODCollectionView reloadData];
                     NSMutableDictionary *languageContentID = [_channelPosterArray2 valueForKey:@"contentId"];
                     NSMutableDictionary *languageServiceProviderID = [_channelPosterArray2 valueForKey:@"serviceProviderId"];
                     [[NSUserDefaults standardUserDefaults] setObject:languageServiceProviderID forKey:@"languageServiceProviderID"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     [[NSUserDefaults standardUserDefaults] setObject:languageContentID forKey:@"LanguageContentID"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                 }
                 
                 [[NSUserDefaults standardUserDefaults] setObject:languageFilterName forKey:@"SelectedLanguageSegmentButtonIndex"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"didSelectLanguageSegment"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             else{
                 NSLog(@"%@ Null Error",connectionError);
             }
         }];
        
    } @catch (NSException *exception) {
        NSLog(@"%@ Exception Error",exception);
    } @finally {
        NSLog(@"");
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 100){
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            if (_selectedchannelIDArray.count == 0) {
                NSMutableArray *defaultserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultserviceproviderkey"];
                serviceProviderID =[defaultserviceProviderID objectAtIndex:selectedButtonIndex.row];
                NSMutableArray *categoryfilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"categoryFilterName"];
                categoryfilterName= [categoryfilerNameArray objectAtIndex:buttonIndex];
                [self PosterFilterforCategory];
                [[NSUserDefaults standardUserDefaults] setObject:categoryfilterName forKey:@"SelectedCategorySegmentButtonIndex"];
            }
            else{
                NSMutableArray *channelserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelserviceproviderkey"];
                serviceProviderID =[channelserviceProviderID objectAtIndex:selectedButtonIndex.row];
                NSMutableArray *categoryfilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"categoryFilterName"];
                categoryfilterName = [categoryfilerNameArray objectAtIndex:buttonIndex];
                [[NSUserDefaults standardUserDefaults] setObject:categoryfilterName forKey:@"SelectedCategorySegmentButtonIndex"];
                [self PosterFilterforCategory];
            }
        }
    }
    else if(actionSheet.tag == 200)
    {
        if (buttonIndex != actionSheet.cancelButtonIndex)
        {
            if (_selectedchannelIDArray.count == 0) {
                NSMutableArray *defaultserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultserviceproviderkey"];
                serviceProviderID =[defaultserviceProviderID objectAtIndex:selectedButtonIndex.row];
                NSMutableArray *genreFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"genreFilterName"];
                genreFilterName = [genreFilerNameArray objectAtIndex:buttonIndex];
                [self PosterFilterforGenre];
                [[NSUserDefaults standardUserDefaults] setObject:genreFilterName forKey:@"SelectedGenreSegmentButtonIndex"];
            }
            else
            {
                NSMutableArray *channelserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelserviceproviderkey"];
                serviceProviderID =[channelserviceProviderID objectAtIndex:selectedButtonIndex.row];
                NSMutableArray *genreFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"genreFilterName"];
                genreFilterName = [genreFilerNameArray objectAtIndex:buttonIndex];
                [self PosterFilterforGenre];
                [[NSUserDefaults standardUserDefaults] setObject:genreFilterName forKey:@"SelectedGenreSegmentButtonIndex"];
            }
        }
    }
    else if (actionSheet.tag == 300)
    {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            if (_selectedchannelIDArray.count == 0) {
                NSMutableArray *defaultserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultserviceproviderkey"];
                serviceProviderID =[defaultserviceProviderID objectAtIndex:selectedButtonIndex.row];
                NSMutableArray *languageFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"languageFilterName"];
                languageFilterName = [languageFilerNameArray objectAtIndex:buttonIndex];
                [self PosterFilterforLanguage];
                [[NSUserDefaults standardUserDefaults] setObject:languageFilterName forKey:@"SelectedLanguageSegmentButtonIndex"];
            }
            else{
                NSMutableArray *channelserviceProviderID = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelserviceproviderkey"];
                serviceProviderID =[channelserviceProviderID objectAtIndex:selectedButtonIndex.row];
                NSMutableArray *languageFilerNameArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"languageFilterName"];
                languageFilterName = [languageFilerNameArray objectAtIndex:buttonIndex];
                [self PosterFilterforLanguage];
                [[NSUserDefaults standardUserDefaults] setObject:languageFilterName forKey:@"SelectedLanguageSegmentButtonIndex"];
            }
        }
    }
}
- (IBAction)playButtonAction:(id)sender

{
  //[_player pause];
    [self.playButton setImage:[UIImage imageNamed:@"play_highlighted.png"] forState:UIControlStateNormal];
    
    if (networkStatus == NotReachable) {
        
        [self.navigationController.view makeToast:@"No Netwotk Available" duration:2.0 position:CSToastPositionCenter style:style];
    }
    else{
        NSLog(@"Network %ld",(long)networkStatus);
    }
    [_player pause];
    
    //***** Updation Of VOD Usage Details ******//
    
    if ([state isEqualToString:@"LiveTV"])
    {
      [self updateChannelUsageDetails];
    }else if ([state isEqualToString:@"VOD"])
    {
        [self updateVODUsageDetails];
    }

       //self.playPauseView.hidden=true;
    
    [self playTVWithValue];
}
- (IBAction)oldPlusButtonAction:(id)sender {
    
    
     CGRect newFrame = _selectedView.frame;
    
    if(_oldPlusButton.tag == 0)
    {
          self.ODCollectionView.hidden = true;
          [self.oldPlusButton setImage:[UIImage imageNamed:@"info-hilight.png"] forState:UIControlStateNormal];
          newFrame.size.height = 500;
          self.selectedDescriptionText.hidden =false;
          [self.selectedView addSubview:_selectedDescriptionText];
          _selectedView.frame = newFrame;
          _oldPlusButton.tag = 1;
        
          }
    
    else if(_oldPlusButton.tag == 1)

      {
          self.ODCollectionView.hidden = false;
          [self.oldPlusButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
          newFrame.size.height = 125;
          self.selectedDescriptionText.hidden =true;
          _selectedView.frame = newFrame;
          _oldPlusButton.tag = 0;
          
     
     }
}


- (IBAction)CategoryPressed:(id)sender {
    
    
    
    [self.categoryButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.genreButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self postdata2];
    
    subFilters=[[NSMutableArray alloc]init];
    
    subFilters = [_channelCategoryArray valueForKey:@"categorySubFilters"];
    NSDictionary *dict = _channelCategoryArray[0];
    NSMutableArray *array1 = [dict objectForKey: @"categorySubFilters"];
    NSMutableArray *array2 = [[NSMutableArray alloc]init];
    for (NSDictionary *dict1 in array1)
    {
        [array2 addObject:[dict1 objectForKey:@"filterName"]];
        
        
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:array2 forKey:@"categoryFilterName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    
    for (int i = 0; i < [array2 count]; i++)
    {
        [actionSheet  addButtonWithTitle:[array2 objectAtIndex:i]];
        
        NSLog(@"%@",[array2 objectAtIndex:i]);
        
    }
    [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    actionSheet.tag = 100;

    
}


- (IBAction)genrePressed:(id)sender {
    
    [self.categoryButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.genreButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.languageButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self postdata2];
    
    genresubFilters=[[NSMutableArray alloc]init];
    
    genresubFilters = [_channelCategoryArray valueForKey:@"genreSubFilters"];
    
    
    NSDictionary *dict1 = _channelCategoryArray[1];
    NSMutableArray *array3 = [dict1 objectForKey: @"genreSubFilters"];
    NSMutableArray *array4 = [[NSMutableArray alloc]init];
    for (NSDictionary *dict2 in array3)
    {
        [array4 addObject:[dict2 objectForKey:@"filterName"]];
    }
    [[NSUserDefaults standardUserDefaults] setObject:array4 forKey:@"genreFilterName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    
    for (int i = 0; i < [array4 count]; i++)
    {
        [actionSheet  addButtonWithTitle:[array4 objectAtIndex:i]];
        NSLog(@"%@",[array4 objectAtIndex:i]);
        
    }
    [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    
    actionSheet.tag = 200;
    posterSeleted = YES;
    
    
}

- (IBAction)languagePressed:(id)sender {
    
    [self.categoryButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.genreButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(102.0/255.0) blue:(102.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [self.languageButton setTitleColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0] forState:UIControlStateNormal];

    
    [self postdata2];
    
    langsubFilters=[[NSMutableArray alloc]init];
    
    langsubFilters = [_channelCategoryArray valueForKey:@"languageSubFilters"];
    
    
    NSDictionary *dict2= _channelCategoryArray[2];
    NSMutableArray *array5 = [dict2 objectForKey: @"languageSubFilters"];
    NSMutableArray *array6 = [[NSMutableArray alloc]init];
    for (NSDictionary *dict3 in array5)
    {
        [array6 addObject:[dict3 objectForKey:@"filterName"]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:array6 forKey:@"languageFilterName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIButton* button = (UIButton*)sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (int i = 0; i < [array6 count]; i++)
    {
        [actionSheet  addButtonWithTitle:[array6 objectAtIndex:i]];
  
    }
    
    
    
    [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];
    
    
    [actionSheet showFromRect:button.frame inView:self.view animated:YES];
    
    actionSheet.tag = 300;
    
posterSeleted = YES;
    
    
    
}
//- (IBAction)pinOkPressed:(id)sender {
//    self.PinView.hidden = YES;
//    
//    _AVPlayerController.view.hidden = NO;
//    [self.view endEditing:YES];
//    
//    @try {
//        [self.view endEditing:YES];
//        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
//        NSString *pinString = @"";
//        pinString = self.pinTextField.text;
//        self.pinTextField.text = @"";
//        
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/verifyPIN1",[[SprngIPConf getSharedInstance]getCurrentIpInUse]]];
//        
//        NSString *post = [NSString stringWithFormat:@"customerId=%@&PIN=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"],pinString];
//        //        NSLog(@"post %@",post);
//        
//        NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
//        
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//        [request setHTTPMethod:@"POST"];
//        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
//        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
//        [request setHTTPBody:postData];
//        
//        NSURLResponse *response;
//        NSError *error = nil;
//        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        //        NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
//        //        NSDictionary *json_dict = (NSDictionary *)json_string;
//        //        NSLog(@"json_dict\n%@",json_dict);
//        //        NSLog(@"json_string\n%@",json_string);
//        
//        //        NSLog(@"%@",response);
//        //        NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
//        NSLog(@" error is %@",error);
//        
//        if (!error) {
//            NSError *myError = nil;
//            NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
//            //            NSLog(@"%@",dictionary);
//            temp = [dictionary objectForKey:@"root"];
//            if (temp) {
//                NSDictionary *sDic = temp[0];
//                if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Not Found"]) {
//                    [self.view makeToast:@"Token Not Found" duration:2.0 position:CSToastPositionCenter style:style];
//                    self.PinView.hidden = NO;
//                    //         self.lastSeenTvImageView.hidden = NO;
//                    self.playPauseButton.hidden = NO;
//                    _AVPlayerController.view.hidden = YES;
//                    [self goToRootView];
//                }
//                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Token Expired"]) {
//                    [self.view makeToast:@"Token Expired" duration:2.0 position:CSToastPositionCenter style:style];
//                    self.PinView.hidden = NO;
//                    //          self.lastSeenTvImageView.hidden = NO;
//                    self.playPauseButton.hidden = NO;
//                    _AVPlayerController.view.hidden = YES;
//                    [self goToRootView];
//                }
//                else if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists"]){
//                    [self.view makeToast:@"Customer Does Not Exists" duration:2.0 position:CSToastPositionCenter style:style];
//                    [self goToRootView];
//                }
//                
//                
//                else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Not Verified" ]) {
//                    
//                    [self.view makeToast:@"Enter Correct PIN" duration:2.0 position:CSToastPositionCenter style:style];
//                    self.PinView.hidden = NO;
//                    //       self.lastSeenTvImageView.hidden = NO;
//                    self.playPauseButton.hidden = NO;
//                    _AVPlayerController.view.hidden = YES;
//                }
//                else if ([[sDic objectForKey:@"status"]isEqualToString:@"PIN Verified" ]) {
//                    
//                    //  [_player pause];
//                    
//                    //                    if ([defaultScreen  isEqual: @"LiveTV"]) {
//                    //                        //****** Getting the Update Channel Usage Details Of Chaneels *****//
//                    //                        //[self updateChannelUsageDetails];
//                    //                    }else if ([defaultScreen  isEqual: @"Radio"])
//                    //                    {
//                    //                        //****** Getting the Update Radio Usage Details Of Radio Chaneels*****//
//                    //                        //[self updateRadioUsageDetails];
//                    //                    }
//                    //
//                    
//                    // self.lastSeenTvImageView.hidden = YES;
//                    self.playPauseButton.hidden = YES;
//                    _AVPlayerController.view.hidden = NO;
//                    
//                    //[_player play];
//                    //****** Getting the Add Channel Usage Details Of chaneels *****//
//                    //[self addingChannelUsageDetails];
//                    
//                    _player = [AVPlayer playerWithURL:[NSURL URLWithString:lastSeenTVUrl]];
//                    _AVPlayerController = [[AVPlayerViewController alloc] init];
//                    _AVPlayerController.view.frame = self.tvView.bounds;
//                    [self addChildViewController:_AVPlayerController];
//                    [self.tvView addSubview:_AVPlayerController.view];
//                    _AVPlayerController.player = _player;
//                    _AVPlayerController.showsPlaybackControls = false;
//                    
//                    [_AVPlayerController.player setActionAtItemEnd:AVPlayerActionAtItemEndNone];
//                    [_AVPlayerController.view bringSubviewToFront:_tvView];
//                    
//                    _loader.hidden = YES;
//                    [_loader hidesWhenStopped];
//                    
//                    [_player play];
//                    
//                    
//                    
//                    //****** Getting the Add Channel Usage Details Of chaneels *****//
//                    //[self addingChannelUsageDetails];
//                    
//                }
//                
//                
//                
//                
//                else{
//                    NSLog(@"temp is getting Null");
//                }
//                
//                
//            }
//            else
//            {
//                NSLog(@"%@",error);
//            }
//            
//        }
//    }
//    @catch (NSException *exception) {
//        
//    }
//    @finally {
//        
//    }
//    
//
//}
//- (IBAction)pinCancelPressed:(id)sender {
//    
//    self.PinView.hidden = YES;
//    [self.view endEditing:YES];
//    [self.playPauseButton setImage:[UIImage imageNamed:@"HomePlayNew.png"] forState:UIControlStateNormal];
//    [self.playPauseButton setHidden:NO];
//    _AVPlayerController.view.hidden = YES;
//    _loader.hidden = YES;
//    [_loader hidesWhenStopped];
//}

- (IBAction)trailerButtonAction:(id)sender

{
    
}
#pragma mark goToRootView



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
