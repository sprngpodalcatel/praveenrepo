//
//  favEditTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 01/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface favEditTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *favEditLabel;


@end
