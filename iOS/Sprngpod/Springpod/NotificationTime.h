//
//  NotificationTime.h
//  Asianet Mobile TV Plus
//
//  Created by Shrishail Diggi on 19/08/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationTime : NSObject

-(NSString *)timeToString:(NSString *)date_rec format:(NSString *)format;

@end
