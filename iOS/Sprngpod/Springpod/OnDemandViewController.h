//
//  OnDemandViewController.h
//  Sprngpod
//
//  Created by XperioLabs on 19/05/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreData/CoreData.h>


@interface OnDemandViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,AVPlayerViewControllerDelegate,UIAlertViewDelegate>
{
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
    AVPlayer *AVPlayer;
    
}

-(void)setNavigationTitle:(NSString *)title;
-(void)timedOut;
-(void)reloadTableview;
-(void)dismissKeyboard;

//-(void)sessionTimeOut;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBar;
@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;
@property (weak, nonatomic) IBOutlet UIButton *expationButton;
@property (strong)NSManagedObject *imagedb;


@end
