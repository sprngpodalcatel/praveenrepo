//
//  FavpopUpViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 03/02/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavpopUpViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (nonatomic , strong) UIViewController *parentVC;

@end
