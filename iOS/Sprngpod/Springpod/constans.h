//
//  constans.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 06/02/17.
//  Copyright © 2017 Xperio Labs Limited. All rights reserved.
//

#ifndef constans_h
#define constans_h

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen]bounds].size.height == 667.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen]bounds].size.height == 736.0f)

#endif /* constans_h */
