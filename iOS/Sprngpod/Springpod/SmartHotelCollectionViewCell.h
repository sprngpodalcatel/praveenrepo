//
//  SmartHotelCollectionViewCell.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 18/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmartHotelCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *smrtHotelImageView;
@property (weak, nonatomic) IBOutlet UILabel *smrtHotelNameLbl;

@end
