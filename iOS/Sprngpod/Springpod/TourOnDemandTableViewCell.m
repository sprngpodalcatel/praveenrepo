//
//  TourOnDemandTableViewCell.m
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 13/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import "TourOnDemandTableViewCell.h"

@implementation TourOnDemandTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    if(selected) {
        
        [self.TourLibraryName setTextColor:[UIColor colorWithRed:(187.0/255.0) green:(29.0/255.0) blue:(17.0/255.0) alpha:1.0]];
        self.TourLibraryName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    }
    else{
        [self.TourLibraryName setTextColor:[UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]];
        self.TourLibraryName.font = [UIFont fontWithName:@"HelveticaNeue-Roman" size:14];
    }
}

@end
