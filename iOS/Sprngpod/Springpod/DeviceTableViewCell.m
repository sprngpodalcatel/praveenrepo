//
//  DeviceTableViewCell.m
//  Springpod
//
//  Created by Shrishail Diggi on 22/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "DeviceTableViewCell.h"

@implementation DeviceTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
