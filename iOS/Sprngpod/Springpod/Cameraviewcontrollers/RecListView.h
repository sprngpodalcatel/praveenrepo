//
//  RecListView.h
//  AnyCam
//
//  Created by roadjun on 21/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopTitleView.h"

@class RecListView;

@protocol RecListViewDelegate 

- (void)onRecListClick: (RecListView *)view fileName:(const char *)pszFileName handle:(void *)pHandle type:(int)iDevType;
- (void)onRecListBackClick: (RecListView *)view  handle:(void *)pHandle type:(int)iDevType;

@end

@interface RecListView : UIView<TopTitleViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet id <RecListViewDelegate> delegate;

- (void)InitView;

- (void)SetTitleName:(const char *)pszTitleName;

- (void)GetRecordList: (int)isRecordFile handle:(void *)pHandle type:(int)iDevType;

- (int)IsRecordFile;

- (void)onBackClick: (TopTitleView *)view;

- (void)AddRecFile: (const char *)pszFileName;
- (void)ClearAll;

@end
