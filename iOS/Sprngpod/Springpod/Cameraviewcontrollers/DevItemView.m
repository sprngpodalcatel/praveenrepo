//
//  DevItemView.m
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "DevItemView.h"

@interface DevItemView ()
{
    UIImageView *mItemView;
    UIImageView *mDevStatus;
    UILabel *mDevName;
    UIImageView *mDevDel;
}

@property (nonatomic, retain) UIImageView *mItemView;
@property (nonatomic, retain) UIImageView *mDevStatus;
@property (nonatomic, retain) UILabel *mDevName;
@property (nonatomic, retain) UIImageView *mDevDel;

@end

@implementation DevItemView
@synthesize mItemView;
@synthesize mDevStatus;
@synthesize mDevName;
@synthesize mDevDel;

- (void)StatusClick:(UIGestureRecognizer *)gestureRecognizer {
    [self.delegate onClick:self];
    //NSLog(@"StatusClick()");
}

- (void)ItemClick:(UIGestureRecognizer *)gestureRecognizer {
    [self.delegate onClick:self];
    //NSLog(@"ItemClick()");
}

- (void)DelClick:(UIGestureRecognizer *)gestureRecognizer {
    [self.delegate onDelClick:self];
    //NSLog(@"DelClick()");
}

- (void)InitView {
    CGFloat width, height;
    CGRect bounds = self.frame;
    
    width = bounds.size.width;
    height = bounds.size.height;
    self.mItemView = [[UIImageView alloc] init];
    self.mItemView.frame = CGRectMake(0, 0, width, height);
    [self addSubview: self.mItemView];
    [self.mItemView setBackgroundColor:[UIColor whiteColor]];
    [self.mItemView setUserInteractionEnabled: YES];
    [self.mItemView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ItemClick:)]];
    
    width = 50; height = 50;
    self.mDevStatus = [[UIImageView alloc] init];
    self.mDevStatus.frame = CGRectMake(0, 0, width, height);
    [self addSubview: self.mDevStatus];
    [self.mDevStatus setUserInteractionEnabled: YES];
    [self.mDevStatus addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StatusClick:)]];
    
    width = bounds.size.width - 100;
    self.mDevName = [[UILabel alloc] init];
    self.mDevName.frame = CGRectMake(50, 0, width, height);
    [self addSubview: self.mDevName];
    self.mDevName.textAlignment = NSTextAlignmentCenter;
    
    width = 50;
    self.mDevDel = [[UIImageView alloc] init];
    self.mDevDel.frame = CGRectMake(bounds.size.width-50, 0, width, height);
    [self addSubview: self.mDevDel];
    [self.mDevDel setImage:[UIImage imageNamed:@"cam_del"]];
    [self.mDevDel setUserInteractionEnabled: YES];
    [self.mDevDel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DelClick:)]];
    
    self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
}

- (void)dealloc
{
    [self.mItemView release];
    [self.mDevStatus release];
    [self.mDevName release];
    [self.mDevDel release];
    
    [super dealloc];
}

- (void)SetDevName: (const char *)pszDevName {
    [self.mDevName setText: [NSString stringWithUTF8String:pszDevName]];
}

- (void)UpdateDevStatus: (int)isOnline {
    UIImage *image;
    if (isOnline == 1) {
        image = [UIImage imageNamed:@"online"];
    } else {
        image = [UIImage imageNamed:@"offline"];
    }
    [self.mDevStatus setImage: image];
}

@end
