//
//  RecParamView.m
//  AnyCam
//
//  Created by roadjun on 21/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "RecParamView.h"
#import "livecam.h"
#import "defs.h"
#include "DeviceStruct.h"

@interface RecParamView ()
{
    TopTitleView *mTopTitleView;
    UIImageView *mItemView;
    UILabel     *mEnableLabel;
    UIButton    *mCheckButton;
    UIButton    *mConfirmButton;
    
    void        *m_pDevHandle;
    int         m_iDevType;
    char        *m_pszRecBuffer;
}

@property (nonatomic, retain) TopTitleView *mTopTitleView;
@property (nonatomic, retain) UIImageView *mItemView;
@property (nonatomic, retain) UILabel *mEnableLabel;
@property (nonatomic, retain) UIButton *mCheckButton;
@property (nonatomic, retain) UIButton *mConfirmButton;

@end


@implementation RecParamView
@synthesize mTopTitleView;
@synthesize mItemView;
@synthesize mEnableLabel;
@synthesize mCheckButton;
@synthesize mConfirmButton;

- (void)checkButtonClick:(UIButton *)button {
    button.selected = !button.selected;
}

- (void)confirmButtonClick {
    int i, j, ret = -1;
    PRECORDPARAM pstRecParam;
    
    if (!m_pszRecBuffer)
    {
        return;
    }
    
    pstRecParam = (PRECORDPARAM)m_pszRecBuffer;
    if (self.mCheckButton.selected) {
        pstRecParam->byEnable = 1;
        for (i=0; i<7; i++) {
            for (j=0; j<4; j++) {
                pstRecParam->stPlanTime[i][j].byEnable = 1;
            }
        }
        NSLog(@"confirmButtonClick() has checked");
    } else {
        pstRecParam->byEnable = 0;
        for (i=0; i<7; i++) {
            for (j=0; j<4; j++) {
                pstRecParam->stPlanTime[i][j].byEnable = 0;
            }
        }
        NSLog(@"confirmButtonClick() has not checked");
    }
    
    if (m_iDevType == DEV_TYPE_TCP) {
        ret = SetRecordParam(m_pDevHandle, 0, m_pszRecBuffer, sizeof(RECORDPARAM));
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        ret = P2P_SetRecordParam(m_pDevHandle, 0, m_pszRecBuffer, sizeof(RECORDPARAM));
    } else if (m_iDevType == DEV_TYPE_TUTK_P2P) {
        
    }
    
    if (!ret) {
        NSLog(@"set param is ok\n");
    } else {
        NSLog(@"set param is failure\n");
    }
        
}

- (void)InitView {
    CGFloat width, height, offset;
    CGRect bounds = [UIScreen mainScreen].bounds;
    CGRect stsRect = [[UIApplication sharedApplication] statusBarFrame];
    
    width = bounds.size.width;
    height = 150;
    self.mItemView = [[UIImageView alloc] init];
    self.mItemView.frame = CGRectMake(0, 40, width, height);
    [self addSubview: self.mItemView];
    [self.mItemView setBackgroundColor:[UIColor whiteColor]];
    self.mItemView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    self.mItemView.layer.cornerRadius = 8;
    self.mItemView.layer.masksToBounds = YES;
    
    offset = stsRect.size.height;
    width = bounds.size.width;
    height = 50;
    
    self.mTopTitleView = [[TopTitleView alloc]init];
    self.mTopTitleView.frame = CGRectMake(0, offset, width, 40);
    [self.mTopTitleView InitView:TRUE];
    [self.mTopTitleView SetTitle:"Record Parameter"];
    [self addSubview: self.mTopTitleView];
    self.mTopTitleView.delegate = self;
    
    offset += 50;
    self.mEnableLabel = [[UILabel alloc]init];
    self.mEnableLabel.frame = CGRectMake(0, offset, 100, height);
    [self addSubview: self.mEnableLabel];
    self.mEnableLabel.textAlignment = NSTextAlignmentLeft;
    self.mEnableLabel.text = @"Enable";
    
    self.mCheckButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.mCheckButton.frame = CGRectMake(100, offset, 36, 36);
    [self.mCheckButton setImage: [UIImage imageNamed:@"check_off"] forState:UIControlStateNormal];
    [self.mCheckButton setImage: [UIImage imageNamed:@"check"] forState:UIControlStateSelected];
    [self.mCheckButton addTarget:self action:@selector(checkButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview: self.mCheckButton];
    
    offset += 50;
    self.mConfirmButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.mConfirmButton.backgroundColor = [UIColor redColor];
    [self.mConfirmButton setTitle:@"OK" forState:UIControlStateNormal];
    self.mConfirmButton.frame = CGRectMake(0, offset, width, 40.0);
    [self.mConfirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.mConfirmButton];
    
    [self setBackgroundColor:[UIColor lightGrayColor]];
    
    self.mCheckButton.selected = TRUE;
    
    m_pszRecBuffer = (char *)malloc(sizeof(RECORDPARAM));
}

- (void)dealloc {
    if (m_pszRecBuffer) {
        free(m_pszRecBuffer);
        m_pszRecBuffer = NULL;
    }
    [self.mTopTitleView release];
    [self.mItemView release];
    [self.mEnableLabel release];
    [self.mCheckButton release];
    [self.mConfirmButton release];
    
    [super dealloc];
}

- (void)UpdateParam: (void *)handle type:(int)iDevType {
    int ret = -1;
    PRECORDPARAM pstRecParam;
    
    m_pDevHandle = handle;
    m_iDevType = iDevType;
    
    if (!m_pszRecBuffer)
    {
        return;
    }
    
    if (m_iDevType == DEV_TYPE_TCP) {
        ret = GetRecordParam(m_pDevHandle, 0, m_pszRecBuffer, sizeof(RECORDPARAM));
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        ret = P2P_GetRecordParam(m_pDevHandle, 0, m_pszRecBuffer, sizeof(RECORDPARAM));
    } else if (m_iDevType == DEV_TYPE_TUTK_P2P) {
        
    }
    
    if (!ret) {
        pstRecParam = (PRECORDPARAM)m_pszRecBuffer;
        NSLog(@"record status %d\n", (int)pstRecParam->byEnable);
        if (pstRecParam->byEnable) {
            self.mCheckButton.selected = TRUE;
        } else {
            self.mCheckButton.selected = FALSE;
        }
    }
}

- (void)onBackClick: (TopTitleView *)view {
    [self.delegate onRecParamClick:self handle:m_pDevHandle type:m_iDevType];
    NSLog(@"RecParamView onBackClick");
}

@end
