//
//  PlaybackView.h
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OpenGLView20;
@class PlaybackView;

@protocol PlaybackViewDelegate
- (void)onPlayBackClick:(PlaybackView *)view handle:(void *)pHandle type:(int)iDevType;
@end

@interface PlaybackView : UIView
{
    UIButton *mBackButton;
    
    void *m_pDevHandle;
    int m_iDevType;
    
    void *m_pDecHandle;
    void *m_pADecHandle;
    
    char *m_pszYUVBuffer;
    char *m_pszPFVideoData;
    
    char *m_pszPFAudioData;
    char *m_pszDecAudioData;
    
    unsigned int m_dwPFVideoLen;
    unsigned int m_dwPFAudioLen;
    
    void *m_pAudioOut;
}

@property (nonatomic, retain) OpenGLView20 *mGLView20;
@property (nonatomic, retain) IBOutlet id <PlaybackViewDelegate> delegate;

+ (instancetype)playbackView;

- (void)InitView;

- (void)ChangeViewSize: (BOOL)bIsLandScape;

- (int)InitService;
- (int)DestroyService;

- (int)playbackStart: (void *)devHandle devType:(int)iDevType fileName:(const char *)pszFileName;
- (int)playbackStop;

-(int) PFProcData: (void *) handle mediaData:(const char *)pszData frameLen:(unsigned int)dwFrameLen mediahead:(const char *)pHead isVideo:(int)_isVideo;

@end
