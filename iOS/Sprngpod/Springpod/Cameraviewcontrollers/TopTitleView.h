//
//  TopTitleView.h
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TopTitleView;

@protocol TopTitleViewDelegate
- (void)onBackClick: (TopTitleView *)view;
@end

@interface TopTitleView : UIView

@property (nonatomic, retain) IBOutlet id <TopTitleViewDelegate> delegate;

- (void)InitView: (BOOL)hasBackButton;

- (void)SetTitle: (const char *)pszTitle;

@end
