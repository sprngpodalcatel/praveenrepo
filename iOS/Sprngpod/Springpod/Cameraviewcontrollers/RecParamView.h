//
//  RecParamView.h
//  AnyCam
//
//  Created by roadjun on 21/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopTitleView.h"

@class RecParamView;

@protocol RecParamViewDelegate
- (void)onRecParamClick: (RecParamView *)view handle:(void *)pHandle type:(int)iDevType;
@end

@interface RecParamView : UIView<TopTitleViewDelegate>

@property (nonatomic, retain) IBOutlet id <RecParamViewDelegate> delegate;

- (void)InitView;

- (void)UpdateParam: (void *)handle type:(int)iDevType;

- (void)onBackClick: (TopTitleView *)view;

@end
