//
//  SetItemView.h
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SetItemView;

@protocol SetItemViewDelegate
- (void)onClick: (SetItemView *)view;
@end

@interface SetItemView : UIView

@property (nonatomic, retain) IBOutlet id <SetItemViewDelegate> delegate;

- (void)InitView:(const char *)pszItemName;

@end
