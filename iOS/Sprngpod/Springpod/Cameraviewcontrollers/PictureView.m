//
//  PictureView.m
//  AnyCam
//
//  Created by roadjun on 22/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "PictureView.h"

#include "livecam.h"
#include "defs.h"
#include "DeviceStruct.h"
#import <pthread.h>

@interface PictureView ()
{
    TopTitleView    *mTopTitleView;
    UIImageView     *mImageView;
    
    void            *m_pDevHandle;
    int             m_iDevType;
    
    char            m_szFileName[256];
    char            m_szLocalFileName[256];
}

@property (nonatomic, retain) TopTitleView *mTopTitleView;
@property (nonatomic, retain) UIImageView *mImageView;

@end


@implementation PictureView
@synthesize mTopTitleView;
@synthesize mImageView;

- (void)InitView {
    CGRect frame;
    CGFloat width, height, offset;
    CGRect bounds = [UIScreen mainScreen].bounds;
    CGRect stsRect = [[UIApplication sharedApplication] statusBarFrame];
    
    offset = stsRect.size.height;
    width = bounds.size.width;
    
    self.mTopTitleView = [[TopTitleView alloc]init];
    self.mTopTitleView.frame = CGRectMake(0, offset, width, 40);
    [self.mTopTitleView InitView:TRUE];
    [self.mTopTitleView SetTitle:"Record List"];
    [self addSubview: self.mTopTitleView];
    self.mTopTitleView.delegate = self;
    
    offset += 40; height = bounds.size.height - offset;
    frame = CGRectMake(0, offset, width, height);
    self.mImageView = [[UIImageView alloc] init];
    self.mImageView.frame = frame;
    [self addSubview: self.mImageView];
    
    //[self setBackgroundColor:[UIColor blackColor]];
}

- (void)dealloc {
    [self.mTopTitleView release];
    [self.mImageView release];
    
    [super dealloc];
}

- (void)StartDowload {
    if (m_iDevType == DEV_TYPE_TCP) {
        
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        P2P_StartDowloadFile(m_pDevHandle, 2, m_szFileName, m_szLocalFileName);
    }
}

static void *DownloadThread(void *pContext)
{
    PictureView *poView = (PictureView *)pContext;
    if (poView)
    {
        [poView StartDowload];
    }
    
    return NULL;
}

- (void)GetPicture: (const char *)pszFileName handle:(void *)pHandle type:(int)iDevType {
    pthread_t threadID;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    //NSString *cachesDir = NSHomeDirectory();
    strcpy(m_szLocalFileName, [cachesDir cString]);
    //strcpy(m_szLocalFileName, [cachesDir cStringUsingEncoding:NSUnicodeStringEncoding]);
    strcat(m_szLocalFileName, "/");
    strcat(m_szLocalFileName, pszFileName);
    strcpy(m_szFileName, pszFileName);
    
    m_pDevHandle = pHandle;
    m_iDevType = iDevType;
    
    pthread_create(&threadID, NULL, DownloadThread, self);
}

- (void)onBackClick: (TopTitleView *)view {
    P2P_StopDownloadFile(m_pDevHandle);
    [self.delegate onPictureBackClick: self handle:m_pDevHandle type:m_iDevType];
    NSLog(@"PictureView onBackClick");
}

- (void)UpdateUI {
    UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:m_szLocalFileName]];
    
    [self.mImageView setImage:img];
    //[self.mImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:m_szLocalFileName]]];
    NSLog(@"PictureView UpdateUI %p", img);
}

- (void)DownloadNotify: (int)iPercent {
    NSLog(@"has downloaded %d\n", iPercent);
    if (iPercent == 100) {
        [self performSelectorOnMainThread:@selector(UpdateUI) withObject:nil waitUntilDone:YES];
    }
}


@end
