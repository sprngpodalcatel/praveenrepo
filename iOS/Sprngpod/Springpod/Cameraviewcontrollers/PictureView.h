//
//  PictureView.h
//  AnyCam
//
//  Created by roadjun on 22/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopTitleView.h"

@class PictureView;

@protocol PictureViewDelegate

- (void)onPictureBackClick: (PictureView *)view  handle:(void *)pHandle type:(int)iDevType;

@end

@interface PictureView : UIView<TopTitleViewDelegate>

@property (nonatomic, retain) IBOutlet id <PictureViewDelegate> delegate;

- (void)InitView;

- (void)GetPicture: (const char *)pszFileName handle:(void *)pHandle type:(int)iDevType;

- (void)onBackClick: (TopTitleView *)view;

- (void)DownloadNotify: (int)iPercent;

@end
