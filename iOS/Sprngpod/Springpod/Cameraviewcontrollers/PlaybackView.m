//
//  PlaybackView.m
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "PlaybackView.h"
#include "h264dec.h"
#include "auddec.h"
#include "livecam.h"
#include "audio.h"
#import "OpenGLView20.h"

#include "defs.h"
#include "DeviceStruct.h"

@implementation PlaybackView
@synthesize mGLView20;

static int cb_PFProcData(void *handle, const char *pszData, unsigned int dwFrameLen, const char *pHead, int _isVideo, void *pContext)
{
    PlaybackView *poView = (PlaybackView *)pContext;
    //printf("isvideo %d framelen %d\n", _isVideo, dwFrameLen);
    if (poView)
    {
        [poView PFProcData:handle mediaData:pszData frameLen:dwFrameLen mediahead:pHead isVideo:_isVideo];
    }
    
    return 0;
}

+ (instancetype)playbackView {
    NSBundle *rootBundle = [NSBundle mainBundle];
    return [[rootBundle loadNibNamed:@"PlayBack" owner:nil options:nil] lastObject];
}

- (void)BackButtonClick {
    [self playbackStop];
    [self.delegate onPlayBackClick:self handle:m_pDevHandle type:m_iDevType];
}

- (void)InitView {
    CGRect frame = CGRectMake(0.0, 10.0, 40.0, 40.0);
    mBackButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    mBackButton.backgroundColor = [UIColor clearColor];
    [mBackButton setTitle:@"back" forState:UIControlStateNormal];
    mBackButton.frame = frame;
    [mBackButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [mBackButton addTarget:self action:@selector(BackButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:mBackButton];
    
    mBackButton.hidden = YES;
    
    CGFloat width, height;
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    width = 16; height = 9;
    if (bounds.size.width < bounds.size.height)
    {
        frame.size.width = bounds.size.width;
        frame.size.height = bounds.size.width * height / width;
        frame.origin.x = 0;
        frame.origin.y = (bounds.size.height - frame.size.height) /  2;
    }
    else
    {
        frame.size.width = bounds.size.height * width / height;
        frame.size.height = bounds.size.height;
        frame.origin.x = (bounds.size.width - frame.size.width) / 2;
        frame.origin.y = 0;
    }
    
    mGLView20 = [[OpenGLView20 alloc]initWithFrame:frame];
    [mGLView20 setVideoSize:1920 height:1080];
    [self addSubview: mGLView20];
    
    [self InitService];
    
    [self setBackgroundColor:[UIColor blackColor]];
}

- (void)ChangeViewSize: (BOOL)bIsLandScape {
    CGRect frame;
    CGFloat width, height, w, h;
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    width = 16; height = 9;
    if (bounds.size.width < bounds.size.height)
    {
        w = bounds.size.height; h = bounds.size.width;
    }
    else
    {
        w = bounds.size.width; h = bounds.size.height;
    }
    
    if (FALSE == bIsLandScape)
    {
        frame.size.width = h;
        frame.size.height = h * height / width;
        frame.origin.x = 0;
        frame.origin.y = (w - frame.size.height) /  2;
    }
    else
    {
        frame.size.width = h * width / height;
        frame.size.height = h;
        frame.origin.x = (w - frame.size.width) / 2;
        frame.origin.y = 0;
    }
    
    NSLog(@"screen(%.2f %.2f)x %.2f y %.2f w %.2f h %.2f", bounds.size.width, bounds.size.height, frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    
    mGLView20.frame = frame;
}

- (void)dealloc {
    [self DestroyService];
    [mBackButton release];
    [mGLView20 release];
    [super dealloc];
}

- (int)InitService {
    
    m_pDecHandle = InitDecoder(1920, 1080);
    m_pADecHandle = InitAudioDecoder();
    
    m_pszYUVBuffer = malloc(1920 * 1080 * 2);
    m_pszPFVideoData = malloc(800 * 1024);
    m_pszPFAudioData = malloc(10 * 1024);
    m_pszDecAudioData = malloc(10 * 1024);
    m_dwPFVideoLen = 0;
    m_dwPFAudioLen = 0;
    
    m_pAudioOut = InitAudioPlayer();
    StartAudioPlayer(m_pAudioOut);
    
    return 0;
}

- (int)DestroyService {
    
    UninitDecoder(m_pDecHandle);
    UninitAudioDecoder(m_pADecHandle);
    
    free(m_pszYUVBuffer);
    m_pszYUVBuffer = NULL;
    free(m_pszPFVideoData);
    m_pszPFVideoData = NULL;
    free(m_pszPFAudioData);
    m_pszPFAudioData = NULL;
    free(m_pszDecAudioData);
    m_pszDecAudioData = NULL;
    
    StopAudioPlayer(m_pAudioOut);
    DestroyAudioPlayer(m_pAudioOut);
    
    return 0;
}

- (int)playbackStart: (void *)devHandle devType:(int)iDevType fileName:(const char *)pszFileName {
    char *pos, szFileName[40];
    
    if (!pszFileName) {
        return -1;
    }
    
    strcpy(szFileName, pszFileName);
    pos = strstr(szFileName, ".flv");
    if (pos)
    {
        *pos = 0;
        strcat(szFileName, ".ivd");
    }
    
    m_pDevHandle = devHandle;
    m_iDevType = iDevType;
    
    //StartAudioPlayer(m_pAudioOut);
    
    if (m_iDevType == DEV_TYPE_TCP) {
        RegisterPlayDataFunc(m_pDevHandle, cb_PFProcData, self);
        
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        P2P_RegisterPlayDataFunc(m_pDevHandle, cb_PFProcData, self);
        P2P_Playback(m_pDevHandle, 0, 0, szFileName);
    } else if (m_iDevType == DEV_TYPE_TUTK_P2P) {
        
    }
    
    return 0;
}

- (int)playbackStop {
    //StopAudioPlayer(m_pAudioOut);
    if (m_iDevType == DEV_TYPE_TCP) {
        RegisterProcDataFunc(m_pDevHandle, NULL, NULL);
        
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        P2P_RegisterProcDataFunc(m_pDevHandle, NULL, NULL);
        P2P_StopPlayback(m_pDevHandle);
    } else if (m_iDevType == DEV_TYPE_TUTK_P2P) {
        
    }
    
    
    return 0;
}

#pragma mark - Touch handling methods

- (void)myTouch:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInView:touch.view];
        NSLog(@"cur postion %.2f %.2f", location.x, location.y);
        
        mBackButton.hidden = !mBackButton.hidden;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self myTouch:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self myTouch:touches withEvent:event];
}

- (void)UpdateUI {
    [self.mGLView20 displayYUV420pData: m_pszYUVBuffer width:GetWidth(m_pDecHandle) height:GetHeight(m_pDecHandle)];
}

-(int) PFProcData: (void *) handle mediaData:(const char *)pszData frameLen:(unsigned int)dwFrameLen mediahead:(const char *)pHead isVideo:(int)_isVideo {
    
    int ret;
    PVIDEODATAHEAD pstVideoHead;
    PAUDIODATAHEAD pstAudioHead;
    
    if (_isVideo == 1) {
        pstVideoHead = (PVIDEODATAHEAD)pHead;
        if (pstVideoHead->byPackIndex == 0) {
            m_dwPFVideoLen = 0;
        }
        memcpy(m_pszPFVideoData+m_dwPFVideoLen, pszData, pstVideoHead->nDataLength);
        m_dwPFVideoLen += pstVideoHead->nDataLength;
        if (m_dwPFVideoLen == pstVideoHead->dwFrameLength)
        {
            printf("video frameType %d frameID %d frameLength %d\n", pstVideoHead->byFrameType, pstVideoHead->dwFrameID, pstVideoHead->dwFrameLength);
            ret = DecoderNal2(m_pDecHandle, m_pszPFVideoData, m_dwPFVideoLen, m_pszYUVBuffer);
            NSLog(@"decode one frame %d, ret %d.\n", m_dwPFVideoLen, ret);
            if (ret > 0)
            {
                [self performSelectorOnMainThread:@selector(UpdateUI) withObject:nil waitUntilDone:YES];
            }
        }
    } else {
        pstAudioHead = (PAUDIODATAHEAD)pHead;
        if (pstAudioHead->byPackIndex == 0) {
            m_dwPFAudioLen = 0;
        }
        memcpy(m_pszPFAudioData+m_dwPFAudioLen, pszData, pstAudioHead->nDataLength);
        m_dwPFAudioLen += pstAudioHead->nDataLength;
        if (m_dwPFAudioLen == pstAudioHead->dwFrameLength)
        {
            printf("audio frameID %d frameLength %d\n", pstAudioHead->dwFrameID, pstAudioHead->dwFrameLength);
            ret = DecodeAudio(m_pADecHandle, (const char *)m_pszPFAudioData, (int)m_dwPFAudioLen, (char *)m_pszDecAudioData);
            if (ret > 0)
            {
                PlayAudioData(m_pAudioOut, (unsigned char *)m_pszDecAudioData, ret);
            }
        }
    }
    return 0;
}

@end
