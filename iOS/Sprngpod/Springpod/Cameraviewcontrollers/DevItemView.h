//
//  DevItemView.h
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DevItemView;

@protocol DevItemViewDelegate
- (void)onClick: (DevItemView *)view;
- (void)onDelClick: (DevItemView *)view;
@end

@interface DevItemView : UIView

@property (nonatomic, retain) IBOutlet id <DevItemViewDelegate> delegate;

- (void)InitView;

- (void)SetDevName: (const char *)pszDevName;
- (void)UpdateDevStatus: (int)isOnline;

@end
