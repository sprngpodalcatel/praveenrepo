//
//  SetItemView.m
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "SetItemView.h"

@interface SetItemView ()
{
    UIImageView *mItemView;
    UILabel *mItemNameLabel;
}

@property (nonatomic, retain) UIImageView *mItemView;
@property (nonatomic, retain) UILabel *mItemNameLabel;

@end


@implementation SetItemView
@synthesize mItemView;
@synthesize mItemNameLabel;

- (void)ItemClick:(UIGestureRecognizer *)gestureRecognizer {
    [self.delegate onClick:self];
    //NSLog(@"ItemClick()");
}

- (void)InitView:(const char *)pszItemName {
    CGFloat width, height;
    CGRect bounds = self.frame;
    
    width = bounds.size.width;
    height = bounds.size.height;
    self.mItemView = [[UIImageView alloc] init];
    self.mItemView.frame = CGRectMake(0, 0, width, height);
    [self addSubview: self.mItemView];
    [self.mItemView setBackgroundColor:[UIColor whiteColor]];
    [self.mItemView setUserInteractionEnabled: YES];
    [self.mItemView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ItemClick:)]];
    
    self.mItemNameLabel = [[UILabel alloc] init];
    self.mItemNameLabel.frame = CGRectMake(0, 0, width, height);
    [self addSubview: self.mItemNameLabel];
    self.mItemNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.mItemNameLabel setText: [NSString stringWithUTF8String:pszItemName]];
    
    self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
}

- (void)dealloc {
    [self.mItemView release];
    [self.mItemNameLabel release];
    
    [super dealloc];
}


@end
