//
//  RecListView.m
//  AnyCam
//
//  Created by roadjun on 21/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "RecListView.h"
#include "livecam.h"
#include "defs.h"
#include "DeviceStruct.h"

@interface RecListView ()
{
    TopTitleView    *mTopTitleView;
    UITableView     *mTableView;
    NSArray         *mRecordArray;
    NSMutableArray  *mArrayValue;
    
    void            *m_pDevHandle;
    int             m_iDevType;
    int             m_bIsRecordFile;
}

@property (nonatomic, retain) TopTitleView *mTopTitleView;
@property (nonatomic, retain) UITableView *mTableView;
@property (nonatomic, retain) NSArray *mRecordArray;
@property (nonatomic, retain) NSMutableArray *mArrayValue;
@end

@implementation RecListView
@synthesize mTopTitleView;
@synthesize mTableView;
@synthesize mRecordArray;
@synthesize mArrayValue;

- (void)InitView {
    CGRect frame;
    CGFloat width, height, offset;
    CGRect bounds = [UIScreen mainScreen].bounds;
    CGRect stsRect = [[UIApplication sharedApplication] statusBarFrame];
    
    offset = stsRect.size.height;
    width = bounds.size.width;
    height = 50;
    
    //NSLog(@"--------width %.2f\n", width);
    
    self.mTopTitleView = [[TopTitleView alloc]init];
    self.mTopTitleView.frame = CGRectMake(0, offset, width, 40);
    [self.mTopTitleView InitView:TRUE];
    [self.mTopTitleView SetTitle:"Record List"];
    [self addSubview: self.mTopTitleView];
    self.mTopTitleView.delegate = self;
    
    offset += 40; height = bounds.size.height - offset;
    frame = CGRectMake(0, offset, width, height);
    //self.mTableView = [[UITableView alloc]init];
    //self.mTableView.frame = frame;
    self.mTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    [self addSubview: self.mTableView];
    
    self.mArrayValue = [[NSMutableArray alloc]init];
    [self.mArrayValue removeAllObjects];
    self.mRecordArray = self.mArrayValue;
    
    [self setBackgroundColor:[UIColor blueColor]];
}

- (void)dealloc {
    [self.mTopTitleView release];
    [self.mTableView release];
    if (self.mRecordArray != nil)
    {
        [self.mRecordArray release];
        self.mRecordArray = nil;
    }
    
    [super dealloc];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;//self.mRecordArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mRecordArray.count;
    
    
    NSLog(@"Rec List Array Value :-  %lu",self.mRecordArray.count);

    NSLog(@"Rec List Array Value :-  %lu",self.mRecordArray.count);
    NSLog(@"Rec List Array Value :-  %lu",self.mRecordArray.count);
    NSLog(@"Rec List Array Value :-  %lu",self.mRecordArray.count);
    
    NSLog(@"Rec List Array Value :-  %lu",self.mRecordArray.count);
    
    NSLog(@"Rec List Array Value :-  %lu",self.mRecordArray.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellWithIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellWithIndentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellWithIndentifier];
    }
    NSInteger row = [indexPath row];
    //cell.textLabel.text = [self.mRecordArray objectAtIndex:row];
    cell.detailTextLabel.text = [self.mRecordArray objectAtIndex:row];
    //cell.imageView.image = [UIImage imageNamed:@""];
    //cell.detailTextLabel.text = @"describle";
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    NSString *str = [self.mRecordArray objectAtIndex:row];
    [self.delegate onRecListClick:self fileName:[str cString] handle:m_pDevHandle type:m_iDevType];
    //NSLog(@"%d %@ %s\n", row, str, [str cString]);
}

- (void)SetTitleName:(const char *)pszTitleName {
    [self.mTopTitleView SetTitle:pszTitleName];
}

- (void)GetRecordList: (int)isRecordFile handle:(void *)pHandle type:(int)iDevType {
    m_pDevHandle = pHandle;
    m_iDevType = iDevType;
    m_bIsRecordFile = isRecordFile;
    
    if (m_iDevType == DEV_TYPE_TCP) {
        
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        if (!m_bIsRecordFile) {
            P2P_GetSnapFileList(m_pDevHandle, 0, 0);
        } else {
            P2P_GetRecFileList(m_pDevHandle, 0, 0);
        }
    }
}

- (int)IsRecordFile {
    return m_bIsRecordFile;
}

- (void)onBackClick: (TopTitleView *)view {
    [self.delegate onRecListBackClick:self handle:m_pDevHandle type:m_iDevType];
    
    NSLog(@"RecListView onBackClick");
}

- (void)UpdateUI {
    [self.mTableView reloadData];
}

- (void)AddRecFile: (const char *)pszFileName {
    [self.mArrayValue addObject:[NSString stringWithUTF8String:pszFileName]];
    self.mRecordArray = self.mArrayValue;
    [self performSelectorOnMainThread:@selector(UpdateUI) withObject:nil waitUntilDone:YES];
}

- (void)ClearAll {
    [self.mArrayValue removeAllObjects];
    self.mRecordArray = self.mArrayValue;
    [self.mTableView reloadData];
}

@end
