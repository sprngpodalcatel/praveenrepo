//
//  TopTitleView.m
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "TopTitleView.h"

@interface TopTitleView ()
{
    UIImageView *mItemView;
    UIImageView *mBackView;
    UILabel *mTitleLabel;
}

@property (nonatomic, retain) UIImageView *mItemView;
@property (nonatomic, retain) UIImageView *mBackView;
@property (nonatomic, retain) UILabel *mTitleLabel;

@end

@implementation TopTitleView
@synthesize mItemView;
@synthesize mBackView;
@synthesize mTitleLabel;

- (void)BackImageClick:(UIGestureRecognizer *)gestureRecognizer {
    [self.delegate onBackClick:self];
    NSLog(@"BackImageClick()");
}

- (void)InitView: (BOOL)hasBackButton {
    CGFloat width, height, offset;
    CGRect bounds = self.frame;
    
    width = bounds.size.width;
    height = bounds.size.height;
    self.mItemView = [[UIImageView alloc] init];
    self.mItemView.frame = CGRectMake(0, 0, width, height);
    [self addSubview: self.mItemView];
    [self.mItemView setImage: [UIImage imageNamed:@"toptitle"]];
    
    offset = 0;
    if (hasBackButton) {
        height -= 2; width = height; offset = width + 2;
        self.mBackView = [[UIImageView alloc] init];
        self.mBackView.frame = CGRectMake(1, 1, width, height);
        [self addSubview: self.mBackView];
        [self.mBackView setImage: [UIImage imageNamed:@"back"]];
        [self.mBackView setUserInteractionEnabled: YES];
        [self.mBackView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackImageClick:)]];
    }
    
    height = bounds.size.height;
    width = bounds.size.width - offset;
    self.mTitleLabel = [[UILabel alloc] init];
    self.mTitleLabel.frame = CGRectMake(offset, 0, width, height);
    [self addSubview: self.mTitleLabel];
    self.mTitleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)dealloc {
    [self.mItemView release];
    [self.mBackView release];
    [self.mTitleLabel release];
    
    [super dealloc];
}

- (void)SetTitle: (const char *)pszTitle {
    [self.mTitleLabel setText: [NSString stringWithUTF8String:pszTitle]];
}

@end
