//
//  SettingsView.h
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TopTitleView.h"
#import "SetItemView.h"

@class SettingsView;

@protocol SettingsViewDelegate
- (void)onClick: (SettingsView *)view index:(int)_index handle:(void *)pHandle type:(int)iDevType;
@end

@interface SettingsView : UIView <TopTitleViewDelegate, SetItemViewDelegate>

@property (nonatomic, retain) IBOutlet id <SettingsViewDelegate> delegate;

- (void)InitView;

- (int)SetDevice: (void *)handle type:(int)iDevType;

- (void)onBackClick: (TopTitleView *)view;
- (void)onClick: (SetItemView *)view;

@end
