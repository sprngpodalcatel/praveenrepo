//
//  SettingsView.m
//  AnyCam
//
//  Created by roadjun on 20/09/2017.
//  Copyright © 2017 roadjun. All rights reserved.
//

#import "SettingsView.h"
#import "TopTitleView.h"
#import "SetItemView.h"
#include "livecam.h"
#include "defs.h"
#include "DeviceStruct.h"

@interface SettingsView ()
{
    TopTitleView *mTopTitleView;
    
    SetItemView *mRecParamView;
    SetItemView *mSnapShotView;
    SetItemView *mRecListView;
    SetItemView *mPicListView;
    
    void        *m_pDevHandle;
    int         m_iDevType;
}

@property (nonatomic, retain) TopTitleView *mTopTitleView;
@property (nonatomic, retain) SetItemView *mRecParamView;
@property (nonatomic, retain) SetItemView *mSnapShotView;
@property (nonatomic, retain) SetItemView *mRecListView;
@property (nonatomic, retain) SetItemView *mPicListView;

@end

@implementation SettingsView
@synthesize mTopTitleView;
@synthesize mRecParamView;
@synthesize mSnapShotView;
@synthesize mRecListView;
@synthesize mPicListView;

- (void)InitView {
    CGFloat width, height, offset;
    CGRect bounds = [UIScreen mainScreen].bounds;
    CGRect stsRect = [[UIApplication sharedApplication] statusBarFrame];
    
    offset = stsRect.size.height;
    width = bounds.size.width;
    height = 50;
    
    self.mTopTitleView = [[TopTitleView alloc]init];
    self.mTopTitleView.frame = CGRectMake(0, offset, width, 40);
    [self.mTopTitleView InitView:TRUE];
    [self.mTopTitleView SetTitle:"Parameter List"];
    [self addSubview: self.mTopTitleView];
    self.mTopTitleView.delegate = self;
    
    offset += 50;
    self.mRecParamView = [[SetItemView alloc]init];
    self.mRecParamView.frame = CGRectMake(0, offset, width, height);
    [self.mRecParamView InitView:"Record Parameter"];
    [self addSubview: self.mRecParamView];
    self.mRecParamView.delegate = self;
    
    offset += 60;
    self.mSnapShotView = [[SetItemView alloc]init];
    self.mSnapShotView.frame = CGRectMake(0, offset, width, height);
    [self.mSnapShotView InitView:"SnapShot"];
    [self addSubview: self.mSnapShotView];
    self.mSnapShotView.delegate = self;
    
    offset += 60;
    self.mRecListView = [[SetItemView alloc]init];
    self.mRecListView.frame = CGRectMake(0, offset, width, height);
    [self.mRecListView InitView:"Record List"];
    [self addSubview: self.mRecListView];
    self.mRecListView.delegate = self;
    
    offset += 60;
    self.mPicListView = [[SetItemView alloc]init];
    self.mPicListView.frame = CGRectMake(0, offset, width, height);
    [self.mPicListView InitView:"Picture List"];
    [self addSubview: self.mPicListView];
    self.mPicListView.delegate = self;
    
    [self setBackgroundColor:[UIColor lightGrayColor]];
}

- (void)dealloc {
    [self.mTopTitleView release];
    [self.mRecParamView release];
    [self.mSnapShotView release];
    [self.mRecListView release];
    [self.mPicListView release];
    
    [super dealloc];
}

- (int)SetDevice: (void *)handle type:(int)iDevType {
    m_pDevHandle = handle;
    m_iDevType = iDevType;
    
    return 0;
}

- (void)onBackClick: (TopTitleView *)view {
    [self.delegate onClick:self index:-1 handle:m_pDevHandle type:m_iDevType];
    NSLog(@"SettingsView onBackClick");
}

- (void)SnapShot {
    int ret = -1;
    if (m_iDevType == DEV_TYPE_TCP) {
        ret = Snapshot(m_pDevHandle, 0, 0, 1);
    } else if (m_iDevType == DEV_TYPE_ZHANG_P2P) {
        ret = P2P_Snapshot(m_pDevHandle, 0, 0, 1);
    } else if (m_iDevType == DEV_TYPE_TUTK_P2P) {
        
    }
    
    printf("snapshot result %d\n", ret);
}

- (void)onClick: (SetItemView *)view {
    if (view == self.mRecParamView) {
        [self.delegate onClick:self index:0 handle:m_pDevHandle type:m_iDevType];
    } else if (view == self.mSnapShotView) {
        [self SnapShot];
    } else if (view == self.mRecListView) {
        [self.delegate onClick:self index:2 handle:m_pDevHandle type:m_iDevType];
    } else if (view == self.mPicListView) {
        [self.delegate onClick:self index:3 handle:m_pDevHandle type:m_iDevType];
    }
    NSLog(@"SettingsView onClick");
}

@end
