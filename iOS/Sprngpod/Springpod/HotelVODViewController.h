//
//  HotelVODViewController.h
//  Asianet Mobile TV Plus
//
//  Created by XperioLabs on 20/10/16.
//  Copyright © 2016 Xperio Labs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreData/CoreData.h>

@interface HotelVODViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,AVPlayerViewControllerDelegate,UIAlertViewDelegate>
{
    
    MPMoviePlayerViewController *moviePlayerController;
    AVPlayerViewController *moviewPlayer;
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
    AVPlayer *AVPlayer;
     NSMutableArray *arr;
    NSArray *arrayWithoutDuplicates;
    NSString *strprice;
    NSMutableArray *PriceCustomArr;
    NSMutableArray *Custprice;
    NSMutableString *salesOrders;
    NSMutableArray *proudutsfilte;
    
}

-(void)setNavigationTitle:(NSString *)title;
-(void)timedOut;
-(void)reloadTableview;

-(void)SpaFreshchat;
//-(void)sessionTimeOut;
@property (strong, nonatomic) IBOutlet UICollectionView *MinimarketCollview;
@property (strong, nonatomic) IBOutlet UIView *Minimarketmenu;
- (IBAction)products:(id)sender;
- (IBAction)brands:(id)sender;

- (IBAction)promotions:(id)sender;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBar;


@property (strong)NSManagedObject *imagedb;


@end
