//
//  LoginViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <Google/SignIn.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,CLLocationManagerDelegate,GIDSignInUIDelegate>

{
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
}
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UITextField *userIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *loginImage;
@property (weak, nonatomic) IBOutlet UIButton *loginBUtton;
@property (weak, nonatomic) IBOutlet UIButton *googleLogin;
//@property (weak, nonatomic) IBOutlet FBSDKButton *fbLogin;
//@property (weak, nonatomic) IBOutlet FBSDKButton *fbLogin;
@property (weak, nonatomic) IBOutlet FBSDKButton *fbLogin;

//@property (weak, nonatomic) IBOutlet GIDSignInButton *GGLSignIn;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *fbLoginButton;

@property (weak, nonatomic) IBOutlet UIButton *gpButton;




@end
