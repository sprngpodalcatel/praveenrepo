//
//  DeviceRegViewController.m
//  Springpod
//
//  Created by Shrishail Diggi on 16/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "DeviceRegViewController.h"
#import "DeviceTableViewCell.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SprngIPConf.h"

@interface DeviceRegViewController ()
{
    NSMutableArray *temp;
    NSString *userName;
    NSString *password;
    NSString *lastTvChannelID;
    NSString *deviceName;
    NSString *platform;
    NSString *osVersion;
    NSString *MAC;
    NSString *SCID;
    NSString *serialNumber;
    NSString *deviceModel;
    NSString *socialID;
    NSString *socialType;
    NSString *lastRadioID;
    NSString *lastRadioLanguage;
    NSString *lastListenMalyalamRadioChannelId,*lastListenHindiRadioChannelId,*lastListenEnglishRadioChannelId;
    
    NSMutableArray *deviceList;
}
@property (weak, nonatomic) IBOutlet UINavigationBar *deviceRegNav;
@property (weak, nonatomic) IBOutlet UITextField *loginNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *deviceIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *platformTextField;
@property (weak, nonatomic) IBOutlet UITextField *osVersionTextField;
@property (weak, nonatomic) IBOutlet UITextField *MACTextField;
@property (weak, nonatomic) IBOutlet UITextField *SCIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *serialNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *deviceMOdelTextField;
@property (weak, nonatomic) IBOutlet UITextField *socialIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *socialTypeTextField;
@property (weak, nonatomic) IBOutlet UITableView *deviceTableView;

@end

@implementation DeviceRegViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUUID *uuid = [[UIDevice currentDevice]identifierForVendor];
    NSString *UUID;
    UUID = [uuid UUIDString];
    _deviceIDTextField.text = UUID;
    deviceName = [[UIDevice currentDevice] name];
    [self.deviceRegNav setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.deviceRegNav.shadowImage = [UIImage new];
    self.deviceRegNav.translucent = YES;
    self.deviceRegNav.backgroundColor = [UIColor clearColor];
//    [self callApiToFetchData];
//    _platformTextField.text = [[UIDevice currentDevice]systemName];
    /*
    _osVersionTextField.text = [[UIDevice currentDevice]systemVersion];
    _platformTextField.text = @"iOS";
    _MACTextField.text = @"9018875397";
    _SCIDTextField.text = @"233123123";
    _serialNumberTextField.text = @"1";
    _deviceMOdelTextField.text = [[UIDevice currentDevice]model];
    _socialIDTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbUserID"];
    _socialTypeTextField.text = @"facebook";
     */
    platform = @"iOS";
    osVersion = [[UIDevice currentDevice]systemVersion];
    MAC = @"AFMAC06";
    SCID = @"1234";
    serialNumber = @"1";
    deviceModel = [[UIDevice currentDevice]model];
    socialID = [[NSUserDefaults standardUserDefaults]objectForKey:@"fbUserID"];
    socialType = @"facebook";
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return deviceList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceListCell" forIndexPath:indexPath];
    cell.deviceListLabel.text = [deviceList objectAtIndex:indexPath.row];
    return  cell;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backPressed:(id)sender {
    FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
    FBSDKAccessToken.currentAccessToken = nil;
    [manager logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registerPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/socialStoreDeviceDetails",[[SprngIPConf getSharedInstance] getCurrentIpInUse]]];
    /*
    loginName,password,deviceId,platform,osVersion,MAC,
    SCID,serialNumber,deviceModel,loginId,type
    */
    NSString *post = [NSString stringWithFormat:@"loginName=%@&password=%@&deviceId=%@&platform=%@&osVersion=%@&MAC=%@&SCID=%@&serialNumber=%@&deviceModel=%@&loginId=%@&type=%@",_loginNameTextField.text,_passwordTextField.text,_deviceIDTextField.text,platform,osVersion,MAC,SCID,serialNumber,deviceModel,[[NSUserDefaults standardUserDefaults] objectForKey:@"fbUserID"],@"facebook"];
//    NSLog(@"post %@",post);
    
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
//    NSLog(@"json_dict\n%@",json_dict);
//    NSLog(@"json_string\n%@",json_string);
//    
//    NSLog(@"%@",response);
//    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
//    NSLog(@" error is %@",error);
    
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        NSLog(@"%@",dictionary);
        temp = [dictionary objectForKey:@"root"];
        if (temp) {
            NSDictionary *sDic = temp[0];
            NSString *state = [sDic objectForKey:@"status"];
            NSLog(@"status = %@",state);
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Social Media Id Already Exists"])
            {
                [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Social Media Id Already Exists,Please reg. Wirh Other FacebookID" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            }
          else if ([[sDic objectForKey:@"status"]isEqualToString:@"Active"]) {
                userName = _loginNameTextField.text;
                lastTvChannelID  = [sDic objectForKey:@"lastSeenChannelId"];
                lastRadioID = [sDic objectForKey:@"lastListenRadioChannelId"];
                lastRadioLanguage = [sDic objectForKey:@"lastListenRadioChannelLanguage"];
                lastListenMalyalamRadioChannelId = [sDic objectForKey:@"lastListenMalyalamRadioChannelId"];
                lastListenHindiRadioChannelId = [sDic objectForKey:@"lastListenHindiRadioChannelId"];
                lastListenEnglishRadioChannelId = [sDic objectForKey:@"lastListenEnglishRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"usename"];
                lastTvChannelID = [sDic objectForKey:@"lastSeenChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:lastTvChannelID forKey:@"lastTV"];
                [[NSUserDefaults standardUserDefaults] setObject:lastRadioID forKey:@"lastRadio"];
                [[NSUserDefaults standardUserDefaults] setObject:lastRadioLanguage forKey:@"lastRadioLanguage"];
                [[NSUserDefaults standardUserDefaults] setObject:lastListenMalyalamRadioChannelId forKey:@"lastListenMalyalamRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:lastListenEnglishRadioChannelId forKey:@"lastListenEnglishRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] setObject:lastListenHindiRadioChannelId forKey:@"lastListenHindiRadioChannelId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
//                NSLog(@"%@",userName);
                [self performSegueWithIdentifier:@"homeSegue" sender:self];
            }
            if ([[sDic objectForKey:@"status"]isEqualToString:@"Customer Does Not Exists" ]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Customer Does Not Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            else if ([[sDic objectForKey:@"status"]isEqualToString:@"Maximum Device Limit Crossed" ]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Maximum Device Limit Crossed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
            else{
                NSLog(@"temp is getting Null");
            }
            
            
        }
        else
        {
            NSLog(@"%@",error);
        }
        
    }
    
}

-(void)callApiToFetchData
{
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/index.php/?devicesList1=%@",[[SprngIPConf getSharedInstance] getCurrentIpInUse],[[NSUserDefaults standardUserDefaults] objectForKey:@"customerID"]]]];
    //    [[NSUserDefaults standardUserDefaults] objectForKey:@"usename"]]
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
    
    NSString *json_string = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *json_dict = (NSDictionary *)json_string;
//    NSLog(@"json_dict\n%@",json_dict);
//    NSLog(@"json_string\n%@",json_string);
    
//    NSLog(@"%@",response);
    //    NSLog(@"received Data is : %@  Class is : %@",receivedData,[response class]);
    NSLog(@" error is %@",error);
    if (!error) {
        NSError *myError = nil;
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&myError];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger status =  [httpResponse statusCode];
        temp =[dictionary objectForKey:@"root"];
        NSLog(@"status = %ld",(long)status);
        deviceList = [[NSMutableArray alloc]init];
        
        
        if(temp && status == 200) {
//            NSLog(@"temp = %@",temp);
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = temp[0];
            if ([[dict objectForKey:@"status"]isEqualToString:@"No Data Found"]) {
                [deviceList addObject:@"No Device Found"];
            }
            else
            {
                for (int i = 0; i < temp.count; i++) {
                    dict = temp[i];
                    [deviceList addObject:[dict objectForKey:@"deviceModel"]];
                    [self.deviceTableView reloadData];
                }
            }
        }
        
    }    else{
        NSLog (@"Oh Sorry");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alter" message:@"Check Your UserId and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
}


@end
