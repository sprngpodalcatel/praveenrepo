//
//  DeviceSettingsTableViewCell.m
//  Springpod
//
//  Created by Shrishail Diggi on 23/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "DeviceSettingsTableViewCell.h"

@implementation DeviceSettingsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        if (self.frame.size.height == 140) {
            [self.expationButton setImage:[UIImage imageNamed:@"info-hilight.png"] forState:UIControlStateNormal];
        }
        
        else{
            [self.expationButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
        }
    }
    
    else{
        if (self.frame.size.height == 140) {
//            [self.expationButton setImage:[UIImage imageNamed:@"minus5.png"] forState:UIControlStateNormal];
        }
        else{
            [self.expationButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
        }
    }
    // Configure the view for the selected state
}

@end
