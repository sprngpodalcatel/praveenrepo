//
//  DeviceRegViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 16/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceRegViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
