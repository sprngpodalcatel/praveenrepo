//
//  FourthViewController.h
//  Springpod
//
//  Created by Shrishail Diggi on 07/11/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
@interface FourthViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    CLLocationManager *_locationManager;
    CLLocation *currentLocation;
}

-(void)doSetUp;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIView *baseView;


@end
