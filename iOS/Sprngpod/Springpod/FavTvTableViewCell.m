//
//  FavTvTableViewCell.m
//  Springpod
//
//  Created by Shrishail Diggi on 02/12/15.
//  Copyright © 2015 Appface. All rights reserved.
//

#import "FavTvTableViewCell.h"

@implementation FavTvTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if(selected) {
        [self.programLabel setTextColor:[UIColor colorWithRed:(255.0/255.0) green:(153.0/255.0) blue:(200.0/255.0) alpha:1.0]];
        self.programLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        //        [self.timeLabel setTextColor:[UIColor redColor]];
        //        [self.ratingLabel setTextColor:[UIColor redColor]];
        //        [self.descriptionTextField setTextColor:[UIColor redColor]];
        self.descriptionTextField.hidden = NO;
        if (self.frame.size.height == 140) {
            [self.descriptionButton setImage:[UIImage imageNamed:@"info-hilight.png"] forState:UIControlStateNormal];
            self.descriptionTextField.textContainer.maximumNumberOfLines = 5;
        }
        else{
            [self.descriptionButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
            self.descriptionTextField.textContainer.maximumNumberOfLines = 1;
        }
        
        
        //        [self.descriptionButton setImage:[UIImage imageNamed:@"Plus.png"] forState:UIControlStateNormal];
    } else {
        
        [self.programLabel setTextColor:[UIColor colorWithRed:(141.0/255.0) green:(28.0/255.0) blue:(88.0/255.0) alpha:1.0]];
        self.programLabel.font = [UIFont fontWithName:@"HelveticaNeue-Roman" size:14];
        //        [self.timeLabel setTextColor:[UIColor whiteColor]];
        //        [self.ratingLabel setTextColor:[UIColor whiteColor]];
        //        [self.descriptionTextField setTextColor:[UIColor whiteColor]];
        self.descriptionTextField.hidden = NO;
        
        if (self.frame.size.height == 140) {
            [self.descriptionButton setImage:[UIImage imageNamed:@"info-hilight.png"] forState:UIControlStateNormal];
            self.descriptionTextField.textContainer.maximumNumberOfLines = 5;
        }
        else{
            [self.descriptionButton setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
            self.descriptionTextField.textContainer.maximumNumberOfLines = 1;
        }
        
        
        //        [self.descriptionButton setImage:[UIImage imageNamed:@"Plus.png"] forState:UIControlStateNormal];
        
    }

    // Configure the view for the selected state
}

@end
