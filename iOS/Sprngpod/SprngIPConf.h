//
//  SprngIPConf.h
//  Sprngpod
//
//  Created by Shrishail Diggi on 28/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAppVarsion @"1.0.0"
#define kProductionFlurryID @"4NB3PCYXVT4XVJT575NN"
#define kDevelopmentFlurryID @"VNVS4BP7SMYN4JFRW5RT"
#define kDevelopmentAboutUsUrl @"https://www.sprngpod.com/aboutus_new.html"
#define kProductionAboutUsUrl @"https://sprngpodmw.asianetmobiletv.com:9090/aboutus.html"
#define kProductionFAQUrl @"https://sprngpodmw.asianetmobiletv.com:9090/adminPortal/FAQsTemplate.html"
#define kDevelopementFAQUrl @"https://www.sprngpod.com/adminPortal/FAQsTemplate.html"


@interface SprngIPConf : NSObject

+(SprngIPConf *)getSharedInstance;
-(NSString *)getCurrentIpInUse;
-(NSString *)getIpForImage;

@end
