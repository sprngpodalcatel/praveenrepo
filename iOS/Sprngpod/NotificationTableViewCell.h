//
//  NotificationTableViewCell.h
//  Springpod
//
//  Created by Shrishail Diggi on 13/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *notfLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeNofifLabel;
@property (weak, nonatomic) IBOutlet UILabel *notifTitle;
@property (weak, nonatomic) IBOutlet UILabel *notifTime;
@property (strong, nonatomic) IBOutlet UIButton *Sharebutton;

@end
