//
//  NotificationTableViewCell.m
//  Springpod
//
//  Created by Shrishail Diggi on 13/01/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
