//
//  SprngIPConf.m
//  Sprngpod
//
//  Created by Shrishail Diggi on 28/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "SprngIPConf.h"

static SprngIPConf *sharedInstance = nil;
//static NSString *ipInUse = @"http://52.22.22.229/xperiolabsapi";//Development For Data
static NSString *ipInUse = @"https://www.sprngpod.com/xperiolabsapi/index.php"; //https Development For data.
//static NSString *ipInUse = @"https://sprngpodapp.asianetmobiletv.com:8080";// Production For Data

//static NSString *ipForImage = @"https://sprngpodmw.asianetmobiletv.com:9090";//Production For Image
static NSString *ipForImage = @"https://www.sprngpod.com/xperiolabsapi";//HTTPS development for Image
//static NSString *ipForImage = @"http://52.22.22.229/xperiolabsapi";//Development for Images.

@implementation SprngIPConf

+(SprngIPConf *)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
    }
    return sharedInstance;
}

-(NSString *)getCurrentIpInUse{
    return ipInUse;
}

-(NSString *)getIpForImage{
    return ipForImage;
}



@end
