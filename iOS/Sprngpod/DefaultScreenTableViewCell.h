//
//  DefaultScreenTableViewCell.h
//  Sprngpod
//
//  Created by Shrishail Diggi on 26/03/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefaultScreenTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *defaultLabel;

@end
