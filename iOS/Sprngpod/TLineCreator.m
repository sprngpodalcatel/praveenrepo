//
//  TLineCreator.m
//  Springpod
//
//  Created by Shrishail Diggi on 20/02/16.
//  Copyright © 2016 Appface. All rights reserved.
//

#import "TLineCreator.h"


@implementation TLineCreator

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
    UIBezierPath *linePath1 = [UIBezierPath bezierPath];
    [linePath1 moveToPoint:CGPointMake(4.0, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(236.0 , 82.0)];
    [linePath1 moveToPoint:CGPointMake(120.0, 82.0)];
    [linePath1 addLineToPoint:CGPointMake(120, 119.0)];
    linePath1.lineWidth = 1.0;
    
    [[[UIColor grayColor]colorWithAlphaComponent:0.8] setStroke];
    [[UIColor clearColor] setFill];
    [linePath1 stroke];
    
}


@end
